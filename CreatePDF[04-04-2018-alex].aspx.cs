﻿using System.Net;
using System.Security.Policy;
using System.Web.UI.WebControls.WebParts;
using ICSharpCode.SharpZipLib.Zip;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Org.BouncyCastle.Ocsp;
using System.Data;
using System.Collections;

public partial class CreatePDF : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        if (!IsPostBack)
        {
            if (Request.RequestType == "POST")
            {
                if (Request.Form["conferenceID"] != null)
                {
                    string conferenceId = HttpUtility.UrlDecode(Request.Form["conferenceID"] != null ? Request.Form["conferenceID"].ToString() : "");
                    string txtSponsoringGroupName = HttpUtility.UrlDecode(Request.Form["txtSponsoringGroupName"] != null ? Request.Form["txtSponsoringGroupName"].ToString() : "");
                    string txtAgency = HttpUtility.UrlDecode(Request.Form["txtAgency"] != null ? Request.Form["txtAgency"].ToString() : "");
                    string txtPhone = HttpUtility.UrlDecode(Request.Form["txtPhone"] != null ? Request.Form["txtPhone"].ToString() : "");
                    string txtFax = HttpUtility.UrlDecode(Request.Form["txtFax"] != null ? Request.Form["txtFax"].ToString() : "");
                    string txtEMail = HttpUtility.UrlDecode(Request.Form["txtEMail"] != null ? Request.Form["txtEMail"].ToString() : "");
                    string txtPostalCode = HttpUtility.UrlDecode(Request.Form["txtPostalCode"] != null ? Request.Form["txtPostalCode"].ToString() : "");
                    string txtCity = HttpUtility.UrlDecode(Request.Form["txtCity"] != null ? Request.Form["txtCity"].ToString() : "");
                    string txtAddress = HttpUtility.UrlDecode(Request.Form["txtAddress"] != null ? Request.Form["txtAddress"].ToString() : "");
                    string ddlHotel1 = HttpUtility.UrlDecode(Request.Form["ddlHotel1"] != null ? Request.Form["ddlHotel1"].ToString() : "");
                    string ddlHotel2 = HttpUtility.UrlDecode(Request.Form["ddlHotel2"] != null ? Request.Form["ddlHotel2"].ToString() : "");
                    string ddlHotel3 = HttpUtility.UrlDecode(Request.Form["ddlHotel3"] != null ? Request.Form["ddlHotel3"].ToString() : "");
                    string ddlCountry = HttpUtility.UrlDecode(Request.Form["ddlCountry"] != null ? Request.Form["ddlCountry"].ToString() : "");
                    string ddlState = HttpUtility.UrlDecode(Request.Form["ddlState"] != null ? Request.Form["ddlState"].ToString() : "");
                    decimal sprice = Decimal.Parse(Request.Form["sprice"] != null ? Request.Form["sprice"].ToString() : "0");
                    decimal dprice = Decimal.Parse(Request.Form["dprice"] != null ? Request.Form["dprice"].ToString() : "0");
                    int numberOfSingleRooms = int.Parse(Request.Form["numberOFSingleRooms"] != null ? Request.Form["numberOFSingleRooms"].ToString() : "0");
                    int numberOfDoubleRooms = int.Parse(Request.Form["numberOFDoubleRooms"] != null ? Request.Form["numberOFDoubleRooms"].ToString() : "0");
                    DateTime dateFrom = DateTime.Parse(Request.Form["dateFrom"] != null ? Request.Form["dateFrom"].ToString() : System.DateTime.MinValue.ToShortDateString());
                    DateTime dateTo = DateTime.Parse(Request.Form["dateTo"] != null ? Request.Form["dateTo"].ToString() : System.DateTime.MaxValue.ToShortDateString());
                    string lastEditedBy = HttpUtility.UrlDecode(Request.Form["userId"] != null ? Request.Form["userId"].ToString() : "");
                    string editNights = HttpUtility.UrlDecode(Request.Form["editNights"] != null ? Request.Form["editNights"].ToString() : "false");
                    string sn = HttpUtility.UrlDecode(Request.Form["sn"] != null ? Request.Form["sn"].ToString() : "");
                    string dn = HttpUtility.UrlDecode(Request.Form["dn"] != null ? Request.Form["dn"].ToString() : "");
                    string sp = HttpUtility.UrlDecode(Request.Form["sp"] != null ? Request.Form["sp"].ToString() : "");
                    string dp = HttpUtility.UrlDecode(Request.Form["dp"] != null ? Request.Form["dp"].ToString() : "");

                    CreatePDFDocument(conferenceId, txtSponsoringGroupName, txtAgency, txtPhone, txtFax, txtEMail, txtPostalCode, txtCity, txtAddress, ddlHotel1, ddlHotel2, ddlHotel3, ddlCountry, ddlState, sprice, dprice, numberOfSingleRooms, numberOfDoubleRooms, dateFrom, dateTo, lastEditedBy, editNights, sn, dn, sp, dp);
                }
            }
        }
    }

    private bool CheckVariableRates(Dictionary<int, decimal> rates)
    {
        bool variableRates = false;

        var rate = (from r in rates
                    where r.Value != 0M
                    select r).FirstOrDefault();

        foreach (KeyValuePair<int, decimal> s in rates)
        {
            if (s.Value == 0M)
                continue;

            if (s.Value != rate.Value)
            {
                variableRates = true;
                break;
            }
        }

        return variableRates;
    }

    protected void CreatePDFDocument(string conferenceID, string txtSponsoringGroupName, string txtAgency, string txtPhone, string txtFax, string txtEMail, string txtPostalCode, string txtCity, string txtAddress, string ddlHotel1, string ddlHotel2, string ddlHotel3, string ddlCountry, string ddlState, decimal sprice, decimal dprice, int numberOfSingleRooms, int numberOfDoubleRooms, DateTime dateFrom, DateTime dateTo, string lastEditeBy, string editNights, string sn, string dn, string sp, string dp)
    {
        string haFile = String.Empty;
        string snFile = String.Empty;

        var ddlDateFrom = new DropDownList();
        var ddlDateTo = new DropDownList();

        FillConferenceDates(conferenceID, ref ddlDateFrom, ref ddlDateTo);

        bool editNight = Boolean.Parse(editNights);
        string[] singleNights = sn.Split(',');
        string[] doubleNights = dn.Split(',');

        var sNights = new Dictionary<int, int>();
        var dNights = new Dictionary<int, int>();

        var sPrices = new Dictionary<int, decimal>();
        var dPrices = new Dictionary<int, decimal>();
        

        for (int j = 0; j < ddlDateFrom.Items.Count; j++)
        {
            if (!editNight) break;

            if (ddlDateFrom.Items[j].Value != "-1")
            {
                sNights.Add(DateTime.Parse(ddlDateFrom.Items[j].Value).Day, int.Parse(singleNights[j]));
                dNights.Add(DateTime.Parse(ddlDateFrom.Items[j].Value).Day, int.Parse(doubleNights[j]));
            }
        }


        if (sprice == 0 && dprice == 0)
        {
            string[] singlePrices = sp.Split(',');
            string[] doublePrices = dp.Split(',');

            int j = 0;

            for (int i = 0; i <= singlePrices.Count(); i++)
            {
                if (ddlDateFrom.Items[i].Value != "-1")
                {
                    sPrices.Add(DateTime.Parse(ddlDateFrom.Items[i].Value).Day, decimal.Parse(singlePrices[j]));
                    dPrices.Add(DateTime.Parse(ddlDateFrom.Items[i].Value).Day, decimal.Parse(doublePrices[j]));

                    j++;
                }
            }
        }

        //int i = 0;

        //foreach (ListItem li in ddlDateFrom.Items)
        //{
        //    if (!editNight) break;

        //    if (li.Value != "-1")
        //    {
        //        sNights.Add(DateTime.Parse(li.Value).Day, int.Parse(singleNights[i]));
        //        dNights.Add(DateTime.Parse(li.Value).Day, int.Parse(doubleNights[i]));

        //        i++;
        //    }
        //}


        //if (sprice == 0 && dprice == 0)
        //{
        //    int j = 0;

        //    string[] singlePrices = sp.Split(',');
        //    string[] doublePrices = dp.Split(',');

        //    foreach (ListItem li in ddlDateFrom.Items)
        //    {              
        //        if (li.Value != "-1")
        //        {
        //            sPrices.Add(DateTime.Parse(li.Value).Day, decimal.Parse(singlePrices[j]));
        //            dPrices.Add(DateTime.Parse(li.Value).Day, decimal.Parse(doublePrices[j]));

        //            j++;
        //        }
        //    }
        //}

        string numberOfRoomsSingle = "0";
        string numberOfRoomsDouble = "0";

        if (!conferenceID.Contains("AASLD"))
        {
            numberOfRoomsSingle = editNight ? MaximumNumberOfNightsSingleDouble(sNights, dNights, ddlDateFrom) : numberOfSingleRooms.ToString();
            numberOfRoomsDouble = editNight ? MaximumNumberOfNightsSingleDouble(sNights, dNights, ddlDateFrom) : numberOfDoubleRooms.ToString();
        }
        else
        {
            numberOfRoomsSingle = editNight ? MaximumNumberOfNightsSingleDoubleSeparate(sNights, dNights, ddlDateFrom)[0].ToString() : numberOfSingleRooms.ToString();
            numberOfRoomsDouble = editNight ? MaximumNumberOfNightsSingleDoubleSeparate(sNights, dNights, ddlDateFrom)[1].ToString() : numberOfDoubleRooms.ToString();
        }


        DateTime Date = System.DateTime.Now;

        string documentID_HA = "G" + Date.ToString("yyMMddhhmmssff"); //Date.ToString("yyMMdd") + System.DateTime.Now.ToString("hh") + System.DateTime.Now.ToString("mm") + System.DateTime.Now.Ticks.ToString().Substring(System.DateTime.Now.Ticks.ToString().Length - 4, 4);

        //string pdfTemplate = Server.MapPath("docs") + "\\HousingAgreement_" + conferenceID + ".pdf";
        string pdfTemplate = String.Empty;

        if (!conferenceID.Contains("AASLD"))
        {
            pdfTemplate = Server.MapPath("docs") + "\\HousingAgreement_" + conferenceID + ".pdf";
        }
        else
        {
            string maximumNumberOfRooms = editNight ? MaximumNumberOfNightsSingleDouble(sNights, dNights, ddlDateFrom) : (int.Parse(numberOfRoomsSingle) + int.Parse(numberOfRoomsDouble)).ToString();

            if (int.Parse(maximumNumberOfRooms) < 25)
            {
                pdfTemplate = Server.MapPath("docs") + "\\HousingAgreement_" + conferenceID + ".pdf";
            }
            else
            {
                pdfTemplate = Server.MapPath("docs") + "\\HousingAgreement_" + conferenceID + "_Ver2.pdf";
            }
        }

        string newFile = documentID_HA + "_HousingAgreement_" + conferenceID + ".pdf";
        //string newFile = hashCode + "_HousingAgreement_" + conferenceID + ".pdf";

        haFile = newFile;

        PdfReader pdfReader = new PdfReader(pdfTemplate);
        PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(Server.MapPath("tempAgreements") + "\\" + newFile, FileMode.Create), '\0', true);

        //PDF Encryption//
        //pdfStamper.SetEncryption(true, null, "Dvladh09091976", PdfWriter.ALLOW_FILL_IN | PdfWriter.ALLOW_PRINTING);


        AcroFields pdfFormFields = pdfStamper.AcroFields;

        //Postavljanje ddlState polja da bude vidljivo
        pdfFormFields.SetFieldProperty("ddlState", "flags", PdfAnnotation.FLAGS_PRINT, null);
        //Postavljanje ddlState polja da bude readonly
        pdfFormFields.SetFieldProperty("ddlState", "setfflags", PdfFormField.FF_READ_ONLY, null);
        //Postavljanje ddlCountry polja da bude readonly
        pdfFormFields.SetFieldProperty("ddlCountry", "setfflags", PdfFormField.FF_READ_ONLY, null);
        //Postavljanje txtSponsoringGroupName polja da bude readonly
        //pdfFormFields.SetFieldProperty("txtSponsoringGroupName", "setfflags", PdfFormField.FF_READ_ONLY, null);
        //Postavljanje txtAgency polja da bude readonly
        pdfFormFields.SetFieldProperty("txtAgency", "setfflags", PdfFormField.FF_READ_ONLY, null);
        //Postavljanje txtPhone polja da bude readonly
        pdfFormFields.SetFieldProperty("txtPhone", "setfflags", PdfFormField.FF_READ_ONLY, null);
        //Postavljanje txtFax polja da bude readonly
        pdfFormFields.SetFieldProperty("txtFax", "setfflags", PdfFormField.FF_READ_ONLY, null);
        //Postavljanje txtEMail polja da bude readonly
        pdfFormFields.SetFieldProperty("txtEMail", "setfflags", PdfFormField.FF_READ_ONLY, null);
        //Postavljanje txtPostalCode polja da bude readonly
        pdfFormFields.SetFieldProperty("txtPostalCode", "setfflags", PdfFormField.FF_READ_ONLY, null);
        //Postavljanje txtCity polja da bude readonly
        pdfFormFields.SetFieldProperty("txtCity", "setfflags", PdfFormField.FF_READ_ONLY, null);
        //Postavljanje txtAddress polja da bude readonly
        pdfFormFields.SetFieldProperty("txtAddress", "setfflags", PdfFormField.FF_READ_ONLY, null);


        pdfFormFields.SetField("txtSponsoringGroupName", txtSponsoringGroupName);
        pdfFormFields.SetField("txtAgency", txtAgency);
        pdfFormFields.SetField("txtPhone", txtPhone);
        pdfFormFields.SetField("txtFax", txtFax);
        pdfFormFields.SetField("txtEMail", txtEMail);
        pdfFormFields.SetField("txtPostalCode", txtPostalCode);
        pdfFormFields.SetField("txtCity", txtCity);
        pdfFormFields.SetField("txtAddress", txtAddress);
        pdfFormFields.SetField("txtDocumentID", documentID_HA);

        pdfFormFields.SetListOption("ddlHotel1", new string[] { "1" }, new string[] { ddlHotel1 });
        pdfFormFields.SetListOption("ddlHotel2", new string[] { "2" }, new string[] { ddlHotel2 });
        pdfFormFields.SetListOption("ddlHotel3", new string[] { "3" }, new string[] { ddlHotel3 });
        pdfFormFields.SetListOption("ddlCountry", new string[] { "2" }, new string[] { ddlCountry });
	pdfFormFields.SetListOption("ddlState", new string[] { "2" }, new string[] { ddlState });

        pdfFormFields.SetListSelection("ddlHotel1", new string[] { ddlHotel1 });
        pdfFormFields.SetListSelection("ddlHotel2", new string[] { ddlHotel2 });
        pdfFormFields.SetListSelection("ddlHotel3", new string[] { ddlHotel3 });
        pdfFormFields.SetListSelection("ddlCountry", new string[] { ddlCountry });
	pdfFormFields.SetListSelection("ddlState", new string[] { ddlState });

        pdfFormFields.SetField("ddlHotel1", ddlHotel1);
        pdfFormFields.SetField("ddlHotel2", ddlHotel2);
        pdfFormFields.SetField("ddlHotel3", ddlHotel3);
        pdfFormFields.SetField("ddlCountry", ddlCountry);
	pdfFormFields.SetField("ddlState", ddlState);

        pdfFormFields.SetField("txtUserKey", documentID_HA);

        bool sidenights = false;

        int totalNumberOfRoomsSingle = 0;
        int totalNumberOfRoomsDouble = 0;

        int totalSidenightsNumberOfRoomsSingle = 0;
        int totalSidenightsNumberOfRoomsDouble = 0;

        //string numberOfRoomsSingle = numberOfSingleRooms.ToString();
        //string numberOfRoomsDouble = numberOfDoubleRooms.ToString();

        //string numberOfRoomsSingle = editNight ? MaximumNumberOfNightsSingleDouble(sNights, dNights, ddlDateFrom) : numberOfSingleRooms.ToString();
        //string numberOfRoomsDouble = editNight ? MaximumNumberOfNightsSingleDouble(sNights, dNights, ddlDateFrom) : numberOfDoubleRooms.ToString();



        foreach (ListItem li in ddlDateFrom.Items)
        {
            if (li.Value != "-1" && li.Attributes["sidenight"].ToString() == "false")
            {
                if ((DateTime.Parse(li.Value) >= dateFrom) && (DateTime.Parse(li.Value) < dateTo))
                {
                    if (!editNight)
                    {
                        pdfFormFields.SetField("SR" + DateTime.Parse(li.Value).Day, numberOfRoomsSingle);
                        totalNumberOfRoomsSingle += int.Parse(numberOfRoomsSingle);

                        pdfFormFields.SetField("DR" + DateTime.Parse(li.Value).Day, numberOfRoomsDouble);
                        totalNumberOfRoomsDouble += int.Parse(numberOfRoomsDouble);
                    }
                    else
                    {
                        int sValue = sNights[DateTime.Parse(li.Value).Day];
                        int dValue = dNights[DateTime.Parse(li.Value).Day];

                        pdfFormFields.SetField("SR" + DateTime.Parse(li.Value).Day, sValue.ToString());
                        totalNumberOfRoomsSingle += sValue;

                        pdfFormFields.SetField("DR" + DateTime.Parse(li.Value).Day, dValue.ToString());
                        totalNumberOfRoomsDouble += dValue;
                    }
                }
            }

            if (li.Value != "-1" && li.Attributes["sidenight"].ToString() == "true")
            {
                if ((DateTime.Parse(li.Value) >= dateFrom) && (DateTime.Parse(li.Value) < dateTo))
                {
                    sidenights = true;
                }
            }
        }

        pdfFormFields.SetField("SRTotal", totalNumberOfRoomsSingle.ToString());
        pdfFormFields.SetField("DRTotal", totalNumberOfRoomsDouble.ToString());

        Decimal singleDeposit = Decimal.Parse(pdfFormFields.GetField("txtSingleDeposit"));
        Decimal doubleDeposit = Decimal.Parse(pdfFormFields.GetField("txtDoubleDeposit"));

        if (!editNight)
        {
            if (singleDeposit == 0 && doubleDeposit == 0)
            {
                pdfFormFields.SetField("txtSingleDeposit", sprice.ToString("F"));
                pdfFormFields.SetField("txtDoubleDeposit", dprice.ToString("F"));

                singleDeposit = sprice;
                doubleDeposit = dprice;

                pdfFormFields.SetField("txtNumSR", (int.Parse(numberOfRoomsSingle)).ToString());
                pdfFormFields.SetField("txtNumDR", (int.Parse(numberOfRoomsDouble)).ToString());

                pdfFormFields.SetField("txtTotalS", ((int.Parse(numberOfRoomsSingle) * singleDeposit) * 2).ToString("F"));
                pdfFormFields.SetField("txtTotalD", ((int.Parse(numberOfRoomsDouble) * doubleDeposit) * 2).ToString("F"));  
            }
            else
            {
                pdfFormFields.SetField("txtNumSR", (int.Parse(numberOfRoomsSingle) + int.Parse(numberOfRoomsDouble)).ToString());
                pdfFormFields.SetField("txtTotalS", ((int.Parse(numberOfRoomsSingle) + int.Parse(numberOfRoomsDouble)) * singleDeposit).ToString("F"));                
            }
            
        }
        else
        {
            if (singleDeposit == 0 && doubleDeposit == 0)
            {
                pdfFormFields.SetField("txtSingleDeposit", sprice.ToString("F"));
                pdfFormFields.SetField("txtDoubleDeposit", dprice.ToString("F"));

                singleDeposit = sprice;
                doubleDeposit = dprice;

                pdfFormFields.SetField("txtNumSR", (int.Parse(numberOfRoomsSingle)).ToString());
                pdfFormFields.SetField("txtNumDR", (int.Parse(numberOfRoomsDouble)).ToString());

                pdfFormFields.SetField("txtTotalS", ((int.Parse(numberOfRoomsSingle) * singleDeposit) * 2).ToString("F"));
                pdfFormFields.SetField("txtTotalD", ((int.Parse(numberOfRoomsDouble) * doubleDeposit) * 2).ToString("F"));  
            }
            else
            {
                pdfFormFields.SetField("txtNumSR", (int.Parse(numberOfRoomsSingle)).ToString());
                pdfFormFields.SetField("txtTotalS", ((int.Parse(numberOfRoomsSingle)) * singleDeposit).ToString("F"));                
            }
        }

        if (singleDeposit != 0 && doubleDeposit != 0)
        {
            pdfFormFields.SetField("txtTotalSD", (Decimal.Parse(pdfFormFields.GetField("txtTotalS")) + Decimal.Parse(pdfFormFields.GetField("txtTotalD"))).ToString("F"));
        }

        bool registration = pdfFormFields.GetField("txtRegistration") != null;
        Decimal registrationPrice = 0;

        if (pdfFormFields.GetField("txtConfirmationDate") == null || DateTime.Parse(pdfFormFields.GetField("txtConfirmationDate").ToString()) >= System.DateTime.Now)
        {
            //-----------------------------------------------------------------------
            //-------------------------Estimated Balance before confirmation date-----------------------------

            pdfFormFields.SetField("txtTotalRN", totalNumberOfRoomsSingle.ToString());
            pdfFormFields.SetField("txtTotalSN", totalNumberOfRoomsDouble.ToString());

            if ((sprice != 0) || (dprice != 0))
            {
                pdfFormFields.SetField("txtTotalRN", totalNumberOfRoomsSingle.ToString());
                pdfFormFields.SetField("txtTotalSN", totalNumberOfRoomsDouble.ToString());

                pdfFormFields.SetField("txtSPrice", sprice.ToString("F"));
                pdfFormFields.SetField("txtDPrice", dprice.ToString("F"));

                pdfFormFields.SetField("txtTotalRNPrice", (totalNumberOfRoomsSingle * sprice).ToString("F"));
                pdfFormFields.SetField("txtTotalSNPrice", (totalNumberOfRoomsDouble * dprice).ToString("F"));

                pdfFormFields.SetField("txtTotalDepositePaid", (Decimal.Parse(pdfFormFields.GetField("txtTotalS")) + Decimal.Parse(pdfFormFields.GetField("txtTotalD"))).ToString("F"));

                pdfFormFields.SetField("txtTotalPrice", (((totalNumberOfRoomsSingle * sprice) + (totalNumberOfRoomsDouble * dprice)) - (Decimal.Parse(pdfFormFields.GetField("txtTotalS")) + Decimal.Parse(pdfFormFields.GetField("txtTotalD")))).ToString("F"));

                pdfFormFields.SetField("txtTotalPrice", (Decimal.Parse(pdfFormFields.GetField("txtTotalPrice")) + registrationPrice).ToString("F"));
            }
            else
            {
                Decimal totalRoomPriceSingle = 0;
                Decimal totalRoomPriceDouble = 0;

                foreach (ListItem li in ddlDateFrom.Items)
                {
                    if (li.Value != "-1" && li.Attributes["sidenight"].ToString() == "false")
                    {
                        if ((DateTime.Parse(li.Value) >= dateFrom) && (DateTime.Parse(li.Value) < dateTo))
                        {
                            if (!editNight)
                            {
                                totalRoomPriceSingle += int.Parse(numberOfRoomsSingle) * sPrices[DateTime.Parse(li.Value).Day];
                                totalRoomPriceDouble += int.Parse(numberOfRoomsDouble) * dPrices[DateTime.Parse(li.Value).Day];
                            }
                            else
                            {
                                totalRoomPriceSingle += sNights[DateTime.Parse(li.Value).Day] * sPrices[DateTime.Parse(li.Value).Day];
                                totalRoomPriceDouble += dNights[DateTime.Parse(li.Value).Day] * dPrices[DateTime.Parse(li.Value).Day];
                            }
                        }
                    }
                }

                bool variableRatesSingle = CheckVariableRates(sPrices);
                bool variableRatesDouble = CheckVariableRates(dPrices);

                var singlePriceFromDic = (from spr in sPrices
                                          where spr.Value != 0M
                                          select spr).FirstOrDefault();
                var doublePriceFromDic = (from dpr in dPrices
                                          where dpr.Value != 0M
                                          select dpr).FirstOrDefault();


                pdfFormFields.SetField("txtSPrice", variableRatesSingle ? "VNR" : singlePriceFromDic.Value.ToString("F"));
                pdfFormFields.SetField("txtDPrice", variableRatesDouble ? "VNR" : doublePriceFromDic.Value.ToString("F"));

                pdfFormFields.SetField("txtTotalRNPrice", (totalRoomPriceSingle).ToString("F"));
                pdfFormFields.SetField("txtTotalSNPrice", (totalRoomPriceDouble).ToString("F"));

                pdfFormFields.SetField("txtTotalDepositePaid", (Decimal.Parse(pdfFormFields.GetField("txtTotalS")) + Decimal.Parse(pdfFormFields.GetField("txtTotalD"))).ToString("F"));
                pdfFormFields.SetField("txtTotalPrice", ((totalRoomPriceSingle) + (totalRoomPriceDouble) - (Decimal.Parse(pdfFormFields.GetField("txtTotalS")) + Decimal.Parse(pdfFormFields.GetField("txtTotalD")))).ToString("F"));
            }

            //------------------------------------------------------------------------
        }
        else
        {
            //-----------------------------------------------------------------------
            //-------------------------Estimated Balance after confirmation date-----------------------------

            pdfFormFields.SetField("txtTotalRN1", totalNumberOfRoomsSingle.ToString());
            pdfFormFields.SetField("txtTotalSN1", totalNumberOfRoomsDouble.ToString());

            pdfFormFields.SetField("txtSPrice1", sprice.ToString("F"));
            pdfFormFields.SetField("txtDPrice1", dprice.ToString("F"));

            pdfFormFields.SetField("txtTotalRNPrice1", ((totalNumberOfRoomsSingle * sprice) / 2).ToString("F"));
            pdfFormFields.SetField("txtTotalSNPrice1", ((totalNumberOfRoomsDouble * dprice) / 2).ToString("F"));

            pdfFormFields.SetField("txtTotalPrice1", (((totalNumberOfRoomsSingle * sprice) / 2) + ((totalNumberOfRoomsDouble * dprice) / 2)).ToString("F"));

            pdfFormFields.SetField("txtTotalRN", totalNumberOfRoomsSingle.ToString());
            pdfFormFields.SetField("txtTotalSN", totalNumberOfRoomsDouble.ToString());

            pdfFormFields.SetField("txtSPrice", sprice.ToString("F"));
            pdfFormFields.SetField("txtDPrice", dprice.ToString("F"));

            pdfFormFields.SetField("txtTotalRNPrice", (totalNumberOfRoomsSingle * sprice).ToString("F"));
            pdfFormFields.SetField("txtTotalSNPrice", (totalNumberOfRoomsDouble * dprice).ToString("F"));

            pdfFormFields.SetField("txtTotalDepositePaid", (((totalNumberOfRoomsSingle * sprice) / 2) + ((totalNumberOfRoomsDouble * dprice) / 2)).ToString("F"));

            pdfFormFields.SetField("txtTotalPrice", (((totalNumberOfRoomsSingle * sprice) + (totalNumberOfRoomsDouble * dprice)) - (Decimal.Parse(pdfFormFields.GetField("txtTotalDepositePaid")) + Decimal.Parse(pdfFormFields.GetField("txtTotalD")))).ToString("F"));

            pdfFormFields.SetField("txtTotalPrice", (Decimal.Parse(pdfFormFields.GetField("txtTotalPrice")) + registrationPrice).ToString("F"));

            //------------------------------------------------------------------------
        }

        pdfFormFields.SetField("hfButton", "0");
        pdfFormFields.SetField("txtEditedBy", lastEditeBy + " " + System.DateTime.Now.ToShortDateString());
        
        pdfStamper.FormFlattening = false;

        pdfStamper.Close();

        Response.AddHeader("Content-Type", "application/zip");
        Response.AddHeader("Content-disposition", "attachment; filename=\"" + documentID_HA + ".zip\"");
        Response.AddHeader("Content-Transfer-Encoding", "Binary");
        Response.AppendHeader("Cache-Control", "no-cache");

        //Response.AddHeader("Content-Type", "application/octet-stream");
        //Response.AddHeader("Content-Transfer-Encoding", "Binary");
        //Response.AddHeader("Content-disposition", "attachment; filename=\"" + newFile + "\"");
        //Response.WriteFile(Server.MapPath("tempAgreements") + "\\" + newFile);


        ZipFile z = ZipFile.Create(Server.MapPath("tempAgreements") + "\\" + documentID_HA + ".zip");       //(filezipname);
        z.BeginUpdate();
        z.Add(Server.MapPath("tempAgreements") + "\\" + newFile, Path.GetFileName(Server.MapPath("tempAgreements") + "\\" + newFile));


        if (sidenights)
        {
            DateTime DateSN = System.DateTime.Now;
            string documentID_SN = "G" + Date.ToString("yyMMddhhmmssff"); //Date.ToString("yyMMdd") + System.DateTime.Now.ToString("hh") + System.DateTime.Now.ToString("mm") + System.DateTime.Now.Ticks.ToString().Substring(System.DateTime.Now.Ticks.ToString().Length - 4, 4);

            string pdfSidenightsTemplate = Server.MapPath("docs") + "\\Sidenights_" + conferenceID + ".pdf";

            string sidenightsNewFile = documentID_SN + "_Sidenights_" + conferenceID + ".pdf";

            snFile = sidenightsNewFile;

            PdfReader pdfSidenightsReader = new PdfReader(pdfSidenightsTemplate);
            PdfStamper pdfSidenightsStamper = new PdfStamper(pdfSidenightsReader, new FileStream(Server.MapPath("tempAgreements") + "\\" + sidenightsNewFile, FileMode.Create), '\0', true);

            //PDF Encryption//
            //pdfSidenightsStamper.SetEncryption(true, null, "Dvladh09091976", PdfWriter.ALLOW_FILL_IN | PdfWriter.ALLOW_PRINTING);

            AcroFields pdfSidenightsFormFields = pdfSidenightsStamper.AcroFields;

            pdfSidenightsFormFields.SetField("txtGroupName", txtSponsoringGroupName);
            pdfSidenightsFormFields.SetField("txtAgency", txtAgency);
            pdfSidenightsFormFields.SetField("txtEMail", txtEMail);
            pdfSidenightsFormFields.SetField("txtHotelBooked", ddlHotel1);
            pdfSidenightsFormFields.SetField("txtDocumentID", documentID_SN);

            foreach (ListItem li in ddlDateFrom.Items)
            {
                if (li.Value != "-1" && li.Attributes["sidenight"].ToString() == "true")
                {
                    if ((DateTime.Parse(li.Value) >= dateFrom) && (DateTime.Parse(li.Value) < dateTo))
                    {
                        if (!editNight)
                        {
                            pdfSidenightsFormFields.SetField("txtGR" + DateTime.Parse(li.Value).Day, numberOfRoomsSingle);
                            totalSidenightsNumberOfRoomsSingle += int.Parse(numberOfRoomsSingle);
                            pdfSidenightsFormFields.SetField("txtSR" + DateTime.Parse(li.Value).Day, numberOfRoomsDouble);
                            totalSidenightsNumberOfRoomsDouble += int.Parse(numberOfRoomsDouble);
                        }
                        else
                        {
                            int sValue = sNights[DateTime.Parse(li.Value).Day];
                            int dValue = dNights[DateTime.Parse(li.Value).Day];

                            pdfSidenightsFormFields.SetField("txtGR" + DateTime.Parse(li.Value).Day, sValue.ToString());
                            totalSidenightsNumberOfRoomsSingle += sValue;

                            pdfSidenightsFormFields.SetField("txtSR" + DateTime.Parse(li.Value).Day, dValue.ToString());
                            totalSidenightsNumberOfRoomsDouble += dValue;
                        }

                    }
                }
            }

            pdfSidenightsFormFields.SetField("txtGRTotal", totalSidenightsNumberOfRoomsSingle.ToString());
            pdfSidenightsFormFields.SetField("txtSRTotal", totalSidenightsNumberOfRoomsDouble.ToString());

            if ((sprice != 0) || (dprice != 0))
            {
                pdfSidenightsFormFields.SetField("txtRoomRate", sprice.ToString("F"));
                pdfSidenightsFormFields.SetField("txtSuiteRate", dprice.ToString("F"));

                pdfSidenightsFormFields.SetField("txtRoomNights", totalSidenightsNumberOfRoomsSingle.ToString());
                pdfSidenightsFormFields.SetField("txtSuiteNights", totalSidenightsNumberOfRoomsDouble.ToString());

                pdfSidenightsFormFields.SetField("txtRoomTotal", (totalSidenightsNumberOfRoomsSingle * sprice).ToString("F"));
                pdfSidenightsFormFields.SetField("txtSuiteTotal", (totalSidenightsNumberOfRoomsDouble * dprice).ToString("F"));

                pdfSidenightsFormFields.SetField("txtTotalAmount", ((totalSidenightsNumberOfRoomsSingle * sprice) + (totalSidenightsNumberOfRoomsDouble * dprice)).ToString("F"));
            }
            else
            {
                Decimal totalRoomPriceSidenightsSingle = 0;
                Decimal totalRoomPriceSidenightsDouble = 0;

                foreach (ListItem li in ddlDateFrom.Items)
                {
                    if (li.Value != "-1" && li.Attributes["sidenight"].ToString() == "true")
                    {
                        if ((DateTime.Parse(li.Value) >= dateFrom) && (DateTime.Parse(li.Value) < dateTo))
                        {
                            if (!editNight)
                            {
                                totalRoomPriceSidenightsSingle += int.Parse(numberOfRoomsSingle) * sPrices[DateTime.Parse(li.Value).Day];
                                totalRoomPriceSidenightsDouble += int.Parse(numberOfRoomsDouble) * dPrices[DateTime.Parse(li.Value).Day];
                                //totalRoomPriceSidenightsSingle += totalSidenightsNumberOfRoomsSingle * sPrices[DateTime.Parse(li.Value).Day];
                                //totalRoomPriceSidenightsDouble += totalSidenightsNumberOfRoomsDouble * dPrices[DateTime.Parse(li.Value).Day];
                            }
                            else
                            {
                                totalRoomPriceSidenightsSingle += sNights[DateTime.Parse(li.Value).Day] * sPrices[DateTime.Parse(li.Value).Day];
                                totalRoomPriceSidenightsDouble += dNights[DateTime.Parse(li.Value).Day] * dPrices[DateTime.Parse(li.Value).Day];
                            }
                        }
                    }
                }

                bool variableRatesSingle = CheckVariableRates(sPrices);
                bool variableRatesDouble = CheckVariableRates(dPrices);

                var singlePriceFromDic = (from spr in sPrices
                                          where spr.Value != 0M
                                          select spr).FirstOrDefault();
                var doublePriceFromDic = (from dpr in dPrices
                                          where dpr.Value != 0M
                                          select dpr).FirstOrDefault();

                pdfSidenightsFormFields.SetField("txtRoomRate", variableRatesSingle ? "VNR" : singlePriceFromDic.Value.ToString("F"));
                pdfSidenightsFormFields.SetField("txtSuiteRate", variableRatesDouble ? "VNR" : doublePriceFromDic.Value.ToString("F"));

                pdfSidenightsFormFields.SetField("txtRoomNights", totalSidenightsNumberOfRoomsSingle.ToString());
                pdfSidenightsFormFields.SetField("txtSuiteNights", totalSidenightsNumberOfRoomsDouble.ToString());

                pdfSidenightsFormFields.SetField("txtRoomTotal", (totalRoomPriceSidenightsSingle).ToString("F"));
                pdfSidenightsFormFields.SetField("txtSuiteTotal", (totalRoomPriceSidenightsDouble).ToString("F"));

                pdfSidenightsFormFields.SetField("txtTotalAmount", ((totalRoomPriceSidenightsSingle) + (totalRoomPriceSidenightsDouble)).ToString("F"));
            }


            //Novo polje za placanje koje prikazuje ukupan iznos bez umanjenja deposita
            pdfFormFields.SetField("txtFullPaymentTotalPrice", ((totalNumberOfRoomsSingle * sprice) + (totalNumberOfRoomsDouble * dprice)).ToString("F"));

            pdfSidenightsFormFields.SetField("txtUserKey", documentID_SN);
            //pdfSidenightsFormFields.SetField("txtUserKey", hashCode.ToString());

            pdfSidenightsStamper.FormFlattening = false;

            pdfSidenightsStamper.Close();


            //Response.WriteFile(Server.MapPath("tempAgreements") + "\\" + sidenightsNewFile);

            z.Add(Server.MapPath("tempAgreements") + "\\" + sidenightsNewFile, Path.GetFileName(Server.MapPath("tempAgreements") + "\\" + sidenightsNewFile));

        }


        z.CommitUpdate();
        z.Close();


        Response.WriteFile(Server.MapPath("tempAgreements") + "\\" + documentID_HA + ".zip");

        File.Delete(Server.MapPath("tempAgreements") + "\\" + haFile);

        if (sidenights)
            File.Delete(Server.MapPath("tempAgreements") + "\\" + snFile);


        Response.End();

    }

    protected void FillConferenceDates(string conferenceID, ref DropDownList ddlDateFrom, ref DropDownList ddlDateTo)
    {
        ddlDateFrom.Items.Clear();
        ddlDateTo.Items.Clear();

        DataSet dsConferences = new DataSet();

        System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(Server.MapPath("\\") + "\\" + "App_Data\\Conferences.xml");

        dsConferences.ReadXml(reader);

        ListItem item = new ListItem("Select", "-1");

        foreach (DataRow dr in dsConferences.Tables[0].Select("conferenceID='" + conferenceID + "'"))
        {
            DataRow[] da = dsConferences.Tables[1].Select("conference_id='" + dr[0] + "'");

            for (int i = 0; i < da.Length; i++)
            {
                ListItem liFrom = new ListItem(da[i][1].ToString(), da[i][1].ToString());
                liFrom.Attributes.Add("sidenight", da[i][0].ToString());
                ddlDateFrom.Items.Add(liFrom);

                ListItem liTo = new ListItem(da[i][1].ToString(), da[i][1].ToString());
                liTo.Attributes.Add("sidenight", da[i][0].ToString());
                ddlDateTo.Items.Add(liTo);
            }
        }

        ddlDateFrom.Items.Insert(0, item);
        ddlDateTo.Items.Insert(0, item);
    }


    private string MaximumNumberOfNightsSingleDouble(Dictionary<int, int> sNights, Dictionary<int, int> dNights, DropDownList ddlDateFrom)
    {
        var sumSingleDouble = new ArrayList();

        for (int i = 0; i < sNights.Count; i++)
        {
            if (ddlDateFrom.Items[i + 1].Attributes["sidenight"].ToString() == "false")
            {
                int numberOfNights = sNights[DateTime.Parse(ddlDateFrom.Items[i + 1].Value).Day] + dNights[DateTime.Parse(ddlDateFrom.Items[i + 1].Value).Day];
                sumSingleDouble.Add(numberOfNights);
            }
        }

        sumSingleDouble.Sort();

        sumSingleDouble.Reverse();

        return sumSingleDouble[0].ToString();
    }

    private List<int> MaximumNumberOfNightsSingleDoubleSeparate(Dictionary<int, int> sNights, Dictionary<int, int> dNights, DropDownList ddlDateFrom)
    {
        var sumSingleDoubleNights = new List<int>();
        var singleNights = new List<int>();
        var doubleNights = new List<int>();

        var singleDoublePair = new List<int>();

        for (int i = 0; i < sNights.Count; i++)
        {

            if (ddlDateFrom.Items[i + 1].Attributes["sidenight"].ToString() == "false")
            {
                int numberOfSingleDoubleNights = sNights[DateTime.Parse(ddlDateFrom.Items[i + 1].Value).Day] + dNights[DateTime.Parse(ddlDateFrom.Items[i + 1].Value).Day];
                int numberOfSingleNights = sNights[DateTime.Parse(ddlDateFrom.Items[i + 1].Value).Day];
                int numberOfDoubleNights = dNights[DateTime.Parse(ddlDateFrom.Items[i + 1].Value).Day];

                singleNights.Add(numberOfSingleNights);
                doubleNights.Add(numberOfDoubleNights);
                sumSingleDoubleNights.Add(numberOfSingleDoubleNights);
            }

        }


        var index = sumSingleDoubleNights.IndexOf(sumSingleDoubleNights.Max());

        int singleMax1 = singleNights[index];
        int doubleMax1 = doubleNights[index];

        singleNights.RemoveAt(index);
        doubleNights.RemoveAt(index);
        sumSingleDoubleNights.RemoveAt(index);

        index = sumSingleDoubleNights.IndexOf(sumSingleDoubleNights.Max());

        int singleMax2 = singleNights[index];
        int doubleMax2 = doubleNights[index];

        singleDoublePair.Add(singleMax1 + singleMax2);
        singleDoublePair.Add(doubleMax1 + doubleMax2);

        return singleDoublePair;
    }
}