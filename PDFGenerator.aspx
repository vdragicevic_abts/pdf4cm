﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="PDFGenerator.aspx.cs" Inherits="_PDFGenerator" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    
    <title>Housing Agreement Generator</title>
    <link href="visuals/css/main.css" rel="stylesheet" type="text/css" />  
    <link href="visuals/css/showLoading.css" rel="stylesheet" />
    <style type="text/css">
        .loadinggif {
            background: url('visuals/img/ui-anim_basic_16x16.gif') no-repeat left center;
        }
    </style>
    <%--<script src="dialogWindow/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="dialogWindow/jquery-ui-1.8.11.custom.min.js" type="text/javascript"></script>
    <link href="dialogWindow/css/custom-theme/jquery-ui-1.8.11.custom.css" rel="stylesheet" type="text/css" />--%>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">    
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>      
    <script src="js/validation.js" type="text/javascript"></script>
    <script src="js/jquery.showLoading.min.js"></script>    

    <script type="text/javascript">

        function SetPrice() {

            if ($('#ddlRoomTypeSingle')[0].selectedIndex != '-1') {
                $('#ddlRoomTypeDouble').attr('display', 'false');
            }
            else {
                $('#ddlRoomTypeDouble').attr('display', 'true');
            }


            if ($('#ddlRoomTypeDouble')[0].selectedIndex != '-1') {
                $('#ddlRoomTypeSingle').attr('display', 'false');
            }
            else {
                $('#ddlRoomTypeSingle').attr('display', 'true');
            }

            $('#divNightPrices').hide();

            var sprice = parseFloat($('#ddlRoomTypeSingle option:selected').attr('sglPrice')).toFixed(2);
            var dprice = parseFloat($('#ddlRoomTypeDouble option:selected').attr('dblPrice')).toFixed(2);

            if (!isNaN(sprice) || !isNaN(dprice)) {

                sprice = isNaN(sprice) ? 0 : sprice;
                dprice = isNaN(dprice) ? 0 : dprice;

                $('#lblSingleDoublePrice').html('$ ' + sprice + ' / ' + '$ ' + dprice);

            } else {

                $('#divNightPrices').show();
                $('#divNightPrices').height(100);

                $('#lblSingleDoublePrice').hide();
                $('#lblNightPrices').show();

                var selectobject = $('#ddlHotels option:selected');

                var datefrom = document.getElementById("ddlDateFrom");

                for (var i = 1; i < datefrom.length; i++) {

                    var ch = '/';
                    var date = datefrom.options[i].value.replace('/', "-").replace('/', "-");

                    var d = new Date(datefrom.options[i].value);

                    $('#tbsr' + d.getDate()).val(parseFloat(selectobject.attr('[' + date + ']').split('|')[0].toString()).toFixed(2));
                    $('#tbdr' + d.getDate()).val(parseFloat(selectobject.attr('[' + date + ']').split('|')[1].toString()).toFixed(2));

                }
            }
        }

        function isNumeric(keyCode) {
            return ((keyCode >= 48 && keyCode <= 57) || keyCode == 8 || keyCode == 9 || keyCode == 189 || (keyCode >= 96 && keyCode <= 105)); // keyCode == 32 ||
        }

        $(document).ready(
            function () {

                // LOOP FOR NO_OF_ROOMS SELECT TAG
                var options = $('#ddlRooms').html();
                for (i = 1; i < 401; i++) {
                    var nja = "<option value='" + i + "'>" + i + "</option>";
                    options += nja;
                }
                $('#ddlRooms').html(options);


                $(".formFields").focus(
                    function () {
                        $(this).css('background', '#f1f1f1');
                    }
                )

                $(".formFields").blur(
                   function () {
                       $(this).css('background', '#fff');
                   }
               )


                $currentItem = 0;
                $lastItem = 0;
                for ($i = 1; $i < 20; $i++) {
                    if ($('#dlRSS_ctl0' + $i + '_TitleLink').length > 0) {
                        $('#dlRSS_ctl0' + $i + '_TitleLink').parent().parent().css('display', 'none');
                        $lastItem = $i;
                    }
                }
                $('#previous').click(
                    function () {
                        if ($currentItem > 0) {
                            $('#dlRSS_ctl0' + $currentItem + '_TitleLink').parent().parent().css('display', 'none');
                            $('#dlRSS_ctl0' + ($currentItem - 1) + '_TitleLink').parent().parent().fadeIn();
                            $currentItem--;
                            $('#newsNav').children('a:nth-child(2)').css('color', '#0A568C');
                        }
                        else {
                            $(this).css('color', '#999');
                        }
                    }
                );
                $('#next').click(
                    function () {
                        if ($currentItem < $lastItem) {
                            $('#dlRSS_ctl0' + $currentItem + '_TitleLink').parent().parent().css('display', 'none');
                            $('#dlRSS_ctl0' + ($currentItem + 1) + '_TitleLink').parent().parent().fadeIn();
                            $currentItem++;
                            $('#newsNav').children('a:nth-child(1)').css('color', '#0A568C');
                        }
                        else {
                            $(this).css('color', '#999');
                        }
                    }
                );

                GetCompanyInformationByEmail();
                GetCompanyInformationByCompanyName();
                GetCompanyInformationByGroupName();
            }
        );

    </script>
    <script type="text/javascript">

        var _complete = false;
        var _complete_2 = false;
        var _complete_3 = false;

        function copyAllTableNights2HiddenFields() {

            $("#divNights table tr td").each(function () {
                // $(this) refers to the div
                if ($(this)[0].firstChild.type == 'text') {
                    if ($(this)[0].firstChild.id.substring(0, 3) != 'tbd') {
                        $('#hf' + $(this)[0].firstChild.id.substring(2)).val($(this)[0].firstChild.value);
                    }
                    else {
                        $('#hfd' + $(this)[0].firstChild.id.substring(3)).val($(this)[0].firstChild.value);
                    }
                }
            }
                    );
        }

        function resetAllHiddenFields() {

            $("#divNights table tr td").each(function () {
                // $(this) refers to the div
                if ($(this)[0].firstChild.type == 'text') {
                    $($(this)[0].firstChild.id.substring(2)).val($('#ddlRooms').val());
                }
            }
                    );

        }

        function showTableNightPrices() {
           
            $("#divNightPrices").dialog('open');          
        }

        function showTableNights() {

            if ($('#ddlRooms').val() != '-1') {

                if ($("#hfEditNights").val() == 'false') {


                    if (($('#ddlRoomTypeSingle').val() != "-1") && (($('#ddlRoomTypeDouble').val() == "-1"))) {
                        $('#divNights table tr:eq(1)').show();
                        $('#divNights table tr:eq(2)').hide();
                    }

                    if (($('#ddlRoomTypeSingle').val() == "-1") && (($('#ddlRoomTypeDouble').val() != "-1"))) {
                        $('#divNights table tr:eq(1)').hide();
                        $('#divNights table tr:eq(2)').show();
                    }

                    if (($('#ddlRoomTypeSingle').val() != "-1") && (($('#ddlRoomTypeDouble').val() != "-1"))) {
                        $('#divNights table tr:eq(1)').show();
                        $('#divNights table tr:eq(2)').show();
                    } 

                    //switch ($('#ddlRoomType').val()) {
                    //    case '3':
                    //        {
                    //            $('#divNights table tr:eq(1)').show();
                    //            $('#divNights table tr:eq(2)').show();
                    //        }
                    //        break;
                    //    case '2':
                    //        {
                    //            $('#divNights table tr:eq(1)').hide();
                    //            $('#divNights table tr:eq(2)').show();
                    //        }
                    //        break;
                    //    case '1':
                    //        {
                    //            $('#divNights table tr:eq(1)').show();
                    //            $('#divNights table tr:eq(2)').hide();
                    //        }
                    //        break;
                    //}

                    $("#divNights table tr td").each(function () {
                        // $(this) refers to the div
                        if ($(this)[0].firstChild.type == 'text') {
                                                                      
                            if (($('#ddlRoomTypeSingle').val() != "-1") && (($('#ddlRoomTypeDouble').val() == "-1"))) {
                                if ($(this)[0].firstChild.id.indexOf('tbd') == '-1') {
                                    $(this)[0].firstChild.value = $('#ddlRooms').val();
                                }
                                else {
                                    $(this)[0].firstChild.value = '0';
                                }
                            }

                            if (($('#ddlRoomTypeSingle').val() == "-1") && (($('#ddlRoomTypeDouble').val() != "-1"))) {
                                if ($(this)[0].firstChild.id.indexOf('tbd') == '0') {
                                    $(this)[0].firstChild.value = $('#ddlRooms').val();
                                }
                                else {
                                    $(this)[0].firstChild.value = '0';
                                }
                            }

                            if (($('#ddlRoomTypeSingle').val() != "-1") && (($('#ddlRoomTypeDouble').val() != "-1"))) {
                                $(this)[0].firstChild.value = $('#ddlRooms').val();
                            }

                            //switch ($('#ddlRoomType').val()) {
                            //    case '3':
                            //        {
                            //            $(this)[0].firstChild.value = $('#ddlRooms').val();
                            //        }
                            //        break;
                            //    case '2':
                            //        {
                            //            if ($(this)[0].firstChild.id.indexOf('tbd') == '0') {
                            //                $(this)[0].firstChild.value = $('#ddlRooms').val();
                            //            }
                            //            else {
                            //                $(this)[0].firstChild.value = '0';
                            //            }
                            //        }
                            //        break;


                            //    case '1':
                            //        {
                            //            if ($(this)[0].firstChild.id.indexOf('tbd') == '-1') {
                            //                $(this)[0].firstChild.value = $('#ddlRooms').val();
                            //            }
                            //            else {
                            //                $(this)[0].firstChild.value = '0';
                            //            }
                            //        }
                            //        break;
                            //}
                        }
                    }
                    );
                }

                $("#divNights").dialog('open');

            }
            else {

                alert('Please first select number of rooms to continue with editing!');

            }


        }

        function closeTableNights() {
            $("#divNights").dialog('close');
        }

        function GetCustomerInformation(bookingID) {
                        
            $.ajax({
                type: "POST",
                url: "Users.asmx/GetProposalCompanyInformationByBookingNumber",
                data: "{ 'bookingNumber' : '" + bookingID + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var parsed = $.parseJSON(data.d);
                    $.each(parsed, function (i, jsondata) {
                        $('#tbFullName').val(jsondata.GroupName);
                        $('#tbPhone').val(jsondata.ContactPhone);
                        $('#tbEmail').val(jsondata.ContactEmail);
                        $('#tbAddress').val(jsondata.Address);                       
                        $("#ddlCountry option:contains(" + jsondata.Country + ")").attr('selected', 'selected');
                        $("#tbAgencyName").val(jsondata.CompanyName);
                        $("#tbCity").val(jsondata.City);
                        $("#tbPhone").val(jsondata.ContactPhone);
                        $("#tbEmail").val(jsondata.ContactEmail);
                        $("#tbZip").val(jsondata.ZIP);

                        if ($("#ddlConferences option:selected").text() == jsondata.ProjectName.substring(5).split('/')[0] + jsondata.ProjectName.substring(0, 4)) {

                            $("#ddlHotels option[value='" + jsondata.HotelCode + "']").prop('selected', true);
                            $('#bookingNumber').removeClass('loadinggif');

                            //if ($("#ddlHotels").find(":contains('" + jsondata.Hotel.capitalize('title') + "')").length > 0) {
                            //    $("#ddlHotels").find(":contains('" + jsondata.Hotel.capitalize('title') + "')").attr('selected', 'selected');
                            //    $("#ddlHotels").change();
                            //    $('#bookingNumber').removeClass('loadinggif'); 
                            //}                              
                        }
                        else {
                            $("#ddlConferences option:contains(" + jsondata.ProjectName.substring(5).split('/')[0] + jsondata.ProjectName.substring(0, 4) + ")").attr('selected', 'selected');
                            $("#ddlConferences").change();
                        }

                        //if ($("#ddlHotels").find(":contains('" + jsondata.Hotel.capitalize('title') + "')").length > 0) {
                        //    $("#ddlHotels").find(":contains('" + jsondata.Hotel.capitalize('title') + "')").attr('selected', 'selected');                            
                        //}
                    });
                },
                error: function (XHR, errStatus, errorThrown) {
                    var err = JSON.parse(XHR.responseText);
                    errorMessage = err.Message;
                    alert(errorMessage);
                }
            });
        }

        

        function GetCompanyInformationByEmail() {              
            
            $("#tbEmail").autocomplete({
                source: function (request, response) {                    
                    if (_complete) {                                               
                        LoaderShow(true, $("#loaderEmail"), $("#imgEmail"));
                        $.ajax({
                            type: "POST",
                            url: "PDFGenerator.aspx/GetCompanyInfoByEmail",
                            data: "{ 'email' : '" + $("#tbEmail").val() + "'}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                if (data.d.length > 0) {
                                    //response(data.d);
                                    response($.map(data.d, function (item) {
                                        return {
                                            label: item.split('-')[0],
                                            value: item.split('-')[1]
                                        }                                        
                                    }))
                                }
                                else {
                                    //$("#loaderEmail").hideLoading();
                                    LoaderShow(false, $("#loaderEmail"), $("#imgEmail"));
                                    alert("No Match");
                                }                                   
                                _complete = false;
                                 LoaderShow(false, $("#loaderEmail"), $("#imgEmail"));
                            },
                            error: function (result) {
                                _complete = false;
                                  LoaderShow(false, $("#loaderEmail"), $("#imgEmail"));
                                //$("#loaderEmail").hideLoading();
                                alert("No Match");                                
                            }                           
                        });
                    }
                },
                select: function (event, ui) {                    
                    _complete = false;
                    GetCompanyDetailsByCompanyCode(ui.item.value);
                    LoaderShow(false, $("#loaderEmail"), $("#imgEmail"));
                    //$("#loaderEmail").hideLoading();
                    event.preventDefault();
                },                
                minLength: 3
            }).focus(function () {
                _complete = false;
                LoaderShow(false, $("#loaderEmail"), $("#imgEmail"));
                //$("#loaderEmail").hideLoading();
                event.preventDefault();                
            });         
        }

        function GetCompanyInformationByCompanyName() {            
            $("#tbAgencyName").autocomplete({
                source: function (request, response) {
                    if (_complete_2) { 
                        //$("#loaderAgency").showLoading();
                        LoaderShow(true, $("#loaderAgency"), $("#imgAgencyName"));
                        $.ajax({
                            type: "POST",
                            url: "PDFGenerator.aspx/GetCompanyInfoByCompanyName",
                            data: "{ 'agency' : '" + $("#tbAgencyName").val() + "'}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                //response(data.d);
                                if (data.d.length > 0) {
                                    response($.map(data.d, function (item) {
                                        return {
                                            label: item.split('-')[0],
                                            value: item.split('-')[1]
                                        }
                                    }))
                                }
                                else {
                                    LoaderShow(false, $("#loaderAgency"), $("#imgAgencyName"));
                                    //$("#loaderAgency").hideLoading();
                                    alert("No Match");
                                }
                                _complete_2 = false;
                                LoaderShow(false, $("#loaderAgency"), $("#imgAgencyName"));
                            },
                            error: function (result) {
                                _complete_2 = false;
                                //$("#loaderAgency").hideLoading();
                                LoaderShow(false, $("#loaderAgency"), $("#imgAgencyName"));
                                alert("No Match");
                            }
                        });
                    }
                },
                select: function (event, ui) {
                    _complete_2 = false;
                    //$('#tbAgencyName').val(ui.item.value);
                    GetCompanyDetailsByCompanyCode(ui.item.value);
                    LoaderShow(false, $("#loaderAgency"), $("#imgAgencyName"));
                    //$("#loaderAgency").hideLoading();
                    event.preventDefault();
                },
                minLength: 3
            }).focus(function () {
                _complete_2 = false;
                LoaderShow(false, $("#loaderAgency"), $("#imgAgencyName"));
                //$("#loaderAgency").hideLoading();
                event.preventDefault();                  
            });
        } 

        function GetCompanyInformationByGroupName() {            
            $("#tbFullName").autocomplete({
                source: function (request, response) {
                    if (_complete_3) { 
                        //$("#loaderAgency").showLoading();
                        LoaderShow(true, $("#loaderGroupName"), $("#imgGroupName"));
                        $.ajax({
                            type: "POST",
                            url: "PDFGenerator.aspx/GetCompanyInfoByCompanyName",
                            data: "{ 'agency' : '" + $("#tbFullName").val() + "'}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                //response(data.d);
                                if (data.d.length > 0) {
                                    response($.map(data.d, function (item) {
                                        return {
                                            label: item.split('-')[0],
                                            value: item.split('-')[1]
                                        }
                                    }))
                                }
                                else {
                                    LoaderShow(false, $("#loaderGroupName"), $("#imgGroupName"));
                                    //$("#loaderAgency").hideLoading();
                                    alert("No Match");
                                }
                                _complete_3 = false;
                                LoaderShow(false, $("#loaderGroupName"), $("#imgGroupName"));
                            },
                            error: function (result) {
                                _complete_3 = false;
                                //$("#loaderAgency").hideLoading();
                                LoaderShow(false, $("#loaderGroupName"), $("#imgGroupName"));
                                alert("No Match");
                            }
                        });
                    }
                },
                select: function (event, ui) {
                    _complete_3 = false;
                    //$('#tbAgencyName').val(ui.item.value);
                    GetCompanyDetailsByCompanyCode(ui.item.value);
                    LoaderShow(false, $("#loaderGroupName"), $("#imgGroupName"));
                    //$("#loaderAgency").hideLoading();
                    event.preventDefault();
                },
                minLength: 3
            }).focus(function () {
                _complete_3 = false;
                LoaderShow(false, $("#loaderGroupName"), $("#imgGroupName"));
                //$("#loaderAgency").hideLoading();
                event.preventDefault();                  
            });
        }


        function GetCompanyDetailsByCompanyCode(code) {

            $.ajax({
                type: "POST",
                url: "PDFGenerator.aspx/GetCompanyDetailsByCompanyCode",
                data: "{ 'companyCode' : '" + code + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var companyDetails = jQuery.parseJSON(data.d);
                    ResetCompanyDetails();
                    PopulateCompanyDetails(companyDetails[0]);
                },
                error: function (result) {                    
                    alert("No Match");
                }
            });
        }

        function PopulateCompanyDetails(details) {

            details.Country = details.Country == "USA" ? 'United States' : details.Country;

            if ($('#tbAgencyName').is(":focus") || $('#tbEmail').is(":focus")) {
                $('#tbAgencyName').val(details.Name);
            }
            else if ($('#tbFullName').is(":focus")) {
                $('#tbFullName').val(details.Name);
            }            

            $('#tbAddress').val(details.Address);
            $('#tbZip').val(details.ZIP);
            $('#tbCity').val(details.City);            

            $("#ddlCountry option").each(function () {
                if ($(this).html() == details.Country) {
                    $(this).attr("selected", "selected");
                    return;
                }
            });
                   
            $('#ddlState').val(details.State);
            $('#tbPhone').val(details.GeneralPhone);

            $('#hfCompanyCode').val(details.Code);
        }

        function ResetCompanyDetails() {
             $('#tbAgencyName').val('');
            $('#tbAddress').val('');
            $('#tbZip').val('');
            $('#tbCity').val('');            
            $("#ddlCountry option:first-child").attr("selected", "selected");                                                 
            $('#ddlState').val('');
            $('#tbPhone').val('');
        }

        function LoaderShow(show, obj, btn) {
            if (show) {
                $(obj).showLoading();
                $(btn).attr("src", "");
            }
            else {
                $(obj).hideLoading();
                $(btn).attr("src", "visuals/img/if_magnifier_zoom_12100.png");
            }
        }

        //JQ popup
        $(document).ready(
            function () {
                $("#divNights").dialog({
                    height: 200,
                    width: 800,
                    modal: true,
                    autoOpen: false,
                    show: 'slide',
                    resizable: false,

                    buttons: {
                        "Confirm": function () {
                            $("#hfEditNights").val('true');
                            copyAllTableNights2HiddenFields();
                            $(this).dialog("close");
                        },
                        Cancel: function () {
                            $("#hfEditNights").val('false');
                            resetAllHiddenFields();
                            $(this).dialog("close");
                        }
                    }
                });
            }
        );

        $(document).ready(
            function () {
                $('#divNightPrices').hide();
            });

    </script>

</head>
<body>
    <form id="form1" runat="server">
                    <!-- Table for Nights -->
                    <asp:Panel id="divNights" title="Room Nights Editor" runat="server">                   
                                      
                    </asp:Panel> 
                    <asp:Panel ID="divHFNights" runat="server">
                    
                    </asp:Panel>  
                    <!-- Company Code -->
                    <asp:HiddenField ID="hfCompanyCode" runat="server" />

    <div id="page_Wrapper">
        <div id="page_mainContent">
            <div id="mainContent_top">
                <div id="contactForm">                                   
                    <div id="contactForm_wrapper">                        
                        <div id="labelsInfo" align="center"><span>Housing Agreement Generator</span></div>
                        <div align="center" style="padding: 5px; margin-left: 185px;">
                            <div style="float:left; margin-top:1px;">
                                <input type="text" id="bookingNumber" placeholder="Booking Number" style="border: 1px solid grey; height: 20px; width:145px;" />
                            </div>
                            <div style="float:left; margin-left:3px;">
                                <button type="button" style="background-color: ButtonFace ActiveBorder; height: 26px;" class="ui-widget" onclick="GetCustomerInformation($('#bookingNumber').val()); $('#bookingNumber').addClass('loadinggif');">Retrieve</button>
                            </div>
                        </div>  
                        <div style="clear:both; margin-bottom:5px;"></div>
                        
                        <div class="labels">  
                            <span>Conference:</span>                          
                            <span>Agency Name/Company:</span>
                            <span>Email:</span>
                            <span>Sponsoring Group Name:</span>
                            <span>Phone:</span>                            
                            <span>Country:</span>                            
                            <span>Address:</span>
                            <span>City:</span>
                            <span>Zip:</span>
                            <span>Preferred Hotel Choice 1:</span>
                            <span>Preferred Hotel Choice 2:</span>
                            <span>Preferred Hotel Choice 3:</span>
                            <span>Single/Double room price: </span>
                        </div>
                        
                        <div class="fields">
                            <asp:DropDownList class="formDropDown" runat="server" ID="ddlConferences" AutoPostBack="true" message="*Select Conference!" display="true"                             OnSelectedIndexChanged="ddlConferences_SelectedIndexChanged">                                
                            </asp:DropDownList>
                            <div id="loaderAgency"><div style="float:left; margin-bottom:-5px; margin-top:-3px; margin-right:3px;"><asp:TextBox CssClass="formFields" ID="tbAgencyName" runat="server" message="*Enter Agency Name" display="true"></asp:TextBox></div><div style="float:left; margin-top:2px;"><button style="height:26px;width:38px;" id="btnSearchAgency" type="button" onclick="if($('#tbAgencyName').val().length > 2) {  _complete_2 = true; } $('#tbAgencyName').autocomplete('search', $('#tbAgencyName').val());"><img id="imgAgencyName" src="visuals/img/if_magnifier_zoom_12100.png" /></button></div></div>
                            <div style="clear:both;"></div>
                            <div id="loaderEmail"><div style="float:left; margin-bottom:-2px; margin-top:-4px; margin-right:3px; "><asp:TextBox autocomplete="off" CssClass="formFields" ID="tbEmail"  runat="server" message="*Enter Email!" display="true" regularexpression="^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6}$" regmessage="*Not a valid Email!"></asp:TextBox></div><div style="float:left; margin-top:1px;"><button style="height:26px; width:38px;" id="btnSearchEmail" onclick="if($('#tbEmail').val().length > 2) { _complete = true; } $('#tbEmail').autocomplete('search', $('#tbEmail').val());" type="button"><img id="imgEmail" src="visuals/img/if_magnifier_zoom_12100.png" /></button></div></div>                            
                            <div style="clear:both;"></div>
                            <div id="loaderGroupName"><div style="float:left; margin-bottom:-2px; margin-top:-4px; margin-right:3px; "><asp:TextBox CssClass="formFields" ID="tbFullName" runat="server" message="*Enter Sponsoring Group Name!" display="true" ></asp:TextBox></div><div style="float:left; margin-top:1px;"><button style="height:26px; width:38px;" id="btnSearchGroupName" onclick="if($('#tbFullName').val().length > 2) { _complete_3 = true; } $('#tbFullName').autocomplete('search', $('#tbFullName').val());" type="button"><img id="imgGroupName" src="visuals/img/if_magnifier_zoom_12100.png" /></button></div></div>                            
                            <div style="clear:both;"></div>
                            <asp:TextBox CssClass="formFields" ID="tbPhone"  runat="server" message="*Enter Phone!" display="true" ></asp:TextBox>                           
                            <%--<asp:TextBox CssClass="formFields" ID="tbEmail"  runat="server" message="*Enter Email!" display="true" regularexpression="^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6}$" regmessage="*Not a valid Email!"></asp:TextBox>--%>
                            <select class="formDropDown" runat="server" id="ddlCountry" onchange="document.getElementById('hdCountry').value = ddlCountry.options[ddlCountry.selectedIndex].value;" message="*Enter Country!" display="true"></select> 
                            <%--<asp:TextBox CssClass="formFields" ID="tbAgencyName" runat="server" message="*Enter Agency Name" display="true"></asp:TextBox>--%>
                            <asp:TextBox CssClass="formFields" ID="tbAddress" runat="server" message="*Enter Address" display="true"> </asp:TextBox>
                            <asp:TextBox CssClass="formFields" ID="tbCity"  runat="server" message="*Enter City" display="true"></asp:TextBox>
                            <asp:TextBox CssClass="formFields" ID="tbZip" runat="server" message="*Enter Zip" display="true"></asp:TextBox>
                            <asp:DropDownList CssClass="formDropDown" ID="ddlHotels" runat="server"  message="*Select Hotel choice 1!" display="true" AutoPostBack="true" OnSelectedIndexChanged="ddlHotels_SelectedIndexChanged"></asp:DropDownList>                                
                            <asp:DropDownList CssClass="formDropDown" ID="ddlHotels1" runat="server"  message="*Select Hotel choice 2!" display="false"></asp:DropDownList>
                            <asp:DropDownList CssClass="formDropDown" ID="ddlHotels2" runat="server"  message="*Select Hotel choice 3!" display="false"></asp:DropDownList>
                            <asp:Label CssClass="formFields" style="width:125px; font-size:12px;" ID="lblSingleDoublePrice" Text="sgl: $ 0.00 / dbl: $ 0.00" runat="server"></asp:Label>
                        </div>
                            
                            <div style="clear:both;"></div>
                            <asp:Panel ID="divNightPrices" title="Prices per night" runat="server"></asp:Panel>  
                            
                            <div class="labels" >                                 
                                <div style="width: 150px; height: 70px; margin-left:10px;">
                                    <%--<span style="color: #6984b2; font-size: 13px;">Display Name (Optional):</span>--%>                                    
                                    <div>
                                        <input id="txtSingleDisplayName" runat="server" type="text" placeholder="SGL Room (Display Name)" style="border: 0px; height: 20px; font-size:11px; text-align:center;" />&nbsp;:
                                    </div>
                                    <div style="margin-top: 14px;">
                                        <input id="txtDoubleDisplayName" runat="server" type="text" placeholder="DBL Room (Display Name)" style="border: 0px; height: 20px; font-size:11px; text-align:center;"/>&nbsp;:
                                    </div>
                                </div>
                                <%--<span>Single Room Type:</span>                            
                                <span>Double Room Type:</span> --%>                           
                                <span>Number of Rooms:</span>
                                <span>Date From:</span>
                                <span>Date To:</span>
                            </div>
                            
                            <div class="fields" style="margin-top:-5px;">

                                <select style="width:400px;" class="formDropDown" id="ddlRoomTypeSingle" runat="server" message="*Select Room Type!" display="true" onchange="SetPrice();">
                                            <option value="-1">Select</option>
                                </select>

                                <select style="width:400px;" class="formDropDown" id="ddlRoomTypeDouble" runat="server" message="*Select Room Type!" display="true" onchange="SetPrice();">
                                            <option value="-1">Select</option>
                                </select>                               
                                                      
                                <select class="formDropDown" ID="ddlRooms" style="float:left; margin-top:0px;" runat="server" message="*Select Number of Rooms!" display="true" onchange="$('#hfEditNights').val('false');">   
                                    <option value="-1">Select</option>
                                </select>
                                
                                <asp:Label style="width:60px; cursor:pointer; display:block; float:left; margin-top:0px; margin-left:5px; border:2px solid #d4d4d4; padding:4px; color:Red;" ID="lblEditTableNights" runat="server" Text="Edit Nights" onclick="showTableNights();"></asp:Label><br /><br /><br />                         
                            
                                <select class="formDropDown" ID="ddlDateFrom" style="margin-top:-5px;" runat="server" message="*Select Date From!" display="true">                                   									                                    
                                </select>
                            
                                <select class="formDropDown" ID="ddlDateTo" style="margin-top:0px;" runat="server" message="*Select Date To!" display="true">                                                                        									
                                </select>                            
                            
                                <asp:Button CssClass="submitButton" ID="btnSubmit" runat="server" Text="Submit" onclick="SendMessage" OnClientClick="return Validate(this.form);"  />
                                <asp:HiddenField ID="hdCountry" runat="server" />
                                                        
                                <div style="margin-top:-37px; margin-left:75px;"><span style="color:Red;">Please disable your popup blocker<br />before clicking the submit button</span></div>                                                       
                            </div>                                                         
                        </div>                        
                    </div>
                   <%-- <div style="position: absolute; margin-left: 606px; margin-top: 535px; width: 150px; height: 70px;">
                        <span style="color:#6984b2; font-size:13px;">Display Name (Optional):</span>
                        <div>
                            <input id="txtSingleDisplayName" runat="server" type="text" placeholder="SGL Room" style="border:0px; height:20px;" />
                        </div>
                        <div style="margin-top:14px;">
                            <input id="txtDoubleDisplayName" runat="server" type="text" placeholder="DBL Room" style="border:0px; height:20px;" />
                        </div>
                    </div>--%>
                </div>
            </div>
            <div id="mainContent_bottom">
              <%--<div style="position:absolute; cursor:pointer; margin-top:581px; margin-left:300px; border:2px solid #d4d4d4; padding:4px;" onclick="showTableNights();"><span style="color:Red;">Edit nights</span></div>--%>
            </div>
        </div>
        <div id="page_footer" style="display:none;">
            
             <div id="news" style="float:left;">
                        <%--<a ztarget="_blank" href="#"><div id="newsTitle_wrapper"><h3 id="newsTitle">News/RSS feed section</h3></div></a>--%>
                       <br /> 
                        <div id="news_wrapper">                           
                            <asp:DataList ID="dlRSS" runat="server" RepeatDirection="Horizontal">
                                <ItemTemplate>
                                    
                                    <div class="RSSTitle">
                                        <asp:HyperLink ID="TitleLink" Target="_blank" runat="server" Text='<%# Eval("Title") %>'
                                            NavigateUrl='<%# Eval("link") %>' />
                                    </div>
                                    
                                    <div class="RSSSubtitle">
                                        <asp:Label ID="SubtitleLabel" runat="server" Text='<%# Eval("description") %>' />
                                    </div>
                                    
                                    <div class="RSSInfo" style="display:none;">
                                        posted on
                                        <asp:Label ID="DateRSSedLabel" runat="server" Text='<%# Eval("pubDate", "{0:d}") %>' />
                                    </div>
                                    
                                </ItemTemplate>
                            </asp:DataList>                           
                        </div>
                        <div id="newsNav" style="margin-top:20px;">
                            <a id="previous" href="#">previous</a>&nbsp;
                            <a id="next" href="#">next</a>
                        </div>
                        
                        <%--<div id="chat" title="chat"></div>--%>
                        
            </div>
            
            <div title="chat" style="float:left; width:247px; height:119px; zbackground:#ccc; margin-left:17px;background:url(visuals/img/chat.png); cursor:pointer;" onclick="window.open('http://chat.roomhandler.com/chat/chatstart.aspx?domain=www.apa-igd.com', 'lovechild','height=430,width=540,left=600,top=200,resizable=0,menubar=0,toolbar=0,location=0,directories=0,scrollbars=1,status=0,border=0')""></div>          
            
            <div style="clear:both; zbackground-color:Red;"></div>
            
            <div id="contactInfo" style="width:900px; height:150px; zbackground:#eee; margin-top:0px; color:#444; margin-left:auto; margin-right:auto; zletter-spacing:3px;line-height:18px;">
                    <p class="aboutUs" style="width:900px; margin:0px;padding:0px; text-align:center;">

                        <p class="Address" style="margin:0px;padding:0px;float:left; height:100px; width:205px; zbackground:#ccc; margin-right:165px; ">	
			                <b style="font-size:16px; font-weight:normal;color:#006078; display:block; margin-bottom:5px;">Miami, U.S.A</b> 
			                1666 Kennedy Causeway, Suite 702<br />
					        North Bay Village, FL 33141<br />
			                Tel:+1 (305) 865-4648<br />
			                Fax:+1 (305) 865-4382<br />
		                </p>

		                <p class="Address" style="margin:0px;padding:0px;float:left; height:100px; width:215px; zbackground:#ccc;margin-right:155px;">	
			                <b style="font-size:16px; color:#006078;font-weight:normal;display:block; margin-bottom:5px;font-family:Calibri;">Rome, E.U.</b>
			                Via Giacomo Trevis, 88 Sc.A int. 2<br />
                            Rome, 00147<br />
			                Tel:+39 (06) 9028-6042<br />
			                Fax:+39 (06) 233-24-5675<br />
		                </p>

		                <p class="Address" style="margin:0px;padding:0px;float:left; height:100px; width:150px; zbackground:#ccc;">	
			                <b style="font-size:16px;color:#006078; font-weight:normal;display:block; margin-bottom:5px;">San Francisco, U.S.A.</b>
			                10 Moss Lane<br />
			                Emeryville, CA 94608<br />
			                Tel:+1 (415) 738-4839<br />
			                Fax:+1 (305) 865-4382<br />
		               </p>
                  </p>
                    <p>
                    </p>
             </div>
        </div>                
    </div>
    <asp:HiddenField ID="hfEditNights" runat="server" Value="false" />                  
    </form>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-6149449-44']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

</script>
</body>
</html>
