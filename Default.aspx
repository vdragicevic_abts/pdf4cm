﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <span>Username: </span><br />
        <asp:TextBox ID="tbUserName" runat="server"></asp:TextBox><br />
        <span>Password: </span><br />
        <asp:TextBox ID="tbPassword" runat="server" TextMode="Password"></asp:TextBox><br /><br />
        <asp:Button ID="btnLogin" runat="server" Text="Login" onclick="btnLogin_Click" />
    </div>
    </form>
</body>
</html>
