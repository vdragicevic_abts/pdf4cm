﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.Expressions;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net.Mail;
using System.Xml;
using System.IO;
using System.Web.SessionState;
using HotelRooms;
using iTextSharp.text.pdf;
using System.Collections;
using System.Collections.Generic;

public partial class _Default : System.Web.UI.Page 
{
    private void Page_PreRender(object sender, System.EventArgs e)
    {
        if (Request.Browser.Browser == "Chrome")
        {
            Response.Write("<script type='text/javascript'>alert('Google Chrome is not supported browser.\\nPlease use Firefox or Internet Explorer instead.');</script>");
            Response.End();
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {       
          
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
			ServicePointManager.Expect100Continue = true;
            ServicePointManager.DefaultConnectionLimit = 9999;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
			
            FillDDLCountries();
            FillDDLSelectItem();
            DeleteTempFiles();

            LoadConferences();

            Stream inputStream = Request.InputStream;

            if (inputStream.Length > 0)
            {
                SavePDF(inputStream);

                //Response.Redirect("/2pdf4cm/Success.pdf");                
            }
        }

        PrerenderTableNights();      
                
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        
    }

    protected void FillDDLConferences()
    {

    }

    protected void SendMessage(object sender, EventArgs e)
    {
        try
        {
            PopulatePDFForm(ddlConferences.SelectedValue);
            ClearControls();
        }
        catch (Exception ex)
        {
            Page.ClientScript.RegisterStartupScript(typeof(Page), "003", "<script type='text/javascript'>alert('" + ex.Message + "');</script>");
        }
    }   

    protected void ClearControls()
    {
        this.tbFullName.Text = "";
        this.tbPhone.Text = "";
        this.tbEmail.Text = "";
        this.tbAgencyName.Text = "";
        //this.tbHotel.Text = "";
        this.ddlRooms.SelectedIndex = 0;
        this.ddlHotels.SelectedIndex = 0;
        this.ddlHotels1.SelectedIndex = 0;
        this.ddlHotels2.SelectedIndex = 0;
        this.tbCity.Text = "";
        this.tbZip.Text = "";
        this.tbAddress.Text = "";
        this.ddlCountry.SelectedIndex = 0;      
        this.ddlDateFrom.SelectedIndex = 0;
        this.ddlDateTo.SelectedIndex = 0;
        this.ddlRoomType.SelectedIndex = 0;
    }   

    public void SavePDF(Stream myFile)
    {              
        //SessionIDManager Manager = new SessionIDManager();
        //string sessionID = Manager.CreateSessionID(Context);
        string hashCode = String.Empty; //Manager.GetHashCode();        

        try
        {
            PdfReader pdfReader = new PdfReader(myFile);
            string conferenceID = pdfReader.AcroFields.GetField("txtConferenceID");
            hashCode = pdfReader.AcroFields.GetField("txtUserKey");
            bool sidenights = Boolean.Parse(pdfReader.AcroFields.GetField("txtSidenights"));
            string fileName = String.Empty;
            string clientEmail = pdfReader.AcroFields.GetField("txtEMail");
            string groupName = pdfReader.AcroFields.GetField("txtSponsoringGroupName");
            string groupNameSideNights = pdfReader.AcroFields.GetField("txtGroupName");
            string txtInitials = pdfReader.AcroFields.GetField("txtInitials");
            string txtInitialDate = pdfReader.AcroFields.GetField("txtInitialDate");
            string txtClientAuthorizedRepresentative = pdfReader.AcroFields.GetField("txtClientAuthorizedRepresentative");
            string txtClientRepresentative = pdfReader.AcroFields.GetField("txtClientRepresentative");   
            string txtTitle = pdfReader.AcroFields.GetField("txtTitle");
            string txtSignature = pdfReader.AcroFields.GetField("txtSignature");
            string txtDate = pdfReader.AcroFields.GetField("txtDate");
            string txtCardNumber = pdfReader.AcroFields.GetField("txtCardNumber");
            string txtExpMoYr = pdfReader.AcroFields.GetField("txtExpMoYr");
            string txtCardholderName = pdfReader.AcroFields.GetField("txtCardholderName");
            string rbPaymentMethod = pdfReader.AcroFields.GetField("rbPaymentMethod");
            string rbCreditCard = pdfReader.AcroFields.GetField("rbCreditCard");
            string txtCreditCardSignature = pdfReader.AcroFields.GetField("txtSignatureCC");
            string txtClientSignature = pdfReader.AcroFields.GetField("txtClientSignature");
            string txtExpDate = pdfReader.AcroFields.GetField("txtExpDate");
            string txtAgency = pdfReader.AcroFields.GetField("txtAgency");
            string rbPaymentMethodSideNights = pdfReader.AcroFields.GetField("Group11");
            string rbCreditCardSideNights = pdfReader.AcroFields.GetField("Group12");
            string[] txtCountry = pdfReader.AcroFields.GetListOptionDisplay("ddlCountry");
            bool createProposal = pdfReader.AcroFields.GetField("txtCreateProposal") != null ? bool.Parse(pdfReader.AcroFields.GetField("txtCreateProposal")) : false;
            string txtTotalSd = pdfReader.AcroFields.GetField("txtTotalSD");
            string transactionID = string.Empty;
            string txtToken = pdfReader.AcroFields.GetField("txtToken");
            string txtMerchant = pdfReader.AcroFields.GetField("txtMerchant");
			Merchant? merchantTerminal = new Users().GetMerchantIDFromABTSolute(conferenceID);

            var pa = new PaymentAuthorizationFirstData();

            if (txtToken != null)
            {
                if (!sidenights)
                {
                    pa.CardHolderName = txtCardholderName;
                    pa.CreditCardNumber = txtCardNumber;
                    pa.ExparationMonthYear = (Convert.ToInt32(txtExpMoYr.Split('/')[0])) <= 9 && (!txtExpMoYr.Split('/')[0].StartsWith("0")) ? "0" + txtExpMoYr.Split('/')[0] + txtExpMoYr.Split('/')[1] : txtExpMoYr.Split('/')[0] + txtExpMoYr.Split('/')[1];
                    pa.ProductPrice = txtTotalSd;
                    pa.ProductDescription = conferenceID;
                    pa.ProductCode = hashCode;
                    pa.ExactID = DatabaseConnection.GetGatewayID(Merchant.IgdInternationlGroup);
                    pa.Password = DatabaseConnection.GetGatewayPassword(Merchant.IgdInternationlGroup);
                    pa.Test = false;
                    pa.Email = clientEmail;
                    pa.HashID = DatabaseConnection.GetHashID(Merchant.IgdInternationlGroup);
                    pa.HashKey = DatabaseConnection.GetHashPassword(Merchant.IgdInternationlGroup);
                    pa.Merchant = (Merchant)merchantTerminal; //String.IsNullOrEmpty(txtMerchant) || int.Parse(txtMerchant) == 0 ? Merchant.IgdInternationlGroup : (Merchant)int.Parse(txtMerchant);

                    switch (rbCreditCard)
                    {
                        case "1":
                            pa.CardType = "Visa";
                            break;
                        case "2":
                            pa.CardType = "Mastercard";
                            break;
                        case "3":
                            pa.CardType = "American Express";
                            break;
                        default:
                            pa.CardType = "Visa";
                            break;
                    }

                    //transactionID = DepositPaymentHousingAgreementPicknights(ref pa);

                    TokenizeHousingAgreement(ref pa);

                    if (pa.Token.Length <= 3)
                    {
                        if (!pa.Merchant.ToString().Contains("PP"))
                        {
                            var firstData = ((InternetSecureAPI.FirstData)int.Parse(pa.Token));
                            string result = firstData.ToString();
                            var res = new string((result.SelectMany((c, i) => i != 0 && char.IsUpper(c) && !char.IsUpper(result[i - 1]) ? new char[] { ' ', c } : new char[] { c })).ToArray());
                        }

                        Response.Redirect("/2pdf4cm/Error.pdf");
                        //Response.Redirect("/Error.pdf?res=" + res.Replace(" ", ""));
                        
                        //Response.ContentType = "application/vnd.fdf";
                        //Response.Write(Helpers.CreateAdobeAcrobatFdfResponseMessage("Housing Agreement not Successfully Submitted. " + res));
                        //Response.End();
                    }
                }
                else
                {
                    pa.CardHolderName = txtCardholderName;
                    pa.CreditCardNumber = txtCardNumber;

                    pa.ExparationMonthYear = (Convert.ToInt32(txtExpDate.Split('/')[0])) <= 9 && (!txtExpDate.Split('/')[0].StartsWith("0")) ? "0" + txtExpDate.Split('/')[0] + txtExpDate.Split('/')[1] : txtExpDate.Split('/')[0] + txtExpDate.Split('/')[1];
                    pa.ProductPrice = "0";
                    pa.ProductDescription = conferenceID;
                    pa.ProductCode = hashCode;
                    pa.ExactID = DatabaseConnection.GetGatewayID(Merchant.IgdInternationlGroup);
                    pa.Password = DatabaseConnection.GetGatewayPassword(Merchant.IgdInternationlGroup);
                    pa.Test = false;
                    pa.Email = clientEmail;
                    pa.CardType = rbCreditCardSideNights.Replace("rb", "");
                    pa.HashID = DatabaseConnection.GetHashID(Merchant.IgdInternationlGroup);
                    pa.HashKey = DatabaseConnection.GetHashPassword(Merchant.IgdInternationlGroup);
                    pa.Merchant = (Merchant)merchantTerminal; //String.IsNullOrEmpty(txtMerchant) || int.Parse(txtMerchant) == 0 ? Merchant.IgdInternationlGroup : (Merchant)int.Parse(txtMerchant);

                    switch (pa.CardType)
                    {
                        case "Visa":
                            pa.CardType = "Visa";
                            break;
                        case "MasterCard":
                            pa.CardType = "Mastercard";
                            break;
                        case "AmericanExpress":
                            pa.CardType = "American Express";
                            break;
                        default:
                            pa.CardType = "Visa";
                            break;
                    }                    

                    TokenizeHousingAgreement(ref pa);

                    if (pa.Token.Length <= 3)
                    {
                        if (pa.Token.Length <= 3)
                        {
                            var firstData = ((InternetSecureAPI.FirstData)int.Parse(pa.Token));
                            string result = firstData.ToString();
                            var res = new string((result.SelectMany((c, i) => i != 0 && char.IsUpper(c) && !char.IsUpper(result[i - 1]) ? new char[] { ' ', c } : new char[] { c })).ToArray());
                        }

                        Response.Redirect("/2pdf4cm/Error.pdf");
                        //Response.Redirect("/Error.pdf?res=" + res);
                        
                        //Response.ContentType = "application/vnd.fdf";
                        //Response.Write(Helpers.CreateAdobeAcrobatFdfResponseMessage("Housing Agreement not Successfully Submitted. " + res));
                        //Response.End();
                    }
                }
            }



            if (sidenights)
            {
                txtCountry = new string[1];
                txtCountry[0] = "n/a";
                fileName = "Sidenights";
            }
            else
            {
                fileName = "Housingagreement";
            }

            if (createProposal)
            {
                var gen = new PDFGenerator();
                string file = gen.GetPdfDocument(conferenceID, fileName + "_" + hashCode);

                if (file.Length > 3)
                {
                    Response.Redirect("/2pdf4cm/AlreadySubmitted.pdf");

                    //Response.ContentType = "application/vnd.fdf";
                    //Response.Write(Helpers.CreateAdobeAcrobatFdfResponseMessage("Housing Agreement already Submitted."));
                    //Response.End();
                }
            }

            PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(Server.MapPath("agreements\\" + conferenceID) + "\\" + fileName + "_" + hashCode + ".pdf", FileMode.Create));


            //Encrypt credit card number
            AcroFields pdfFormFields = pdfStamper.AcroFields;
            AcroFields.Item ccItem = pdfFormFields.GetFieldItem("txtCardNumber");
            PdfDictionary dic = ccItem.GetMerged(0);
            dic.Put(PdfName.MAXLEN, new PdfNumber(60));
            //pdfFormFields.SetField("txtCardNumber", StringHelpers.Encrypt(txtCardNumber));
            //Encrypt credit card number

            if (txtToken != null)
            {
                //Overwrite credit card number with token and encrypt it!
                pdfFormFields.SetField("txtCardNumber", StringHelpers.Encrypt(pa.Token));

                //Old Code//
                //pdfFormFields.SetField("txtCardNumber", StringHelpers.Encrypt(txtCardNumber));

                //pdfFormFields.SetField("txtCardNumber", txtCardNumber.Length > 15 ? StringHelpers.Encrypt("000000000000" + txtCardNumber.Substring(txtCardNumber.Length - 4)) : StringHelpers.Encrypt("00000000000" + txtCardNumber.Substring(txtCardNumber.Length - 4)));
                //Encrypt credit card number

                //Set credit card token
                pdfFormFields.SetField("txtToken", StringHelpers.Encrypt(pa.Token));
                //Set credit card token

                //Set merchant
                pdfFormFields.SetField("txtMerchant", ((int)pa.Merchant).ToString());
                //Set merchant

                //Deposit payment transactionID for Housing Agreement picknights
                //if (!sidenights) pdfFormFields.SetField("txtDepositTransactionID", transactionID);
            }
            else
            {
                pdfFormFields.SetField("txtCardNumber", StringHelpers.Encrypt(txtCardNumber));
                //Encrypt credit card number
            }

            pdfStamper.Close();
            pdfReader.Close();

            SaveRestrictedCopy(Server.MapPath("agreements\\" + conferenceID) + "\\" + fileName  + "_" + hashCode + ".pdf", conferenceID);

            string filePath = Server.MapPath("agreements\\" + conferenceID) + "\\" + "View_" + fileName + "_" + hashCode + ".pdf";

            string validateHA = String.Empty;
            string validateSN = String.Empty;

            if (fileName == "Housingagreement")
            {
                validateHA = ValidateHAFields(txtInitials, txtInitialDate, txtClientAuthorizedRepresentative, txtTitle, txtSignature, txtDate, rbPaymentMethod, rbCreditCard, txtCardNumber, txtExpMoYr, txtCardholderName, txtCreditCardSignature);
            }
            else
            {
                validateSN = ValidateSNFields(groupNameSideNights, txtAgency, rbPaymentMethodSideNights, rbCreditCardSideNights, txtCardNumber, txtExpDate, txtCardholderName, txtCreditCardSignature, txtClientRepresentative, txtTitle, txtClientSignature, txtDate);
            }
            
            if (createProposal)
            {
                byte[] bytes = File.ReadAllBytes(Server.MapPath("agreements\\" + conferenceID) + "\\" + fileName + "_" + hashCode + ".pdf");
                var ms = new MemoryStream(bytes);

                CreateProposalInABTsolute(ms);
            }

            SendEmail(conferenceID, hashCode, sidenights, sidenights ? groupNameSideNights : groupName, clientEmail, filePath, validateHA + validateSN, txtAgency, txtCountry[0]);

            Response.Redirect("/2pdf4cm/Success.pdf");  
            
            //Response.ContentType = "application/vnd.fdf";
            //Response.Write(Helpers.CreateAdobeAcrobatFdfResponseMessage("Housing Agreement Successfully Submitted. "));
            //Response.End();
            
        }
        catch (Exception ex)
        {
			using (StreamWriter sw = File.AppendText("C:\\CustomLogs\\Log.txt"))
			{
				//Save Transaction to Log
				sw.WriteLine(ex.Message + ", " + System.DateTime.Now.ToString() + ", PDF HA ERROR" );
			}
			
            ClientScript.RegisterStartupScript(this.GetType(), "001", "alert('" + ex.Message + "')", true);
        }
    }

    private void CreateProposalInABTsolute(Stream myFile)
    {
        System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

        var hr = new HotelRooms.ABTSoluteWebServices();
        var sProposal = new HotelRooms.SProposal();

        var pdfReader = new PdfReader(myFile);
        string conferenceID = pdfReader.AcroFields.GetField("txtConferenceID");
        string hashCode = pdfReader.AcroFields.GetField("txtUserKey");
        bool sidenights = Boolean.Parse(pdfReader.AcroFields.GetField("txtSidenights"));
        string fileName = String.Empty;
        string clientEmail = pdfReader.AcroFields.GetField("txtEMail");
        string groupName = pdfReader.AcroFields.GetField("txtSponsoringGroupName");
        string groupNameSideNights = pdfReader.AcroFields.GetField("txtGroupName");
        string txtInitials = pdfReader.AcroFields.GetField("txtInitials");
        string txtInitialDate = pdfReader.AcroFields.GetField("txtInitialDate");
        string txtClientAuthorizedRepresentative = pdfReader.AcroFields.GetField("txtClientAuthorizedRepresentative");
        string txtClientRepresentative = pdfReader.AcroFields.GetField("txtClientRepresentative");
        string txtTitle = pdfReader.AcroFields.GetField("txtTitle");
        string txtSignature = pdfReader.AcroFields.GetField("txtSignature");
        string txtDate = pdfReader.AcroFields.GetField("txtDate");
        string txtCardNumber = pdfReader.AcroFields.GetField("txtCardNumber");
        string txtExpMoYr = pdfReader.AcroFields.GetField("txtExpMoYr");
        string txtCardholderName = pdfReader.AcroFields.GetField("txtCardholderName");
        string rbPaymentMethod = pdfReader.AcroFields.GetField("rbPaymentMethod");
        string rbCreditCard = pdfReader.AcroFields.GetField("rbCreditCard");
        string txtCreditCardSignature = pdfReader.AcroFields.GetField("txtSignatureCC");
        string txtClientSignature = pdfReader.AcroFields.GetField("txtClientSignature");
        string txtExpDate = pdfReader.AcroFields.GetField("txtExpDate");
        string txtAgency = pdfReader.AcroFields.GetField("txtAgency");
        string rbPaymentMethodSideNights = pdfReader.AcroFields.GetField("Group11");
        string rbCreditCardSideNights = pdfReader.AcroFields.GetField("Group12");
        string[] txtCountry = pdfReader.AcroFields.GetListOptionDisplay("ddlCountry");

        long idServiceItemSingle = long.Parse(pdfReader.AcroFields.GetField("txtIDServiceItemSingle"));
        long idServiceItemDouble = long.Parse(pdfReader.AcroFields.GetField("txtIDServiceItemDouble"));
        string abtsoluteKey = pdfReader.AcroFields.GetField("txtDocumentID");
        string housingAgreementKey = "HousingAgreement_" + abtsoluteKey;

        sProposal.HAKey = housingAgreementKey;
        sProposal.CompanyName = txtAgency;
        sProposal.ContactPerson = txtClientAuthorizedRepresentative;
        sProposal.ContactPersonRegistration = ""; //txtClientRepresentative;

        if (!sidenights)
        {
            sProposal.GroupName = groupName;
            sProposal.ContactPerson = txtClientAuthorizedRepresentative;
        }
        else
        {
            sProposal.GroupName = groupNameSideNights;
            sProposal.ContactPerson = txtClientRepresentative;
        }

        sProposal.IDProposal = 0;

        if (conferenceID.Contains("AAOHNSF"))
        {
            sProposal.ProjectName = conferenceID.Substring(conferenceID.Length - 4) + " AAO-HNSF";
        }
        else
        {
            sProposal.ProjectName = conferenceID.Substring(conferenceID.Length - 4) + " " + conferenceID.Substring(0, conferenceID.Length - 4);
        }

	//if (conferenceID.Contains("AAAAI"))
 //       {
	//   sProposal.ProjectName = conferenceID.Substring(conferenceID.Length - 4) + " " + "AAAAI/WAO";
	//}


        if ((idServiceItemSingle != 0) && (idServiceItemDouble != 0))
            sProposal.ServiceItemDefinitionArray = new SServiceItemDefinition[2];
        else
            sProposal.ServiceItemDefinitionArray = new SServiceItemDefinition[1];

        if (idServiceItemSingle != 0)
        {
            sProposal.ServiceItemDefinitionArray[0] = new SServiceItemDefinition();
            sProposal.ServiceItemDefinitionArray[0].IDServiceItem = idServiceItemSingle;
            sProposal.ServiceItemDefinitionArray[0].Note = "Came from Webstie";
        }

        if (idServiceItemDouble != 0)
        {
            sProposal.ServiceItemDefinitionArray[sProposal.ServiceItemDefinitionArray.Length-1] = new SServiceItemDefinition();
            sProposal.ServiceItemDefinitionArray[sProposal.ServiceItemDefinitionArray.Length-1].IDServiceItem = idServiceItemDouble;
            sProposal.ServiceItemDefinitionArray[sProposal.ServiceItemDefinitionArray.Length-1].Note = "Came from Webstie";
        }

        var dsConferences = new DataSet();
        var reader = new System.Xml.XmlTextReader(Server.MapPath("\\") + "\\" + "App_Data\\Conferences.xml");

        dsConferences.ReadXml(reader);

        foreach (DataRow dr in dsConferences.Tables[0].Select("conferenceID='" + conferenceID + "'"))
        {
            DataRow[] da = dsConferences.Tables[1].Select("conference_id='" + dr[0] + "' and sidenight=" + sidenights);

            if (idServiceItemSingle != 0)
            {
                sProposal.ServiceItemDefinitionArray[0].ServiceItemDayArray = new SServiceItemDay[da.Length];
            }
            if (idServiceItemDouble != 0)
            {
                sProposal.ServiceItemDefinitionArray[sProposal.ServiceItemDefinitionArray.Length - 1].ServiceItemDayArray = new SServiceItemDay[da.Length];
            }

            for (int i = 0; i < da.Length; i++)
            {
                bool sideNights = Boolean.Parse(da[i][0].ToString());
                string date = da[i][1].ToString();

                if (idServiceItemSingle != 0)
                {
                    sProposal.ServiceItemDefinitionArray[0].ServiceItemDayArray[i] = new SServiceItemDay();
                    sProposal.ServiceItemDefinitionArray[0].ServiceItemDayArray[i].Date = DateTime.Parse(date);
                    if (!sideNights)
                    {
                        sProposal.ServiceItemDefinitionArray[0].ServiceItemDayArray[i].Quantity = int.Parse(pdfReader.AcroFields.GetField("SR" + DateTime.Parse(date).Day));
                        sProposal.ServiceItemDefinitionArray[0].ServiceItemDayArray[i].SellingPrice = 0M; //Decimal.Parse(pdfReader.AcroFields.GetField("txtSPrice"));
                    }
                    else
                    {
                        sProposal.ServiceItemDefinitionArray[0].ServiceItemDayArray[i].Quantity = int.Parse(pdfReader.AcroFields.GetField("txtGR" + DateTime.Parse(date).Day));
                        sProposal.ServiceItemDefinitionArray[0].ServiceItemDayArray[i].SellingPrice = 0M; //Decimal.Parse(pdfReader.AcroFields.GetField("txtRoomRate"));
                    }
                }
                if (idServiceItemDouble != 0)
                {
                    sProposal.ServiceItemDefinitionArray[sProposal.ServiceItemDefinitionArray.Length - 1].ServiceItemDayArray[i] = new SServiceItemDay();
                    sProposal.ServiceItemDefinitionArray[sProposal.ServiceItemDefinitionArray.Length - 1].ServiceItemDayArray[i].Date = DateTime.Parse(date);
                    if (!sideNights)
                    {
                        sProposal.ServiceItemDefinitionArray[sProposal.ServiceItemDefinitionArray.Length - 1].ServiceItemDayArray[i].Quantity = int.Parse(pdfReader.AcroFields.GetField("DR" + DateTime.Parse(date).Day));
                        sProposal.ServiceItemDefinitionArray[sProposal.ServiceItemDefinitionArray.Length - 1].ServiceItemDayArray[i].SellingPrice = 0M; //Decimal.Parse(pdfReader.AcroFields.GetField("txtDPrice"));
                    }
                    else
                    {
                        sProposal.ServiceItemDefinitionArray[sProposal.ServiceItemDefinitionArray.Length - 1].ServiceItemDayArray[i].Quantity = int.Parse(pdfReader.AcroFields.GetField("txtSR" + DateTime.Parse(date).Day));
                        sProposal.ServiceItemDefinitionArray[sProposal.ServiceItemDefinitionArray.Length - 1].ServiceItemDayArray[i].SellingPrice = 0M; //Decimal.Parse(pdfReader.AcroFields.GetField("txtSuiteRate"));
                    }
                }
            }
        }

        try
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

            ProposalSavingResult psr = hr.SaveProposalForGroup(sProposal, "9DF3B529-54B6-49AD-8B6E-46F7BA0F737D");

            using (StreamWriter sw = File.AppendText(Server.MapPath("Log.txt")))
            {
                sw.WriteLine(psr.BookingNumber + ", " + psr.IDProposal + ", " + psr.ProposalSavingStatus + ", " + psr.ErrorMessage + ", " + System.DateTime.Now);
                //sw.WriteLine("BookingNumber: " + psr.BookingNumber + ", IDProposal: " + psr.IDProposal + ", SavingStatus:" + psr.ProposalSavingStatus + ", ErrorMessage: " + psr.ErrorMessage + ", TimeStamp: " + System.DateTime.Now);
            }

        }
        catch (Exception ex)
        {
            using (StreamWriter sw = File.AppendText(Server.MapPath("Log.txt")))
            {
                sw.WriteLine(", " + ", " + ", " + ex.InnerException.Message + " , " + System.DateTime.Now);
            }
        }
    }



    private void SaveRestrictedCopy(string path, string conferenceID)
    {
        try
        {
            string pdfTemplate = path;

            string newFile = "View_" + path.Substring(path.LastIndexOf("\\") + 1);

            PdfReader pdfReader = new PdfReader(pdfTemplate);
            PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(Server.MapPath("agreements\\" + conferenceID) + "\\" + newFile, FileMode.Create));

            AcroFields pdfFormFields = pdfStamper.AcroFields;

            string cardNumber = pdfReader.AcroFields.GetField("txtCardNumber");

            cardNumber = StringHelpers.Decrypt(cardNumber);
            
            string hiddenCreditNumber = Helpers.AddX(cardNumber.Length - 5) + "-" + cardNumber.Substring(cardNumber.Length - 4);

            pdfFormFields.SetField("txtCardNumber", hiddenCreditNumber);
            pdfFormFields.SetField("txtCVV", "XXX");
            pdfFormFields.SetField("txtExpMoYr", "XX/XX");
            pdfFormFields.SetField("txtExpDate", "XX/XX");

            pdfStamper.Close();
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "002", "alert('" + ex.Message + "')", true);
        }
    }

    private string MaximumNumberOfNightsSingle()
    {        
        int numberOfNightsSingle = 0;
        int j = 0;
                
        for(int i=0; i < divHFNights.Controls.Count; i++)
        {
            if (((HiddenField)divHFNights.Controls[i]).ID.Contains("hfd"))
            {
                continue;
            }

            if (numberOfNightsSingle < int.Parse(((HiddenField)divHFNights.Controls[i]).Value))
            {
                if (ddlDateFrom.Items[j + 1].Attributes["sidenight"].ToString() == "false")
                {
                    numberOfNightsSingle = int.Parse(((HiddenField)divHFNights.Controls[i]).Value);                    
                }
            }

            j++;
        }

        return numberOfNightsSingle.ToString();
    }

    private string MaximumNumberOfNightsSingleDouble()
    {
        ArrayList sumSingleDouble = new ArrayList();

        List<Control> inputDoubleNights = (from Control c in divHFNights.Controls
                                           where c.ID.Contains("hfd")
                                           //orderby int.Parse(c.ID.Substring(3))
                                           select c).ToList();

        List<Control> inputSingleNights = (from Control c in divHFNights.Controls
                                           where !c.ID.Contains("hfd")
                                           //orderby int.Parse(c.ID.Substring(2))
                                           select c).ToList();

        for (int i = 0; i < inputSingleNights.Count; i++)
        {
            if (int.Parse(((HiddenField)inputSingleNights[i]).ID.Substring(2)) == int.Parse(((HiddenField)inputDoubleNights[i]).ID.Substring(3)))
            {
                if (ddlDateFrom.Items[i + 1].Attributes["sidenight"].ToString() == "false")
                {
                    int numberOfNights = int.Parse(((HiddenField)inputSingleNights[i]).Value) + int.Parse(((HiddenField)inputDoubleNights[i]).Value);
                    sumSingleDouble.Add(numberOfNights);
                }
            }                     
        }

        sumSingleDouble.Sort();

        sumSingleDouble.Reverse();

        return sumSingleDouble[0].ToString();               
    }

    private List<int> MaximumNumberOfNightsSingleDoubleSeparate()
    {
        List<Control> inputDoubleNights = (from Control c in divHFNights.Controls
                                           where c.ID.Contains("hfd")
                                           //orderby int.Parse(c.ID.Substring(3))
                                           select c).ToList();

        List<Control> inputSingleNights = (from Control c in divHFNights.Controls
                                           where !c.ID.Contains("hfd")
                                           //orderby int.Parse(c.ID.Substring(2))
                                           select c).ToList();

        var sumSingleDoubleNights = new List<int>();
        var singleNights = new List<int>();
        var doubleNights = new List<int>();

        var singleDoublePair = new List<int>();

        for (int i = 0; i < inputSingleNights.Count; i++)
        {
            if (int.Parse(((HiddenField)inputSingleNights[i]).ID.Substring(2)) == int.Parse(((HiddenField)inputDoubleNights[i]).ID.Substring(3)))
            {
                if (ddlDateFrom.Items[i + 1].Attributes["sidenight"].ToString() == "false")
                {
                    int numberOfSingleDoubleNights = int.Parse(((HiddenField)inputSingleNights[i]).Value) + int.Parse(((HiddenField)inputDoubleNights[i]).Value);
                    int numberOfSingleNights = int.Parse(((HiddenField) inputSingleNights[i]).Value);
                    int numberOfDoubleNights = int.Parse(((HiddenField) inputDoubleNights[i]).Value);
                    
                    singleNights.Add(numberOfSingleNights);
                    doubleNights.Add(numberOfDoubleNights);
                    sumSingleDoubleNights.Add(numberOfSingleDoubleNights);
                }
            }
        }


        var index = sumSingleDoubleNights.IndexOf(sumSingleDoubleNights.Max());

        int singleMax1 = singleNights[index];
        int doubleMax1 = doubleNights[index];

        singleNights.RemoveAt(index);
        doubleNights.RemoveAt(index);
        sumSingleDoubleNights.RemoveAt(index);

        index = sumSingleDoubleNights.IndexOf(sumSingleDoubleNights.Max());

        int singleMax2 = singleNights[index];
        int doubleMax2 = doubleNights[index];
        
        //singleDoublePair.Add(singleMax1 + singleMax2);
        //singleDoublePair.Add(doubleMax1 + doubleMax2);
		
		singleDoublePair.Add(singleMax1);
        singleDoublePair.Add(doubleMax1);

        return singleDoublePair;
    }

    private string MaximumNumberOfNightsDouble()
    {
        int numberOfNightsDouble = 0;
        int j = 0;

        for (int i = 0; i < divHFNights.Controls.Count; i++)
        {
            if (((HiddenField)divHFNights.Controls[i]).ID.Contains("hfd"))
            {
                if (numberOfNightsDouble < int.Parse(((HiddenField)divHFNights.Controls[i]).Value))
                {
                    if (ddlDateFrom.Items[j + 1].Attributes["sidenight"].ToString() == "false")
                    {
                        numberOfNightsDouble = int.Parse(((HiddenField)divHFNights.Controls[i]).Value);                       
                    }
                }

                j++;
            }

            
        }

        return numberOfNightsDouble.ToString();
    }

    private void PopulatePDFForm(string conferenceID)
    {
        FillDDLHotelsAttributes(conferenceID);
        FillConferenceDatesAttirbutes(conferenceID);
		
		string conferenceName = String.Empty;

        if (conferenceID.Contains("Overflow"))
        {
            conferenceName = "2019 AAN-Overflow";
        }
        else
        {
            conferenceName = conferenceID.Substring(conferenceID.Length - 4) + " " + conferenceID.Substring(0, conferenceID.Length - 4);
        }

        ServicePointManager.Expect100Continue = true;
        ServicePointManager.DefaultConnectionLimit = 9999;
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

        //Get Merchant Terminal Info From ABTSolute
        ABTSoluteWebServices.ABTSoluteWebServices ser = new ABTSoluteWebServices.ABTSoluteWebServices();        
        var merchantInfo = ser.GetMerchantAccountInfoByConferenceName(conferenceName, "9DD873E8-3465-4426-B498-38EA9589090E");

        bool sidenights = false;
        string address = this.tbAddress.Text;
        string zip = this.tbZip.Text;
        string city = this.tbCity.Text;
        string fullName = this.tbFullName.Text;
        string phone = this.tbPhone.Text;
        string email = this.tbEmail.Text;
        string agencyName = this.tbAgencyName.Text;
        string hotelName1 = this.ddlHotels.SelectedItem.Text; //this.tbHotel.Text = "";
        string hoteVlaue1 = this.ddlHotels.SelectedItem.Value;
        decimal sprice = this.ddlHotels.SelectedItem.Attributes["sprice"] == null ? 0 : Decimal.Parse(this.ddlHotels.SelectedItem.Attributes["sprice"].ToString());
        decimal dprice = this.ddlHotels.SelectedItem.Attributes["dprice"] == null ? 0 : Decimal.Parse(this.ddlHotels.SelectedItem.Attributes["dprice"].ToString());
        string hotelName2 = this.ddlHotels1.SelectedItem.Text; //this.tbHotel.Text = "";
        string hoteVlaue2 = this.ddlHotels1.SelectedItem.Value;
        string hotelName3 = this.ddlHotels2.SelectedItem.Text; //this.tbHotel.Text = "";
        string hoteVlaue3 = this.ddlHotels2.SelectedItem.Value;
        bool editNights = Boolean.Parse(hfEditNights.Value);
		string merchantID = merchantInfo != null && merchantInfo.Rows.Count > 0 ? merchantInfo.Rows[0]["merchantID"].ToString() : "0";

        DateTime Date = System.DateTime.Now;
        string documentID_HA = "G" + Date.ToString("yyMMdd") + System.DateTime.Now.ToString("hh") + System.DateTime.Now.ToString("mm") + System.DateTime.Now.Ticks.ToString().Substring(System.DateTime.Now.Ticks.ToString().Length - 4, 4);

        //string numberOfRoomsSingle = editNights ? MaximumNumberOfNightsSingle() : Request.Form["ddlRooms"];
        //string numberOfRoomsDouble = editNights ? MaximumNumberOfNightsDouble() : Request.Form["ddlRooms"];


        //string numberOfRooms = Request.Form["ddlRooms"]; //this.ddlRooms.Items[this.ddlRooms.SelectedIndex].Text;
        DateTime dateFrom = DateTime.Parse(this.ddlDateFrom.Items[this.ddlDateFrom.SelectedIndex].Text);
        DateTime dateTo = DateTime.Parse(this.ddlDateTo.Items[this.ddlDateTo.SelectedIndex].Text);
        string country = this.ddlCountry.Items[this.ddlCountry.SelectedIndex].Value;
        string countryName = this.ddlCountry.Items[this.ddlCountry.SelectedIndex].Text;
        int totalNumberOfRoomsSingle = 0;
        int totalNumberOfRoomsDouble = 0;
        int totalSidenightsNumberOfRoomsSingle = 0;
        int totalSidenightsNumberOfRoomsDouble = 0;
        int roomType = int.Parse(this.ddlRoomType.Items[this.ddlRoomType.SelectedIndex].Value);
        //decimal calcPrice = roomType == 1 ? sprice : dprice;

        string numberOfRoomsSingle = "0";
        string numberOfRoomsDouble = "0";
		
		//numberOfRoomsSingle = editNights ? MaximumNumberOfNightsSingleDouble() : roomType == 2 ? "0" : Request.Form["ddlRooms"];
        //numberOfRoomsDouble = editNights ? MaximumNumberOfNightsSingleDouble() : roomType == 1 ? "0" : Request.Form["ddlRooms"];

        if (!conferenceID.Contains("AASLD"))
        {
            numberOfRoomsSingle = editNights ? MaximumNumberOfNightsSingleDouble() : roomType == 2 ? "0" : Request.Form["ddlRooms"];
            numberOfRoomsDouble = editNights ? MaximumNumberOfNightsSingleDouble() : roomType == 1 ? "0" : Request.Form["ddlRooms"];
        }
        else
        {
            numberOfRoomsSingle = editNights ? MaximumNumberOfNightsSingleDoubleSeparate()[0].ToString() : roomType == 2 ? "0" : Request.Form["ddlRooms"];
            numberOfRoomsDouble = editNights ? MaximumNumberOfNightsSingleDoubleSeparate()[1].ToString() : roomType == 1 ? "0" : Request.Form["ddlRooms"];
        }

        //SessionIDManager Manager = new SessionIDManager();
        //string sessionID = Manager.CreateSessionID(Context);
        //int hashCode = Manager.GetHashCode();


        //long hashCode = System.DateTime.Now.Ticks;

        string pdfTemplate = string.Empty;
		
		pdfTemplate = Server.MapPath("docs") + "\\HousingAgreement_" + conferenceID + ".pdf";

        // if (!conferenceID.Contains("AASLD"))
        // {
            // pdfTemplate = Server.MapPath("docs") + "\\HousingAgreement_" + conferenceID + ".pdf";
        // }
        // else
        // {
            // //(int.Parse(numberOfRoomsSingle) + int.Parse(numberOfRoomsDouble)
            // string maximumNumberOfRooms = editNights? MaximumNumberOfNightsSingleDouble() : (int.Parse(numberOfRoomsSingle) + int.Parse(numberOfRoomsDouble)).ToString();

            // if (int.Parse(maximumNumberOfRooms) < 25)
            // {
                // pdfTemplate = Server.MapPath("docs") + "\\HousingAgreement_" + conferenceID + ".pdf";
            // }
            // else
            // {
                // pdfTemplate = Server.MapPath("docs") + "\\HousingAgreement_" + conferenceID + "_Ver2.pdf";
            // }
        // }

        string newFile = documentID_HA + "_HousingAgreement_" + conferenceID + ".pdf";
        //string newFile = hashCode + "_HousingAgreement_" + conferenceID + ".pdf";

        PdfReader pdfReader = new PdfReader(pdfTemplate);
        PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(Server.MapPath("tempAgreements") + "\\" + newFile, FileMode.Create), '\0', true);

        //PDF Encryption//
        //pdfStamper.SetEncryption(true, null, "Dvladh09091976", PdfWriter.ALLOW_FILL_IN | PdfWriter.ALLOW_PRINTING);


        AcroFields pdfFormFields = pdfStamper.AcroFields;

        pdfFormFields.SetField("txtSponsoringGroupName", fullName);
        //pdfFormFields.SetFieldProperty("txtSponsoringGroupName", fullName, PdfFormField.FF_READ_ONLY, null);
        //pdfFormFields.SetFieldProperty("txtSponsoringGroupName", fullName, PdfFormField.FLAGS_READONLY, null);       
        pdfFormFields.SetField("txtAgency", agencyName);
        pdfFormFields.SetField("txtPhone", phone);
        pdfFormFields.SetField("txtFax", phone);
        pdfFormFields.SetField("txtEMail", email);
        pdfFormFields.SetField("txtPostalCode", zip);
        pdfFormFields.SetField("txtCity", city);
        pdfFormFields.SetField("txtAddress", address);
        pdfFormFields.SetField("txtDocumentID", documentID_HA);

        pdfFormFields.SetListOption("ddlHotel1", new string[] { hoteVlaue1 }, new string[] { hotelName1 });
        pdfFormFields.SetListOption("ddlHotel2", new string[] { hoteVlaue2 }, new string[] { hotelName2 });
        pdfFormFields.SetListOption("ddlHotel3", new string[] { hoteVlaue3 }, new string[] { hotelName3 });
        pdfFormFields.SetListOption("ddlCountry", new string[] { country }, new string[] { countryName });

        pdfFormFields.SetListSelection("ddlHotel1", new string[] { hotelName1 });
        pdfFormFields.SetListSelection("ddlHotel2", new string[] { hotelName2 });
        pdfFormFields.SetListSelection("ddlHotel3", new string[] { hotelName3 });
        pdfFormFields.SetListSelection("ddlCountry", new string[] { country });


        pdfFormFields.SetField("ddlCountry", country);
        pdfFormFields.SetField("ddlHotel1", hotelName1);
        pdfFormFields.SetField("ddlHotel2", hotelName2);
        pdfFormFields.SetField("ddlHotel3", hotelName3);
		
		pdfFormFields.SetField("txtMerchant", merchantID);

        //Check if pdf form has both pick and side nights togather
        string mixed = pdfFormFields.GetField("txtMixed");

        if (mixed != null)
        {
            #region MixedHA

            int numOfRoomsSingle = 0;
            int numOfRoomsDouble = 0;

            pdfFormFields.SetField("txtUserKey", documentID_HA);
            //pdfFormFields.SetField("txtUserKey", hashCode.ToString());

            foreach (ListItem li in ddlDateFrom.Items)
            {
                if (li.Value != "-1")
                {
                    if ((DateTime.Parse(li.Value) >= dateFrom) && (DateTime.Parse(li.Value) < dateTo))
                    {
                        if (!editNights)
                        {
                            if (roomType == 3)
                            {
                                pdfFormFields.SetField("SR" + DateTime.Parse(li.Value).Day, numberOfRoomsSingle);
                                pdfFormFields.SetField("DR" + DateTime.Parse(li.Value).Day, numberOfRoomsDouble);
                                numOfRoomsSingle += int.Parse(numberOfRoomsSingle);
                                numOfRoomsDouble += int.Parse(numberOfRoomsDouble);
                            }

                            if (roomType == 1)
                            {
                                pdfFormFields.SetField("SR" + DateTime.Parse(li.Value).Day, numberOfRoomsSingle);
                                numOfRoomsSingle += int.Parse(numberOfRoomsSingle);
                            }

                            if (roomType == 2)
                            {
                                pdfFormFields.SetField("DR" + DateTime.Parse(li.Value).Day, numberOfRoomsDouble);
                                numOfRoomsDouble += int.Parse(numberOfRoomsDouble);
                            }
                        }
                        else
                        {
                            if (roomType == 3)
                            {
                                pdfFormFields.SetField("SR" + DateTime.Parse(li.Value).Day, ((HiddenField)divHFNights.FindControl("hf" + DateTime.Parse(li.Value).Day.ToString())).Value);
                                pdfFormFields.SetField("DR" + DateTime.Parse(li.Value).Day, ((HiddenField)divHFNights.FindControl("hfd" + DateTime.Parse(li.Value).Day.ToString())).Value);
                                numOfRoomsSingle += int.Parse(((HiddenField)divHFNights.FindControl("hf" + DateTime.Parse(li.Value).Day.ToString())).Value);
                                numOfRoomsDouble += int.Parse(((HiddenField)divHFNights.FindControl("hfd" + DateTime.Parse(li.Value).Day.ToString())).Value);
                            }

                            if (roomType == 1)
                            {
                                pdfFormFields.SetField("SR" + DateTime.Parse(li.Value).Day, ((HiddenField)divHFNights.FindControl("hf" + DateTime.Parse(li.Value).Day.ToString())).Value);
                                numOfRoomsSingle += int.Parse(((HiddenField)divHFNights.FindControl("hf" + DateTime.Parse(li.Value).Day.ToString())).Value);
                            }

                            if (roomType == 2)
                            {
                                pdfFormFields.SetField("DR" + DateTime.Parse(li.Value).Day, ((HiddenField)divHFNights.FindControl("hfd" + DateTime.Parse(li.Value).Day.ToString())).Value);
                                numOfRoomsDouble += int.Parse(((HiddenField)divHFNights.FindControl("hfd" + DateTime.Parse(li.Value).Day.ToString())).Value);
                            }
                        }
                    }
                }
            }

            pdfFormFields.SetField("SRTotal", numOfRoomsSingle.ToString());
            pdfFormFields.SetField("DRTotal", numOfRoomsDouble.ToString());

            pdfFormFields.SetField("txtNumSR", (int.Parse(numberOfRoomsSingle) + int.Parse(numberOfRoomsDouble)).ToString());

            Decimal _singleDeposit = Decimal.Parse(pdfFormFields.GetField("txtSingleDeposit"));
            Decimal _doubleDeposit = Decimal.Parse(pdfFormFields.GetField("txtDoubleDeposit"));

            pdfFormFields.SetField("txtTotalS", ((int.Parse(numberOfRoomsSingle) + int.Parse(numberOfRoomsDouble)) * _singleDeposit).ToString("F"));

            pdfFormFields.SetField("txtTotalSD", (Decimal.Parse(pdfFormFields.GetField("txtTotalS")) + Decimal.Parse(pdfFormFields.GetField("txtTotalD"))).ToString("F"));

            pdfFormFields.SetField("txtTotalRN", totalNumberOfRoomsSingle.ToString());
            pdfFormFields.SetField("txtTotalSN", totalNumberOfRoomsDouble.ToString());

            pdfFormFields.SetField("txtSPrice", sprice.ToString("F"));

            pdfFormFields.SetField("txtTotalRNPrice", (totalNumberOfRoomsSingle * sprice).ToString("F"));

            pdfFormFields.SetField("txtTotalDepositePaid", (Decimal.Parse(pdfFormFields.GetField("txtTotalS")) + Decimal.Parse(pdfFormFields.GetField("txtTotalD"))).ToString("F"));

            pdfFormFields.SetField("txtTotalPrice", (((totalNumberOfRoomsSingle * sprice) + (totalNumberOfRoomsDouble * dprice)) - (Decimal.Parse(pdfFormFields.GetField("txtTotalS")) + Decimal.Parse(pdfFormFields.GetField("txtTotalD")))).ToString("F"));

            pdfStamper.FormFlattening = false;

            pdfStamper.Close();

            LoadPDFForm(newFile);

            return;

            #endregion
        }

        foreach (ListItem li in ddlDateFrom.Items)
        {
            if (li.Value != "-1" && li.Attributes["sidenight"].ToString() == "false")
            {
                if ((DateTime.Parse(li.Value) >= dateFrom) && (DateTime.Parse(li.Value) < dateTo))
                {
                    if (!editNights)
                    {
                        if (roomType == 3)
                        {
                            pdfFormFields.SetField("SR" + DateTime.Parse(li.Value).Day, numberOfRoomsSingle); totalNumberOfRoomsSingle += int.Parse(numberOfRoomsSingle);
                            pdfFormFields.SetField("DR" + DateTime.Parse(li.Value).Day, numberOfRoomsDouble); totalNumberOfRoomsDouble += int.Parse(numberOfRoomsDouble);
                        }

                        if (roomType == 1)
                        {
                            pdfFormFields.SetField("SR" + DateTime.Parse(li.Value).Day, numberOfRoomsSingle); totalNumberOfRoomsSingle += int.Parse(numberOfRoomsSingle);
                        }

                        if (roomType == 2)
                        {
                            pdfFormFields.SetField("DR" + DateTime.Parse(li.Value).Day, numberOfRoomsDouble); totalNumberOfRoomsDouble += int.Parse(numberOfRoomsDouble);
                        }

                    }
                    else
                    {
                        if (roomType == 3)
                        {
                            pdfFormFields.SetField("SR" + DateTime.Parse(li.Value).Day, ((HiddenField)divHFNights.FindControl("hf" + DateTime.Parse(li.Value).Day.ToString())).Value);
                            totalNumberOfRoomsSingle += int.Parse(((HiddenField)divHFNights.FindControl("hf" + DateTime.Parse(li.Value).Day.ToString())).Value);

                            pdfFormFields.SetField("DR" + DateTime.Parse(li.Value).Day, ((HiddenField)divHFNights.FindControl("hfd" + DateTime.Parse(li.Value).Day.ToString())).Value);
                            totalNumberOfRoomsDouble += int.Parse(((HiddenField)divHFNights.FindControl("hfd" + DateTime.Parse(li.Value).Day.ToString())).Value);
                        }

                        if (roomType == 1)
                        {
                            pdfFormFields.SetField("SR" + DateTime.Parse(li.Value).Day, ((HiddenField)divHFNights.FindControl("hf" + DateTime.Parse(li.Value).Day.ToString())).Value);
                            totalNumberOfRoomsSingle += int.Parse(((HiddenField)divHFNights.FindControl("hf" + DateTime.Parse(li.Value).Day.ToString())).Value);
                        }

                        if (roomType == 2)
                        {
                            pdfFormFields.SetField("DR" + DateTime.Parse(li.Value).Day, ((HiddenField)divHFNights.FindControl("hfd" + DateTime.Parse(li.Value).Day.ToString())).Value);
                            totalNumberOfRoomsDouble += int.Parse(((HiddenField)divHFNights.FindControl("hfd" + DateTime.Parse(li.Value).Day.ToString())).Value);
                        }
                    }
                }
            }

            if (li.Value != "-1" && li.Attributes["sidenight"].ToString() == "true")
            {
                if ((DateTime.Parse(li.Value) >= dateFrom) && (DateTime.Parse(li.Value) < dateTo))
                {
                    sidenights = true;
                }
            }
        }

        pdfFormFields.SetField("SRTotal", totalNumberOfRoomsSingle.ToString());
        pdfFormFields.SetField("DRTotal", totalNumberOfRoomsDouble.ToString());

        Decimal singleDeposit = Decimal.Parse(pdfFormFields.GetField("txtSingleDeposit"));
        Decimal doubleDeposit = Decimal.Parse(pdfFormFields.GetField("txtDoubleDeposit"));

        if (!editNights)
        {
            //pdfFormFields.SetField("txtNumSR", (int.Parse(numberOfRoomsSingle) + int.Parse(numberOfRoomsDouble)).ToString());
            //pdfFormFields.SetField("txtTotalS", ((int.Parse(numberOfRoomsSingle) + int.Parse(numberOfRoomsDouble)) * singleDeposit).ToString("F"));
            if (singleDeposit == 0 && doubleDeposit == 0)
            {
                pdfFormFields.SetField("txtSingleDeposit", sprice.ToString("F"));
                pdfFormFields.SetField("txtDoubleDeposit", dprice.ToString("F"));

                singleDeposit = sprice;
                doubleDeposit = dprice;

                pdfFormFields.SetField("txtNumSR", (int.Parse(numberOfRoomsSingle)).ToString());
                pdfFormFields.SetField("txtNumDR", (int.Parse(numberOfRoomsDouble)).ToString());
                
                pdfFormFields.SetField("txtTotalS", ((int.Parse(numberOfRoomsSingle) * singleDeposit) * 2).ToString("F"));
                pdfFormFields.SetField("txtTotalD", ((int.Parse(numberOfRoomsDouble) * doubleDeposit) * 2).ToString("F"));  
				
            }
            else
            {
                pdfFormFields.SetField("txtNumSR", (int.Parse(numberOfRoomsSingle) + int.Parse(numberOfRoomsDouble)).ToString());
                pdfFormFields.SetField("txtTotalS", ((int.Parse(numberOfRoomsSingle) + int.Parse(numberOfRoomsDouble)) * singleDeposit).ToString("F"));  
            }
        }
        else
        {
            //pdfFormFields.SetField("txtNumSR", (int.Parse(numberOfRoomsSingle)).ToString());
            //pdfFormFields.SetField("txtTotalS", ((int.Parse(numberOfRoomsSingle)) * singleDeposit).ToString("F"));
            if (singleDeposit == 0 && doubleDeposit == 0)
            {
                pdfFormFields.SetField("txtSingleDeposit", sprice.ToString("F"));
                pdfFormFields.SetField("txtDoubleDeposit", dprice.ToString("F"));

                singleDeposit = sprice;
                doubleDeposit = dprice;

                pdfFormFields.SetField("txtNumSR", (int.Parse(numberOfRoomsSingle)).ToString());
                pdfFormFields.SetField("txtNumDR", (int.Parse(numberOfRoomsDouble)).ToString());

                pdfFormFields.SetField("txtTotalS", ((int.Parse(numberOfRoomsSingle) * singleDeposit) * 2).ToString("F"));
                pdfFormFields.SetField("txtTotalD", ((int.Parse(numberOfRoomsDouble) * doubleDeposit) * 2).ToString("F"));  
				 
            }
            else
            {
                pdfFormFields.SetField("txtNumSR", (int.Parse(numberOfRoomsSingle)).ToString());
                pdfFormFields.SetField("txtTotalS", ((int.Parse(numberOfRoomsSingle))*singleDeposit).ToString("F"));
            }
        }       
                
        //pdfFormFields.SetField("txtTotalSD", (Decimal.Parse(pdfFormFields.GetField("txtTotalS")) + Decimal.Parse(pdfFormFields.GetField("txtTotalD"))).ToString("F"));
         if (singleDeposit != 0 && doubleDeposit != 0)
        {
            pdfFormFields.SetField("txtTotalSD", (Decimal.Parse(pdfFormFields.GetField("txtTotalS")) + Decimal.Parse(pdfFormFields.GetField("txtTotalD"))).ToString("F"));
        }

        bool registration = pdfFormFields.GetField("txtRegistration") != null;
        Decimal registrationPrice = 0;

        if (pdfFormFields.GetField("txtConfirmationDate") == null || DateTime.Parse(pdfFormFields.GetField("txtConfirmationDate").ToString()) >= System.DateTime.Now)
        {
            //-----------------------------------------------------------------------
            //-------------------------Estimated Balance before confirmation date-----------------------------

            pdfFormFields.SetField("txtTotalRN", totalNumberOfRoomsSingle.ToString());
            pdfFormFields.SetField("txtTotalSN", totalNumberOfRoomsDouble.ToString());

            if ((sprice != 0) || (dprice != 0))
            {
                pdfFormFields.SetField("txtSPrice", sprice.ToString("F"));
                pdfFormFields.SetField("txtDPrice", dprice.ToString("F"));

                pdfFormFields.SetField("txtTotalRNPrice", (totalNumberOfRoomsSingle*sprice).ToString("F"));
                pdfFormFields.SetField("txtTotalSNPrice", (totalNumberOfRoomsDouble*dprice).ToString("F"));

                pdfFormFields.SetField("txtTotalDepositePaid", (Decimal.Parse(pdfFormFields.GetField("txtTotalS")) + Decimal.Parse(pdfFormFields.GetField("txtTotalD"))).ToString("F"));

                pdfFormFields.SetField("txtTotalPrice", (((totalNumberOfRoomsSingle*sprice) + (totalNumberOfRoomsDouble*dprice)) - (Decimal.Parse(pdfFormFields.GetField("txtTotalS")) + Decimal.Parse(pdfFormFields.GetField("txtTotalD")))).ToString("F"));

                pdfFormFields.SetField("txtTotalPrice", (Decimal.Parse(pdfFormFields.GetField("txtTotalPrice")) + registrationPrice).ToString("F"));
            }
            else
            {
                Decimal totalRoomPriceSingle = 0;
                Decimal totalRoomPriceDouble = 0;

                foreach (ListItem li in ddlDateFrom.Items)
                {
                    if (li.Value != "-1" && li.Attributes["sidenight"].ToString() == "false")
                    {
                        if ((DateTime.Parse(li.Value) >= dateFrom) && (DateTime.Parse(li.Value) < dateTo))
                        {
                            if (!editNights)
                            {
                                totalRoomPriceSingle += int.Parse(numberOfRoomsSingle) * Decimal.Parse(ddlHotels.SelectedItem.Attributes["[" + li.Value.Replace("/", "-") + "]"].Split('|')[0].ToString());
                                totalRoomPriceDouble += int.Parse(numberOfRoomsDouble) * Decimal.Parse(ddlHotels.SelectedItem.Attributes["[" + li.Value.Replace("/", "-") + "]"].Split('|')[1].ToString());
                            }
                            else
                            {
                                totalRoomPriceSingle += int.Parse(((HiddenField)divHFNights.FindControl("hf" + DateTime.Parse(li.Value).Day.ToString())).Value) * Decimal.Parse(ddlHotels.SelectedItem.Attributes["[" + li.Value.Replace("/", "-") + "]"].Split('|')[0].ToString());
                                totalRoomPriceDouble += int.Parse(((HiddenField)divHFNights.FindControl("hfd" + DateTime.Parse(li.Value).Day.ToString())).Value) * Decimal.Parse(ddlHotels.SelectedItem.Attributes["[" + li.Value.Replace("/", "-") + "]"].Split('|')[1].ToString());
                            }
                        }
                    }
                }

                pdfFormFields.SetField("txtTotalRNPrice", (totalRoomPriceSingle).ToString("F"));
                pdfFormFields.SetField("txtTotalSNPrice", (totalRoomPriceDouble).ToString("F"));

                pdfFormFields.SetField("txtTotalDepositePaid", (Decimal.Parse(pdfFormFields.GetField("txtTotalS")) + Decimal.Parse(pdfFormFields.GetField("txtTotalD"))).ToString("F"));
                pdfFormFields.SetField("txtTotalPrice", ((totalRoomPriceSingle) + (totalRoomPriceDouble) - (Decimal.Parse(pdfFormFields.GetField("txtTotalS")) + Decimal.Parse(pdfFormFields.GetField("txtTotalD")))).ToString("F"));                
				
				//Novo polje za placanje koje prikazuje ukupan iznos bez umanjenja deposita
                pdfFormFields.SetField("txtFullPaymentTotalPrice", (totalRoomPriceSingle + totalRoomPriceDouble).ToString("F"));
            }

            //------------------------------------------------------------------------
        }
        else
        {
            //-----------------------------------------------------------------------
            //-------------------------Estimated Balance after confirmation date-----------------------------
            
            pdfFormFields.SetField("txtTotalRN1", totalNumberOfRoomsSingle.ToString());
            pdfFormFields.SetField("txtTotalSN1", totalNumberOfRoomsDouble.ToString());

            pdfFormFields.SetField("txtSPrice1", sprice.ToString("F"));
            pdfFormFields.SetField("txtDPrice1", dprice.ToString("F"));

            pdfFormFields.SetField("txtTotalRNPrice1", ((totalNumberOfRoomsSingle * sprice) / 2).ToString("F"));
            pdfFormFields.SetField("txtTotalSNPrice1", ((totalNumberOfRoomsDouble * dprice) / 2).ToString("F"));

            pdfFormFields.SetField("txtTotalPrice1", (((totalNumberOfRoomsSingle * sprice) / 2) + ((totalNumberOfRoomsDouble * dprice) / 2)).ToString("F"));

            pdfFormFields.SetField("txtTotalRN", totalNumberOfRoomsSingle.ToString());
            pdfFormFields.SetField("txtTotalSN", totalNumberOfRoomsDouble.ToString());

            pdfFormFields.SetField("txtSPrice", sprice.ToString("F"));
            pdfFormFields.SetField("txtDPrice", dprice.ToString("F"));

            pdfFormFields.SetField("txtTotalRNPrice", (totalNumberOfRoomsSingle * sprice).ToString("F"));
            pdfFormFields.SetField("txtTotalSNPrice", (totalNumberOfRoomsDouble * dprice).ToString("F"));

            pdfFormFields.SetField("txtTotalDepositePaid", (((totalNumberOfRoomsSingle * sprice) / 2) + ((totalNumberOfRoomsDouble * dprice) / 2)).ToString("F"));

            pdfFormFields.SetField("txtTotalPrice", (((totalNumberOfRoomsSingle * sprice) + (totalNumberOfRoomsDouble * dprice)) - (Decimal.Parse(pdfFormFields.GetField("txtTotalDepositePaid")) + Decimal.Parse(pdfFormFields.GetField("txtTotalD")))).ToString("F"));

            pdfFormFields.SetField("txtTotalPrice", (Decimal.Parse(pdfFormFields.GetField("txtTotalPrice")) + registrationPrice).ToString("F"));
            
            //------------------------------------------------------------------------
        }
       

        if (registration)
        {
            //if (!editNights)
            //{
            //    registrationPrice = Decimal.Parse(pdfFormFields.GetField("txtRPrice"))*(int.Parse(numberOfRoomsSingle) + int.Parse(numberOfRoomsDouble));
            //    pdfFormFields.SetField("txtTotalAttendees", (int.Parse(numberOfRoomsSingle) + int.Parse(numberOfRoomsDouble)).ToString());
            //}
            //else
            //{
            //    string numberOfRoomsRegSingle = MaximumNumberOfNightsSingle();
            //    string numberOfRoomsRegDouble = MaximumNumberOfNightsDouble();

            //    registrationPrice = Decimal.Parse(pdfFormFields.GetField("txtRPrice")) * (int.Parse(numberOfRoomsRegSingle) + int.Parse(numberOfRoomsRegDouble));
            //    pdfFormFields.SetField("txtTotalAttendees", (int.Parse(numberOfRoomsRegSingle) + int.Parse(numberOfRoomsRegDouble)).ToString());
            //}
           
            //pdfFormFields.SetField("txtRegistrationPrice", registrationPrice.ToString("F"));
        }

       //Novo polje za placanje koje prikazuje ukupan iznos bez umanjenja deposita
	   if (sprice != 0 && dprice != 0)
			pdfFormFields.SetField("txtFullPaymentTotalPrice", ((totalNumberOfRoomsSingle * sprice) + (totalNumberOfRoomsDouble * dprice)).ToString("F"));
        
        pdfFormFields.SetField("txtUserKey", documentID_HA);
        //pdfFormFields.SetField("txtUserKey", hashCode.ToString());

        pdfStamper.FormFlattening = false;

        pdfStamper.Close();

        LoadPDFForm(newFile);

        if (sidenights)
        {
            DateTime DateSN = System.DateTime.Now;
            string documentID_SN = "G" + Date.ToString("yyMMdd") + System.DateTime.Now.ToString("hh") + System.DateTime.Now.ToString("mm") + System.DateTime.Now.Ticks.ToString().Substring(System.DateTime.Now.Ticks.ToString().Length - 4, 4);

            string pdfSidenightsTemplate = Server.MapPath("docs") + "\\Sidenights_" + conferenceID + ".pdf";

            string sidenightsNewFile = documentID_SN + "_Sidenights_" + conferenceID + ".pdf";

           
            PdfReader pdfSidenightsReader = new PdfReader(pdfSidenightsTemplate);
            PdfStamper pdfSidenightsStamper = new PdfStamper(pdfSidenightsReader, new FileStream(Server.MapPath("tempAgreements") + "\\" + sidenightsNewFile, FileMode.Create), '\0', true);

            //PDF Encryption//
            //pdfSidenightsStamper.SetEncryption(true, null, "Dvladh09091976", PdfWriter.ALLOW_FILL_IN | PdfWriter.ALLOW_PRINTING);

            AcroFields pdfSidenightsFormFields = pdfSidenightsStamper.AcroFields;

            pdfSidenightsFormFields.SetField("txtGroupName", fullName);
            pdfSidenightsFormFields.SetField("txtAgency", agencyName);
            pdfSidenightsFormFields.SetField("txtEMail", email);
            pdfSidenightsFormFields.SetField("txtHotelBooked", hotelName1);
            pdfSidenightsFormFields.SetField("txtDocumentID", documentID_SN);
			pdfSidenightsFormFields.SetField("txtMerchant", merchantID);

            foreach (ListItem li in ddlDateFrom.Items)
            {
                if (li.Value != "-1" && li.Attributes["sidenight"].ToString() == "true")
                {
                    if ((DateTime.Parse(li.Value) >= dateFrom) && (DateTime.Parse(li.Value) < dateTo))
                    {
                        if (!editNights)
                        {
                            if (roomType == 3)
                            {
                                pdfSidenightsFormFields.SetField("txtGR" + DateTime.Parse(li.Value).Day, numberOfRoomsSingle); totalSidenightsNumberOfRoomsSingle += int.Parse(numberOfRoomsSingle);
                                pdfSidenightsFormFields.SetField("txtSR" + DateTime.Parse(li.Value).Day, numberOfRoomsDouble); totalSidenightsNumberOfRoomsDouble += int.Parse(numberOfRoomsDouble);
                            }

                            if (roomType == 1)
                            {
                                pdfSidenightsFormFields.SetField("txtGR" + DateTime.Parse(li.Value).Day, numberOfRoomsSingle); totalSidenightsNumberOfRoomsSingle += int.Parse(numberOfRoomsSingle);
                            }

                            if (roomType == 2)
                            {
                                pdfSidenightsFormFields.SetField("txtSR" + DateTime.Parse(li.Value).Day, numberOfRoomsDouble); totalSidenightsNumberOfRoomsDouble += int.Parse(numberOfRoomsDouble);
                            }
                        }
                        else
                        {
                            if (roomType == 3)
                            {
                                pdfSidenightsFormFields.SetField("txtGR" + DateTime.Parse(li.Value).Day, ((HiddenField)divHFNights.FindControl("hf" + DateTime.Parse(li.Value).Day.ToString())).Value);
                                totalSidenightsNumberOfRoomsSingle += int.Parse(((HiddenField)divHFNights.FindControl("hf" + DateTime.Parse(li.Value).Day.ToString())).Value);

                                pdfSidenightsFormFields.SetField("txtSR" + DateTime.Parse(li.Value).Day, ((HiddenField)divHFNights.FindControl("hfd" + DateTime.Parse(li.Value).Day.ToString())).Value);
                                totalSidenightsNumberOfRoomsDouble += int.Parse(((HiddenField)divHFNights.FindControl("hfd" + DateTime.Parse(li.Value).Day.ToString())).Value);
                            }

                            if (roomType == 1)
                            {
                                pdfSidenightsFormFields.SetField("txtGR" + DateTime.Parse(li.Value).Day, ((HiddenField)divHFNights.FindControl("hf" + DateTime.Parse(li.Value).Day.ToString())).Value);
                                totalSidenightsNumberOfRoomsSingle += int.Parse(((HiddenField)divHFNights.FindControl("hf" + DateTime.Parse(li.Value).Day.ToString())).Value);
                            }

                            if (roomType == 2)
                            {
                                pdfSidenightsFormFields.SetField("txtSR" + DateTime.Parse(li.Value).Day, ((HiddenField)divHFNights.FindControl("hfd" + DateTime.Parse(li.Value).Day.ToString())).Value);
                                totalSidenightsNumberOfRoomsDouble += int.Parse(((HiddenField)divHFNights.FindControl("hfd" + DateTime.Parse(li.Value).Day.ToString())).Value);
                            }
                        }
                    }
                }
            }

            pdfSidenightsFormFields.SetField("txtGRTotal", totalSidenightsNumberOfRoomsSingle.ToString());
            pdfSidenightsFormFields.SetField("txtSRTotal", totalSidenightsNumberOfRoomsDouble.ToString());

            if ((sprice != 0) || (dprice != 0))
            {
                pdfSidenightsFormFields.SetField("txtRoomRate", sprice.ToString("F"));
                pdfSidenightsFormFields.SetField("txtSuiteRate", dprice.ToString("F"));

                pdfSidenightsFormFields.SetField("txtRoomNights", totalSidenightsNumberOfRoomsSingle.ToString());
                pdfSidenightsFormFields.SetField("txtSuiteNights", totalSidenightsNumberOfRoomsDouble.ToString());

                pdfSidenightsFormFields.SetField("txtRoomTotal", (totalSidenightsNumberOfRoomsSingle*sprice).ToString("F"));
                pdfSidenightsFormFields.SetField("txtSuiteTotal", (totalSidenightsNumberOfRoomsDouble*dprice).ToString("F"));

                pdfSidenightsFormFields.SetField("txtTotalAmount", ((totalSidenightsNumberOfRoomsSingle*sprice) + (totalSidenightsNumberOfRoomsDouble*dprice)).ToString("F"));
            }
            else
            {
                Decimal totalRoomPriceSidenightsSingle = 0;
                Decimal totalRoomPriceSidenightsDouble = 0;

                foreach (ListItem li in ddlDateFrom.Items)
                {
                    if (li.Value != "-1" && li.Attributes["sidenight"].ToString() == "true")
                    {
                        if ((DateTime.Parse(li.Value) >= dateFrom) && (DateTime.Parse(li.Value) < dateTo))
                        {
                            if (!editNights)
                            {
                                totalRoomPriceSidenightsSingle += int.Parse(numberOfRoomsSingle) * Decimal.Parse(ddlHotels.SelectedItem.Attributes["[" + li.Value.Replace("/", "-") + "]"].Split('|')[0].ToString());
                                totalRoomPriceSidenightsDouble += int.Parse(numberOfRoomsDouble) * Decimal.Parse(ddlHotels.SelectedItem.Attributes["[" + li.Value.Replace("/", "-") + "]"].Split('|')[1].ToString());
                                
                                //totalRoomPriceSidenightsSingle += totalSidenightsNumberOfRoomsSingle * Decimal.Parse(ddlHotels.SelectedItem.Attributes["[" + li.Value.Replace("/", "-") + "]"].Split('|')[0].ToString());
                                //totalRoomPriceSidenightsDouble += totalSidenightsNumberOfRoomsDouble * Decimal.Parse(ddlHotels.SelectedItem.Attributes["[" + li.Value.Replace("/", "-") + "]"].Split('|')[1].ToString());
                            }
                            else
                            {
                                totalRoomPriceSidenightsSingle += int.Parse(((HiddenField)divHFNights.FindControl("hf" + DateTime.Parse(li.Value).Day.ToString())).Value) * Decimal.Parse(ddlHotels.SelectedItem.Attributes["[" + li.Value.Replace("/", "-") + "]"].Split('|')[0].ToString());
                                totalRoomPriceSidenightsDouble += int.Parse(((HiddenField)divHFNights.FindControl("hfd" + DateTime.Parse(li.Value).Day.ToString())).Value) * Decimal.Parse(ddlHotels.SelectedItem.Attributes["[" + li.Value.Replace("/", "-") + "]"].Split('|')[1].ToString());
                            }
                        }
                    }
                }

                pdfSidenightsFormFields.SetField("txtRoomNights", totalSidenightsNumberOfRoomsSingle.ToString());
                pdfSidenightsFormFields.SetField("txtSuiteNights", totalSidenightsNumberOfRoomsDouble.ToString());

                pdfSidenightsFormFields.SetField("txtRoomTotal", (totalRoomPriceSidenightsSingle).ToString("F"));
                pdfSidenightsFormFields.SetField("txtSuiteTotal", (totalRoomPriceSidenightsDouble).ToString("F"));

                pdfSidenightsFormFields.SetField("txtTotalAmount", ((totalRoomPriceSidenightsSingle) + (totalRoomPriceSidenightsDouble)).ToString("F"));
            }

            pdfSidenightsFormFields.SetField("txtUserKey", documentID_SN);
            //pdfSidenightsFormFields.SetField("txtUserKey", hashCode.ToString());

            pdfSidenightsStamper.FormFlattening = false;

            pdfSidenightsStamper.Close();

            LoadSidenightsPDFForm(sidenightsNewFile);
        }
    }

    protected void LoadPDFForm(string path)
    {
       ClientScript.RegisterStartupScript(this.GetType(), "000", "window.open('/2pdf4cm/tempAgreements/" + path + "', 'lovechild','resizable=1,menubar=0,toolbar=0,location=0,directories=0,scrollbars=0,status=0');", true);
    }

    protected void LoadSidenightsPDFForm(string path)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "010", "window.open('/2pdf4cm/tempAgreements/" + path + "', 'lovechild1','resizable=1,menubar=0,toolbar=0,location=0,directories=0,scrollbars=0,status=0');", true);
    }

    protected void FillDDLCountries()
    {
        DataSet dsCountries = new DataSet();

        System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(Server.MapPath("\\") + "\\" + "App_Data\\Countries.xml");

        dsCountries.ReadXml(reader);

        ListItem item = new ListItem("Select", "-1");

        ddlCountry.DataTextField = "Country";
        ddlCountry.DataValueField = "id";
        ddlCountry.DataSource = dsCountries.Tables[0];
        ddlCountry.DataBind();

        ddlCountry.Items.Insert(0, item);

    }

    protected void FillDDLHotels(string conferenceID)
    {
        ddlHotels.Items.Clear();
        ddlHotels1.Items.Clear();
        ddlHotels2.Items.Clear();

        DataSet dsHotels = new DataSet();
        DataSet dsHotelPrices = new DataSet();

        System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(Server.MapPath("\\") + "\\" + "App_Data\\Hotels.xml");
        System.Xml.XmlTextReader reader1 = new System.Xml.XmlTextReader(Server.MapPath("\\") + "\\" + "App_Data\\HotelPrices.xml");

        dsHotels.ReadXml(reader);
        dsHotelPrices.ReadXml(reader1);        

        ListItem item = new ListItem("Select", "-1");

        foreach (DataRow dr in dsHotels.Tables[0].Select("conferenceID='" + conferenceID + "'"))
        {
            ListItem li = new ListItem(dr["name"].ToString(), dr["vendorID"].ToString());

            if (dr["disabled"] != null && dr["disabled"].ToString() == "true")
            {
                li.Attributes.Add("disabled", "disabled");
            }

            if (dr["sprice"].ToString().Length == 0 && dr["sprice"].ToString().Length == 0)
            {

                DataRow[] rows1 = dsHotelPrices.Tables[0].Select("vendorID='" + dr["vendorID"].ToString() + "'" + " and " + "conferenceID='" + conferenceID + "'");
                DataRow[] rowsSprice = dsHotelPrices.Tables["sprice"].Select("prices_Id=" + rows1[0]["prices_Id"].ToString());
                DataRow[] rowsDprice = dsHotelPrices.Tables["dprice"].Select("prices_Id=" + rows1[0]["prices_Id"].ToString());

                for (int i = 0; i < rowsSprice.Length; i++)
                {
                    li.Attributes.Add("[" + rowsSprice[i]["Date"].ToString().Replace("/", "-") + "]", rowsSprice[i]["price"].ToString() + "|" + rowsDprice[i]["price"].ToString());
                }
            }
            else
            {
            li.Attributes.Add("sprice", dr["sprice"].ToString());
            li.Attributes.Add("dprice", dr["dprice"].ToString());
            }

            ddlHotels.Items.Add(li);
            ddlHotels1.Items.Add(li);
            ddlHotels2.Items.Add(li);
        }

        ddlHotels.Items.Insert(0, item);
        ddlHotels1.Items.Insert(0, item);
        ddlHotels2.Items.Insert(0, item);
            
    }

    protected void FillDDLSelectItem()
    {
        ListItem item = new ListItem("Select", "-1");
       
        ddlDateFrom.Items.Insert(0, item);
        ddlDateTo.Items.Insert(0, item);

        ddlHotels.Items.Insert(0, item);
        ddlHotels1.Items.Insert(0, item);
        ddlHotels2.Items.Insert(0, item);
    }

    protected void FillConferenceDates(string conferenceID)
    {
        ddlDateFrom.Items.Clear();
        ddlDateTo.Items.Clear();

        DataSet dsConferences = new DataSet();

        System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(Server.MapPath("\\") + "\\" + "App_Data\\Conferences.xml");

        dsConferences.ReadXml(reader);
        
        ListItem item = new ListItem("Select", "-1");

        foreach (DataRow dr in dsConferences.Tables[0].Select("conferenceID='" + conferenceID + "'"))
        {
            DataRow[] da = dsConferences.Tables[1].Select("conference_id='" + dr[0] + "'");

            for(int i=0; i<da.Length; i++)
            {
                ListItem liFrom = new ListItem(da[i][1].ToString(), da[i][1].ToString());
                liFrom.Attributes.Add("sidenight", da[i][0].ToString());
                ddlDateFrom.Items.Add(liFrom);

                ListItem liTo = new ListItem(da[i][1].ToString(), da[i][1].ToString());
                liTo.Attributes.Add("sidenight", da[i][0].ToString());
                ddlDateTo.Items.Add(liTo);
            }
        }

        ddlDateFrom.Items.Insert(0, item);
        ddlDateTo.Items.Insert(0, item);
    }

    protected void FillConferenceDatesAttirbutes(string conferenceID)
    {
        DataSet dsConferences = new DataSet();

        System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(Server.MapPath("\\") + "\\" + "App_Data\\Conferences.xml");

        dsConferences.ReadXml(reader);

        foreach (DataRow dr in dsConferences.Tables[0].Select("conferenceID='" + conferenceID + "'"))
        {
            DataRow[] da = dsConferences.Tables[1].Select("conference_id='" + dr[0] + "'");

            for (int i = 0; i < da.Length; i++)
            {
                ddlDateFrom.Items[i + 1].Attributes.Add("sidenight", da[i][0].ToString());
                ddlDateTo.Items[i + 1].Attributes.Add("sidenight", da[i][0].ToString());
            }
        }

    }

    protected void FillDDLHotelsAttributes(string conferenceID)
    {
        int selected = this.ddlHotels.SelectedIndex;

        DataSet dsHotels = new DataSet();
        DataSet dsHotelPrices = new DataSet();

        System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(Server.MapPath("\\") + "\\" + "App_Data\\Hotels.xml");
        System.Xml.XmlTextReader reader1 = new System.Xml.XmlTextReader(Server.MapPath("\\") + "\\" + "App_Data\\HotelPrices.xml");

        dsHotels.ReadXml(reader);
        dsHotelPrices.ReadXml(reader1);
        DataRow[] dr = dsHotels.Tables[0].Select("conferenceID='" + conferenceID + "'");
        DataRow[] dr1 = dsHotelPrices.Tables[0].Select("conferenceID='" + conferenceID + "'");

        int i = 0;

        foreach (ListItem item in ddlHotels.Items)
        {
            if (item.Value != "-1")
            {
                if (dr1.Length == 0)
                {
                    item.Attributes.Add("sprice", dr[i]["sprice"].ToString());
                    item.Attributes.Add("dprice", dr[i]["dprice"].ToString());
                    i++;
                }
                else
                {
                    DataRow[] rows1 = dsHotelPrices.Tables[0].Select("vendorID='" + item.Value + "'" + " and " + "conferenceID='" + conferenceID + "'");
                    DataRow[] rowsSprice = dsHotelPrices.Tables["sprice"].Select("prices_Id=" + rows1[0]["prices_Id"].ToString());
                    DataRow[] rowsDprice = dsHotelPrices.Tables["dprice"].Select("prices_Id=" + rows1[0]["prices_Id"].ToString());

                    for (int j = 0; j < rowsSprice.Length; j++)
                    {
                        item.Attributes.Add("[" + rowsSprice[j]["Date"].ToString().Replace("/", "-") + "]", rowsSprice[j]["price"].ToString() + "|" + rowsDprice[j]["price"].ToString());
                    }
                }
            }
        }
    }

    protected void DeleteTempFiles()
    {
        string[] strFiles = Directory.GetFiles(Server.MapPath("/tempAgreements"), "*.pdf", SearchOption.TopDirectoryOnly);
      
        foreach (string file in strFiles)
        {           
            FileInfo fi = new FileInfo(file);
            string name = fi.Name;
            DateTime creationDate = fi.CreationTime;

            if (DateTime.Compare(DateTime.Parse(creationDate.ToShortDateString()), DateTime.Parse(System.DateTime.Now.ToShortDateString())) < 0)
            {
                try
                {
                    fi.Delete();
                }
                catch
                {
                    continue;
                }
            }           
        }    
    }

    protected void PrerenderTableNights()
    {
        if (Session["tNights"] != null)
        {
            Table t = (Table)Session["tNights"];
            divNights.Controls.Clear();
            divNights.Controls.Add(t);

            ArrayList al = (ArrayList)Session["hFields"];

            divHFNights.Controls.Clear();

            foreach (HiddenField hf in al)
            {
                divHFNights.Controls.Add(hf);
            }
        }
    }

    protected void ddlConferences_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillDDLHotels(ddlConferences.SelectedValue);
        FillConferenceDates(ddlConferences.SelectedValue);
        PopulateTableNights(ddlConferences.SelectedValue);      
        PopulateTableNightPrices(ddlConferences.SelectedValue); // Nova metoda
    }

    protected void PopulateTableNights(string conferenceID)
    {
        divHFNights.Controls.Clear();
        divNights.Controls.Clear();

        ArrayList hFields = new ArrayList();

        Table tNights = new Table();       
        TableHeaderRow tHeader = new TableHeaderRow();
        TableRow tRow = new TableRow();
        TableRow tRow1 = new TableRow();

        TableCell tcRoomType = new TableCell();
        tcRoomType.BorderWidth = 2;
        tcRoomType.BorderColor = System.Drawing.Color.Gray;       
        tcRoomType.Text = "Room Type";
        tcRoomType.HorizontalAlign = HorizontalAlign.Center;

        TableCell tcSingleRoom = new TableCell();
        tcSingleRoom.BorderWidth = 2;
        tcSingleRoom.BorderColor = System.Drawing.Color.Gray;
        tcSingleRoom.Text = "Single";
        tcSingleRoom.HorizontalAlign = HorizontalAlign.Center;

        TableCell tcDoubleRoom = new TableCell();
        tcDoubleRoom.BorderWidth = 2;
        tcDoubleRoom.BorderColor = System.Drawing.Color.Gray;
        tcDoubleRoom.Text = "Double";
        tcDoubleRoom.HorizontalAlign = HorizontalAlign.Center;


        DataSet dsConferences = new DataSet();

        System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(Server.MapPath("\\") + "\\" + "App_Data\\Conferences.xml");

        dsConferences.ReadXml(reader);
        
        foreach (DataRow dr in dsConferences.Tables[0].Select("conferenceID='" + conferenceID + "'"))
        {
            DataRow[] da = dsConferences.Tables[1].Select("conference_id='" + dr[0] + "'");

            tHeader.Cells.Add(tcRoomType);

            tRow.Cells.Add(tcSingleRoom);

            tRow1.Cells.Add(tcDoubleRoom);

            for (int i = 0; i < da.Length; i++)
            {
                tHeader.Height = 30;
                TableCell tc = new TableCell();
                DateTime day = DateTime.Parse(da[i][1].ToString());
                tc.Text = String.Format("{0:ddd, MMM d}", day);
                tc.BorderWidth = 2;              
                tc.BorderColor = System.Drawing.Color.Gray;
                tHeader.Cells.Add(tc);

                HiddenField hf = new HiddenField();
                hf.ID = "hf" + day.Day.ToString();

                HiddenField hfd = new HiddenField();
                hfd.ID = "hfd" + day.Day.ToString();

                divHFNights.Controls.Add(hf);
                hFields.Add(hf);

                divHFNights.Controls.Add(hfd);
                hFields.Add(hfd);
            }

            for (int i = 0; i < da.Length; i++)
            {
                tRow.Height = 30;
                TableCell tc1 = new TableCell();

                tc1.Text = "0";
                tc1.BorderWidth = 1;
                tc1.HorizontalAlign = HorizontalAlign.Center;

                TextBox tb = new TextBox();
                tb.ID = "tb" + DateTime.Parse(da[i][1].ToString()).Day;
                tb.Text = "0";
                tb.MaxLength = 3;
                tb.Width = 40;
                tb.BorderColor = System.Drawing.Color.Gray;
                tb.BorderStyle = BorderStyle.Groove;
                tb.Attributes.Add("onblur", "if(this.value.trim().length == 0) { this.value = '0'; } $('#hf" + DateTime.Parse(da[i][1].ToString()).Day + "').val(this.value);");
                tb.Attributes.Add("onkeydown", "return isNumeric(event.keyCode);");

                tc1.Controls.Add(tb);

                tRow.Cells.Add(tc1);
            }

            for (int i = 0; i < da.Length; i++)
            {
                tRow1.Height = 30;
                TableCell tc2 = new TableCell();

                tc2.Text = "0";
                tc2.BorderWidth = 1;
                tc2.HorizontalAlign = HorizontalAlign.Center;

                TextBox tb1 = new TextBox();
                tb1.ID = "tbd" + DateTime.Parse(da[i][1].ToString()).Day;
                tb1.Text = "0";
                tb1.MaxLength = 3;
                tb1.Width = 40;
                tb1.BorderColor = System.Drawing.Color.Gray;
                tb1.BorderStyle = BorderStyle.Groove;
                tb1.Attributes.Add("onblur", "if(this.value.trim().length == 0) { this.value = '0'; } $('#hfd" + DateTime.Parse(da[i][1].ToString()).Day + "').val(this.value);");
                tb1.Attributes.Add("onkeydown", "return isNumeric(event.keyCode);");

                tc2.Controls.Add(tb1);

                tRow1.Cells.Add(tc2);
            }
        }

        tNights.Rows.Add(tHeader);
        tNights.Rows.Add(tRow);
        tNights.Rows.Add(tRow1);

        divNights.Controls.Add(tNights);

        Session["tNights"] = tNights;
        Session["hFields"] = hFields;

    }

    protected void PopulateTableNightPrices(string conferenceID)
    {
        divNightPrices.Controls.Clear();

        Table tNights = new Table();
        TableHeaderRow tHeader = new TableHeaderRow();
        TableRow tRow = new TableRow();
        TableRow tRow1 = new TableRow();

        tNights.Attributes.Add("style", "border: 1px solid #999; border-collapse: collapse; width: 300px; text-align:center; margin:auto;");

        TableCell tcRoomType = new TableCell();
        tcRoomType.BorderWidth = 2;
        tcRoomType.BorderColor = System.Drawing.Color.Gray;
        tcRoomType.Text = "Room Type";
        tcRoomType.HorizontalAlign = HorizontalAlign.Center;
        tcRoomType.Attributes.Add("style", "background: #f5f5f5; border: 1px solid #999; font-size:10px; padding:0px;");

        TableCell tcSingleRoom = new TableCell();
        tcSingleRoom.BorderWidth = 2;
        tcSingleRoom.BorderColor = System.Drawing.Color.Gray;
        tcSingleRoom.Text = "Single";
        tcSingleRoom.HorizontalAlign = HorizontalAlign.Center;

        tcSingleRoom.Attributes.Add("style", "background: #f5f5f5; border: 1px solid #999; font-size:10px; padding:0px;");

        TableCell tcDoubleRoom = new TableCell();
        tcDoubleRoom.BorderWidth = 2;
        tcDoubleRoom.BorderColor = System.Drawing.Color.Gray;
        tcDoubleRoom.Text = "Double";
        tcDoubleRoom.HorizontalAlign = HorizontalAlign.Center;
        tcDoubleRoom.Attributes.Add("style", "background: #f5f5f5; border: 1px solid #999; font-size:10px; padding:0px;");

        DataSet dsConferences = new DataSet();

        System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(Server.MapPath("\\") + "\\" + "App_Data\\Conferences.xml");

        dsConferences.ReadXml(reader);

        foreach (DataRow dr in dsConferences.Tables[0].Select("conferenceID='" + conferenceID + "'"))
        {
            DataRow[] da = dsConferences.Tables[1].Select("conference_id='" + dr[0] + "'");

            tHeader.Cells.Add(tcRoomType);

            tRow.Cells.Add(tcSingleRoom);

            tRow1.Cells.Add(tcDoubleRoom);

            for (int i = 0; i < da.Length; i++)
            {
                tHeader.Height = 30;
                TableCell tc = new TableCell();
                DateTime day = DateTime.Parse(da[i][1].ToString());
                tc.Text = String.Format("{0:ddd,<br/> MMM d}", day);
                tc.BorderWidth = 2;
                tc.BorderColor = System.Drawing.Color.Gray;

                tc.Attributes.Add("style", "background: #f5f5f5; border: 1px solid #999; font-size:10px; padding:0px;");

                tHeader.Cells.Add(tc);
            }

            for (int i = 0; i < da.Length; i++)
            {
                tRow.Height = 30;
                TableCell tc1 = new TableCell();

                tc1.Text = "0.00";
                tc1.BorderWidth = 1;
                tc1.HorizontalAlign = HorizontalAlign.Center;
                tc1.Attributes.Add("style", "background: #f5f5f5; border: 1px solid #999; font-size:10px; padding:0px;");

                TextBox tb = new TextBox();
                tb.ID = "tbsr" + DateTime.Parse(da[i][1].ToString()).Day;
                tb.Text = "0.00";
                tb.MaxLength = 3;
                tb.Width = 40;
                tb.BorderColor = System.Drawing.Color.Gray;
                tb.BorderStyle = BorderStyle.None;
                tb.ReadOnly = true;

                tb.Style.Add("padding", "1px");
                tb.Style.Add("font-size", "11px");
                tb.Style.Add("text-align", "center");
                tb.Style.Add("background", "#f5f5f5");

                tc1.Controls.Add(tb);

                tRow.Cells.Add(tc1);
            }

            for (int i = 0; i < da.Length; i++)
            {
                tRow1.Height = 30;
                TableCell tc2 = new TableCell();

                tc2.Text = "0.00";
                tc2.BorderWidth = 1;
                tc2.HorizontalAlign = HorizontalAlign.Center;
                tc2.Attributes.Add("style", "background: #f5f5f5; border: 1px solid #999; font-size:10px; padding:0px;");

                TextBox tb1 = new TextBox();
                tb1.ID = "tbdr" + DateTime.Parse(da[i][1].ToString()).Day;
                tb1.Text = "0.00";
                tb1.MaxLength = 3;
                tb1.Width = 40;
                tb1.BorderColor = System.Drawing.Color.Gray;
                tb1.BorderStyle = BorderStyle.None;
                tb1.ReadOnly = true;

                tb1.Style.Add("padding", "1px");
                tb1.Style.Add("font-size", "11px");
                tb1.Style.Add("text-align", "center");
                tb1.Style.Add("background", "#f5f5f5");

                tc2.Controls.Add(tb1);

                tRow1.Cells.Add(tc2);
            }
        }

        tNights.Rows.Add(tHeader);
        tNights.Rows.Add(tRow);
        tNights.Rows.Add(tRow1);

        divNightPrices.Controls.Add(tNights);
    }

    protected void LoadConferences()
    {
        DataSet dsConferences = new DataSet();

        System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(Server.MapPath("\\") + "\\" + "App_Data\\Conferences.xml");

        dsConferences.ReadXml(reader);

        foreach (DataRow dr in dsConferences.Tables[0].Rows)
        {
            if (Boolean.Parse(dr["active"].ToString()))
            {
                ListItem li = new ListItem(dr["conferenceID"].ToString(), dr["conferenceID"].ToString());
                ddlConferences.Items.Add(li);
            }
            else
            {
                ListItem li = new ListItem(dr["conferenceID"].ToString(), dr["conferenceID"].ToString());
                li.Enabled = false;
                ddlConferences.Items.Add(li);
            }
        }

        ddlConferences.DataBind();

        ListItem select = new ListItem("Select", "-1");

        ddlConferences.Items.Insert(0, select);
    }

    protected void SendEmail(string conferenceID, string hashCode, bool sideNights, string name, string email, string pdfPath, string validation, string agency, string country)
    {
        DataSet dsEmails = new DataSet();

        System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(Server.MapPath("\\") + "\\" + "App_Data\\Email.xml");

        dsEmails = new DataSet();
        dsEmails.ReadXml(reader);

        DataRow[] dr = dsEmails.Tables[0].Select("conferenceID='" + conferenceID + "'");
        
        if (dr.Length == 0)
        {
            return;
        }

        string subject = dr[0][0].ToString();
        string clientEmail = email;
        string emailFrom = dr[0][1].ToString();
        string emailTo = dr[0][2].ToString();
        string conferenceManagerBody = dr[0][3].ToString().Replace("[name]", name).Replace("[number]", hashCode).Replace("[HA]", sideNights ? "Housing Agreement side nights" : "Housing Agreement").Replace("\r\n", "<br/>").Replace("[agency]", "Customer/Agency: " + agency).Replace("[country]", "Country: " + country);
        string clientBody = dr[0][4].ToString().Replace("[name]", name).Replace("[br]", "<br/>");

        EmailHelper.SendClientEmail(subject, clientEmail, emailFrom, clientBody, pdfPath);
        EmailHelper.SendConferenceManagerEmail(subject, emailTo, emailFrom, conferenceManagerBody, pdfPath, validation);

    }

    protected string ValidateHAFields(string txtInitials, string txtInitialDate, string txtClientAuthorizedRepresentative, string txtTitle, string txtSignature, string txtSignatureDate, string rbPaymentMethod, string rbCreditCard, string txtCardNumber, string txtExpMoYr, string txtCardholderName, string txtCreditCardSignature)
    {
        string validate = String.Empty;
        string begginig = "User didn't enter these fields in HA PDF Form:<br/><br/>";

        validate += (txtInitials == null) || (txtInitials.Length == 0) ? " Initials;" : "";
        validate += (txtInitialDate == null) || (txtInitialDate.Length == 0) ? " Initial Date;" : "";
        validate += (txtClientAuthorizedRepresentative == null) || (txtClientAuthorizedRepresentative.Length == 0) ? " ClientAuthorizedRepresentative;" : "";
        validate += (txtTitle == null) || (txtTitle.Length == 0) ? " Title;" : "";
        validate += (txtSignature == null) || (txtSignature.Length == 0) ? " Signature;" : "";
        validate += (txtSignatureDate == null) || (txtSignatureDate.Length == 0) ? " SignatureDate;" : "";
        validate += (rbPaymentMethod == null) || (rbPaymentMethod.Length == 0) ? " PaymentMethod;" : "";
        validate += (rbCreditCard == null) || (rbCreditCard.Length == 0) ? " CreditCard Type;" : "";
        validate += (txtCardNumber == null) || (txtCardNumber.Length == 0) ? " CardNumber;" : "";
        validate += (txtExpMoYr == null) || (txtExpMoYr.Length == 0) ? " ExpMoYr;" : "";
        validate += (txtCardholderName == null) || (txtCardholderName.Length == 0) ? " CardholderName;" : "";
        //validate += (txtCreditCardSignature == null) || (txtCreditCardSignature.Length == 0) ? " CreditCardSignature" : "";
        validate += (txtCreditCardSignature == null) || (txtCreditCardSignature.Length == 0) ? " Electronic Signature" : "";

        if (validate.Length == 0)
        {
            return validate;
        }
        else
        {
            begginig += validate;
            return begginig;
        }        
    }

    protected string ValidateSNFields(string txtGroupName, string txtAgency, string rbPaymentMethod, string rbCreditCard, string txtCardNumber, string txtExpDate, string txtCardholderName, string txtCreditCardSignature, string txtClientRepresentative, string txtTitle, string txtClientSignature, string txtClientSignatureDate)
    {
        string validate = String.Empty;
        string begginig = "User didn't enter these fields in SN PDF Form:<br/><br/>";

        validate += (rbPaymentMethod == null) || (rbPaymentMethod.Length == 0) ? " PaymentMethod;" : "";
        validate += (rbCreditCard == null) || (rbCreditCard.Length == 0) ? " CreditCard Type;" : "";
        validate += (rbCreditCard == null) || (txtCardNumber.Length == 0) ? " CardNumber;" : "";
        validate += (txtExpDate == null) || (txtExpDate.Length == 0) ? " ExpDate;" : "";
        validate += (txtCardholderName == null) || (txtCardholderName.Length == 0) ? " CardholderName;" : "";
        //validate += (txtCreditCardSignature == null) || (txtCreditCardSignature.Length == 0) ? " CreditCardSignature;" : "";
        validate += (txtCreditCardSignature == null) || (txtCreditCardSignature.Length == 0) ? "Electronic Signature;" : "";
        validate += (txtClientRepresentative == null) || (txtClientRepresentative.Length == 0) ? " ClientRepresentative;" : "";
        validate += (txtTitle == null) || (txtTitle.Length == 0) ? " Title;" : "";
        validate += (txtClientSignature == null) || (txtClientSignature.Length == 0) ? " ClientSignature;" : "";
        validate += (txtClientSignatureDate == null) || (txtClientSignatureDate.Length == 0) ? " ClientSignatureDate" : "";

        if (validate.Length == 0)
        {
            return validate;
        }
        else
        {
            begginig += validate;
            return begginig;
        }
    }

    protected string DepositPaymentHousingAgreementPicknights(ref PaymentAuthorizationFirstData pa)
    {
        string token = String.Empty;
        string transactionID = InternetSecureAPI.SendUserInformation2BoaRestService(pa, ref token);
        if (transactionID.Length > 3)
        {
            pa.Token = token;
        }


        return transactionID;
    }

    protected void TokenizeHousingAgreement(ref PaymentAuthorizationFirstData pa)
    {
        string token = String.Empty;
        string accountID = string.Empty;

        if (pa.Merchant.ToString().Contains("PP"))
        {
            new Planet.PlanetAPI(pa.Merchant).CheckCreditCardAndGetTokenPlanetService(pa.CardHolderName, pa.Address, pa.Country, pa.City, pa.Province, pa.Postal, pa.CreditCardNumber, Planet.Currency.USD, pa.ExparationMonthYear.Substring(0, 2), pa.ExparationMonthYear.Substring(2, 2), pa.CCV, pa.Test, ref accountID, pa.Merchant);
            pa.Token = accountID;
        }
        else
        {
            token = InternetSecureAPI.CheckCreditCardAndGetToken(pa);
            pa.Token = token;
        }
    }
}
