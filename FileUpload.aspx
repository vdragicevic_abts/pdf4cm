﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FileUpload.aspx.cs" Inherits="FileUpload" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Housing Agreement Upload page</title>      
    <style type="text/css">
        
        body
        {
            font-family:Verdana, Times New Roman;
            font-size:14px;
            margin:0;
            padding:0;
        }
        #wrapper
        {
           width:340px;
            height:250px;
            background-color:#CCCCCC;
            margin: auto;
            border: 1px solid black;  
            padding:15px;          
        }
        
        #title
        {
            font-weight:bold;
            font-size:17px;
            border: 0px solid black; 
            margin: auto;
            width:320px;
            padding:0px;
            margin-top:200px;
            margin-bottom:2px;
        }
        
        #tbList
        {
            font-family:Times New Roman;
            font-size:12px;
        }
        
        #lbPDF
        {
            text-decoration: none;            
        }
        
        #lbPDF:hover
        {
            text-decoration: underline;
        }
        
        #aPDF
        {
            text-decoration: none;            
        }
        
        #aPDF:hover
        {
            text-decoration: underline;            
        }
        
        #agree 
        {
           margin-bottom:20px;
        }
              
    </style>
   <script src="js/jquery.js"></script>
    <script>
        $(document).ready(function () {
            $('#btnUpload').attr('disabled', 'disabled');
            $('#btnUpload').fadeTo('fast', 0.5);
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="title">
        <span>Housing Agreement File Upload</span>
    </div>
    <div id="wrapper">
           <div style="border: 1px solid black; padding:7px; color:red;" id="agree">
                <input onchange="if(this.checked) { $('#btnUpload').removeAttr('disabled'); $('#btnUpload').fadeTo('fast', 1); } else { $('#btnUpload').attr('disabled', 'disabled'); $('#btnUpload').fadeTo('fast', 0.5); }" type="checkbox" id="cbAgree" name="" />I have read and agree to the <a href="#" onclick="window.open('http://www.internationalgroupsdepartment.com/privacy.aspx','targetWindow', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=800, height=1000'); return false;">Privacy Policy</a>
            </div>
            <div>
                <asp:FileUpload ID="fuHousingAgreement" runat="server" BorderStyle="NotSet" />&nbsp;&nbsp;<asp:Button
                    ID="btnUpload" runat="server" Text="Upload" BorderStyle="NotSet"
                    OnClick="btnUpload_Click" ToolTip="Read Privacy Policy and agree to upload Housing Agreement"  />
            </div>
            <br/>
            <span>Uploaded files: </span>
            <br />
       <asp:TextBox ID="tbList" runat="server" BorderStyle="Ridge" TextMode="MultiLine" Height="125px" Width="305px"></asp:TextBox>
    </div>
        <br />
        <br />
    </form>
    
    
</body>
</html>
