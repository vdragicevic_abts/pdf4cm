﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
        }
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        XmlDocument doc = new XmlDocument();
        doc.Load(Server.MapPath(".") + "\\App_Data\\" + "Users.xml");

        XmlNode node = doc.SelectSingleNode("//user[./password/text()='" + this.tbPassword.Text.Trim() + "' and ./username/text()='" + this.tbUserName.Text + "']");

        if (node == null) return;

        Response.Redirect("Home.aspx");
    }
}