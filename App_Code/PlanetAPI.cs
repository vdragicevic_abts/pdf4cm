﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Xml.Linq;
using Enterprise.XTrans;
using Newtonsoft.Json;


/// <summary>
/// Summary description for PlanetAPI
/// </summary>
namespace Planet
{  
    #region ENUMERATINS
    public enum Currency
    {
        DZD = 012,
        ARS = 032,
        AUD = 036,
        BSD = 044,
        BHD = 048,
        THB = 764,
        PAB = 590,
        BBD = 052,
        BZD = 084,
        BMD = 060,
        VEF = 937,
        BOB = 068,
        BRL = 986,
        BND = 096,
        CAD = 124,
        KYD = 136,
        GHS = 936,
        XAF = 950,
        CLP = 152,
        COP = 170,
        CRC = 188,
        CZK = 203,
        DKK = 208,
        DOP = 214,
        XCD = 951,
        EGP = 818,
        EUR = 978,
        HUF = 348,
        PYG = 600,
        HKD = 344,
        UAH = 980,
        ISK = 352,
        INR = 356,
        JMD = 388,
        JOD = 400,
        KWD = 414,
        AOA = 973,
        LVL = 428,
        LBP = 422,
        LSL = 426,
        MYR = 458,
        MUR = 480,
        MXN = 484,
        NGN = 566,
        NAD = 516,
        AZN = 944,
        ILS = 376,
        TWD = 901,
        TRY = 949,
        NZD = 554,
        NOK = 578,
        PEN = 604,
        PKR = 586,
        MOP = 446,
        UYU = 858,
        PHP = 608,
        GBP = 826,
        BWP = 072,
        QAR = 634,
        GTQ = 320,
        ZAR = 710,
        OMR = 51,
        MVR = 462,
        IDR = 360,
        RUB = 643,
        SAR = 682,
        SGD = 702,
        LKR = 144,
        SEK = 752,
        CHF = 756,
        SYP = 760,
        BDT = 050,
        TZS = 834,
        KZT = 398,
        TTD = 780,
        AED = 784,
        USD = 840,
        KRW = 410,
        JPY = 392,
        CNY = 156,
        PL = 98
    }

    public enum CurrencyIndicator
    {
        DomesticTransaction = 0,
        McpTransaction = 1,
        PycTransaction = 2
    }

    public enum TransactionIndicator
    {
        Email = 'M',
        Purchase = 'P',
        Telephone = 'T',
        ReccuringTransaction = '2',
        AuthenticatedTransaction = '5',
        AuthenticatedAttemptedButFailed = '6',
        AccountInformationReceivedFromSecuredInternetSite = '7'
    }

    public enum ServiceType
    {
        ACCOUNT,
        BILLING,
        CLIENT,
        CREDIT,
        DEBIT,
        PRODUCT,
        SCHEDULE,
        RATE,
        STATUS
    }

    public enum ServiceSubType
    {
        AUTH,
        CAPTURE,
        DELETE,
        INSERT,
        MODIFY,
        REFUND,
        REPLACE,
        SALE,
        VOID,
        QUERY,
        REVERSAL,
        AVS_ONLY
    }

    public enum Service
    {
        ACH,
        CC,
        DBT,
        RECUR,
        WALLET,
        REPOSITORY,
        TEMPLATE,
        CURRENCY,
        NETWORK
    }

    public enum VerboseResponse
    {
        Disable = 0,
        Enable = 1
    }
    #endregion

    #region CLASSES
    public class PlanetCodesJson
    {
        public string code { get; set; }
        public string text { get; set; }
        public string description { get; set; }
    }

    public class PlanetCodesList
    {
        public List<PlanetCodesJson> resultCodes { get; set; }
    }
    
    public class PlanetAPI
    {
        private readonly PlanetCodesList planetCodesList;
        private readonly Processor api;

        //Default Constructor
        public PlanetAPI(Merchant merchant)
        {
            planetCodesList = JsonConvert.DeserializeObject<Planet.PlanetCodesList>(File.ReadAllText(HttpContext.Current.Server.MapPath("~/PlanetCodes.json")));
            api = new Processor
            (
               DatabaseConnection.GetPlanetPaymentServerIP(),
               DatabaseConnection.GetPlanetPaymentServerPort(),
               DatabaseConnection.GetPlanetPaymentCompanyKey(merchant),
               DatabaseConnection.GetPlanetPaymentTimeOut()
            );

            api.EncryptionMode = EncryptionModeType.TripleDES;
            api.AddEncryptionKey(DatabaseConnection.GetPlanetPaymentEncryptionKey(merchant, DatabaseConnection.PLanetPaymentEncryptionKeys.Key1));
            api.AddEncryptionKey(DatabaseConnection.GetPlanetPaymentEncryptionKey(merchant, DatabaseConnection.PLanetPaymentEncryptionKeys.Key2));
            api.AddEncryptionKey(DatabaseConnection.GetPlanetPaymentEncryptionKey(merchant, DatabaseConnection.PLanetPaymentEncryptionKeys.Key3));

            //Force tls 1.2
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.DefaultConnectionLimit = 9999;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        }

        //Constructor with configuration parameters for outside input
        public PlanetAPI(string serverIP, int serverPort, string companyKey, int timeOut)
        {
            planetCodesList = JsonConvert.DeserializeObject<Planet.PlanetCodesList>(File.ReadAllText(HttpContext.Current.Server.MapPath("~/PlanetCodes.json")));
            api = new Processor(serverIP, serverPort, companyKey, timeOut);
        }

        //Planet Payment Final Balance Direct Payment
        public string SendUserInformation2PlanetRestService(string productPrice, string productID, string productName, string name, string company, string address, string city, string state, string zipCode, string country, string phone, string email, string cardNumber, string ccMonth, string ccYear, string cvv, string cardType, Currency currency, bool test, ref string token, Merchant merchant, string websiteBookingID = "", string code = "")
        {
            var success = string.Empty;            

            api.SetValue("TRANSACTION_INDICATOR", Convert.ToString((char)TransactionIndicator.AccountInformationReceivedFromSecuredInternetSite));
            api.SetValue("SERVICE", Service.CC.ToString());
            api.SetValue("SERVICE_TYPE", ServiceType.DEBIT.ToString());
            api.SetValue("SERVICE_SUBTYPE", ServiceSubType.AUTH.ToString());
            api.SetValue("SERVICE_FORMAT", "1010");
            api.SetValue("ACCOUNT_NUMBER", cardNumber);
            api.SetValue("FIRST_NAME", name.Split(' ')[0]);
            api.SetValue("LAST_NAME", name.Split(' ')[1]);
            if (!test) api.SetValue("PIN", "1234");
            api.SetValue("ADDRESS", address);
            api.SetValue("CITY", city);
            if (!String.IsNullOrEmpty(state)) { api.SetValue("STATE", state); } else { api.SetValue("STATE", "XX"); }
            api.SetValue("POSTAL_CODE", zipCode);
            api.SetValue("CURRENCY_CODE", Convert.ToString((int)currency));
            api.SetValue("CVV", cvv);
            //api.SetValue("COUNTRY", country);
            api.SetValue("AMOUNT", productPrice);
            api.SetValue("EXPIRATION", ccMonth + ccYear.Substring(ccYear.Length - 2));
            api.SetValue("TERMINAL_ID", DatabaseConnection.GetPlanetPaymentTerminalId(merchant));
            api.SetValue("VERBOSE_RESPONSE", "1");

            api.Build();

            string str = api.ProcessRequest();

            XElement res = XElement.Parse(api.ResponseXml);

            var responseMessage = SaveTransactionResponse2File(res);

            string mrc = res.Descendants("FIELD").FirstOrDefault(a => a.Attribute("KEY").Value == "MRC").Value;
            string arc = res.Descendants("FIELD").FirstOrDefault(a => a.Attribute("KEY").Value == "ARC").Value;


            if (arc == "00" || arc == "ER")
            {
                if (mrc == "00" || mrc == "IK" || mrc == "MK")
                {
                    success = res.Descendants("FIELD").FirstOrDefault(a => a.Attribute("KEY").Value == "RESPONSE_TEXT").Value;

                    if (mrc == "00")
                    {
                        token = res.Descendants("FIELD").FirstOrDefault(a => a.Attribute("KEY").Value == "TRANSACTION_ID").Value;

                        EmailHelper.SendUserPaymentDetailsPlanetChargeAdmin(res.Descendants("FIELD"), currency, cardType, name, websiteBookingID, code, productID, "", productName, responseMessage);
                    }
                    else
                    {
                        token = "ER";
                    }
                }
                else
                {
                    success = planetCodesList.resultCodes.Where(r => r.code == mrc).FirstOrDefault().description;
                    token = "ER";
                }
            }
            else
            {
                success = planetCodesList.resultCodes.Where(r => r.code == arc).FirstOrDefault().description;
                token = "ER";
            }

            api.ResetObjects();

            return success;
        }

        //Planet Payment WALLET INSERTION for later charging
        public string CheckCreditCardAndGetTokenPlanetService(string name, string address, string country, string city, string state, string zip, string cardNumber, Currency currency, string ccMonth, string ccYear, string cvv, bool test, ref string accountID, Merchant merchant)
        {
            var success = string.Empty; 
			var firstName = name.Trim().Split(' ')[0];
            var lastName = name.Trim().Split(' ')[1];

            api.SetValue("SERVICE", Service.WALLET.ToString());
            api.SetValue("SERVICE_TYPE", ServiceType.CLIENT.ToString());
            api.SetValue("SERVICE_SUBTYPE", ServiceSubType.INSERT.ToString());
            api.SetValue("SERVICE_FORMAT", "1010");
            api.SetValue("TERMINAL_ID", DatabaseConnection.GetPlanetPaymentTerminalId(merchant));
            api.SetValue("FIRST_NAME", firstName);
            api.SetValue("LAST_NAME", lastName);
            //api.SetValue("ADDRESS", address);
            //api.SetValue("COUNTRY", country);
            //api.SetValue("CITY", city);
            //if (!String.IsNullOrEmpty(state)) { api.SetValue("STATE", state); } else { api.SetValue("STATE", "XX"); }
            //api.SetValue("POSTAL_CODE", zip);
			api.SetValue("STATE", "XX");
            api.SetValue("ACCOUNT", "CC");
            api.SetValue("ACCOUNT_NUMBER", cardNumber);
            api.SetValue("EXPIRATION", ccMonth + ccYear);
            //api.SetValue("CVV", cvv);
            api.SetValue("BILLING_TRANSACTION", "2");
            api.SetValue("CURRENCY_CODE", Convert.ToString((int)currency));
            api.SetValue("CURRENCY_INDICATOR", Convert.ToString((int)CurrencyIndicator.DomesticTransaction));
            api.SetValue("TRANSACTION_INDICATOR", Convert.ToString((char)TransactionIndicator.AccountInformationReceivedFromSecuredInternetSite));
            if (!test) api.SetValue("PIN", "1234");
            api.SetValue("VERBOSE_RESPONSE", "1");
            api.SetValue("OPERATOR", "ABTS");


            api.Build();

            string str = api.ProcessRequest();

            XElement res = XElement.Parse(api.ResponseXml);

            SaveTransactionResponse2File(res);

            api.ResetObjects();

            string mrc = res.Descendants("FIELD").FirstOrDefault(a => a.Attribute("KEY").Value == "MRC").Value;
            string arc = res.Descendants("FIELD").FirstOrDefault(a => a.Attribute("KEY").Value == "ARC").Value;

            if (arc == "00" || arc == "ER")
            {
                if (mrc == "00" || mrc == "IK" || mrc == "MK")
                {
                    success = res.Descendants("FIELD").FirstOrDefault(a => a.Attribute("KEY").Value == "RESPONSE_TEXT").Value;

                    if (mrc == "00")
                    {
                        accountID = res.Descendants("FIELD").FirstOrDefault(a => a.Attribute("KEY").Value == "ACCOUNT_ID").Value;
                    }
                    else
                    {                        
						success = "Error " + res.Descendants("FIELD").FirstOrDefault(a => a.Attribute("KEY").Value == "RESPONSE_TEXT").Value;
						accountID = success;
                    }
                }
                else
                {
                    success = "Error " + res.Descendants("FIELD").FirstOrDefault(a => a.Attribute("KEY").Value == "RESPONSE_TEXT").Value;
                    accountID = success;
                }
            }
            else
            {
                success = "Error " + res.Descendants("FIELD").FirstOrDefault(a => a.Attribute("KEY").Value == "RESPONSE_TEXT").Value;
                accountID = success;
            }

            return success;
        }

        //Planet Payment Direct Payment using Wallet AccountID
        public string SendUserInformation2PlanetTokenVersion(string account_id, string productPrice, string productID, string productName, Currency currency, bool test, Merchant merchant, ref string responseText, CurrencyIndicator currencyIndicator, string cardType = "", string guestName = "", string websiteBookingID = "", string code = "", string Reference_No = "", string Reference_3 = "", string Customer_Ref = "")
        {
            var success = string.Empty;            

            api.SetValue("SERVICE", Service.CC.ToString());
            api.SetValue("SERVICE_TYPE", ServiceType.DEBIT.ToString());
            api.SetValue("SERVICE_SUBTYPE", ServiceSubType.SALE.ToString());
            api.SetValue("SERVICE_FORMAT", "1010");
            api.SetValue("TERMINAL_ID", DatabaseConnection.GetPlanetPaymentTerminalId(merchant));
            api.SetValue("CURRENCY_CODE", Convert.ToString((int)currency));
            api.SetValue("CURRENCY_INDICATOR", Convert.ToString((int)currencyIndicator));
            api.SetValue("ENTRY_MODE", "3");
            api.SetValue("AMOUNT", productPrice);
            if (!test) api.SetValue("PIN", "1234");
            api.SetValue("ACCOUNT_ID", account_id);
            api.SetValue("TRANSACTION_INDICATOR", Convert.ToString((char)TransactionIndicator.AccountInformationReceivedFromSecuredInternetSite));
            api.SetValue("VERBOSE_RESPONSE", "1");

            api.Build();

            string str = api.ProcessRequest();

            XElement res = XElement.Parse(api.ResponseXml);

            var responseMessage = SaveTransactionResponse2File(res);

            api.ResetObjects();

            string mrc = res.Descendants("FIELD").FirstOrDefault(a => a.Attribute("KEY").Value == "MRC").Value;
            string arc = res.Descendants("FIELD").FirstOrDefault(a => a.Attribute("KEY").Value == "ARC").Value;

            if (arc == "00" || arc == "ER")
            {
                if (mrc == "00" || mrc == "IK" || mrc == "MK")
                {
                    responseText = res.Descendants("FIELD").FirstOrDefault(a => a.Attribute("KEY").Value == "RESPONSE_TEXT").Value;

                    if (mrc == "00")
                    {
                        success = res.Descendants("FIELD").FirstOrDefault(a => a.Attribute("KEY").Value == "TRANSACTION_ID").Value;

                        EmailHelper.SendUserPaymentDetailsPlanetChargeAdmin(res.Descendants("FIELD"), currency, cardType, guestName, websiteBookingID, code, Reference_No, Reference_3, Customer_Ref, responseMessage, merchant);
                    }
                    else
                    {
                        success = "ER";
                    }
                }
                else
                {
                    success = "ER";
                    responseText = planetCodesList.resultCodes.Where(r => r.code == mrc).FirstOrDefault().description;

                }
            }
            else
            {
                success = "ER";
                responseText = planetCodesList.resultCodes.Where(r => r.code == arc).FirstOrDefault().description;
            }

            return success;
        }

        //Planet Payment Query for returning current rate for specific currency code
        public string SendUserInformation2PlanetRestServiceCurrencyRateRequestQuery(string country, string currencyCode, Merchant merchant, bool test, ref string responseText)
        {
            var success = string.Empty;            

            api.SetValue("SERVICE", Service.CURRENCY.ToString());
            api.SetValue("SERVICE_TYPE", ServiceType.RATE.ToString());
            api.SetValue("SERVICE_SUBTYPE", ServiceSubType.QUERY.ToString());
            api.SetValue("SERVICE_FORMAT", "0000");
            api.SetValue("TERMINAL_ID", DatabaseConnection.GetPlanetPaymentTerminalId(merchant));
            api.SetValue("QUERY_TYPE", "0");
            api.SetValue("CURRENCY_INDICATOR", Convert.ToString((int)CurrencyIndicator.McpTransaction));
            api.SetValue("CURRENCY_CODE", currencyCode);
            if (!test) api.SetValue("PIN", "1234");  //ssl
            api.SetValue("VERBOSE_RESPONSE", "1");

            api.Build();

            string str = api.ProcessRequest();

            XElement res = XElement.Parse(api.ResponseXml);

            SaveTransactionResponse2File(res);

            api.ResetObjects();

            string mrc = res.Descendants("FIELD").FirstOrDefault(a => a.Attribute("KEY").Value == "MRC").Value;
            string arc = res.Descendants("FIELD").FirstOrDefault(a => a.Attribute("KEY").Value == "ARC").Value;

            if (arc == "00" || arc == "ER")
            {
                if (mrc == "00" || mrc == "IK" || mrc == "MK")
                {
                    responseText = res.Descendants("FIELD").FirstOrDefault(a => a.Attribute("KEY").Value == "RESPONSE_TEXT").Value;

                    if (mrc == "00")
                    {
                        success = res.Descendants("FIELD").FirstOrDefault(a => a.Attribute("KEY").Value == "EXCHANGE_RATE").Value;
                    }
                    else
                    {
                        success = "ER";
                    }
                }
                else
                {
                    success = "ER";
                    responseText = planetCodesList.resultCodes.Where(r => r.code == mrc).FirstOrDefault().description;

                }
            }
            else
            {
                success = "ER";
                responseText = planetCodesList.resultCodes.Where(r => r.code == arc).FirstOrDefault().description;
            }

            return success;
        }

        //Planet Payment Direct Payment using foreign currency (Amount calculated using rate for specific currency)
        public string SendUserInformation2PlanetRestServiceCurrencyRate(string productPrice, string productID, string productName, string name, string company, string address, string city, string state, string zipCode, string country, string phone, string email, string cardNumber, string ccMonth, string ccYear, string cvv, string cardType, Currency currency, bool test, ref string token, Merchant merchant, string websiteBookingID = "", string code = "")
        {

            var success = string.Empty;

            //Get currency rate by Country
            var curr = (Planet.Currency)Enum.Parse(typeof(Planet.Currency), new CountryList().GetRegionInfoByName(country).ISOCurrencySymbol, true);
            string currencyCode = Convert.ToString((int)curr);

            string rate = SendUserInformation2PlanetRestServiceCurrencyRateRequestQuery(country, currencyCode, merchant, test, ref success);

            if (rate.Length <= 3)
            {
                return success;
            }

            //Claculate amount by currency rate
            string calculatedAmountByRate = CalculateAmountByCurrencyRate(productPrice, rate);            

            api.SetValue("TRANSACTION_INDICATOR", Convert.ToString((char)TransactionIndicator.AccountInformationReceivedFromSecuredInternetSite));
            api.SetValue("SERVICE", Service.CC.ToString());
            api.SetValue("SERVICE_TYPE", ServiceType.DEBIT.ToString());
            api.SetValue("SERVICE_SUBTYPE", ServiceSubType.AUTH.ToString());
            api.SetValue("SERVICE_FORMAT", "1010");
            api.SetValue("ACCOUNT_NUMBER", cardNumber);
            api.SetValue("FIRST_NAME", name.Split(' ')[0]);
            api.SetValue("LAST_NAME", name.Split(' ')[1]);
            if (!test) api.SetValue("PIN", "1234");
            api.SetValue("ADDRESS", address);
            api.SetValue("CITY", city);
            if (!String.IsNullOrEmpty(state)) { api.SetValue("STATE", state); } else { api.SetValue("STATE", "XX"); }
            //api.SetValue("COUNTRY", country);
            api.SetValue("POSTAL_CODE", zipCode);
            api.SetValue("CURRENCY_CODE", currencyCode);
            api.SetValue("CURRENCY_INDICATOR", Convert.ToString((int)CurrencyIndicator.McpTransaction));
            //api.SetValue("CVV", cvv);            
            api.SetValue("AMOUNT", Math.Round(Decimal.Parse(calculatedAmountByRate), 2).ToString());
            api.SetValue("EXPIRATION", ccMonth + ccYear.Substring(ccYear.Length - 2));
            api.SetValue("TERMINAL_ID", DatabaseConnection.GetPlanetPaymentTerminalId(merchant));
            api.SetValue("VERBOSE_RESPONSE", "1");

            api.Build();

            string str = api.ProcessRequest();

            XElement res = XElement.Parse(api.ResponseXml);

            var responseMessage = SaveTransactionResponse2File(res);

            string mrc = res.Descendants("FIELD").FirstOrDefault(a => a.Attribute("KEY").Value == "MRC").Value;
            string arc = res.Descendants("FIELD").FirstOrDefault(a => a.Attribute("KEY").Value == "ARC").Value;


            if (arc == "00" || arc == "ER")
            {
                if (mrc == "00" || mrc == "IK" || mrc == "MK")
                {
                    success = res.Descendants("FIELD").FirstOrDefault(a => a.Attribute("KEY").Value == "RESPONSE_TEXT").Value;

                    if (mrc == "00")
                    {
                        token = res.Descendants("FIELD").FirstOrDefault(a => a.Attribute("KEY").Value == "TRANSACTION_ID").Value;
                    }
                    else
                    {
                        token = "ER";
                    }
                }
                else
                {
                    success = planetCodesList.resultCodes.Where(r => r.code == mrc).FirstOrDefault().description;
                    token = "ER";
                }
            }
            else
            {
                success = planetCodesList.resultCodes.Where(r => r.code == arc).FirstOrDefault().description;
                token = "ER";
            }

            EmailHelper.SendUserPaymentDetailsPlanetChargeAdmin(res.Descendants("FIELD"), currency, cardType, name, websiteBookingID, code, productID, company, productName, responseMessage);

            api.ResetObjects();

            return success;
        }

        public string CalculateAmountByCurrencyRate(string amount, string currencyRate)
        {
            decimal _amount = Decimal.Parse(amount);
            decimal _currencyRate = Decimal.Parse(currencyRate);

            return Convert.ToString(_amount / _currencyRate);
        }

        public string SaveTransactionResponse2File(XElement res)
        {            
            var moment = System.DateTime.Now.Year + System.DateTime.Now.Month + System.DateTime.Now.Day + System.DateTime.Now.Millisecond;
            res.Save(HttpContext.Current.Server.MapPath("/PlanetTransactions/") + "PlanetPayment_" + moment + ".xml");

            return HttpContext.Current.Server.MapPath("/PlanetTransactions/") + "PlanetPayment_" + moment + ".xml";		
        }
    }
    #endregion
}