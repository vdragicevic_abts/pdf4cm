﻿using ICSharpCode.SharpZipLib.Zip;
using iTextSharp.text.pdf;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI.WebControls;
using System.Net;

/// <summary>
/// Summary description for PDFGenerator
/// </summary>
[WebService(Namespace = "http://www.abtscs.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class PDFGenerator : System.Web.Services.WebService {

    public PDFGenerator () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }

    #region Comment
    //[WebMethod]
    //public string CreatePDFDocument(string conferenceID, string txtSponsoringGroupName, string txtAgency, string txtPhone, string txtFax, string txtEMail, string txtPostalCode, string txtCity, string txtAddress, string ddlHotel1, string ddlHotel2, string ddlHotel3, string ddlCountry, decimal sprice, decimal dprice, int numberOfSingleRooms, int numberOfDoubleRooms, DateTime dateFrom, DateTime dateTo, string lastEditeBy, string editNights, string sn, string dn)
    //{
    //    string haFile = String.Empty;
    //    string snFile = String.Empty;

    //    var ddlDateFrom = new DropDownList();
    //    var ddlDateTo = new DropDownList();

    //    FillConferenceDates(conferenceID, ref ddlDateFrom, ref ddlDateTo);

    //    bool editNight = Boolean.Parse(editNights);
    //    string[] singleNights = sn.Split(',');
    //    string[] doubleNights = dn.Split(',');

    //    var sNights = new Dictionary<int, int>();
    //    var dNights = new Dictionary<int, int>();

    //    int i = 0;

    //    for (DateTime k = dateFrom; k <= dateTo; k = k.AddDays(1))
    //    {
    //        sNights.Add(k.Day, int.Parse(singleNights[i]));
    //        dNights.Add(k.Day, int.Parse(doubleNights[i]));

    //        i++;
    //    }

    //    //foreach (ListItem li in ddlDateFrom.Items)
    //    //{
    //    //    if (!editNight) break;

    //    //    if (li.Value != "-1")
    //    //    {
    //    //        sNights.Add(DateTime.Parse(li.Value).Day, int.Parse(singleNights[i]));
    //    //        dNights.Add(DateTime.Parse(li.Value).Day, int.Parse(doubleNights[i]));

    //    //        i++;
    //    //    }
    //    //}


    //    DateTime Date = System.DateTime.Now;

    //    string documentID_HA = "G" + Date.ToString("yyMMddhhmmssff");  //Date.ToString("yyMMdd") + System.DateTime.Now.ToString("hh") + System.DateTime.Now.ToString("mm") + System.DateTime.Now.Ticks.ToString().Substring(System.DateTime.Now.Ticks.ToString().Length - 4, 4);

    //    string pdfTemplate = Server.MapPath("docs") + "\\HousingAgreement_" + conferenceID + ".pdf";

    //    string newFile = documentID_HA + "_HousingAgreement_" + conferenceID + ".pdf";
    //    //string newFile = hashCode + "_HousingAgreement_" + conferenceID + ".pdf";

    //    haFile = newFile;

    //    PdfReader pdfReader = new PdfReader(pdfTemplate);
    //    PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(Server.MapPath("tempAgreements") + "\\" + newFile, FileMode.Create), '\0', true);

    //    //PDF Encryption//
    //    //pdfStamper.SetEncryption(true, null, "Dvladh09091976", PdfWriter.ALLOW_FILL_IN | PdfWriter.ALLOW_PRINTING);


    //    AcroFields pdfFormFields = pdfStamper.AcroFields;

    //    pdfFormFields.SetField("txtSponsoringGroupName", txtSponsoringGroupName);
    //    pdfFormFields.SetField("txtAgency", txtAgency);
    //    pdfFormFields.SetField("txtPhone", txtPhone);
    //    pdfFormFields.SetField("txtFax", txtFax);
    //    pdfFormFields.SetField("txtEMail", txtEMail);
    //    pdfFormFields.SetField("txtPostalCode", txtPostalCode);
    //    pdfFormFields.SetField("txtCity", txtCity);
    //    pdfFormFields.SetField("txtAddress", txtAddress);
    //    pdfFormFields.SetField("txtDocumentID", documentID_HA);

    //    pdfFormFields.SetListOption("ddlHotel1", new string[] { "1" }, new string[] { ddlHotel1 });
    //    pdfFormFields.SetListOption("ddlHotel2", new string[] { "2" }, new string[] { ddlHotel2 });
    //    pdfFormFields.SetListOption("ddlHotel3", new string[] { "3" }, new string[] { ddlHotel3 });
    //    pdfFormFields.SetListOption("ddlCountry", new string[] { "2" }, new string[] { ddlCountry });

    //    pdfFormFields.SetListSelection("ddlHotel1", new string[] { ddlHotel1 });
    //    pdfFormFields.SetListSelection("ddlHotel2", new string[] { ddlHotel2 });
    //    pdfFormFields.SetListSelection("ddlHotel3", new string[] { ddlHotel3 });
    //    pdfFormFields.SetListSelection("ddlCountry", new string[] { ddlCountry });

    //    pdfFormFields.SetField("ddlHotel1", ddlHotel1);
    //    pdfFormFields.SetField("ddlHotel2", ddlHotel2);
    //    pdfFormFields.SetField("ddlHotel3", ddlHotel3);
    //    pdfFormFields.SetField("ddlCountry", ddlCountry);

    //    pdfFormFields.SetField("txtUserKey", documentID_HA);

    //    bool sidenights = false;

    //    int totalNumberOfRoomsSingle = 0;
    //    int totalNumberOfRoomsDouble = 0;

    //    int totalSidenightsNumberOfRoomsSingle = 0;
    //    int totalSidenightsNumberOfRoomsDouble = 0;

    //    //string numberOfRoomsSingle = numberOfSingleRooms.ToString();
    //    //string numberOfRoomsDouble = numberOfDoubleRooms.ToString();

    //    string numberOfRoomsSingle = editNight ? MaximumNumberOfNightsSingleDouble(sNights, dNights, ddlDateFrom) : numberOfSingleRooms.ToString();
    //    string numberOfRoomsDouble = editNight ? MaximumNumberOfNightsSingleDouble(sNights, dNights, ddlDateFrom) : numberOfDoubleRooms.ToString();



    //    foreach (ListItem li in ddlDateFrom.Items)
    //    {
    //        if (li.Value != "-1" && li.Attributes["sidenight"].ToString() == "false")
    //        {
    //            if ((DateTime.Parse(li.Value) >= dateFrom) && (DateTime.Parse(li.Value) < dateTo))
    //            {
    //                if (!editNight)
    //                {
    //                    pdfFormFields.SetField("SR" + DateTime.Parse(li.Value).Day, numberOfRoomsSingle);
    //                    totalNumberOfRoomsSingle += int.Parse(numberOfRoomsSingle);

    //                    pdfFormFields.SetField("DR" + DateTime.Parse(li.Value).Day, numberOfRoomsDouble);
    //                    totalNumberOfRoomsDouble += int.Parse(numberOfRoomsDouble);
    //                }
    //                else
    //                {
    //                    int sValue = sNights[DateTime.Parse(li.Value).Day];
    //                    int dValue = dNights[DateTime.Parse(li.Value).Day];

    //                    pdfFormFields.SetField("SR" + DateTime.Parse(li.Value).Day, sValue.ToString());
    //                    totalNumberOfRoomsSingle += sValue;

    //                    pdfFormFields.SetField("DR" + DateTime.Parse(li.Value).Day, dValue.ToString());
    //                    totalNumberOfRoomsDouble += dValue;
    //                }
    //            }
    //        }

    //        if (li.Value != "-1" && li.Attributes["sidenight"].ToString() == "true")
    //        {
    //            if ((DateTime.Parse(li.Value) >= dateFrom) && (DateTime.Parse(li.Value) < dateTo))
    //            {
    //                sidenights = true;
    //            }
    //        }
    //    }

    //    pdfFormFields.SetField("SRTotal", totalNumberOfRoomsSingle.ToString());
    //    pdfFormFields.SetField("DRTotal", totalNumberOfRoomsDouble.ToString());

    //    Decimal singleDeposit = Decimal.Parse(pdfFormFields.GetField("txtSingleDeposit"));
    //    Decimal doubleDeposit = Decimal.Parse(pdfFormFields.GetField("txtDoubleDeposit"));

    //    if (!editNight)
    //    {
    //        pdfFormFields.SetField("txtNumSR", (int.Parse(numberOfRoomsSingle) + int.Parse(numberOfRoomsDouble)).ToString());
    //        pdfFormFields.SetField("txtTotalS", ((int.Parse(numberOfRoomsSingle) + int.Parse(numberOfRoomsDouble)) * singleDeposit).ToString("F"));
    //    }
    //    else
    //    {
    //        pdfFormFields.SetField("txtNumSR", (int.Parse(numberOfRoomsSingle)).ToString());
    //        pdfFormFields.SetField("txtTotalS", ((int.Parse(numberOfRoomsSingle)) * singleDeposit).ToString("F"));
    //    }

    //    pdfFormFields.SetField("txtTotalSD", (Decimal.Parse(pdfFormFields.GetField("txtTotalS")) + Decimal.Parse(pdfFormFields.GetField("txtTotalD"))).ToString("F"));

    //    bool registration = pdfFormFields.GetField("txtRegistration") != null;
    //    Decimal registrationPrice = 0;

    //    if (pdfFormFields.GetField("txtConfirmationDate") == null || DateTime.Parse(pdfFormFields.GetField("txtConfirmationDate").ToString()) >= System.DateTime.Now)
    //    {
    //        //-----------------------------------------------------------------------
    //        //-------------------------Estimated Balance before confirmation date-----------------------------

    //        pdfFormFields.SetField("txtTotalRN", totalNumberOfRoomsSingle.ToString());
    //        pdfFormFields.SetField("txtTotalSN", totalNumberOfRoomsDouble.ToString());

    //        pdfFormFields.SetField("txtSPrice", sprice.ToString("F"));
    //        pdfFormFields.SetField("txtDPrice", dprice.ToString("F"));

    //        pdfFormFields.SetField("txtTotalRNPrice", (totalNumberOfRoomsSingle * sprice).ToString("F"));
    //        pdfFormFields.SetField("txtTotalSNPrice", (totalNumberOfRoomsDouble * dprice).ToString("F"));

    //        pdfFormFields.SetField("txtTotalDepositePaid", (Decimal.Parse(pdfFormFields.GetField("txtTotalS")) + Decimal.Parse(pdfFormFields.GetField("txtTotalD"))).ToString("F"));

    //        pdfFormFields.SetField("txtTotalPrice", (((totalNumberOfRoomsSingle * sprice) + (totalNumberOfRoomsDouble * dprice)) - (Decimal.Parse(pdfFormFields.GetField("txtTotalS")) + Decimal.Parse(pdfFormFields.GetField("txtTotalD")))).ToString("F"));

    //        pdfFormFields.SetField("txtTotalPrice", (Decimal.Parse(pdfFormFields.GetField("txtTotalPrice")) + registrationPrice).ToString("F"));

    //        //------------------------------------------------------------------------
    //    }
    //    else
    //    {
    //        //-----------------------------------------------------------------------
    //        //-------------------------Estimated Balance after confirmation date-----------------------------

    //        pdfFormFields.SetField("txtTotalRN1", totalNumberOfRoomsSingle.ToString());
    //        pdfFormFields.SetField("txtTotalSN1", totalNumberOfRoomsDouble.ToString());

    //        pdfFormFields.SetField("txtSPrice1", sprice.ToString("F"));
    //        pdfFormFields.SetField("txtDPrice1", dprice.ToString("F"));

    //        pdfFormFields.SetField("txtTotalRNPrice1", ((totalNumberOfRoomsSingle * sprice) / 2).ToString("F"));
    //        pdfFormFields.SetField("txtTotalSNPrice1", ((totalNumberOfRoomsDouble * dprice) / 2).ToString("F"));

    //        pdfFormFields.SetField("txtTotalPrice1", (((totalNumberOfRoomsSingle * sprice) / 2) + ((totalNumberOfRoomsDouble * dprice) / 2)).ToString("F"));

    //        pdfFormFields.SetField("txtTotalRN", totalNumberOfRoomsSingle.ToString());
    //        pdfFormFields.SetField("txtTotalSN", totalNumberOfRoomsDouble.ToString());

    //        pdfFormFields.SetField("txtSPrice", sprice.ToString("F"));
    //        pdfFormFields.SetField("txtDPrice", dprice.ToString("F"));

    //        pdfFormFields.SetField("txtTotalRNPrice", (totalNumberOfRoomsSingle * sprice).ToString("F"));
    //        pdfFormFields.SetField("txtTotalSNPrice", (totalNumberOfRoomsDouble * dprice).ToString("F"));

    //        pdfFormFields.SetField("txtTotalDepositePaid", (((totalNumberOfRoomsSingle * sprice) / 2) + ((totalNumberOfRoomsDouble * dprice) / 2)).ToString("F"));

    //        pdfFormFields.SetField("txtTotalPrice", (((totalNumberOfRoomsSingle * sprice) + (totalNumberOfRoomsDouble * dprice)) - (Decimal.Parse(pdfFormFields.GetField("txtTotalDepositePaid")) + Decimal.Parse(pdfFormFields.GetField("txtTotalD")))).ToString("F"));

    //        pdfFormFields.SetField("txtTotalPrice", (Decimal.Parse(pdfFormFields.GetField("txtTotalPrice")) + registrationPrice).ToString("F"));

    //        //------------------------------------------------------------------------
    //    }

    //    pdfFormFields.SetField("hfButton", "0");
    //    pdfFormFields.SetField("txtEditedBy", lastEditeBy + " " + System.DateTime.Now.ToShortDateString());

    //    pdfStamper.FormFlattening = false;

    //    pdfStamper.Close();

              
    //    ZipFile z = ZipFile.Create(Server.MapPath("tempAgreements") + "\\" + documentID_HA + ".zip");       //(filezipname);
    //    z.BeginUpdate();
    //    z.Add(Server.MapPath("tempAgreements") + "\\" + newFile, Path.GetFileName(Server.MapPath("tempAgreements") + "\\" + newFile));


    //    if (sidenights)
    //    {
    //        DateTime DateSN = System.DateTime.Now;
    //        string documentID_SN = "G" + Date.ToString("yyMMddhhmmssff"); //Date.ToString("yyMMdd") + System.DateTime.Now.ToString("hh") + System.DateTime.Now.ToString("mm") + System.DateTime.Now.Ticks.ToString().Substring(System.DateTime.Now.Ticks.ToString().Length - 4, 4);

    //        string pdfSidenightsTemplate = Server.MapPath("docs") + "\\Sidenights_" + conferenceID + ".pdf";

    //        string sidenightsNewFile = documentID_SN + "_Sidenights_" + conferenceID + ".pdf";

    //        snFile = sidenightsNewFile;

    //        PdfReader pdfSidenightsReader = new PdfReader(pdfSidenightsTemplate);
    //        PdfStamper pdfSidenightsStamper = new PdfStamper(pdfSidenightsReader, new FileStream(Server.MapPath("tempAgreements") + "\\" + sidenightsNewFile, FileMode.Create), '\0', true);

    //        //PDF Encryption//
    //        //pdfSidenightsStamper.SetEncryption(true, null, "Dvladh09091976", PdfWriter.ALLOW_FILL_IN | PdfWriter.ALLOW_PRINTING);

    //        AcroFields pdfSidenightsFormFields = pdfSidenightsStamper.AcroFields;

    //        pdfSidenightsFormFields.SetField("txtGroupName", txtSponsoringGroupName);
    //        pdfSidenightsFormFields.SetField("txtAgency", txtAgency);
    //        pdfSidenightsFormFields.SetField("txtEMail", txtEMail);
    //        pdfSidenightsFormFields.SetField("txtHotelBooked", ddlHotel1);
    //        pdfSidenightsFormFields.SetField("txtDocumentID", documentID_SN);

    //        foreach (ListItem li in ddlDateFrom.Items)
    //        {
    //            if (li.Value != "-1" && li.Attributes["sidenight"].ToString() == "true")
    //            {
    //                if ((DateTime.Parse(li.Value) >= dateFrom) && (DateTime.Parse(li.Value) < dateTo))
    //                {
    //                    if (!editNight)
    //                    {
    //                        pdfSidenightsFormFields.SetField("txtGR" + DateTime.Parse(li.Value).Day, numberOfRoomsSingle);
    //                        totalSidenightsNumberOfRoomsSingle += int.Parse(numberOfRoomsSingle);
    //                        pdfSidenightsFormFields.SetField("txtSR" + DateTime.Parse(li.Value).Day, numberOfRoomsDouble);
    //                        totalSidenightsNumberOfRoomsDouble += int.Parse(numberOfRoomsDouble);
    //                    }
    //                    else
    //                    {
    //                        int sValue = sNights[DateTime.Parse(li.Value).Day];
    //                        int dValue = dNights[DateTime.Parse(li.Value).Day];

    //                        pdfSidenightsFormFields.SetField("txtGR" + DateTime.Parse(li.Value).Day, sValue.ToString());
    //                        totalSidenightsNumberOfRoomsSingle += sValue;

    //                        pdfSidenightsFormFields.SetField("txtSR" + DateTime.Parse(li.Value).Day, dValue.ToString());
    //                        totalSidenightsNumberOfRoomsDouble += dValue;
    //                    }

    //                }
    //            }
    //        }

    //        pdfSidenightsFormFields.SetField("txtGRTotal", totalSidenightsNumberOfRoomsSingle.ToString());
    //        pdfSidenightsFormFields.SetField("txtSRTotal", totalSidenightsNumberOfRoomsDouble.ToString());

    //        pdfSidenightsFormFields.SetField("txtRoomRate", sprice.ToString("F"));
    //        pdfSidenightsFormFields.SetField("txtSuiteRate", dprice.ToString("F"));

    //        pdfSidenightsFormFields.SetField("txtRoomNights", totalSidenightsNumberOfRoomsSingle.ToString());
    //        pdfSidenightsFormFields.SetField("txtSuiteNights", totalSidenightsNumberOfRoomsDouble.ToString());

    //        pdfSidenightsFormFields.SetField("txtRoomTotal", (totalSidenightsNumberOfRoomsSingle * sprice).ToString("F"));
    //        pdfSidenightsFormFields.SetField("txtSuiteTotal", (totalSidenightsNumberOfRoomsDouble * dprice).ToString("F"));

    //        pdfSidenightsFormFields.SetField("txtTotalAmount", ((totalSidenightsNumberOfRoomsSingle * sprice) + (totalSidenightsNumberOfRoomsDouble * dprice)).ToString("F"));

    //        pdfSidenightsFormFields.SetField("txtUserKey", documentID_SN);
    //        //pdfSidenightsFormFields.SetField("txtUserKey", hashCode.ToString());

    //        pdfSidenightsStamper.FormFlattening = false;

    //        pdfSidenightsStamper.Close();

    //        if ((totalSidenightsNumberOfRoomsSingle + totalSidenightsNumberOfRoomsDouble) != 0)
    //            z.Add(Server.MapPath("tempAgreements") + "\\" + sidenightsNewFile, Path.GetFileName(Server.MapPath("tempAgreements") + "\\" + sidenightsNewFile));
    //        else
    //            File.Delete(Server.MapPath("tempAgreements") + "\\" + sidenightsNewFile);

    //    }


    //    z.CommitUpdate();
    //    z.Close();

    //    string text = String.Empty;

    //    using (FileStream fileStream = File.OpenRead(Server.MapPath("tempAgreements") + "\\" + documentID_HA + ".zip"))
    //    {
    //        MemoryStream memStream = new MemoryStream();
    //        memStream.SetLength(fileStream.Length);
    //        fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);

    //        text = Convert.ToBase64String(memStream.ToArray()); 
    //    }
        
    //    File.Delete(Server.MapPath("tempAgreements") + "\\" + haFile);

    //    if (sidenights)
    //        File.Delete(Server.MapPath("tempAgreements") + "\\" + snFile);

    //    return text + "|" + documentID_HA;

    //}
    #endregion


    private bool CheckVariableRates(Dictionary<int, decimal> rates)
    {
        bool variableRates = false;

        var rate = (from r in rates
                    where r.Value != 0M
                    select r).FirstOrDefault();

        foreach (KeyValuePair<int, decimal> s in rates)
        {
            if (s.Value == 0M)
                continue;

            if (s.Value != rate.Value)
            {
                variableRates = true;
                break;
            }
        }

        return variableRates;
    }

    [WebMethod]
    public string CreatePDFDocument(string conferenceID, string txtSponsoringGroupName, string txtAgency, string txtPhone, string txtFax, string txtEMail, string txtPostalCode, string txtCity, string txtAddress, string ddlHotel1, string ddlHotel2, string ddlHotel3, string ddlCountry, decimal sprice, decimal dprice, int numberOfSingleRooms, int numberOfDoubleRooms, DateTime dateFrom, DateTime dateTo, string lastEditeBy, string editNights, string sn, string dn, string sp, string dp)
    {
		bool isAvailable = CheckIfConferenceIsAvailable(conferenceID);

        if(!isAvailable)
        {
            return "NotAvailable";
        }
		
        string haFile = String.Empty;
        string snFile = String.Empty;

        var ddlDateFrom = new DropDownList();
        var ddlDateTo = new DropDownList();

        string conferenceName = String.Empty;

        if (conferenceID.Contains("Overflow"))
        {
            conferenceID = "AAN2019-Overflow";
            conferenceName = "2019 AAN-Overflow";
        }
        else
        {
            conferenceName = conferenceID.Substring(conferenceID.Length - 4) + " " + conferenceID.Substring(0, conferenceID.Length - 4);
        }

        ServicePointManager.Expect100Continue = true;
        ServicePointManager.DefaultConnectionLimit = 9999;
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

        //Get Merchant Terminal Info From ABTSolute
        ABTSoluteWebServices.ABTSoluteWebServices ser = new ABTSoluteWebServices.ABTSoluteWebServices();
        var merchantInfo = ser.GetMerchantAccountInfoByConferenceName(conferenceName, "9DD873E8-3465-4426-B498-38EA9589090E");

        string merchantID = merchantInfo != null && merchantInfo.Rows.Count > 0 ? merchantInfo.Rows[0]["merchantID"].ToString() : "0";
		
		if(string.IsNullOrEmpty(merchantID))
        {
            return "NoMerchantAccount";
        }

        FillConferenceDates(conferenceID, ref ddlDateFrom, ref ddlDateTo);

        bool editNight = Boolean.Parse(editNights);
        string[] singleNights = sn.Split(',');
        string[] doubleNights = dn.Split(',');

        var sNights = new Dictionary<int, int>();
        var dNights = new Dictionary<int, int>();

        var sPrices = new Dictionary<int, decimal>();
        var dPrices = new Dictionary<int, decimal>();

        int i = 0;

        for (DateTime k = dateFrom; k <= dateTo; k = k.AddDays(1))
        {
            sNights.Add(k.Day, int.Parse(singleNights[i]));
            dNights.Add(k.Day, int.Parse(doubleNights[i]));

            i++;
        }

        if (sprice == 0 && dprice == 0)
        {
            bool added = false;

            string[] singlePrices = sp.Split(',');
            string[] doublePrices = dp.Split(',');

            int n = 0;

            for (DateTime m = dateFrom; m <= dateTo; m = m.AddDays(1))
            {
                if (sNights[m.Day] != 0)
                {
                    sPrices.Add(m.Day, decimal.Parse(singlePrices[n]));
                    added = true;

                }
                else
                {
                    sPrices.Add(m.Day, 0);
                }

                if (dNights[m.Day] != 0)
                {
                    dPrices.Add(m.Day, decimal.Parse(doublePrices[n]));
                    added = true;
                }
                else
                {
                    dPrices.Add(m.Day, 0);
                }

                if (added)
                {
                    n++;
                    added = false;
                }
            }

            #region Commented code
            //int j = 0;

            //for (int l = 0; l < singlePrices.Count(); l++)
            //{
            //    if (ddlDateFrom.Items[l].Value != "-1")
            //    {
            //        sPrices.Add(DateTime.Parse(ddlDateFrom.Items[l].Value).Day, decimal.Parse(singlePrices[j]));
            //        dPrices.Add(DateTime.Parse(ddlDateFrom.Items[l].Value).Day, decimal.Parse(doublePrices[j]));

            //        j++;
            //    }
            //}
            #endregion
        }

        #region Commented code
        //foreach (ListItem li in ddlDateFrom.Items)
        //{
        //    if (!editNight) break;

        //    if (li.Value != "-1")
        //    {
        //        sNights.Add(DateTime.Parse(li.Value).Day, int.Parse(singleNights[i]));
        //        dNights.Add(DateTime.Parse(li.Value).Day, int.Parse(doubleNights[i]));

        //        i++;
        //    }
        //}
        #endregion

        string numberOfRoomsSingle = "0";
        string numberOfRoomsDouble = "0";
		
		//numberOfRoomsSingle = editNight ? MaximumNumberOfNightsSingleDouble(sNights, dNights, ddlDateFrom) : numberOfSingleRooms.ToString();
        //numberOfRoomsDouble = editNight ? MaximumNumberOfNightsSingleDouble(sNights, dNights, ddlDateFrom) : numberOfDoubleRooms.ToString();

        if (!conferenceID.Contains("AASLD"))
        {
            numberOfRoomsSingle = editNight ? MaximumNumberOfNightsSingleDouble(sNights, dNights, ddlDateFrom) : numberOfSingleRooms.ToString();
            numberOfRoomsDouble = editNight ? MaximumNumberOfNightsSingleDouble(sNights, dNights, ddlDateFrom) : numberOfDoubleRooms.ToString();
        }
        else
        {
            numberOfRoomsSingle = editNight ? MaximumNumberOfNightsSingleDoubleSeparate(sNights, dNights, ddlDateFrom)[0].ToString() : numberOfSingleRooms.ToString();
            numberOfRoomsDouble = editNight ? MaximumNumberOfNightsSingleDoubleSeparate(sNights, dNights, ddlDateFrom)[1].ToString() : numberOfDoubleRooms.ToString();
        }

        DateTime Date = System.DateTime.Now;

        string documentID_HA = "G" + Date.ToString("yyMMddhhmmssff");
        //Date.ToString("yyMMdd") + System.DateTime.Now.ToString("hh") + System.DateTime.Now.ToString("mm") + System.DateTime.Now.Ticks.ToString().Substring(System.DateTime.Now.Ticks.ToString().Length - 4, 4);

        string pdfTemplate = String.Empty;
		
		pdfTemplate = Server.MapPath("docs") + "\\HousingAgreement_" + conferenceID + ".pdf";

        /* if (!conferenceID.Contains("AASLD"))
        {
            pdfTemplate = Server.MapPath("docs") + "\\HousingAgreement_" + conferenceID + ".pdf";
        }
        else
        {
            string maximumNumberOfRooms = editNight ? MaximumNumberOfNightsSingleDouble(sNights, dNights, ddlDateFrom) : (int.Parse(numberOfRoomsSingle) + int.Parse(numberOfRoomsDouble)).ToString();
            
            if (int.Parse(maximumNumberOfRooms) < 25)
            {
                pdfTemplate = Server.MapPath("docs") + "\\HousingAgreement_" + conferenceID + ".pdf";
            }
            else
            {
                pdfTemplate = Server.MapPath("docs") + "\\HousingAgreement_" + conferenceID + "_Ver2.pdf";
            }
        } */

        //string pdfTemplate = Server.MapPath("docs") + "\\HousingAgreement_" + conferenceID + ".pdf";

        string newFile = documentID_HA + "_HousingAgreement_" + conferenceID + ".pdf";
        //string newFile = hashCode + "_HousingAgreement_" + conferenceID + ".pdf";

        haFile = newFile;

        PdfReader pdfReader = new PdfReader(pdfTemplate);
        PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(Server.MapPath("tempAgreements") + "\\" + newFile, FileMode.Create), '\0', true);

        //PDF Encryption//
        //pdfStamper.SetEncryption(true, null, "Dvladh09091976", PdfWriter.ALLOW_FILL_IN | PdfWriter.ALLOW_PRINTING);

        AcroFields pdfFormFields = pdfStamper.AcroFields;

        pdfFormFields.SetField("txtSponsoringGroupName", txtSponsoringGroupName);
        pdfFormFields.SetField("txtAgency", txtAgency);
        pdfFormFields.SetField("txtPhone", txtPhone);
        pdfFormFields.SetField("txtFax", txtFax);
        pdfFormFields.SetField("txtEMail", txtEMail);
        pdfFormFields.SetField("txtPostalCode", txtPostalCode);
        pdfFormFields.SetField("txtCity", txtCity);
        pdfFormFields.SetField("txtAddress", txtAddress);
        pdfFormFields.SetField("txtDocumentID", documentID_HA);

        pdfFormFields.SetListOption("ddlHotel1", new string[] { "1" }, new string[] { ddlHotel1 });
        pdfFormFields.SetListOption("ddlHotel2", new string[] { "2" }, new string[] { ddlHotel2 });
        pdfFormFields.SetListOption("ddlHotel3", new string[] { "3" }, new string[] { ddlHotel3 });
        pdfFormFields.SetListOption("ddlCountry", new string[] { "2" }, new string[] { ddlCountry });

        pdfFormFields.SetListSelection("ddlHotel1", new string[] { ddlHotel1 });
        pdfFormFields.SetListSelection("ddlHotel2", new string[] { ddlHotel2 });
        pdfFormFields.SetListSelection("ddlHotel3", new string[] { ddlHotel3 });
        pdfFormFields.SetListSelection("ddlCountry", new string[] { ddlCountry });

        pdfFormFields.SetField("ddlHotel1", ddlHotel1);
        pdfFormFields.SetField("ddlHotel2", ddlHotel2);
        pdfFormFields.SetField("ddlHotel3", ddlHotel3);
        pdfFormFields.SetField("ddlCountry", ddlCountry);

        pdfFormFields.SetField("txtUserKey", documentID_HA);
		
		pdfFormFields.SetField("txtMerchant", merchantID);

        bool sidenights = false;

        int totalNumberOfRoomsSingle = 0;
        int totalNumberOfRoomsDouble = 0;

        int totalSidenightsNumberOfRoomsSingle = 0;
        int totalSidenightsNumberOfRoomsDouble = 0;

        //string numberOfRoomsSingle = numberOfSingleRooms.ToString();
        //string numberOfRoomsDouble = numberOfDoubleRooms.ToString();

        //string numberOfRoomsSingle = editNight? MaximumNumberOfNightsSingleDouble(sNights, dNights, ddlDateFrom) : numberOfSingleRooms.ToString();
        //string numberOfRoomsDouble = editNight? MaximumNumberOfNightsSingleDouble(sNights, dNights, ddlDateFrom) : numberOfDoubleRooms.ToString();



        foreach (ListItem li in ddlDateFrom.Items)
        {
            if (li.Value != "-1" && li.Attributes["sidenight"].ToString() == "false")
            {
                if ((DateTime.Parse(li.Value) >= dateFrom) && (DateTime.Parse(li.Value) < dateTo))
                {
                    if (!editNight)
                    {
                        pdfFormFields.SetField("SR" + DateTime.Parse(li.Value).Day, numberOfRoomsSingle);
                        totalNumberOfRoomsSingle += int.Parse(numberOfRoomsSingle);

                        pdfFormFields.SetField("DR" + DateTime.Parse(li.Value).Day, numberOfRoomsDouble);
                        totalNumberOfRoomsDouble += int.Parse(numberOfRoomsDouble);
                    }
                    else
                    {
                        int sValue = sNights[DateTime.Parse(li.Value).Day];
                        int dValue = dNights[DateTime.Parse(li.Value).Day];

                        pdfFormFields.SetField("SR" + DateTime.Parse(li.Value).Day, sValue.ToString());
                        totalNumberOfRoomsSingle += sValue;

                        pdfFormFields.SetField("DR" + DateTime.Parse(li.Value).Day, dValue.ToString());
                        totalNumberOfRoomsDouble += dValue;
                    }
                }
            }

            if (li.Value != "-1" && li.Attributes["sidenight"].ToString() == "true")
            {
                if ((DateTime.Parse(li.Value) >= dateFrom) && (DateTime.Parse(li.Value) < dateTo))
                {
                    if (sNights[DateTime.Parse(li.Value).Day] > 0 || dNights[DateTime.Parse(li.Value).Day] > 0)
                    {
                        sidenights = true;
                    }
                }
            }
        }

        pdfFormFields.SetField("SRTotal", totalNumberOfRoomsSingle.ToString());
        pdfFormFields.SetField("DRTotal", totalNumberOfRoomsDouble.ToString());

        Decimal singleDeposit = Decimal.Parse(pdfFormFields.GetField("txtSingleDeposit"));
        Decimal doubleDeposit = Decimal.Parse(pdfFormFields.GetField("txtDoubleDeposit"));

        if (!editNight)
        {
            if (singleDeposit == 0 && doubleDeposit == 0)
            {
                pdfFormFields.SetField("txtSingleDeposit", sprice.ToString("F"));
                pdfFormFields.SetField("txtDoubleDeposit", dprice.ToString("F"));

                singleDeposit = sprice;
                doubleDeposit = dprice;

                pdfFormFields.SetField("txtNumSR", (int.Parse(numberOfRoomsSingle)).ToString());
                pdfFormFields.SetField("txtNumDR", (int.Parse(numberOfRoomsDouble)).ToString());

                pdfFormFields.SetField("txtTotalS", ((int.Parse(numberOfRoomsSingle) * singleDeposit) * 2).ToString("F"));
                pdfFormFields.SetField("txtTotalD", ((int.Parse(numberOfRoomsDouble) * doubleDeposit) * 2).ToString("F"));
            }
            else
            {
                pdfFormFields.SetField("txtNumSR", (int.Parse(numberOfRoomsSingle) + int.Parse(numberOfRoomsDouble)).ToString());
                pdfFormFields.SetField("txtTotalS", ((int.Parse(numberOfRoomsSingle) + int.Parse(numberOfRoomsDouble)) * singleDeposit).ToString("F"));                
            }
        }
        else
        {
            if (singleDeposit == 0 && doubleDeposit == 0)
            {
                pdfFormFields.SetField("txtSingleDeposit", sprice.ToString("F"));
                pdfFormFields.SetField("txtDoubleDeposit", dprice.ToString("F"));

                singleDeposit = sprice;
                doubleDeposit = dprice;

                pdfFormFields.SetField("txtNumSR", (int.Parse(numberOfRoomsSingle)).ToString());
                pdfFormFields.SetField("txtNumDR", (int.Parse(numberOfRoomsDouble)).ToString());

                pdfFormFields.SetField("txtTotalS", ((int.Parse(numberOfRoomsSingle) * singleDeposit) * 2).ToString("F"));
                pdfFormFields.SetField("txtTotalD", ((int.Parse(numberOfRoomsDouble) * doubleDeposit) * 2).ToString("F"));
            }
            else
            {
                pdfFormFields.SetField("txtNumSR", (int.Parse(numberOfRoomsSingle)).ToString());
                pdfFormFields.SetField("txtTotalS", ((int.Parse(numberOfRoomsSingle)) * singleDeposit).ToString("F"));
            }

            
        }

        if (singleDeposit != 0 && doubleDeposit != 0)
        {
            pdfFormFields.SetField("txtTotalSD", (Decimal.Parse(pdfFormFields.GetField("txtTotalS")) + Decimal.Parse(pdfFormFields.GetField("txtTotalD"))).ToString("F"));
        }

        bool registration = pdfFormFields.GetField("txtRegistration") != null;
        Decimal registrationPrice = 0;

        if (pdfFormFields.GetField("txtConfirmationDate") == null || DateTime.Parse(pdfFormFields.GetField("txtConfirmationDate").ToString()) >= System.DateTime.Now)
        {
            //-----------------------------------------------------------------------
            //-------------------------Estimated Balance before confirmation date-----------------------------

            pdfFormFields.SetField("txtTotalRN", totalNumberOfRoomsSingle.ToString());
            pdfFormFields.SetField("txtTotalSN", totalNumberOfRoomsDouble.ToString());

            if ((sprice != 0) || (dprice != 0))
            {
                pdfFormFields.SetField("txtSPrice", sprice.ToString("F"));
                pdfFormFields.SetField("txtDPrice", dprice.ToString("F"));

                pdfFormFields.SetField("txtTotalRNPrice", (totalNumberOfRoomsSingle * sprice).ToString("F"));
                pdfFormFields.SetField("txtTotalSNPrice", (totalNumberOfRoomsDouble * dprice).ToString("F"));

                pdfFormFields.SetField("txtTotalDepositePaid", (Decimal.Parse(pdfFormFields.GetField("txtTotalS")) + Decimal.Parse(pdfFormFields.GetField("txtTotalD"))).ToString("F"));

                pdfFormFields.SetField("txtTotalPrice", (((totalNumberOfRoomsSingle * sprice) + (totalNumberOfRoomsDouble * dprice)) - (Decimal.Parse(pdfFormFields.GetField("txtTotalS")) + Decimal.Parse(pdfFormFields.GetField("txtTotalD")))).ToString("F"));

                pdfFormFields.SetField("txtTotalPrice", (Decimal.Parse(pdfFormFields.GetField("txtTotalPrice")) + registrationPrice).ToString("F"));
				
				//Novo polje za placanje koje prikazuje ukupan iznos bez umanjenja deposita
                pdfFormFields.SetField("txtFullPaymentTotalPrice", ((totalNumberOfRoomsSingle * sprice) + (totalNumberOfRoomsDouble * dprice)).ToString("F"));

            }
            else
            {
                Decimal totalRoomPriceSingle = 0;
                Decimal totalRoomPriceDouble = 0;

                foreach (ListItem li in ddlDateFrom.Items)
                {
                    if (li.Value != "-1" && li.Attributes["sidenight"].ToString() == "false")
                    {
                        if ((DateTime.Parse(li.Value) >= dateFrom) && (DateTime.Parse(li.Value) < dateTo))
                        {
                            if (!editNight)
                            {
                                totalRoomPriceSingle += int.Parse(numberOfRoomsSingle) * sPrices[DateTime.Parse(li.Value).Day];
                                totalRoomPriceDouble += int.Parse(numberOfRoomsDouble) * dPrices[DateTime.Parse(li.Value).Day];
                            }
                            else
                            {
                                totalRoomPriceSingle += sNights[DateTime.Parse(li.Value).Day] * sPrices[DateTime.Parse(li.Value).Day];
                                totalRoomPriceDouble += dNights[DateTime.Parse(li.Value).Day] * dPrices[DateTime.Parse(li.Value).Day];
                            }
                        }
                    }
                }

                bool variableRatesSingle = CheckVariableRates(sPrices);
                bool variableRatesDouble = CheckVariableRates(dPrices);

                var singlePriceFromDic = (from spr in sPrices
                                          where spr.Value != 0M
                                          select spr).FirstOrDefault();
                var doublePriceFromDic = (from dpr in dPrices
                                          where dpr.Value != 0M
                                          select dpr).FirstOrDefault();


                pdfFormFields.SetField("txtSPrice", variableRatesSingle ? "VNR" : singlePriceFromDic.Value.ToString("F"));
                pdfFormFields.SetField("txtDPrice", variableRatesDouble ? "VNR" : doublePriceFromDic.Value.ToString("F"));

                pdfFormFields.SetField("txtTotalRNPrice", (totalRoomPriceSingle).ToString("F"));
                pdfFormFields.SetField("txtTotalSNPrice", (totalRoomPriceDouble).ToString("F"));

                pdfFormFields.SetField("txtTotalDepositePaid", (Decimal.Parse(pdfFormFields.GetField("txtTotalS")) + Decimal.Parse(pdfFormFields.GetField("txtTotalD"))).ToString("F"));
                pdfFormFields.SetField("txtTotalPrice", ((totalRoomPriceSingle) + (totalRoomPriceDouble) - (Decimal.Parse(pdfFormFields.GetField("txtTotalS")) + Decimal.Parse(pdfFormFields.GetField("txtTotalD")))).ToString("F"));
				
				//Novo polje za placanje koje prikazuje ukupan iznos bez umanjenja deposita
                pdfFormFields.SetField("txtFullPaymentTotalPrice", (totalRoomPriceSingle + totalRoomPriceDouble).ToString("F"));
            }

            //------------------------------------------------------------------------
        }
        else
        {
            //-----------------------------------------------------------------------
            //-------------------------Estimated Balance after confirmation date-----------------------------

            pdfFormFields.SetField("txtTotalRN1", totalNumberOfRoomsSingle.ToString());
            pdfFormFields.SetField("txtTotalSN1", totalNumberOfRoomsDouble.ToString());

            pdfFormFields.SetField("txtSPrice1", sprice.ToString("F"));
            pdfFormFields.SetField("txtDPrice1", dprice.ToString("F"));

            pdfFormFields.SetField("txtTotalRNPrice1", ((totalNumberOfRoomsSingle * sprice) / 2).ToString("F"));
            pdfFormFields.SetField("txtTotalSNPrice1", ((totalNumberOfRoomsDouble * dprice) / 2).ToString("F"));

            pdfFormFields.SetField("txtTotalPrice1",
                (((totalNumberOfRoomsSingle * sprice) / 2) + ((totalNumberOfRoomsDouble * dprice) / 2)).ToString("F"));

            pdfFormFields.SetField("txtTotalRN", totalNumberOfRoomsSingle.ToString());
            pdfFormFields.SetField("txtTotalSN", totalNumberOfRoomsDouble.ToString());

            pdfFormFields.SetField("txtSPrice", sprice.ToString("F"));
            pdfFormFields.SetField("txtDPrice", dprice.ToString("F"));

            pdfFormFields.SetField("txtTotalRNPrice", (totalNumberOfRoomsSingle * sprice).ToString("F"));
            pdfFormFields.SetField("txtTotalSNPrice", (totalNumberOfRoomsDouble * dprice).ToString("F"));

            pdfFormFields.SetField("txtTotalDepositePaid", (((totalNumberOfRoomsSingle * sprice) / 2) + ((totalNumberOfRoomsDouble * dprice) / 2)).ToString("F"));

            pdfFormFields.SetField("txtTotalPrice", (((totalNumberOfRoomsSingle * sprice) + (totalNumberOfRoomsDouble * dprice)) - (Decimal.Parse(pdfFormFields.GetField("txtTotalDepositePaid")) + Decimal.Parse(pdfFormFields.GetField("txtTotalD")))).ToString("F"));

            pdfFormFields.SetField("txtTotalPrice", (Decimal.Parse(pdfFormFields.GetField("txtTotalPrice")) + registrationPrice).ToString("F"));

            //------------------------------------------------------------------------
        }

        //Novo polje za placanje koje prikazuje ukupan iznos bez umanjenja deposita
		if (sprice != 0 && dprice != 0)
			pdfFormFields.SetField("txtFullPaymentTotalPrice", ((totalNumberOfRoomsSingle * sprice) + (totalNumberOfRoomsDouble * dprice)).ToString("F"));

        pdfFormFields.SetField("hfButton", "0");
        pdfFormFields.SetField("txtEditedBy", lastEditeBy + " " + System.DateTime.Now.ToShortDateString());

        pdfStamper.FormFlattening = false;

        pdfStamper.Close();


        ZipFile z = ZipFile.Create(Server.MapPath("tempAgreements") + "\\" + documentID_HA + ".zip"); //(filezipname);
        z.BeginUpdate();
        z.Add(Server.MapPath("tempAgreements") + "\\" + newFile, Path.GetFileName(Server.MapPath("tempAgreements") + "\\" + newFile));

        

        if (sidenights)
        {
            DateTime DateSN = System.DateTime.Now;
            string documentID_SN = "G" + Date.ToString("yyMMddhhmmssff");
            //Date.ToString("yyMMdd") + System.DateTime.Now.ToString("hh") + System.DateTime.Now.ToString("mm") + System.DateTime.Now.Ticks.ToString().Substring(System.DateTime.Now.Ticks.ToString().Length - 4, 4);

            string pdfSidenightsTemplate = Server.MapPath("docs") + "\\Sidenights_" + conferenceID + ".pdf";

            string sidenightsNewFile = documentID_SN + "_Sidenights_" + conferenceID + ".pdf";

            snFile = sidenightsNewFile;

            PdfReader pdfSidenightsReader = new PdfReader(pdfSidenightsTemplate);
            PdfStamper pdfSidenightsStamper = new PdfStamper(pdfSidenightsReader,
                new FileStream(Server.MapPath("tempAgreements") + "\\" + sidenightsNewFile, FileMode.Create), '\0', true);

            //PDF Encryption//
            //pdfSidenightsStamper.SetEncryption(true, null, "Dvladh09091976", PdfWriter.ALLOW_FILL_IN | PdfWriter.ALLOW_PRINTING);

            AcroFields pdfSidenightsFormFields = pdfSidenightsStamper.AcroFields;

            pdfSidenightsFormFields.SetField("txtGroupName", txtSponsoringGroupName);
            pdfSidenightsFormFields.SetField("txtAgency", txtAgency);
            pdfSidenightsFormFields.SetField("txtEMail", txtEMail);
            pdfSidenightsFormFields.SetField("txtHotelBooked", ddlHotel1);
            pdfSidenightsFormFields.SetField("txtDocumentID", documentID_SN);
			pdfSidenightsFormFields.SetField("txtMerchant", merchantID);

            foreach (ListItem li in ddlDateFrom.Items)
            {
                if (li.Value != "-1" && li.Attributes["sidenight"].ToString() == "true")
                {
                    if ((DateTime.Parse(li.Value) >= dateFrom) && (DateTime.Parse(li.Value) < dateTo))
                    {
                        if (!editNight)
                        {
                            pdfSidenightsFormFields.SetField("txtGR" + DateTime.Parse(li.Value).Day, numberOfRoomsSingle);
                            totalSidenightsNumberOfRoomsSingle += int.Parse(numberOfRoomsSingle);
                            pdfSidenightsFormFields.SetField("txtSR" + DateTime.Parse(li.Value).Day, numberOfRoomsDouble);
                            totalSidenightsNumberOfRoomsDouble += int.Parse(numberOfRoomsDouble);
                        }
                        else
                        {
                            int sValue = sNights[DateTime.Parse(li.Value).Day];
                            int dValue = dNights[DateTime.Parse(li.Value).Day];

                            pdfSidenightsFormFields.SetField("txtGR" + DateTime.Parse(li.Value).Day, sValue.ToString());
                            totalSidenightsNumberOfRoomsSingle += sValue;

                            pdfSidenightsFormFields.SetField("txtSR" + DateTime.Parse(li.Value).Day, dValue.ToString());
                            totalSidenightsNumberOfRoomsDouble += dValue;
                        }

                    }
                }
            }

            pdfSidenightsFormFields.SetField("txtGRTotal", totalSidenightsNumberOfRoomsSingle.ToString());
            pdfSidenightsFormFields.SetField("txtSRTotal", totalSidenightsNumberOfRoomsDouble.ToString());

            if ((sprice != 0) || (dprice != 0))
            {
                pdfSidenightsFormFields.SetField("txtRoomRate", sprice.ToString("F"));
                pdfSidenightsFormFields.SetField("txtSuiteRate", dprice.ToString("F"));

                pdfSidenightsFormFields.SetField("txtRoomNights", totalSidenightsNumberOfRoomsSingle.ToString());
                pdfSidenightsFormFields.SetField("txtSuiteNights", totalSidenightsNumberOfRoomsDouble.ToString());

                pdfSidenightsFormFields.SetField("txtRoomTotal", (totalSidenightsNumberOfRoomsSingle * sprice).ToString("F"));
                pdfSidenightsFormFields.SetField("txtSuiteTotal", (totalSidenightsNumberOfRoomsDouble * dprice).ToString("F"));

                pdfSidenightsFormFields.SetField("txtTotalAmount", ((totalSidenightsNumberOfRoomsSingle * sprice) + (totalSidenightsNumberOfRoomsDouble * dprice)).ToString("F"));
            }
            else
            {
                Decimal totalRoomPriceSidenightsSingle = 0;
                Decimal totalRoomPriceSidenightsDouble = 0;

                foreach (ListItem li in ddlDateFrom.Items)
                {
                    if (li.Value != "-1" && li.Attributes["sidenight"].ToString() == "true")
                    {
                        if ((DateTime.Parse(li.Value) >= dateFrom) && (DateTime.Parse(li.Value) < dateTo))
                        {
                            if (!editNight)
                            {
                                totalRoomPriceSidenightsSingle += int.Parse(numberOfRoomsSingle) * sPrices[DateTime.Parse(li.Value).Day];
                                totalRoomPriceSidenightsDouble += int.Parse(numberOfRoomsDouble) * dPrices[DateTime.Parse(li.Value).Day];
                                //totalRoomPriceSidenightsSingle += totalSidenightsNumberOfRoomsSingle * sPrices[DateTime.Parse(li.Value).Day];
                                //totalRoomPriceSidenightsDouble += totalSidenightsNumberOfRoomsDouble * dPrices[DateTime.Parse(li.Value).Day];
                            }
                            else
                            {
                                totalRoomPriceSidenightsSingle += sNights[DateTime.Parse(li.Value).Day] * sPrices[DateTime.Parse(li.Value).Day];
                                totalRoomPriceSidenightsDouble += dNights[DateTime.Parse(li.Value).Day] * dPrices[DateTime.Parse(li.Value).Day];
                            }
                        }
                    }
                }

                bool variableRatesSingle = CheckVariableRates(sPrices);
                bool variableRatesDouble = CheckVariableRates(dPrices);

                var singlePriceFromDic = (from spr in sPrices
                                          where spr.Value != 0M
                                          select spr).FirstOrDefault();
                var doublePriceFromDic = (from dpr in dPrices
                                          where dpr.Value != 0M
                                          select dpr).FirstOrDefault();

                pdfSidenightsFormFields.SetField("txtRoomRate", variableRatesSingle ? "VNR" : singlePriceFromDic.Value.ToString("F"));
                pdfSidenightsFormFields.SetField("txtSuiteRate", variableRatesDouble ? "VNR" : doublePriceFromDic.Value.ToString("F"));

                pdfSidenightsFormFields.SetField("txtRoomNights", totalSidenightsNumberOfRoomsSingle.ToString());
                pdfSidenightsFormFields.SetField("txtSuiteNights", totalSidenightsNumberOfRoomsDouble.ToString());

                pdfSidenightsFormFields.SetField("txtRoomTotal", (totalRoomPriceSidenightsSingle).ToString("F"));
                pdfSidenightsFormFields.SetField("txtSuiteTotal", (totalRoomPriceSidenightsDouble).ToString("F"));

                pdfSidenightsFormFields.SetField("txtTotalAmount", ((totalRoomPriceSidenightsSingle) + (totalRoomPriceSidenightsDouble)).ToString("F"));
            }

           

            pdfSidenightsFormFields.SetField("txtUserKey", documentID_SN);
            //pdfSidenightsFormFields.SetField("txtUserKey", hashCode.ToString());

            pdfSidenightsStamper.FormFlattening = false;

            pdfSidenightsStamper.Close();

            if ((totalSidenightsNumberOfRoomsSingle + totalSidenightsNumberOfRoomsDouble) != 0)
                z.Add(Server.MapPath("tempAgreements") + "\\" + sidenightsNewFile,
                    Path.GetFileName(Server.MapPath("tempAgreements") + "\\" + sidenightsNewFile));
            else
                File.Delete(Server.MapPath("tempAgreements") + "\\" + sidenightsNewFile);

        }


        z.CommitUpdate();
        z.Close();

        string text = String.Empty;

        using (FileStream fileStream = File.OpenRead(Server.MapPath("tempAgreements") + "\\" + documentID_HA + ".zip"))
        {
            MemoryStream memStream = new MemoryStream();
            memStream.SetLength(fileStream.Length);
            fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);

            text = Convert.ToBase64String(memStream.ToArray());
        }

        File.Delete(Server.MapPath("tempAgreements") + "\\" + haFile);

        if (sidenights)
            File.Delete(Server.MapPath("tempAgreements") + "\\" + snFile);

        return text + "|" + documentID_HA;

    }


    [WebMethod]
    public string GetConferenceDates(string conferenceID)
    {
        string dates = "";

        DataSet dsConferences = new DataSet();

        System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(Server.MapPath("\\") + "\\" + "App_Data\\Conferences.xml");

        dsConferences.ReadXml(reader);        

        foreach (DataRow dr in dsConferences.Tables[0].Select("conferenceID='" + conferenceID + "'"))
        {
            DataRow[] da = dsConferences.Tables[1].Select("conference_id='" + dr[0] + "'");
                       
            dates = da[0][1].ToString() + "|" + da[da.Length - 1][1].ToString();                                
           
        }       

        return dates;
    }

    protected void FillConferenceDates(string conferenceID, ref DropDownList ddlDateFrom, ref DropDownList ddlDateTo)
    {
        ddlDateFrom.Items.Clear();
        ddlDateTo.Items.Clear();

        DataSet dsConferences = new DataSet();

        System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(Server.MapPath("\\") + "\\" + "App_Data\\Conferences.xml");

        dsConferences.ReadXml(reader);

        ListItem item = new ListItem("Select", "-1");

        foreach (DataRow dr in dsConferences.Tables[0].Select("conferenceID='" + conferenceID + "'"))
        {
            DataRow[] da = dsConferences.Tables[1].Select("conference_id='" + dr[0] + "'");

            for (int i = 0; i < da.Length; i++)
            {
                ListItem liFrom = new ListItem(da[i][1].ToString(), da[i][1].ToString());
                liFrom.Attributes.Add("sidenight", da[i][0].ToString());
                ddlDateFrom.Items.Add(liFrom);

                ListItem liTo = new ListItem(da[i][1].ToString(), da[i][1].ToString());
                liTo.Attributes.Add("sidenight", da[i][0].ToString());
                ddlDateTo.Items.Add(liTo);
            }
        }

        ddlDateFrom.Items.Insert(0, item);
        ddlDateTo.Items.Insert(0, item);
    }

    private string MaximumNumberOfNightsSingleDouble(Dictionary<int, int> sNights, Dictionary<int, int> dNights, DropDownList ddlDateFrom)
    {
        var sumSingleDouble = new ArrayList();

        for (int i = 0; i < sNights.Count; i++)
        {
            if (ddlDateFrom.Items[i + 1].Attributes["sidenight"].ToString() == "false")
            {
                int numberOfNights = sNights[DateTime.Parse(ddlDateFrom.Items[i + 1].Value).Day] + dNights[DateTime.Parse(ddlDateFrom.Items[i + 1].Value).Day];
                sumSingleDouble.Add(numberOfNights);
            }
        }

        sumSingleDouble.Sort();

        sumSingleDouble.Reverse();

        return sumSingleDouble[0].ToString();
    }

    private List<int> MaximumNumberOfNightsSingleDoubleSeparate(Dictionary<int, int> sNights, Dictionary<int, int> dNights, DropDownList ddlDateFrom)
    {
        var sumSingleDoubleNights = new List<int>();
        var singleNights = new List<int>();
        var doubleNights = new List<int>();

        var singleDoublePair = new List<int>();

        for (int i = 0; i < sNights.Count; i++)
        {

            if (ddlDateFrom.Items[i + 1].Attributes["sidenight"].ToString() == "false")
            {
                int numberOfSingleDoubleNights = sNights[DateTime.Parse(ddlDateFrom.Items[i + 1].Value).Day] + dNights[DateTime.Parse(ddlDateFrom.Items[i + 1].Value).Day];
                int numberOfSingleNights = sNights[DateTime.Parse(ddlDateFrom.Items[i + 1].Value).Day];
                int numberOfDoubleNights = dNights[DateTime.Parse(ddlDateFrom.Items[i + 1].Value).Day];

                singleNights.Add(numberOfSingleNights);
                doubleNights.Add(numberOfDoubleNights);
                sumSingleDoubleNights.Add(numberOfSingleDoubleNights);
            }
        }

        var a = singleNights.Count(x => x.Equals(0));
        var b = doubleNights.Count(x => x.Equals(0));

        if ((singleNights.Any(o => o != singleNights[0]) || (doubleNights.Any(o => o != doubleNights[0]))) && (a==0 || b==0))
        {
            var index = sumSingleDoubleNights.IndexOf(sumSingleDoubleNights.Max());

            int singleMax1 = singleNights[index];
            int doubleMax1 = doubleNights[index];

            singleNights.RemoveAt(index);
            doubleNights.RemoveAt(index);
            sumSingleDoubleNights.RemoveAt(index);

            index = sumSingleDoubleNights.IndexOf(sumSingleDoubleNights.Max());

            int singleMax2 = singleNights[index];
            int doubleMax2 = doubleNights[index];

            //singleDoublePair.Add(singleMax1 + singleMax2);
            //singleDoublePair.Add(doubleMax1 + doubleMax2);
			
			singleDoublePair.Add(singleMax1);
            singleDoublePair.Add(doubleMax1);
        }
        else
        {
            var index = sumSingleDoubleNights.IndexOf(sumSingleDoubleNights.Max());
            int singleMax1 = singleNights[index];
            int doubleMax1 = doubleNights[index];

            singleDoublePair.Add(singleMax1);
            singleDoublePair.Add(doubleMax1);
        }
        
       
        return singleDoublePair;
    }

    [WebMethod]
    public string GetPdfDocument(string conferenceID, string documentID_HA)
    {
        string text = String.Empty;

        if (!documentID_HA.Contains("View_"))
            documentID_HA = "View_" + documentID_HA;

        try
        {

            using (FileStream fileStream = File.OpenRead(Server.MapPath("agreements") + "\\" + conferenceID + "\\" + documentID_HA + ".pdf"))
            {
                var memStream = new MemoryStream();
                memStream.SetLength(fileStream.Length);
                fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);

                text = Convert.ToBase64String(memStream.ToArray());
            }

        }
        catch (Exception ex)
        {
            text = "404"; //Raise Not found error
        }

        return text;
    }

	protected bool CheckIfConferenceIsAvailable(string conferenceID)
    {
        System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(Server.MapPath("\\") + "\\" + "App_Data\\Conferences.xml");
        DataSet dsConferences = new DataSet();
        dsConferences.ReadXml(reader);

        bool isAvailable = false;

        foreach (DataRow dr in dsConferences.Tables[0].Select("conferenceID='" + conferenceID + "'"))
        {
            isAvailable = bool.Parse(dr[3].ToString());           
        }

        return isAvailable;
    }
	
	public DataTable GetPdfDocumentsByCodes(string conferenceID, string[] codes)
    {

        string[] strFiles = Directory.GetFiles(Server.MapPath("/agreements/" + conferenceID), "*.pdf",
            SearchOption.TopDirectoryOnly).Where(s => !s.Contains("View_")).ToArray();

        DataTable dtFiles = new DataTable("PdfDocuments");


        DataColumn dt1 = new DataColumn();
        dt1.ColumnName = "Code";
        dt1.DataType = Type.GetType("System.String");

        DataColumn dt2 = new DataColumn();
        dt2.ColumnName = "Name";
        dt2.DataType = Type.GetType("System.String");

        DataColumn dt3 = new DataColumn();
        dt3.ColumnName = "Date";
        dt3.DataType = Type.GetType("System.DateTime");

        DataColumn dt4 = new DataColumn();
        dt4.ColumnName = "DepositTransactionID";
        dt4.DataType = Type.GetType("System.String");

        DataColumn dt5 = new DataColumn();
        dt5.ColumnName = "DepositAmount";
        dt5.DataType = Type.GetType("System.String");

        DataColumn dt6 = new DataColumn();
        dt6.ColumnName = "FinalBalanceTransactionID";
        dt6.DataType = Type.GetType("System.String");

        DataColumn dt7 = new DataColumn();
        dt7.ColumnName = "FinalBalanceAmount";
        dt7.DataType = Type.GetType("System.String");

        DataColumn dt8 = new DataColumn();
        dt8.ColumnName = "CardHolderName";
        dt8.DataType = Type.GetType("System.String");

        DataColumn dt9 = new DataColumn();
        dt9.ColumnName = "Email";
        dt9.DataType = Type.GetType("System.String");

        DataColumn dt12 = new DataColumn();
        dt12.ColumnName = "Conference";
        dt12.DataType = Type.GetType("System.String");

        dtFiles.Columns.Add(dt1);
        dtFiles.Columns.Add(dt2);
        dtFiles.Columns.Add(dt3);
        dtFiles.Columns.Add(dt4);
        dtFiles.Columns.Add(dt5);
        dtFiles.Columns.Add(dt6);
        dtFiles.Columns.Add(dt7);
        dtFiles.Columns.Add(dt8);
        dtFiles.Columns.Add(dt9);
        dtFiles.Columns.Add(dt12);

        foreach (string file in strFiles)
        {

            DataRow row = dtFiles.NewRow();

            FileInfo fi = new FileInfo(file);
            string name = fi.Name;
            DateTime creationDate = fi.CreationTime; //fi.LastWriteTime; //fi.CreationTime; 
            string code = name.Substring(name.LastIndexOf("_") + 1).Replace(".pdf", "");

            if (!codes.Contains(code)) continue;

            row["Code"] = code;
            row["Name"] = name;
            row["Date"] = creationDate;
            row["Conference"] = conferenceID;

            ExtractHousingAgreementContext(code, conferenceID, name, ref row);

            dtFiles.Rows.Add(row);
        }

        return dtFiles;
        //return JsonConvert.SerializeObject(dtFiles);
    }

    protected void ExtractHousingAgreementContext(string code, string conferenceId, string fileName, ref DataRow row)
    {
        using (MemoryStream ms = new MemoryStream())
        {
            PdfReader reader = new PdfReader(Server.MapPath("/agreements/" + conferenceId + "/" + fileName));
            PdfStamper stamper = new PdfStamper(reader, ms);

            if (!Server.MapPath("/agreements/" + conferenceId + "/" + fileName).Contains("View_"))
            {
                try
                {
                    AcroFields pdfFormFields = stamper.AcroFields;
                    AcroFields.Item item = pdfFormFields.GetFieldItem("txtDepositTransactionID");
                    AcroFields.Item item1 = pdfFormFields.GetFieldItem("txtFinalBalanceTransactionID");

                    bool sidenights = bool.Parse(pdfFormFields.GetField("txtSidenights"));

                    if (item == null)
                    {
                        row["DepositTransactionID"] = "N/A";
                        row["DepositAmount"] = "0.00";
                    }
                    else
                    {
                        if (!sidenights)
                        {
                            row["DepositTransactionID"] = pdfFormFields.GetField("txtDepositTransactionID") == "0" ? "N/A" : pdfFormFields.GetField("txtDepositTransactionID");
                            row["DepositAmount"] = pdfFormFields.GetField("txtTotalSD");
                        }
                        else
                        {
                            row["DepositTransactionID"] = "N/A";
                            row["DepositAmount"] = "0.00";
                        }
                    }

                    if (item1 == null)
                    {
                        row["FinalBalanceTransactionID"] = "N/A";
                        row["FinalBalanceAmount"] = "N/A";
                    }
                    else
                    {
                        if (!sidenights)
                        {
                            row["FinalBalanceTransactionID"] = pdfFormFields.GetField("txtFinalBalanceTransactionID") == "0" ? "N/A" : pdfFormFields.GetField("txtFinalBalanceTransactionID");
                            row["FinalBalanceAmount"] = pdfFormFields.GetField("txtTotalPrice");
                        }
                        else
                        {
                            row["FinalBalanceTransactionID"] = pdfFormFields.GetField("txtFinalBalanceTransactionID") == "0" ? "N/A" : pdfFormFields.GetField("txtFinalBalanceTransactionID");
                            row["FinalBalanceAmount"] = pdfFormFields.GetField("txtTotalAmount");
                        }
                    }

                    row["CardHolderName"] = pdfFormFields.GetField("txtCardholderName");

                    row["Email"] = pdfFormFields.GetField("txtEMail");
                }
                catch (Exception ex)
                {

                }
            }

            stamper.Close();
        }
    }
}
