﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for HotelAvailabilityManager
/// </summary>
public class HotelAvailabilityManager
{
    public HotelAvailabilityManager()
    {
        
    }

    private Availability.HotelAvailability LoadHotelsByConference(string conference)
    {
        Availability.HotelAvailability conferenceHotels = new Availability.HotelAvailability();

        try
        {
            conferenceHotels.ReadXml(HttpContext.Current.Server.MapPath("~/App_Data/" + conference + "/" + conference + ".xml"), XmlReadMode.ReadSchema);
        }
        catch(Exception){ }

        if(conferenceHotels.SRoomBlock.Count > 0)
        {
            return conferenceHotels;
        }
        else
        {
            return null;
        }
    }

    public void GetHotelsInformationforConference(string conference, ref DropDownList ddlHotels, ref DropDownList ddlHotels1, ref DropDownList ddlHotels2) 
    {
        ddlHotels.Items.Clear();
        ddlHotels1.Items.Clear();
        ddlHotels2.Items.Clear();

        Availability.HotelAvailability dsHotels = LoadHotelsByConference(conference);

        if(dsHotels == null) { return; }  
        
        var hotelDistinctListGroupedByCode = dsHotels.SRoomBlock.GroupBy(item => item.Code).Select(grp => grp.OrderBy(item => item.HotelName).First());        

        foreach (var hotel in hotelDistinctListGroupedByCode)
        {
            ListItem liHotels = new ListItem(hotel.HotelName, hotel.Code.ToString());
            ListItem liHotels1 = new ListItem(hotel.HotelName, hotel.Code.ToString());
            ListItem liHotels2 = new ListItem(hotel.HotelName, hotel.Code.ToString());            

            ddlHotels.Items.Add(liHotels);
            ddlHotels1.Items.Add(liHotels1);
            ddlHotels2.Items.Add(liHotels2);
        }

        ListItem selectItem = new ListItem("Select", "-1");

        ddlHotels.Items.Insert(0, selectItem);
        ddlHotels.DataBind();

        ddlHotels1.Items.Insert(0, selectItem);
        ddlHotels1.DataBind();

        ddlHotels2.Items.Insert(0, selectItem);
        ddlHotels2.DataBind();

        if (HttpContext.Current.Session["HotelSelection"] != null)
        {
            foreach (ListItem li1 in ddlHotels.Items)
            {               
                string hotelCode = li1.Value != "-1" ? li1.Value.ToString() : "0";

                //if (hotel.Contains(HttpContext.Current.Session["HotelSelection"].ToString()))
                if (hotelCode == HttpContext.Current.Session["HotelCodeSelection"].ToString())
                {
                    ((Page)HttpContext.Current.Handler).ClientScript.RegisterStartupScript(this.GetType(), "101", "$('#ddlHotels').val(" + li1.Value + "); $('#ddlHotels').change(); $('#ddlHotels1').val(-1); $('#ddlHotels2').val(-1);", true);
                    break;
                }
            }

        } 

    }

    public void LoadRoomTypesInformationforHotel(string conference, string hotelCode, ref HtmlSelect ddlRoomTypeSingle, ref HtmlSelect ddlRoomTypeDouble)
    {
        Availability.HotelAvailability dsHotels = LoadHotelsByConference(conference);      
        

        var hotelRoomTypesByHotelCode = from srb in dsHotels.SRoomBlock
                                        join srbd in dsHotels.RoomBlockDetails on srb.SRoomBlock_Id equals srbd.SRoomBlock_Id
                                        join srbds in dsHotels.SRoomBlockDetails on srbd.RoomBlockDetails_Id equals srbds.RoomBlockDetails_Id
                                        where srb.Code == ushort.Parse(hotelCode)
                                        select new
                                        {                                           
                                            Name = srb.RoomBlockName,                                             
                                            SellingPrice = srb.VariableRates ? srb.SellingPrice : srbds.SellingPrice, 
                                            Occypancy = srb.Occupancy,
                                            IDServiceItem = srb.IDServiceItem
                                        };

        var hotelRpoomTypesGroupedByHotel = hotelRoomTypesByHotelCode.GroupBy(item => item.Name).Select(grp => grp.OrderBy(item => item.Name).First());

        ddlRoomTypeSingle.Items.Clear();
        ddlRoomTypeDouble.Items.Clear();

        foreach (var roomType in hotelRpoomTypesGroupedByHotel)
        {
            ListItem li = new ListItem(roomType.Name + " (" + "$" + roomType.SellingPrice + ")", roomType.Occypancy.ToString());
            li.Attributes.Add(roomType.Occypancy == 1 ? "sglPrice" : "dblPrice", roomType.SellingPrice.ToString());
            li.Attributes.Add("IDServiceItem", roomType.IDServiceItem.ToString());

            if (roomType.Occypancy == 1)
            {
                ddlRoomTypeSingle.Items.Add(li);
            }
            else
            {
                ddlRoomTypeDouble.Items.Add(li);
            }
        }

        ListItem selectItemSingle = new ListItem("Select", "-1");
        ListItem selectItemDouble = new ListItem("Select", "-1");

        ddlRoomTypeSingle.Items.Insert(0, selectItemSingle);
        ddlRoomTypeDouble.Items.Insert(0, selectItemDouble);


        ddlRoomTypeSingle.DataBind();
        ddlRoomTypeDouble.DataBind();   

    }
}