using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public static class Helpers
{
    public static string EncodeString(string input)
    {
        return HttpServerUtility.UrlTokenEncode(System.Text.Encoding.UTF8.GetBytes(input));
    }

    public static string DecodeString(string input)
    {
        return System.Text.Encoding.UTF8.GetString(HttpServerUtility.UrlTokenDecode(input));
    }

    /// <summary>
    /// DecodeFrom64
    /// </summary>
    /// <param name="encodedData"></param>
    /// <returns></returns>
    public static string DecodeFrom64(string encodedData)
    {

        byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);

        string returnValue = System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);

        return returnValue;

    }

    /// <summary>
    /// DecodeTo64
    /// </summary>
    /// <param name="decodeData"></param>
    /// <returns></returns>
    public static string DecodeTo64(string decodeData)
    {
        byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(decodeData);

        string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);

        return returnValue;
    }

    /// <summary>
    /// Add n times X
    /// </summary>
    /// <param name="n"></param>
    /// <returns></returns>
    public static string AddX(int n)
    {
        return new string('X', n);
    }

    public static string CreateAdobeAcrobatFdfResponseMessage(string m)
    {
        string message = @"%FDF-1.2
                            %����
                            1 0 obj
                            <<
                            /FDF
                                <<
                                /Status(" + m + @".)
                                >>
                            >>
                            endobj
                            trailer
                            <</Root 1 0 R>>
                            %%EOF";

        return message;
    }
}

public class NameValuePair
{
    private string _Name, _Value;

    public string Text
    {
        get { return _Name; }
        set { _Name = value; }
    }

    public string Value
    {
        get { return _Value; }
        set { _Value = value; }
    }

    public NameValuePair(string name, string value)
    {
        _Name = name;
        _Value = value;
    }
}

/// <summary>
/// Summary description for DatabaseConnection
/// </summary>
public class DatabaseConnection
{      
    /// <summary>
    /// GetGatewayID
    /// </summary>
    /// <returns></returns>
    public static string GetGatewayID(Merchant merchant)
    {
        switch ((int)merchant)
        {
            case 1: return ConfigurationManager.AppSettings["GatewayID1"].ToString();
                break;
            case 2: return ConfigurationManager.AppSettings["GatewayID2"].ToString();
                break;
            case 3: return ConfigurationManager.AppSettings["GatewayID3"].ToString();
                break;
            case 4: return ConfigurationManager.AppSettings["GatewayID4"].ToString();
                break;
            case 5: return ConfigurationManager.AppSettings["GatewayID5"].ToString();
                break;
            default: return ConfigurationManager.AppSettings["GatewayID1"].ToString();
                break;
        }
    }

    /// <summary>
    /// GetGatewayPassword
    /// </summary>
    /// <returns></returns>
    public static string GetGatewayPassword(Merchant merchant)
    {
        switch ((int)merchant)
        {
            case 1: return ConfigurationManager.AppSettings["GatewayPassword1"].ToString();
                break;
            case 2: return ConfigurationManager.AppSettings["GatewayPassword2"].ToString();
                break;
            case 3: return ConfigurationManager.AppSettings["GatewayPassword3"].ToString();
                break;
            case 4: return ConfigurationManager.AppSettings["GatewayPassword4"].ToString();
                break;
            case 5: return ConfigurationManager.AppSettings["GatewayPassword5"].ToString();
                break;
            default: return ConfigurationManager.AppSettings["GatewayPassword1"].ToString();
                break;
        }
    }

    /// <summary>
    /// GetHashID
    /// </summary>
    /// <param name="merchant"></param>
    /// <returns></returns>
    public static string GetHashID(Merchant merchant)
    {
        switch ((int)merchant)
        {
            case 1: return ConfigurationManager.AppSettings["keyID1"].ToString();
                break;
            case 2: return ConfigurationManager.AppSettings["keyID2"].ToString();
                break;
            case 3: return ConfigurationManager.AppSettings["keyID3"].ToString();
                break;
            case 4: return ConfigurationManager.AppSettings["keyID4"].ToString();
                break;
            case 5: return ConfigurationManager.AppSettings["keyID5"].ToString();
                break;
            default: return ConfigurationManager.AppSettings["keyID1"].ToString();
                break;
        }
    }

    /// <summary>
    /// GetHashPassword
    /// </summary>
    /// <param name="merchant"></param>
    /// <returns></returns>
    public static string GetHashPassword(Merchant merchant)
    {
        switch ((int)merchant)
        {
            case 1: return ConfigurationManager.AppSettings["key1"].ToString();
                break;
            case 2: return ConfigurationManager.AppSettings["key2"].ToString();
                break;
            case 3: return ConfigurationManager.AppSettings["key3"].ToString();
                break;
            case 4: return ConfigurationManager.AppSettings["key4"].ToString();
                break;
            case 5: return ConfigurationManager.AppSettings["key5"].ToString();
                break;
            default: return ConfigurationManager.AppSettings["key1"].ToString();
                break;
        }
    }

    /// <summary>
    /// GetPaymentAuthorizationEmail
    /// </summary>
    /// <returns></returns>
    public static string GetPaymentAuthorizationEmail()
    {
        return ConfigurationManager.AppSettings["PaymentAuthorizationEmail"].ToString();
    }

    /// <summary>
    /// GetSMTPServer
    /// </summary>
    /// <returns></returns>
    public static string GetSMTPServer()
    {
        return ConfigurationManager.AppSettings["SMTPServer"].ToString();
    }

    /// <summary>
    /// GetUserPaymentAuthorizationEmail
    /// </summary>
    /// <returns></returns>
    public static string GetUserPaymentAuthorizationEmail()
    {
        return ConfigurationManager.AppSettings["UserPaymentAuthorizationEmail"].ToString();
    }

    public static string GetMerchantName()
    {
        return ConfigurationManager.AppSettings["MerchantName"].ToString();
    }

    public static string GetMerchantAddress()
    {
        return ConfigurationManager.AppSettings["MerchantAddress"].ToString();
    }

    public static string GetMerchantCity()
    {
        return ConfigurationManager.AppSettings["MerchantCity"].ToString();
    }

    public static string GetMerchantProvance()
    {
        return ConfigurationManager.AppSettings["MerchantProvance"].ToString();
    }

    public static string GetMerchantPostal()
    {
        return ConfigurationManager.AppSettings["MerchantPostal"].ToString();
    }

    public static string GetMerchantCountry()
    {
        return ConfigurationManager.AppSettings["MerchantCountry"].ToString();
    }

    public static string GetMerchantURL()
    {
        return ConfigurationManager.AppSettings["MerchantURL"].ToString();
    }

    public static string GetPlanetPaymentServerIP()
    {
        return ConfigurationManager.AppSettings["PlanetPaymentServerIP"].ToString();
    }

    public static int GetPlanetPaymentServerPort()
    {
        return int.Parse(ConfigurationManager.AppSettings["PlanetPaymentServerPort"].ToString());
    }

    public static int GetPlanetPaymentTimeOut()
    {
        return int.Parse(ConfigurationManager.AppSettings["PlanetPaymentTimeOut"].ToString());
    }

    public static string GetPlanetPaymentCompanyKey(Merchant companyName)
    {
        var merch = ((Merchant)companyName).ToString();
        var company = ((Merchant)companyName).ToString().ToLower().Substring(0, 3);
        string amex = merch.Contains("Amex") ? "Amex" : "";

        switch (company)
        {
            case "igd": return ConfigurationManager.AppSettings["PlanetPaymentCompanyKeyIGD" + amex].ToString();
            case "cmr": return ConfigurationManager.AppSettings["PlanetPaymentCompanyKeyCMR" + amex].ToString();
            case "igh": return ConfigurationManager.AppSettings["PlanetPaymentCompanyKeyIGH" + amex].ToString();
            default: return "";
        }

    }

    public static string GetPlanetPaymentTerminalId(Merchant merchant)
    {
        var merch = ((Merchant)merchant).ToString();
        var companyName = ((Merchant)merchant).ToString().ToLower().Substring(0, 3);
        string amex = merch.Contains("Amex") ? "Amex" : "";

        switch (companyName)
        {
            case "igd": return ConfigurationManager.AppSettings["PlanetPaymentTerminalIdIGD" + amex].ToString();
            case "cmr": return ConfigurationManager.AppSettings["PlanetPaymentTerminalIdCMR" + amex].ToString();
            case "igh": return ConfigurationManager.AppSettings["PlanetPaymentTerminalIdIGH" + amex].ToString();
            default: return "";
        }

    }

    public static string GetPlanetPaymentEncryptionKey(Merchant companyName, PLanetPaymentEncryptionKeys keyNumber)
    {
        var merch = ((Merchant)companyName).ToString();
        var company = ((Merchant)companyName).ToString().ToLower().Substring(0, 3);
        string amex = merch.Contains("Amex") ? "Amex" : "";

        switch (company)
        {
            case "igd": return ConfigurationManager.AppSettings["PlanetPaymentEncryptionKeyIGD" + amex + (int)keyNumber].ToString();
            case "cmr": return ConfigurationManager.AppSettings["PlanetPaymentEncryptionKeyCMR" + amex + (int)keyNumber].ToString();
            case "igh": return ConfigurationManager.AppSettings["PlanetPaymentEncryptionKeyIGH" + amex + (int)keyNumber].ToString();
            default: return "";
        }

    }

    public static string GetDefaultCurrency()
    {
        return ConfigurationManager.AppSettings["DefaultCurrency"].ToString();
    }

    public enum PLanetPaymentEncryptionKeys
    {
        Key1 = 1,
        Key2 = 2,
        Key3 = 3
    }
}

public enum Merchant
{
    //Test Terminals
    //Demo0955TermRetail = 1,
    //Demo0955TermEcomm = 2,
    //Demo0955TermMoto = 3  

    //Live Terminals
    IgdInternationlGroup = 1,
    CmrGlobalGroupService = 2,
    AbtsConventionServices = 3,
    InternationalGroupHousing = 4,
    Swme = 5,
    IgdInternationlGroup_PP = 6,
    CmrGlobalGroupService_PP = 7,
    IghInternationalGroupHousing_PP = 8,
	CmrGlobalGroupService_PP_Amex = 9,
	IgdInternationlGroup_PP_Amex = 10,
    IghInternationalGroupHousing_PP_Amex = 11
}