﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using OfficeOpenXml;
using System.IO;

/// <summary>
/// Summary description for ExcelExport
/// </summary>
public class ExcelExport
{

    public static bool ExportToExcel(DataTable table, string path, string fileName)
    {
        if(File.Exists(path + fileName + @".xlsx"))
        {
            File.Delete(path + fileName + @".xlsx"); 
        }

        Boolean success = false;

        string putanja = path;
        FileInfo newFile = new FileInfo(putanja + fileName + @".xlsx");
         
        using (ExcelPackage xlPackage = new ExcelPackage(newFile))
        {
            DataTable dtExcel = table;
           
            ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets.Add(fileName);
          
            int i = 1;

            foreach (DataColumn dc in dtExcel.Columns)
            {
                if (dc.ColumnName.ToLower().Contains("id"))
                {
                    continue;
                }
                else
                {
                    worksheet.Cell(1, i).Value = dc.ColumnName;
                }

                i++;
            }

            int p = 2;
            int columns = dtExcel.Columns.Count;
            int k = 1;
            foreach (DataRow dr in dtExcel.Rows)
            {
                for (int j = 0; j < columns; j++)
                {
                    if (dr.Table.Columns[j].ColumnName.ToLower().Contains("id"))
                    {

                    }
                    else
                    {

                        worksheet.Cell(p, k).Value = dr[j].ToString();

                        //if (dr[j].ToString().Contains("\'"))
                        //{
                        //    worksheet.Cell(p, k).Value = dr[j].ToString().Replace("'", " ");
                        //}
                        //else
                        //{
                        //    worksheet.Cell(p, k).Value = dr[j].ToString();
                        //}

                        k++;
                    }
                }

                p++;
                k = 1;
            }

            xlPackage.Save();
            success = true;

            return success;
        }
    }
}
