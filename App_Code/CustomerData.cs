using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.Common;

/// <summary>
/// Summary description for CustomerData
/// </summary>
public class CustomerData
{
    public string Username, FirstName, LastName, Email, Company, Phone;
    public bool IsLoggedIn = false, Activated;
    public Int64 ID;

    private DBConn DBC;
	public CustomerData()
	{
        DBC = new DBConn(HttpContext.Current.Session);
	}

    public void LoadData(Int64 CID)
    {
        ID = CID;
        DbDataReader DR;
        DbCommand cmd = DBC.conn.CreateCommand();

        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "SELECT * FROM Customer WHERE IDCustomer = "+CID.ToString();

        DR = cmd.ExecuteReader();

        if(DR.Read()) {
            Username = DR["Username"].ToString();
            FirstName = DR["FirstName"].ToString();
            LastName = DR["LastName"].ToString();
            Email = DR["Email"].ToString();
            Phone = DR["Phone"].ToString();
            Company = DR["Company"].ToString();
            Activated = bool.Parse(DR["Activated"].ToString());
            if (Activated) IsLoggedIn = true;
        }

        DR.Close();
    }

    public void UnloadData()
    {
        FirstName = LastName = Email = Company = Phone = "";
        Activated = false;
        IsLoggedIn = false;
    }
}


