﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Configuration;
using System.Net.Mail;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using FirstDataGlobalGatewayE4;
using System.Text.RegularExpressions;
using System.Xml.Linq;

/// <summary>
/// Summary description for EmailHelper
/// </summary>
public class EmailHelper
{
    public static bool SendClientEmail(string subject, string clientEmail, string emailFrom, string clientBody, string pdfPath)
    {
        //clientEmail = "itsupport@abtscs.com";

        System.Net.Mail.Attachment attach = new Attachment(pdfPath);

        System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();

        msg.To.Add(clientEmail);
        msg.CC.Add("sysadmin@abtscs.com");

        msg.From = new MailAddress(emailFrom, subject, System.Text.Encoding.UTF8);
        msg.Subject = subject;

        msg.IsBodyHtml = true;

        msg.Body = clientBody;

        msg.Attachments.Add(attach);

        var SC = new SmtpClient(DatabaseConnection.GetSMTPServer());

        SC.DeliveryMethod = SmtpDeliveryMethod.Network;
        SC.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPServerUserName"].ToString(), ConfigurationManager.AppSettings["SMTPServerPassword"].ToString());
        SC.EnableSsl = true;
        SC.Port = 587;

        try
        {
            SC.Send(msg);            
            return true;
        }
        catch (Exception ex)
        {
            ((Page)HttpContext.Current.Handler).ClientScript.RegisterStartupScript(((Page)HttpContext.Current.Handler).GetType(), "333", "alert('" + ex.Message + "');", true);            
            return false;
        }
    }

    public static bool SendConferenceManagerEmail(string subject, string emailTo, string emailFrom, string conferenceManagerBody, string pdfPath, string validation)
    {
        //emailTo = "itsupport@abtscs.com";

        System.Net.Mail.Attachment attach = new Attachment(pdfPath);

        System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();

        msg.To.Add(emailTo);
        msg.CC.Add("sysadmin@abtscs.com");

        msg.From = new MailAddress(emailFrom, subject, System.Text.Encoding.UTF8);
        msg.Subject = subject + "(TOKENIZATION ENABLED)";

        msg.IsBodyHtml = true;

        msg.Body = conferenceManagerBody;
        msg.Body += " Date: " + System.DateTime.Now;
        msg.Body += "<br/><br/><br/>" + validation;

        msg.Attachments.Add(attach);

        var SC = new SmtpClient(DatabaseConnection.GetSMTPServer());

        SC.DeliveryMethod = SmtpDeliveryMethod.Network;
        SC.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPServerUserName"].ToString(), ConfigurationManager.AppSettings["SMTPServerPassword"].ToString());
        SC.EnableSsl = true;
        SC.Port = 587;


        try
        {
            SC.Send(msg);
            
            return true;
        }
        catch (Exception ex)
        {
            ((Page)HttpContext.Current.Handler).ClientScript.RegisterStartupScript(((Page)HttpContext.Current.Handler).GetType(), "333", "alert('" + ex.Message + "');", true);
           
            return false;
        }
    }

    public static bool SendUserPaymentDetailsFirstData(TransactionResult result)
    {
        var msg = new System.Net.Mail.MailMessage();

        msg.CC.Add(DatabaseConnection.GetPaymentAuthorizationEmail());
        msg.CC.Add("sysadmin@abtscs.com");

        msg.From = new MailAddress("info@internationalgroupsdepartment.com", "Payment submitted", System.Text.Encoding.UTF8);
        msg.Subject = "Payment submitted" + "(TOKENIZATION ENABLED)";

        msg.IsBodyHtml = true;

        msg.Body = result.MerchantName + "<br/>";
        msg.Body += result.MerchantAddress + "<br/>";
        msg.Body += result.MerchantCountry + "," + result.MerchantCity + "," + result.MerchantProvince + "," + result.MerchantPostal + "<br/>";
        msg.Body += result.MerchantURL + "<br/><br/><br/>";

        msg.Body += "Transaction Type :" + result.Transaction_Type + "<br/>";
        msg.Body += "Amount :" + result.DollarAmount + "<br/><br/><br/>";

        msg.Body += "Card number:" + result.Card_Number + "<br/>";
        msg.Body += "Card Holder :" + result.CardHoldersName + "<br/><br/>";

        msg.Body += "Customer reference :" + result.Customer_Ref + "<br/>";
        msg.Body += "Reference #3 :" + result.Reference_3 + "<br/>";
        msg.Body += "Reference number :" + result.Reference_No + "<br/><br/>";

        msg.Body += "Transaction Error :" + result.Transaction_Error + "<br/>";
        msg.Body += "Transaction ID :" + result.Transaction_Tag + "<br/>";
        msg.Body += "Transaction Type :" + result.Transaction_Type + "<br/>";
        msg.Body += "Transaction Approved :" + result.Transaction_Approved + "<br/><br/>";

        msg.Body += "Bank Message :" + result.Bank_Message + "<br/>";
        msg.Body += "Bank Response Code :" + result.Bank_Resp_Code + "<br/>";
        msg.Body += "EXact Message :" + result.EXact_Message + "<br/>";
        msg.Body += "EXact Response Code :" + result.EXact_Resp_Code + "<br/>";
        //msg.Body += "Error Description :" + result.Error_Description + "<br/>";
        //msg.Body += "Error Number :" + result.Error_Number + "<br/>";

        msg.Body += "<br/><br/>Do not reply to this email";
        var client = new SmtpClient(DatabaseConnection.GetSMTPServer());

        client.DeliveryMethod = SmtpDeliveryMethod.Network;
        client.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPServerUserName"].ToString(), ConfigurationManager.AppSettings["SMTPServerPassword"].ToString());
        client.EnableSsl = true;
        client.Port = 587;

        try
        {
            client.Send(msg);
            return true;
        }
        catch (Exception ex)
        {
            //((Page)HttpContext.Current.Handler).ClientScript.RegisterStartupScript(((Page)HttpContext.Current.Handler).GetType(), "333", "alert('" + ex.Message + "');", true);
            return false;
        }

    }
	
	public static bool SendUserPaymentDetailsFirstDataChargeAdmin(TransactionResult result)
    {
        var msg = new System.Net.Mail.MailMessage();

        msg.CC.Add("sysadmin@abtscs.com");

        msg.From = new MailAddress("info@internationalgroupsdepartment.com", "Payment successfully submitted", System.Text.Encoding.UTF8);
        msg.Subject = "Payment successfully submitted" + "(TOKENIZATION ENABLED)";

        msg.IsBodyHtml = true;

        msg.Body = result.Bank_Message + "<br/>";
        msg.Body += "========== TRANSACTION RECORD ==========" + "<br/>";
        msg.Body += result.MerchantName + "<br/>";
        msg.Body += result.MerchantAddress + "<br/>";
        msg.Body += result.MerchantCity + "," + result.MerchantProvince + "," + result.MerchantPostal + "<br/>";
        msg.Body += result.MerchantCountry + "<br/>";
        msg.Body += result.MerchantURL + "<br/><br/>";

        msg.Body += "TYPE: Purchase<br/><br/>";

        msg.Body += "ACCT: " + result.CardType + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "$ " + result.DollarAmount + " " + result.Currency + "<br/><br/><br/>";

        //msg.Body += "Transaction Type :" + result.Transaction_Type + "<br/>";
        //msg.Body += "Amount :" + result.DollarAmount + "<br/><br/><br/>";
        //msg.Body += "Card Type:  " + result.CardType + "<br/>";

        msg.Body += "CARDHOLDER NAME:" + result.CardHoldersName + "<br/>";
        msg.Body += "CARD NUMBER:" + "############" + result.TransarmorToken.Substring(result.TransarmorToken.Length - 4) + "<br/>";
        msg.Body += "DATE/TIME:" + System.DateTime.Now.ToString("dd MMMM yy H:mm:ss") + "<br/>";
        msg.Body += "REFERENCE #:" + result.Retrieval_Ref_No + "<br/>";
        msg.Body += "AUTHOR. #:" + result.Authorization_Num + "<br/>";

        msg.Body += "CUSTOMER REFERENCE #:" + result.Customer_Ref + "<br/>";

        if (result.Reference_3.Length > 0)
            msg.Body += "REFERENCE #3:" + result.Reference_3 + "<br/>";

        msg.Body += "REFERENCE NUMBER #:" + result.Reference_No + "<br/><br/>";

        //msg.Body += "Transaction Error:" + result.Transaction_Error + "<br/>";
        //msg.Body += "Transaction ID :" + result.Transaction_Tag + "<br/>";
        //msg.Body += "Transaction Type :" + result.Transaction_Type + "<br/>";
        //msg.Body += "Transaction Approved :" + result.Transaction_Approved + "<br/><br/>";

        //msg.Body += "Bank Message :" + result.Bank_Message + "<br/>";
        //msg.Body += "Bank Response Code :" + result.Bank_Resp_Code + "<br/>";
        //msg.Body += "EXact Message :" + result.EXact_Message + "<br/>";
        //msg.Body += "EXact Response Code :" + result.EXact_Resp_Code + "<br/>";
        //msg.Body += "Error Description :" + result.Error_Description + "<br/>";
        //msg.Body += "Error Number :" + result.Error_Number + "<br/>";

        msg.Body += result.Bank_Message + "-" + " Thank You " + result.Bank_Resp_Code + "<br/><br/>";

        //msg.Body += "<br/><br/>Do not reply to this email<br/><br/>";

        msg.Body += "Please retain this copy for your records<br/><br/>";
        msg.Body += "Cardholder will pay above amount to<br/>card issuer pursuant to cardholder<br/>agreement.<br/>";
        msg.Body += "========================================";

        //msg.Body = result.MerchantName + "<br/>";
        //msg.Body += result.MerchantAddress + "<br/>";
        //msg.Body += result.MerchantCountry + "," + result.MerchantCity + "," + result.MerchantProvince + "," + result.MerchantPostal  + "<br/>";
        //msg.Body += result.MerchantURL + "<br/><br/><br/>";

        //msg.Body += "Transaction Type :" + result.Transaction_Type + "<br/>";
        //msg.Body += "Amount : $" + result.DollarAmount + " " + result.Currency + "<br/><br/>";

        //msg.Body += "Card Type:  " + result.CardType + "<br/>";
        //msg.Body += "Card number:" + "XXXXXXXXXXXX-" + result.TransarmorToken.Substring(result.TransarmorToken.Length - 4) + "<br/>";
        //msg.Body += "Card Holder :" + result.CardHoldersName + "<br/><br/>";

        //msg.Body += "Customer reference :" + result.Customer_Ref + "<br/>";
        //msg.Body += "Reference #3 :" + result.Reference_3 + "<br/>";
        //msg.Body += "Reference number :" + result.Reference_No + "<br/><br/>";

        //msg.Body += "Transaction Error :" + result.Transaction_Error + "<br/>";
        //msg.Body += "Transaction ID :" + result.Transaction_Tag + "<br/>";
        //msg.Body += "Transaction Type :" + result.Transaction_Type + "<br/>";
        //msg.Body += "Transaction Approved :" + result.Transaction_Approved + "<br/><br/>";

        //msg.Body += "Bank Message :" + result.Bank_Message + "<br/>";
        //msg.Body += "Bank Response Code :" + result.Bank_Resp_Code + "<br/>";
        //msg.Body += "EXact Message :" + result.EXact_Message + "<br/>";
        //msg.Body += "EXact Response Code :" + result.EXact_Resp_Code + "<br/>";
        ////msg.Body += "Error Description :" + result.Error_Description + "<br/>";
        ////msg.Body += "Error Number :" + result.Error_Number + "<br/>";
        //msg.Body += "<br/><br/>Do not reply to this email";

        var client = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"].ToString());

        client.DeliveryMethod = SmtpDeliveryMethod.Network;
        client.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPServerUserName"].ToString(), ConfigurationManager.AppSettings["SMTPServerPassword"].ToString());
        client.EnableSsl = true;
        client.Port = 587;


        try
        {
            client.Send(msg);
            return true;
        }
        catch (Exception ex)
        {
            //((Page)HttpContext.Current.Handler).ClientScript.RegisterStartupScript(((Page)HttpContext.Current.Handler).GetType(), "333", "alert('" + ex.Message + "');", true);
            return false;
        }

    }
	
	public static bool SendUserPaymentDetailsFirstDataChargeAdminCTR(TransactionResult result)
    {
        var msg = new System.Net.Mail.MailMessage();

        msg.CC.Add("sysadmin@abtscs.com");

        msg.From = new MailAddress("info@internationalgroupsdepartment.com", "Payment submitted", System.Text.Encoding.UTF8);
        msg.Subject = "Payment submitted" + "(TOKENIZATION ENABLED)";

        msg.IsBodyHtml = false;

        msg.Body = result.CTR;  

        var client = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"].ToString());

        client.DeliveryMethod = SmtpDeliveryMethod.Network;
        client.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPServerUserName"].ToString(), ConfigurationManager.AppSettings["SMTPServerPassword"].ToString());
        client.EnableSsl = true;
        client.Port = 587;


        try
        {
            client.Send(msg);
            return true;
        }
        catch (Exception ex)
        {
            //((Page)HttpContext.Current.Handler).ClientScript.RegisterStartupScript(((Page)HttpContext.Current.Handler).GetType(), "333", "alert('" + ex.Message + "');", true);
            return false;
        }

    }

    public static bool SendUserPaymentDetailsPlanetChargeAdmin(IEnumerable<XElement> responseData, Planet.Currency defaultcurrency, string cardType, string guestName = "", string websiteBookingID = "", string code = "", string Reference_No = "", string Reference_3 = "", string Customer_Ref = "", string responseMessage = "", Merchant merchant = Merchant.IgdInternationlGroup_PP)
    {
        string currency = responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "CURRENCY_CODE") == null ? defaultcurrency.ToString() : Enum.GetName(typeof(Planet.Currency), int.Parse(responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "CURRENCY_CODE").Value));

        var msg = new System.Net.Mail.MailMessage();
		
		if (responseMessage.Length > 0)
            msg.Attachments.Add(new Attachment(responseMessage));

        msg.CC.Add(DatabaseConnection.GetPaymentAuthorizationEmail());
        msg.CC.Add("sysadmin@abtscs.com");

        msg.From = new MailAddress("info@internationalgroupsdepartment.com", "Payment submitted", System.Text.Encoding.UTF8);
        msg.Subject = "Payment submitted" + "(TOKENIZATION ENABLED)";

        msg.IsBodyHtml = true;

        msg.Body = responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "RESPONSE_TEXT").Value + "<br/>";
        msg.Body += "========== TRANSACTION RECORD ==========" + "<br/>";
        msg.Body += Regex.Replace(((Merchant)merchant).ToString().Replace("_", ""), "([a-z])([A-Z])", "$1 $2").ToUpper() + "<br/>";
        msg.Body += DatabaseConnection.GetMerchantAddress() + "<br/>";
        msg.Body += DatabaseConnection.GetMerchantCity() + "," + DatabaseConnection.GetMerchantProvance() + "," + DatabaseConnection.GetMerchantPostal() + "<br/>";
        msg.Body += DatabaseConnection.GetMerchantCountry() + "<br/>";
        msg.Body += DatabaseConnection.GetMerchantURL() + "<br/><br/>";

        msg.Body += "TYPE: Purchase<br/><br/>";

        if (responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "EXCHANGE_RATE").Value != null && responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "EXCHANGE_RATE").Value != "1")
        {
            decimal amountInDefaultCurrency = Decimal.Parse(responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "AMOUNT").Value);
            decimal foreignCurrencyRate = Decimal.Parse(responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "EXCHANGE_RATE").Value);

            msg.Body += "ACCT: " + cardType + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + Math.Round(amountInDefaultCurrency * foreignCurrencyRate, 2) + " " + DatabaseConnection.GetDefaultCurrency() + "<br/>";
            msg.Body += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
                         responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "AMOUNT").Value + " " + currency + "<br/><br/><br/>";
            msg.Body += "Payment " + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "AMOUNT").Value + " " + currency + "<br/><br/><br/>";
        }
        else
        {
            msg.Body += "ACCT: " + cardType + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "AMOUNT").Value + " " + currency + "<br/><br/><br/>";
        }

        if (guestName != null && guestName.Length > 0)
            msg.Body += "GUEST NAME:" + guestName + "<br/>";

        msg.Body += "CARDHOLDER NAME:" + responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "FIRST_NAME").Value + " " + responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "LAST_NAME").Value + "<br/>";
        msg.Body += "CARD NUMBER:" + responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "ACCOUNT_NUMBER").Value + "<br/>";
        msg.Body += "DATE/TIME:" + System.DateTime.Now.ToString("dd MMMM yy H:mm:ss") + "<br/>";
        msg.Body += "REFERENCE #:" + Reference_No + "<br/>";

        msg.Body += "CUSTOMER REFERENCE #:" + Customer_Ref + "<br/>";

        if (Reference_3.Length > 0)
            msg.Body += "REFERENCE #3:" + Reference_3 + "<br/>";

        //msg.Body += "REFERENCE NUMBER #:" + result.Reference_No + "<br/><br/>";
        msg.Body += "TRANSACTION ID #:" + responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "TRANSACTION_ID").Value + "<br/>";

        if (websiteBookingID.Length > 0)
            msg.Body += "WEBSITE BOOKING ID:" + websiteBookingID + "<br/><br/>";

        if (code.Length > 0)
            msg.Body += "CODE:" + code + "<br/><br/>";

        msg.Body += responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "RESPONSE_TEXT").Value + "-" + " Thank You " + "<br/><br/>";

        //msg.Body += "<br/><br/>Do not reply to this email<br/><br/>";

        if (responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "EXCHANGE_RATE").Value != null && responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "EXCHANGE_RATE").Value != "1")
        {
            msg.Body += @"Currency Notes:<br/>
                         * Exchange Rate: 1 USD = " + responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "EXCHANGE_RATE").Value + " " + currency + @"<br/>
                         * Rate includes 3.5 % margin<br/>
                         * Please debit my account for the total<br/>
                         amount of this Transaction in the<br/>
                         Transaction Currency shown above.<br/>
                         I acknowledge that I had a choice to pay<br/>
                         in USD and my currency choice is final.<br/>
                         Currency conversion is conducted by the<br/>
                         merchant. [X] Accepted.<br/><br/>";
        }

        msg.Body += "Please retain this copy for your records<br/><br/>";
        msg.Body += "Cardholder will pay above amount to<br/>card issuer pursuant to cardholder<br/>agreement.<br/>";
        msg.Body += "========================================";

        var client = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"].ToString());

        //client.DeliveryMethod = SmtpDeliveryMethod.Network;
        client.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPServerUserName"].ToString(), ConfigurationManager.AppSettings["SMTPServerPassword"].ToString());
        client.EnableSsl = true;
        client.Port = 587;


        try
        {
            client.Send(msg);
            return true;
        }
        catch (Exception ex)
        {
            //((Page)HttpContext.Current.Handler).ClientScript.RegisterStartupScript(((Page)HttpContext.Current.Handler).GetType(), "333", "alert('" + ex.Message + "');", true);
            return false;
        }
    }

    public EmailHelper()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}