using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.Common;
using System.Web.SessionState;
using System.Collections;
using System.Collections.Generic;

public class DBConn
{
    public DbConnection conn = null;

	public DBConn(Page Caller)
	{
        DbProviderFactory dbfactory = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["AppConnection"].ProviderName);

        try
        {
            if (Caller.Session["conn"] != null) { conn = (DbConnection)Caller.Session["conn"]; }
            else
            {
                conn = dbfactory.CreateConnection();
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["AppConnection"].ConnectionString;
                conn.Open();
                Caller.Session["conn"] = conn;
            }
        }
        catch (Exception ex)
        {
            Caller.Session.Abandon();
        }
	}

    public DBConn(MasterPage Caller)
    {
        DbProviderFactory dbfactory = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["AppConnection"].ProviderName);

        try
        {
            if (Caller.Session["conn"] != null) { conn = (DbConnection)Caller.Session["conn"]; }
            else
            {
                conn = dbfactory.CreateConnection();
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["AppConnection"].ConnectionString;
                conn.Open();
                Caller.Session["conn"] = conn;
            }
        }
        catch (Exception ex)
        {
            Caller.Session.Abandon();
        }
    }


    public DBConn(HttpSessionState Sess)
    {
        DbProviderFactory dbfactory = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings["AppConnection"].ProviderName);

        try
        {
            if (Sess["conn"] != null) { conn = (DbConnection)Sess["conn"]; }
            else
            {
                conn = dbfactory.CreateConnection();
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["AppConnection"].ConnectionString;
                conn.Open();
                Sess["conn"] = conn;
            }
        }
        catch (Exception ex)
        {
            HttpContext.Current.Response.Write(ex.Message);
            Sess.Abandon();
        }
    }

    public DbParameter AddParamIn(DbCommand Cmd, string Name, DbType Type, object Value, int Length)
    {
        DbParameter param = Cmd.CreateParameter();
        param.DbType = Type;
        param.Direction = ParameterDirection.Input;
        param.ParameterName = Name;
        param.Size = Length;
        param.Value = Value;
        return param;
    }

    public object GetParamValue(DbCommand Cmd, string Name)
    {
        object ret = Cmd.Parameters[Name].Value;
        return ret;
    }

    public string DBCGrid(string Table, string Order, string KeyColumn, List<string> HiddenColumns, string Title, string ActionJS)
    {
        string ret = "";
        DbCommand cmd = conn.CreateCommand();
        cmd.CommandText = "SELECT * FROM " + Table + (Order != null ? (" ORDER BY " + Order) : "");
        cmd.CommandType = CommandType.Text;
        DbDataReader DR;
        DataTable DT = new DataTable();
        int ci = 0, numspan = 1, ri = 1;
        
        try
        {
            DR = cmd.ExecuteReader();
            DT.Load(DR);

            ret = "<fieldset><legend class='dbcgrid_title'>"+Title+"</legend><div class='dbcgrid_materheader'><!--span class='dbcgrid_title'>" + Title + "</span-->";
            ret += "<span class='dbcgrid_mod'>Add</span>";
            ret += "<span class='dbcgrid_mod'>Edit</span>";
            ret += "<span class='dbcgrid_mod'>Delete</span>";
            ret += "<table cellpadding=2 cellspacing=0 border=0 class='dbcgrid_frame'><tr>";
            foreach(DataColumn DC in DT.Columns) {
                if(!HiddenColumns.Contains(DC.ColumnName)) {
                    ret += "<th class='dbcgrid_header'";
                    if(numspan==1) ret+=" colspan='2'";
                    ret += ">" + DC.ColumnName + "</th>";
                    numspan++;
                }
            }

            ret+="</tr>";
            
            foreach(DataRow DRW in DT.Rows)
            {
                ci = 0;
                ret+="<tr>";
                ret += "<td align='right' class='dbcgrid_num' itemId='" + DRW[KeyColumn].ToString() + "'>" + ri + ".</td>";
                foreach (DataColumn DC2 in DT.Columns)
                {
                    if (!HiddenColumns.Contains(DC2.ColumnName))
                    {
                        
                        ret += "<td class='dbcgrid_item'";
                        if (DRW[DC2.ColumnName].GetType().ToString().ToUpper().Contains("INT")) ret += " align='right'";
                        ret += " onmouseover='dbcgrid_hilite(this);' onmouseout='dbcgrid_unlite(this);' onclick='dbcgrid_select(this,\"" + ActionJS + "\");'";
                        ret += ">" + DRW[DC2.ColumnName].ToString() + "</td>";
                        ci++;
                    }
                    
                }
                ret+="</tr>";
                ri++;
            }
            DR.Close();

            // add hidden item for addition

            /*
            ret += "<tr style='display:none;'><td class='dbcgrid_num' align='right'>*&nbsp;</td><td class='dbcgrid_new' colspan='" + (numspan - 1) + "'>";

            foreach (DataColumn DC in DT.Columns)
            {
                if (!HiddenColumns.Contains(DC.ColumnName))
                {
                    ret += "&nbsp;"+DC.ColumnName + ": <input type='text' class='editText' id='dbcnew_" + DC.ColumnName + "'>";
                         
                }
            }

            ret += "<button class='EditButtonOK'>a</button><button class='EditButtonCancel' onclick='cancelNew(this);'>r</button></td></tr>";

            ret += "<tr><td class='dbcgrid_item' colspan='" + numspan + "' onclick='tableItemAdd(this);'>*New</td></tr>";
            */

            //ret += "<tr><td colspan='" + numspan + "'>Add, Edit, Delete</td></tr>";

            ret += "</table></div></fieldset>";
        }
        catch (Exception ex)
        {
            ret = "<font color='white'><i>" + ex.Message + "</i></font>";
        }

        return ret;
    }

    public ErrorInfo ExecSQL(string Text)
    {
        DbCommand cmd = conn.CreateCommand();
        cmd.CommandText = Text;
        cmd.CommandType = CommandType.Text;
        ErrorInfo err = new ErrorInfo(0,String.Empty);

        try
        {
            cmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            err.Code = -1;
            err.Message = ex.Message;
        }

        return err;
    }

    public DataTable GetData(string SQL)
    {
        DbDataReader DR;
        DbCommand cmd = this.conn.CreateCommand();
        DataTable ret = new DataTable();

        cmd.CommandText = SQL;
        cmd.CommandType = CommandType.Text;

        DR = cmd.ExecuteReader();

        ret.Load(DR);

        DR.Close();

        return ret;
    }
}

public struct ErrorInfo
{
    public Int64 Code;
    public string Message;

    public ErrorInfo(Int64 _code, string _msg)
    {
        Code = _code;
        Message = _msg;
    }
}