﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services.Protocols;

/// <summary>
/// Summary description for User
/// </summary>
public class User:SoapHeader
{
    private string userID;
    private string password;

    public string UserID
    {
        get
        {
            return userID;
        }

        set
        {
            userID = value;
        }
    }

    public string Password
    {
        get
        {
            return password;
        }

        set
        {
            password = value;
        }
    }
}