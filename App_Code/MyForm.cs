﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace MyNamespace
{

    public class MyForm : System.Web.UI.HtmlControls.HtmlForm
    {

        public bool Render { set; get; }

        public MyForm()
        {
            //By default, render the tag
            Render = true;
        }

        protected override void RenderBeginTag(HtmlTextWriter writer)
        {
            //Only render the tag when Render is set to true, otherwise do nothing.
            if (Render)
                base.RenderBeginTag(writer);
        }

        protected override void RenderEndTag(HtmlTextWriter writer)
        {
            //Only render the tag when Render is set to true, otherwise do nothing.
            if (Render)
                base.RenderEndTag(writer);
        }
    }
}