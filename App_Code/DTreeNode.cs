using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for DTreeNode
/// </summary>
/*public class DTreeNode : TreeNode
{
    public DataBundle Data;

	public DTreeNode AddNode(TreeNode Item, DataBundle DataPacket)
    {
        DTreeNode ret = (DTreeNode)Item;
        ret.Data = DataPacket;
        base.ChildNodes.Add(Item);
        return ret;
    }

    public DataBundle GetData()
    {
        return this.Data;
    }

    /*public static DTreeNode Convert(TreeNode TN)
    {
        DTreeNode ret;
        
        ret = (DTreeNode)TN;
        return ret;
    }*/
//}

public class DataNode
{
    public DataType Type;
    public TreeNode Node;
    public int ID = -1;
    public object Value;
    public int ParentID;

    public DataNode(string _Text, object _Value, DataType _Type)
    {
        Type = _Type;
        Value = _Value;
        ID = (int)(HttpContext.Current.Session["UID"]);
        Node = new TreeNode(_Text, ID.ToString());
        HttpContext.Current.Session["UID"] = ID + 1;
    }

    public static DataNode GetParent(DataNode DN)
    {
        Hashtable HT = (Hashtable)(HttpContext.Current.Session["AHT"]);
        
        foreach(DictionaryEntry de in HT)
        {
            if( ((DataNode)(de.Value)).ID == DN.ParentID) return ((DataNode)de.Value);
        }

        return null;

    }
}

public enum DataType { Conferences = 0, Conference = 1, Locations = 2, Location = 3, Hotel = 4, Customers = 5, Customer = 6, 
    CityGuide = 7, Documents = 8, QA = 9, Payments = 10, PaymentCC = 11, PaymentWT = 12, Document = 13, Hotels = 14, Newsletter = 15, Contents = 16, Content = 17 };


