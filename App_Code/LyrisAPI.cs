﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Net;
using System.Xml;

/// <summary>
/// Summary description for LyrisAPI
/// </summary>
public class LyrisAPI
{
    /// <summary>
    /// 
    /// </summary>
    public static string Conferences = "";

    /// <summary>
    /// 
    /// </summary>
    /// <param name="siteID"></param>
    /// <param name="mlID"></param>
    /// <param name="email"></param>
    /// <param name="firstName"></param>
    /// <param name="lastName"></param>
    /// <param name="countryName"></param>
    /// <param name="companyName"></param>
    /// <param name="conferenceInterested"></param>
    /// <param name="companyType"></param>
    /// <returns></returns>
    public static bool SendUserInformation2LyrisHQ(string siteID, string mlID, string email, string firstName, string lastName, string countryName, string companyName, string conferenceInterested, string companyType)
    {
  
        string xml = "<DATASET>" +
                     "<SITE_ID>" + siteID + "</SITE_ID>" + 
                     "<MLID>" + mlID + "</MLID>" + 
                     "<DATA type='email'>" + email + "</DATA>" + 
                     "<DATA type='demographic' id='1'>" + firstName + "</DATA>" + 
                     "<DATA type='demographic' id='2'>" + lastName + "</DATA>" +                                     
                     "<DATA type='demographic' id='8'>" + countryName + "</DATA>" +
                     "<DATA type='demographic' id='14'><![CDATA[" + companyName + "]]></DATA>" +
                     "<DATA type='demographic' id='39540'>" + conferenceInterested + "</DATA>" +
                     "<DATA type='demographic' id='39552'>" + companyType + "</DATA>" +
                     "<DATA type='extra' id='password'>Albatros2010</DATA>" + 
                     "</DATASET>";

        string url = "https://www.elabs10.com/API/mailing_list.html?type=record&activity=Add&input=";

        bool success = PostXml(url, xml, "Add");

        return success;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="siteID"></param>
    /// <param name="mlID"></param>
    /// <param name="email"></param>
    /// <param name="companyName"></param>
    /// <param name="conferenceInterested"></param>
    /// <param name="companyType"></param>
    /// <returns></returns>
    public static bool UpdateUserInformationInLyrisHQ(string siteID, string mlID, string email, string countryName, string companyName, string conferenceInterested, string companyType)
    {

        string xml = "<DATASET>" +
                     "<SITE_ID>" + siteID + "</SITE_ID>" +
                     "<MLID>" + mlID + "</MLID>" +
                     "<DATA type='email'>" + email + "</DATA>" +
                     "<DATA type='demographic' id='8'>" + countryName + "</DATA>" +
                     "<DATA type='demographic' id='14'><![CDATA[" + companyName + "]]></DATA>" +
                     "<DATA type='demographic' id='39540'>" + conferenceInterested + "</DATA>" +
                     "<DATA type='demographic' id='39552'>" + companyType + "</DATA>" +
                     "<DATA type='extra' id='password'>Albatros2010</DATA>" +
                     "</DATASET>";

        string url = "https://www.elabs10.com/API/mailing_list.html?type=record&activity=Update&input=";

        bool success = PostXml(url, xml, "Update");

        return success;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="siteID"></param>
    /// <param name="mlID"></param>
    /// <param name="email"></param>
    /// <returns></returns>
    public static bool CheckIfUserExistInLyrisHQ(string siteID, string mlID, string email)
    {
        string xml= "<DATASET>" +
                    "<SITE_ID>" + siteID + "</SITE_ID>" +
                    "<MLID>" + mlID + "</MLID>" +
                    "<DATA type='email'>" + email + "</DATA>" + 
                    "<DATA type='extra' id='type'>active</DATA>" +
                    "<DATA type='extra' id='password'>Albatros2010</DATA>" +
                    "</DATASET>";

        string url = "https://www.elabs10.com/API/mailing_list.html?type=record&activity=Query-Data&input=";

        bool success = PostXml(url, xml, "Query-Data");

        return success;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="siteID"></param>
    /// <param name="mlID"></param>
    /// <param name="email"></param>
    /// <param name="firstName"></param>
    /// <param name="lastName"></param>
    /// <param name="countryName"></param>
    /// <param name="companyName"></param>
    /// <param name="conferenceInterested"></param>
    /// <param name="companyType"></param>
    /// <param name="conference"></param>
    /// <param name="action"></param>
    public static void SendUserInformationsToIntegratedLYRISLogFile(string siteID, string mlID, string email, string firstName, string lastName, string countryName, string companyName, string companyType, string conference, string action)
    {
        String objUri = "http://www.roomhandler.com/Lyris/LyrisLog.aspx?TIME=" + System.DateTime.Now.ToShortDateString() + "&SITEID=" + siteID + "&MLID=" + mlID + "&Email=" + email + "&FirstName=" + firstName + "&LastName=" + lastName + "&CountryName=" + countryName + "&AgencyName=" + companyName + "&CompanyType=" + companyType + "&ConferenceName=" + conference + "&Action=" + action;

        HttpWebRequest req = (HttpWebRequest)WebRequest.Create(objUri);

        WebResponse res = (HttpWebResponse)req.GetResponse();
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="url"></param>
    /// <param name="xml"></param>
    /// <param name="type"></param>
    /// <returns></returns>
    private static bool PostXml(string url, string xml, string type)
    {
        bool success = false;

        Encoding encoding = Encoding.GetEncoding("iso-8859-1");

        string __xml = xml;
        string _xml = HttpUtility.UrlEncode(__xml, encoding);
        string _url = url + _xml;


        byte[] bytes = encoding.GetBytes(_url);

        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_url);
        request.Method = "GET";

        try
        {
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    string message = String.Format("POST failed. Received HTTP {0}", response.StatusCode);
                    throw new ApplicationException(message);

                }
                else
                {
                    XmlDocument responseXML = new XmlDocument();
                    responseXML.Load(response.GetResponseStream());

                    string result = "";

                    if (type == "Add")
                    {
                        result = responseXML.InnerText;
                    }

                    if (type == "Update")
                    {
                        result = responseXML.SelectSingleNode("//TYPE").InnerText;

                        if (result == "success")
                        {
                            success = true;
                        }
                    }

                    if (type == "Query-Data")
                    {
                        result = responseXML.SelectSingleNode("//DATA[@type='extra'][@id='state']").InnerText;

                        string some = "";

                        XmlNodeList li = responseXML.SelectNodes("//DATA[@type='demographic'][@id='39540']");

                        foreach (XmlNode xn in li)
                        {
                            if (xn.FirstChild != null)
                                some += xn.FirstChild.Value + "||";
                        }

                        Conferences = some;

                        if (result == "active")
                        {

                            success = true;
                        }
                    }
                }
            }
        }
        catch(Exception ex)
        {

        }

        return success;
    }

    /// <summary>
    /// 
    /// </summary>
	public LyrisAPI()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}