﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.SessionState;
using HotelRooms;
using iTextSharp.text.pdf;
using System.Collections;
using System.Net;
using System.Data;

public partial class FileUpload : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (fuHousingAgreement.HasFile && fuHousingAgreement.PostedFile != null)
        {
            HttpPostedFile myFile = fuHousingAgreement.PostedFile;

            int nFileLen = myFile.ContentLength;
            byte[] myData = new byte[nFileLen];

            myFile.InputStream.Read(myData, 0, nFileLen);

            string strFilename = Path.GetFileName(myFile.FileName);

            //if (strFilename != "2012 APA International Group Housing Agreement.pdf")
            //{
            //    ClientScript.RegisterStartupScript(this.GetType(), "001", "alert('Please select \"2012 APA International Group Housing Agreement\" for uploading!');", true);
            //}

            //SessionIDManager Manager = new SessionIDManager();
            //string sessionID = Manager.CreateSessionID(Context);
            //int hashCode = Manager.GetHashCode();

            strFilename = strFilename.Replace(".pdf", "");

            try
            {
                PdfReader pdfReader = new PdfReader(myData);

                string conferenceID = pdfReader.AcroFields.GetField("txtConferenceID");
                string hashCode = pdfReader.AcroFields.GetField("txtUserKey");
                bool sidenights = Boolean.Parse(pdfReader.AcroFields.GetField("txtSidenights"));
                string fileName = String.Empty;
                string clientEmail = pdfReader.AcroFields.GetField("txtEMail");
                string groupName = pdfReader.AcroFields.GetField("txtSponsoringGroupName");
                string groupNameSideNights = pdfReader.AcroFields.GetField("txtGroupName");
                string txtInitials = pdfReader.AcroFields.GetField("txtInitials");
                string txtInitialDate = pdfReader.AcroFields.GetField("txtInitialDate");
                string txtClientAuthorizedRepresentative = pdfReader.AcroFields.GetField("txtClientAuthorizedRepresentative");
                string txtClientRepresentative = pdfReader.AcroFields.GetField("txtClientRepresentative");
                string txtTitle = pdfReader.AcroFields.GetField("txtTitle");
                string txtSignature = pdfReader.AcroFields.GetField("txtSignature");
                string txtDate = pdfReader.AcroFields.GetField("txtDate");
                string txtCardNumber = pdfReader.AcroFields.GetField("txtCardNumber");
                string txtExpMoYr = pdfReader.AcroFields.GetField("txtExpMoYr");
                string txtCardholderName = pdfReader.AcroFields.GetField("txtCardholderName");
                string rbPaymentMethod = pdfReader.AcroFields.GetField("rbPaymentMethod");
                string rbCreditCard = pdfReader.AcroFields.GetField("rbCreditCard");
                string txtCreditCardSignature = pdfReader.AcroFields.GetField("txtSignatureCC");
                string txtClientSignature = pdfReader.AcroFields.GetField("txtClientSignature");
                string txtExpDate = pdfReader.AcroFields.GetField("txtExpDate");
                string txtAgency = pdfReader.AcroFields.GetField("txtAgency");
                string rbPaymentMethodSideNights = pdfReader.AcroFields.GetField("Group11");
                string rbCreditCardSideNights = pdfReader.AcroFields.GetField("Group12");
				string[] txtCountry = pdfReader.AcroFields.GetListOptionDisplay("ddlCountry");
                bool createProposal = pdfReader.AcroFields.GetField("txtCreateProposal") != null ? bool.Parse(pdfReader.AcroFields.GetField("txtCreateProposal")) : false;
                string txtTotalSd = pdfReader.AcroFields.GetField("txtTotalSD");
                string transactionID = string.Empty;
                string txtToken = pdfReader.AcroFields.GetField("txtToken");

                var pa = new PaymentAuthorizationFirstData();

                if (txtToken != null)
                {
                    if (!sidenights)
                    {
                        pa.CardHolderName = txtCardholderName;
                        pa.CreditCardNumber = txtCardNumber;
						pa.ExparationMonthYear = (Convert.ToInt32(txtExpMoYr.Split('/')[0])) <= 9 && (!txtExpMoYr.Split('/')[0].StartsWith("0")) ? "0" + txtExpMoYr.Split('/')[0] + txtExpMoYr.Split('/')[1] : txtExpMoYr.Split('/')[0] + txtExpMoYr.Split('/')[1];
                        //pa.ExparationMonthYear = Convert.ToInt32(txtExpMoYr.Split('/')[0]) <= 9 ? "0" + txtExpMoYr.Split('/')[0] + txtExpMoYr.Split('/')[1] : txtExpMoYr.Split('/')[0] + txtExpMoYr.Split('/')[1];
                        pa.ProductPrice = txtTotalSd;
                        pa.ProductDescription = conferenceID;
                        pa.ProductCode = hashCode;
                        pa.ExactID = DatabaseConnection.GetGatewayID(Merchant.IgdInternationlGroup);
                        pa.Password = DatabaseConnection.GetGatewayPassword(Merchant.IgdInternationlGroup);
                        pa.HashID = DatabaseConnection.GetHashID(Merchant.IgdInternationlGroup);
                        pa.HashKey = DatabaseConnection.GetHashPassword(Merchant.IgdInternationlGroup);
                        pa.Test = false;
                        pa.Email = clientEmail;

                        switch (rbCreditCard)
                        {
                            case "1":
                                pa.CardType = "Visa";
                                break;
                            case "2":
                                pa.CardType = "Mastercard";
                                break;
                            case "3":
                                pa.CardType = "American Express";
                                break;
                            default:
                                pa.CardType = "Visa";
                                break;
                        }

                        //transactionID = DepositPaymentHousingAgreementPicknights(ref pa);

                        TokenizeHousingAgreement(ref pa);

                        if (pa.Token.Length <= 3)
                        {
                            var firstData = ((InternetSecureAPI.FirstData)int.Parse(transactionID));
                            string result = firstData.ToString();
                            var res = new string((result.SelectMany((c, i) => i != 0 && char.IsUpper(c) && !char.IsUpper(result[i - 1]) ? new char[] { ' ', c } : new char[] { c })).ToArray());
                            
                            ClientScript.RegisterStartupScript(this.GetType(), "012", "alert('This document not Successfully Uploaded! " + res + "');", true);
                            return;
                        }
                    }
                    else
                    {
                        pa.CardHolderName = txtCardholderName;
                        pa.CreditCardNumber = txtCardNumber;

                        pa.ExparationMonthYear = Convert.ToInt32(txtExpDate.Split('/')[0]) <= 9 ? "0" + txtExpDate.Split('/')[0] + txtExpDate.Split('/')[1] : txtExpDate.Split('/')[0] + txtExpDate.Split('/')[1];
                        pa.ProductPrice = "0";
                        pa.ProductDescription = conferenceID;
                        pa.ProductCode = hashCode;
                        pa.ExactID = DatabaseConnection.GetGatewayID(Merchant.IgdInternationlGroup);
                        pa.Password = DatabaseConnection.GetGatewayPassword(Merchant.IgdInternationlGroup);
                        pa.HashID = DatabaseConnection.GetHashID(Merchant.IgdInternationlGroup);
                        pa.HashKey = DatabaseConnection.GetHashPassword(Merchant.IgdInternationlGroup);
                        pa.Test = false;
                        pa.Email = clientEmail;
                        pa.CardType = rbCreditCardSideNights.Replace("rb", "");

                        switch (pa.CardType)
                        {
                            case "Visa":
                                pa.CardType = "Visa";
                                break;
                            case "MasterCard":
                                pa.CardType = "Mastercard";
                                break;
                            case "AmericanExpress":
                                pa.CardType = "American Express";
                                break;
                            default:
                                pa.CardType = "Visa";
                                break;
                        }

                        TokenizeHousingAgreement(ref pa);

                        if (pa.Token.Length <= 3)
                        {
                            var firstData = ((InternetSecureAPI.FirstData)int.Parse(transactionID));
                            string result = firstData.ToString();
                            var res = new string((result.SelectMany((c, i) => i != 0 && char.IsUpper(c) && !char.IsUpper(result[i - 1]) ? new char[] { ' ', c } : new char[] { c })).ToArray());

                            ClientScript.RegisterStartupScript(this.GetType(), "012", "alert('This document not Successfully Uploaded! " + res + "');", true);
                            return;
                        }
                    }
                }

                if (sidenights)
                {
                    txtCountry = new string[1];
                    txtCountry[0] = "n/a";

                    fileName = "Sidenights";
                }
                else
                {
                    fileName = "Housingagreement";
                }

                if (createProposal)
                {
                    var gen = new PDFGenerator();
                    string file = gen.GetPdfDocument(conferenceID, fileName + "_" + hashCode);

                    if (file.Length > 3)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "012", "alert('This document has already been submitted or uploaded!');", true);
                        return;
                    }
                }


                if (!sidenights)
                {
                    if (txtCardNumber.Length < 16 || txtExpMoYr.Length == 0 || txtCardholderName.Length == 0)
                    {
                        if (txtCardNumber.Length < 16)
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "012", "alert('Please enter 16 digits for the credit card and try again!');", true);
                            return;
                        }

                        if (txtExpMoYr.Length == 0)
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "013", "alert('Please enter valid exparation date for the credit card and try again!');", true);
                            return;
                        }

                        if (txtCardholderName.Length == 0)
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "014", "alert('Please enter cardholder name for the credit card and try again!');", true);
                            return;
                        }
                    }
                }
                else
                {
                    if (txtCardNumber.Length < 16 || txtExpDate.Length == 0 || txtCardholderName.Length == 0)
                    {
                        if (txtCardNumber.Length < 16)
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "012", "alert('Please enter 16 digits for the credit card and try again!');", true);
                            return;
                        }

                        if (txtExpDate.Length == 0)
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "013", "alert('Please enter valid exparation date for the credit card and try again!');", true);
                            return;
                        }

                        if (txtCardholderName.Length == 0)
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "014", "alert('Please enter cardholder name for the credit card and try again!');", true);
                            return;
                        }
                    }
                }

                PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(Server.MapPath("\\") + "\\agreements\\" + conferenceID + "\\" + fileName + "_" + hashCode + ".pdf", FileMode.Create));
                //pdfStamper.SetEncryption(true, null, "Dvladh09091976", PdfWriter.ALLOW_FILL_IN | PdfWriter.ALLOW_PRINTING);


                //Encrypt credit card number
                AcroFields pdfFormFields = pdfStamper.AcroFields;
                AcroFields.Item ccItem = pdfFormFields.GetFieldItem("txtCardNumber");
                PdfDictionary dic = ccItem.GetMerged(0);
                dic.Put(PdfName.MAXLEN, new PdfNumber(60));
                //pdfFormFields.SetField("txtCardNumber", StringHelpers.Encrypt(txtCardNumber));
                //Encrypt credit card number

                if (txtToken != null)
                {
                    //Overwrite credit card number with token and encrypt it!
                    pdfFormFields.SetField("txtCardNumber", StringHelpers.Encrypt(pa.Token));

                    //pdfFormFields.SetField("txtCardNumber", txtCardNumber.Length > 15 ? StringHelpers.Encrypt("000000000000" + txtCardNumber.Substring(txtCardNumber.Length - 4)) : StringHelpers.Encrypt("00000000000" + txtCardNumber.Substring(txtCardNumber.Length - 4)));
                    //Encrypt credit card number

                    //Set credit card token
                    pdfFormFields.SetField("txtToken", StringHelpers.Encrypt(pa.Token));
                    //Set credit card token

                    //Deposit payment transactionID for Housing Agreement picknights
                    //if (!sidenights) pdfFormFields.SetField("txtDepositTransactionID", transactionID);
                }
                else
                {
                    pdfFormFields.SetField("txtCardNumber", StringHelpers.Encrypt(txtCardNumber));
                    //Encrypt credit card number
                }
                
                pdfStamper.Close();
                pdfReader.Close();

                SaveRestrictedCopy(Server.MapPath("\\") + "\\agreements\\" + conferenceID + "\\" + fileName + "_" + hashCode + ".pdf", conferenceID);

                tbList.Text = tbList.Text + Environment.NewLine + strFilename + "_" + hashCode + ".pdf";

                string filePath = Server.MapPath("agreements\\" + conferenceID) + "\\" + "View_" + fileName + "_" + hashCode + ".pdf";

                string validateHA = String.Empty;
                string validateSN = String.Empty;

                if (fileName == "Housingagreement")
                {
                    validateHA = ValidateHAFields(txtInitials, txtInitialDate, txtClientAuthorizedRepresentative, txtTitle, txtSignature, txtDate, rbPaymentMethod, rbCreditCard, txtCardNumber, txtExpMoYr, txtCardholderName, txtCreditCardSignature);
                }
                else
                {
                    validateSN = ValidateSNFields(groupNameSideNights, txtAgency, rbPaymentMethodSideNights, rbCreditCardSideNights, txtCardNumber, txtExpDate, txtCardholderName, txtCreditCardSignature, txtClientRepresentative, txtTitle, txtClientSignature, txtDate);
                }               
                
                if (createProposal)
                {
                    byte[] bytes = File.ReadAllBytes(Server.MapPath("agreements\\" + conferenceID) + "\\" + fileName + "_" + hashCode + ".pdf");
                    var ms = new MemoryStream(bytes);

                    CreateProposalInABTsolute(ms);
                }
                
                SendEmail(conferenceID, hashCode, sidenights, sidenights ? groupNameSideNights : groupName, clientEmail, filePath, validateHA + validateSN, txtAgency, txtCountry[0]);

                ClientScript.RegisterStartupScript(this.GetType(), "111", "alert('Document uploaded successfully. Thank you.');", true);

            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "101", "alert('Uploading error! Please select correct file for uploading.');", true);
            }

            //bool success = WriteToFile(Server.MapPath("agreements") + "\\" + strFilename + "_" + hashCode + ".pdf", myData);

            //if (success)
            //{
            //    tbList.Text = tbList.Text + Environment.NewLine + strFilename + ".pdf";
            //}          
        }
        else
        {
            ClientScript.RegisterStartupScript(this.GetType(), "001", "alert('Please select a file for uploading!');", true);
        }
    }

    // Writes file to current folder
    private bool WriteToFile(string strPath, byte[] Buffer)
    {
        try
        {
            // Create a file
            FileStream newFile = new FileStream(strPath, FileMode.Create);

            // Write data to the file
            newFile.Write(Buffer, 0, Buffer.Length);

            // Close file
            newFile.Close();

            SaveRestrictedCopy(strPath, String.Empty);

            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    private void SaveRestrictedCopy(string path, string conferenceID)
    {
        string pdfTemplate = path;       

        string newFile = "View_" + path.Substring(path.LastIndexOf("\\")+1);

        PdfReader pdfReader = new PdfReader(pdfTemplate);
        PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(Server.MapPath("\\") + "\\agreements\\" + conferenceID + "\\" + newFile, FileMode.Create));

        AcroFields pdfFormFields = pdfStamper.AcroFields;

        string cardNumber = pdfReader.AcroFields.GetField("txtCardNumber");
        cardNumber = StringHelpers.Decrypt(cardNumber);

        string hiddenCreditNumber = Helpers.AddX(cardNumber.Length - 5) + "-" + cardNumber.Substring(cardNumber.Length - 4);

        pdfFormFields.SetField("txtCardNumber", hiddenCreditNumber);
        pdfFormFields.SetField("txtCVV", "XXX");
        pdfFormFields.SetField("txtExpMoYr", "XX/XX");

        pdfStamper.Close();       
    }

    protected void lbPDF_Click(object sender, EventArgs e)
    {                
        WebClient myWeb = new WebClient();
        Byte[] myBuff = myWeb.DownloadData(Server.MapPath("agreements") + "\\" + "2012 APA International Group Housing Agreement_66167900.pdf");

        Response.ContentType = "application/pdf";
        Response.AddHeader("content-length", myBuff.Length.ToString());

        Response.BinaryWrite(myBuff);
    }

    protected void SendEmail(string conferenceID, string hashCode, bool sideNights, string name, string email, string pdfPath, string validation, string agency, string country)
    {
        DataSet dsEmails = new DataSet();

        System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(Server.MapPath("\\") + "\\" + "App_Data\\Email.xml");

        dsEmails = new DataSet();
        dsEmails.ReadXml(reader);

        DataRow[] dr = dsEmails.Tables[0].Select("conferenceID='" + conferenceID + "'");

        if (dr.Length == 0)
        {
            return;
        }

        string subject = dr[0][0].ToString();
        string clientEmail = email;
        string emailFrom = dr[0][1].ToString();
        string emailTo = dr[0][2].ToString();
        string conferenceManagerBody = dr[0][3].ToString().Replace("[name]", name).Replace("[number]", hashCode).Replace("[HA]", sideNights ? "Housing Agreement side nights" : "Housing Agreement").Replace("\r\n", "<br/>").Replace("[agency]", "Customer/Agency: " + agency).Replace("[country]", "Country: " + country); ;
        string clientBody = dr[0][4].ToString().Replace("[name]", name).Replace("[br]", "<br/>");

        EmailHelper.SendClientEmail(subject, clientEmail, emailFrom, clientBody, pdfPath);
        EmailHelper.SendConferenceManagerEmail(subject, emailTo, emailFrom, conferenceManagerBody, pdfPath, validation);

    }

    protected string ValidateHAFields(string txtInitials, string txtInitialDate, string txtClientAuthorizedRepresentative, string txtTitle, string txtSignature, string txtSignatureDate, string rbPaymentMethod, string rbCreditCard, string txtCardNumber, string txtExpMoYr, string txtCardholderName, string txtCreditCardSignature)
    {
        string validate = String.Empty;
        string begginig = "User didn't enter these fields: ";

        validate += (txtInitials == null) || (txtInitials.Length == 0) ? " Initials;" : "";
        validate += (txtInitialDate == null) || (txtInitialDate.Length == 0) ? " Initial Date;" : "";
        validate += (txtClientAuthorizedRepresentative == null) || (txtClientAuthorizedRepresentative.Length == 0) ? " ClientAuthorizedRepresentative;" : "";
        validate += (txtTitle == null) || (txtTitle.Length == 0) ? " Title;" : "";
        validate += (txtSignature == null) || (txtSignature.Length == 0) ? " Signature;" : "";
        validate += (txtSignatureDate == null) || (txtSignatureDate.Length == 0) ? " SignatureDate;" : "";
        validate += (rbPaymentMethod == null) || (rbPaymentMethod.Length == 0) ? " PaymentMethod;" : "";
        validate += (rbCreditCard == null) || (rbCreditCard.Length == 0) ? " CreditCard Type;" : "";
        validate += (txtCardNumber == null) || (txtCardNumber.Length == 0) ? " CardNumber;" : "";
        validate += (txtExpMoYr == null) || (txtExpMoYr.Length == 0) ? " ExpMoYr;" : "";
        validate += (txtCardholderName == null) || (txtCardholderName.Length == 0) ? " CardholderName;" : "";
        //validate += (txtCreditCardSignature == null) || (txtCreditCardSignature.Length == 0) ? " CreditCardSignature" : "";
        validate += (txtCreditCardSignature == null) || (txtCreditCardSignature.Length == 0) ? "Electronic Signature" : "";
        if (validate.Length == 0)
        {
            return validate;
        }
        else
        {
            begginig += validate;
            return begginig;
        }
    }

    protected string ValidateSNFields(string txtGroupName, string txtAgency, string rbPaymentMethod, string rbCreditCard, string txtCardNumber, string txtExpDate, string txtCardholderName, string txtCreditCardSignature, string txtClientRepresentative, string txtTitle, string txtClientSignature, string txtClientSignatureDate)
    {
        string validate = String.Empty;
        string begginig = "User didn't enter these fields: ";

        validate += (rbPaymentMethod == null) || (rbPaymentMethod.Length == 0) ? " PaymentMethod;" : "";
        validate += (rbCreditCard == null) || (rbCreditCard.Length == 0) ? " CreditCard Type;" : "";
        validate += (rbCreditCard == null) || (txtCardNumber.Length == 0) ? " CardNumber;" : "";
        validate += (txtExpDate == null) || (txtExpDate.Length == 0) ? " ExpDate;" : "";
        validate += (txtCardholderName == null) || (txtCardholderName.Length == 0) ? " CardholderName;" : "";
        //validate += (txtCreditCardSignature == null) || (txtCreditCardSignature.Length == 0) ? " CreditCardSignature;" : "";
        validate += (txtCreditCardSignature == null) || (txtCreditCardSignature.Length == 0) ? " Electronic Signature;" : "";
        validate += (txtClientRepresentative == null) || (txtClientRepresentative.Length == 0) ? " ClientRepresentative;" : "";
        validate += (txtTitle == null) || (txtTitle.Length == 0) ? " Title;" : "";
        validate += (txtClientSignature == null) || (txtClientSignature.Length == 0) ? " ClientSignature;" : "";
        validate += (txtClientSignatureDate == null) || (txtClientSignatureDate.Length == 0) ? " ClientSignatureDate" : "";

        if (validate.Length == 0)
        {
            return validate;
        }
        else
        {
            begginig += validate;
            return begginig;
        }
    }

    private void CreateProposalInABTsolute(Stream myFile)
    {
        System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

        var hr = new HotelRooms.ABTSoluteWebServices();
        var sProposal = new HotelRooms.SProposal();

        var pdfReader = new PdfReader(myFile);
        string conferenceID = pdfReader.AcroFields.GetField("txtConferenceID");
        string hashCode = pdfReader.AcroFields.GetField("txtUserKey");
        bool sidenights = Boolean.Parse(pdfReader.AcroFields.GetField("txtSidenights"));
        string fileName = String.Empty;
        string clientEmail = pdfReader.AcroFields.GetField("txtEMail");
        string groupName = pdfReader.AcroFields.GetField("txtSponsoringGroupName");
        string groupNameSideNights = pdfReader.AcroFields.GetField("txtGroupName");
        string txtInitials = pdfReader.AcroFields.GetField("txtInitials");
        string txtInitialDate = pdfReader.AcroFields.GetField("txtInitialDate");
        string txtClientAuthorizedRepresentative = pdfReader.AcroFields.GetField("txtClientAuthorizedRepresentative");
        string txtClientRepresentative = pdfReader.AcroFields.GetField("txtClientRepresentative");
        string txtTitle = pdfReader.AcroFields.GetField("txtTitle");
        string txtSignature = pdfReader.AcroFields.GetField("txtSignature");
        string txtDate = pdfReader.AcroFields.GetField("txtDate");
        string txtCardNumber = pdfReader.AcroFields.GetField("txtCardNumber");
        string txtExpMoYr = pdfReader.AcroFields.GetField("txtExpMoYr");
        string txtCardholderName = pdfReader.AcroFields.GetField("txtCardholderName");
        string rbPaymentMethod = pdfReader.AcroFields.GetField("rbPaymentMethod");
        string rbCreditCard = pdfReader.AcroFields.GetField("rbCreditCard");
        string txtCreditCardSignature = pdfReader.AcroFields.GetField("txtSignatureCC");
        string txtClientSignature = pdfReader.AcroFields.GetField("txtClientSignature");
        string txtExpDate = pdfReader.AcroFields.GetField("txtExpDate");
        string txtAgency = pdfReader.AcroFields.GetField("txtAgency");
        string rbPaymentMethodSideNights = pdfReader.AcroFields.GetField("Group11");
        string rbCreditCardSideNights = pdfReader.AcroFields.GetField("Group12");
        string[] txtCountry = pdfReader.AcroFields.GetListOptionDisplay("ddlCountry");

        long idServiceItemSingle = long.Parse(pdfReader.AcroFields.GetField("txtIDServiceItemSingle"));
        long idServiceItemDouble = long.Parse(pdfReader.AcroFields.GetField("txtIDServiceItemDouble"));
        string abtsoluteKey = pdfReader.AcroFields.GetField("txtDocumentID");
        string housingAgreementKey = "HousingAgreement_" + abtsoluteKey;

        sProposal.HAKey = housingAgreementKey;
        sProposal.CompanyName = txtAgency;
        sProposal.ContactPerson = txtClientAuthorizedRepresentative;
        sProposal.ContactPersonRegistration = ""; //txtClientRepresentative;

        if (!sidenights)
        {
            sProposal.GroupName = groupName;
            sProposal.ContactPerson = txtClientAuthorizedRepresentative;
        }
        else
        {
            sProposal.GroupName = groupNameSideNights;
            sProposal.ContactPerson = txtClientRepresentative;
        }

        sProposal.IDProposal = 0;
        sProposal.ProjectName = conferenceID.Substring(conferenceID.Length - 4) + " " + conferenceID.Substring(0, conferenceID.Length - 4);

        if ((idServiceItemSingle != 0) && (idServiceItemDouble != 0))
            sProposal.ServiceItemDefinitionArray = new SServiceItemDefinition[2];
        else
            sProposal.ServiceItemDefinitionArray = new SServiceItemDefinition[1];

        if (idServiceItemSingle != 0)
        {
            sProposal.ServiceItemDefinitionArray[0] = new SServiceItemDefinition();
            sProposal.ServiceItemDefinitionArray[0].IDServiceItem = idServiceItemSingle;
            sProposal.ServiceItemDefinitionArray[0].Note = "Came from Webstie";
        }
        if (idServiceItemDouble != 0)
        {
            sProposal.ServiceItemDefinitionArray[sProposal.ServiceItemDefinitionArray.Length - 1] = new SServiceItemDefinition();
            sProposal.ServiceItemDefinitionArray[sProposal.ServiceItemDefinitionArray.Length - 1].IDServiceItem = idServiceItemDouble;
            sProposal.ServiceItemDefinitionArray[sProposal.ServiceItemDefinitionArray.Length - 1].Note = "Came from Webstie";
        }

        var dsConferences = new DataSet();
        var reader = new System.Xml.XmlTextReader(Server.MapPath("\\") + "\\" + "App_Data\\Conferences.xml");

        dsConferences.ReadXml(reader);

        foreach (DataRow dr in dsConferences.Tables[0].Select("conferenceID='" + conferenceID + "'"))
        {
            DataRow[] da = dsConferences.Tables[1].Select("conference_id='" + dr[0] + "' and sidenight=" + sidenights);

            if (idServiceItemSingle != 0)
            {
                sProposal.ServiceItemDefinitionArray[0].ServiceItemDayArray = new SServiceItemDay[da.Length];
            }
            if (idServiceItemDouble != 0)
            {
                sProposal.ServiceItemDefinitionArray[sProposal.ServiceItemDefinitionArray.Length - 1].ServiceItemDayArray = new SServiceItemDay[da.Length];
            }

            for (int i = 0; i < da.Length; i++)
            {
                bool sideNights = Boolean.Parse(da[i][0].ToString());
                string date = da[i][1].ToString();

                if (idServiceItemSingle != 0)
                {
                    sProposal.ServiceItemDefinitionArray[0].ServiceItemDayArray[i] = new SServiceItemDay();
                    sProposal.ServiceItemDefinitionArray[0].ServiceItemDayArray[i].Date = DateTime.Parse(date);
                    if (!sideNights)
                    {
                        sProposal.ServiceItemDefinitionArray[0].ServiceItemDayArray[i].Quantity = int.Parse(pdfReader.AcroFields.GetField("SR" + DateTime.Parse(date).Day));
                        sProposal.ServiceItemDefinitionArray[0].ServiceItemDayArray[i].SellingPrice = Decimal.Parse(pdfReader.AcroFields.GetField("txtSPrice"));
                    }
                    else
                    {
                        sProposal.ServiceItemDefinitionArray[0].ServiceItemDayArray[i].Quantity = int.Parse(pdfReader.AcroFields.GetField("txtGR" + DateTime.Parse(date).Day));
                        sProposal.ServiceItemDefinitionArray[0].ServiceItemDayArray[i].SellingPrice = Decimal.Parse(pdfReader.AcroFields.GetField("txtRoomRate"));
                    }
                }
                if (idServiceItemDouble != 0)
                {
                    sProposal.ServiceItemDefinitionArray[sProposal.ServiceItemDefinitionArray.Length - 1].ServiceItemDayArray[i] = new SServiceItemDay();
                    sProposal.ServiceItemDefinitionArray[sProposal.ServiceItemDefinitionArray.Length - 1].ServiceItemDayArray[i].Date = DateTime.Parse(date);
                    if (!sideNights)
                    {
                        sProposal.ServiceItemDefinitionArray[sProposal.ServiceItemDefinitionArray.Length - 1].ServiceItemDayArray[i].Quantity = int.Parse(pdfReader.AcroFields.GetField("DR" + DateTime.Parse(date).Day));
                        sProposal.ServiceItemDefinitionArray[sProposal.ServiceItemDefinitionArray.Length - 1].ServiceItemDayArray[i].SellingPrice = Decimal.Parse(pdfReader.AcroFields.GetField("txtDPrice"));
                    }
                    else
                    {
                        sProposal.ServiceItemDefinitionArray[sProposal.ServiceItemDefinitionArray.Length - 1].ServiceItemDayArray[i].Quantity = int.Parse(pdfReader.AcroFields.GetField("txtSR" + DateTime.Parse(date).Day));
                        sProposal.ServiceItemDefinitionArray[sProposal.ServiceItemDefinitionArray.Length - 1].ServiceItemDayArray[i].SellingPrice = Decimal.Parse(pdfReader.AcroFields.GetField("txtSuiteRate"));
                    }
                }
            }
        }

        try
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

            ProposalSavingResult psr = hr.SaveProposalForGroup(sProposal, "9DF3B529-54B6-49AD-8B6E-46F7BA0F737D");

            using (StreamWriter sw = File.AppendText(Server.MapPath("Log.txt")))
            {
                sw.WriteLine("BookingNumber: " + psr.BookingNumber + " IDProposal: " + psr.IDProposal + " SavingStatus:" + psr.ProposalSavingStatus + " ErrorMessage: " + psr.ErrorMessage + " TimeStamp: " + System.DateTime.Now);
            }

        }
        catch (Exception ex)
        {
            using (StreamWriter sw = File.AppendText(Server.MapPath("Log.txt")))
            {
                sw.WriteLine("ErrorMessage: " + ex.Message + " TimeStamp: " + System.DateTime.Now);
            }
        }
    }

    protected void TokenizeHousingAgreement(ref PaymentAuthorizationFirstData pa)
    {
        string token = String.Empty;
        token = InternetSecureAPI.CheckCreditCardAndGetToken(pa);
        pa.Token = token;
    }
}