﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Configuration;
using iTextSharp.text.pdf;

public partial class Test : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {        
		//ExtractDataFromPdfDocuments();
    }


    /* protected void EncryptPdfDocuments()
    {
        if (Directory.Exists(Server.MapPath(@"\agreements\")))
        {
            IEnumerable<string> files = Directory.EnumerateFiles(Server.MapPath(@"\agreements\"), "*.pdf", SearchOption.AllDirectories);

            if (!Directory.Exists(Server.MapPath(@"\agreementsCopy\")))
            {
                Directory.CreateDirectory(Server.MapPath(@"\agreementsCopy\"));
            }

            foreach (string s in files.ToArray())
            {
                var fi = new FileInfo(s);

                if (!s.Contains("View_"))
                {
                    string pdfTemplate = fi.FullName;
                    var pdfReader = new PdfReader(pdfTemplate);

                    if (fi.Directory != null && !Directory.Exists(Server.MapPath(@"\agreementsCopy\" + fi.Directory.Name)))
                    {
                        Directory.CreateDirectory(Server.MapPath(@"\agreementsCopy\" + fi.Directory.Name));
                    }

                    if (fi.Directory != null)
                    {
                        var pdfStamper = new PdfStamper(pdfReader, new FileStream(Server.MapPath(@"\agreementsCopy\") + fi.Directory.Name + "\\" + fi.Name, FileMode.Create), '\0', true);
                        AcroFields pdfFormFields = pdfStamper.AcroFields;
                        string txtCardNumber = pdfFormFields.GetField("txtCardNumber");

                        if (txtCardNumber.Length <= 16)
                        {
                            AcroFields.Item ccItem = pdfFormFields.GetFieldItem("txtCardNumber");
                            PdfDictionary dic = ccItem.GetMerged(0);
                            dic.Put(PdfName.MAXLEN, new PdfNumber(60));

                            txtCardNumber = StringHelpers.Encrypt(txtCardNumber);
                            pdfFormFields.SetField("txtCardNumber", txtCardNumber);
                        }

                        pdfStamper.Close();
                        pdfReader.Close();
                    }
                    
                }
                else
                {
                    if (fi.Directory != null) File.Copy(fi.FullName, Server.MapPath(@"\agreementsCopy\" + fi.Directory.Name + "\\" + fi.Name));
                }
            }

        }
    } */
	
	protected void ExtractDataFromPdfDocuments()
    {

        IEnumerable<string> files = Directory.EnumerateFiles(@"D:\agreements", "*.pdf", SearchOption.AllDirectories);
		
		int numberOfSubmittedHA = 0;
        int numberOfSubmittedSideNights = 0; 

        /* foreach (string s in files.ToArray())
        {
            var fi = new FileInfo(s);

            if (!s.Contains("View_"))
            {
                string pdfTemplate = fi.FullName;
                var pdfReader = new PdfReader(pdfTemplate);

                string email = String.IsNullOrEmpty(pdfReader.AcroFields.GetField("txtEMail")) ? "n/a" : pdfReader.AcroFields.GetField("txtEMail").Replace("||", "").Replace("|", "");
                string phone = String.IsNullOrEmpty(pdfReader.AcroFields.GetField("txtPhone")) ? "n/a" : pdfReader.AcroFields.GetField("txtPhone").Replace("||", "").Replace("|", "");
                string country = String.IsNullOrEmpty(pdfReader.AcroFields.GetField("ddlCountry")) ? "n/a" : pdfReader.AcroFields.GetField("ddlCountry").Replace("||", "").Replace("|", "");
                string agency = String.IsNullOrEmpty(pdfReader.AcroFields.GetField("txtAgency")) ? "n/a" : pdfReader.AcroFields.GetField("txtAgency").Replace("||", "").Replace("|", "");
                string id = String.IsNullOrEmpty(pdfReader.AcroFields.GetField("txtDocumentID")) ? "n/a" : pdfReader.AcroFields.GetField("txtDocumentID").Replace("||", "").Replace("|", "");
                string timestamp = String.IsNullOrEmpty(pdfReader.AcroFields.GetField("txtDate")) ? "n/a" : pdfReader.AcroFields.GetField("txtDate").Replace("||", "").Replace("|", "");
		string conference = String.IsNullOrEmpty(pdfReader.AcroFields.GetField("txtConferenceID")) ? "n/a" : pdfReader.AcroFields.GetField("txtConferenceID").Replace("||", "").Replace("|", "");

                using (StreamWriter sw = File.AppendText(Server.MapPath("/RepositoryClients.txt")))
                {
                    sw.WriteLine(id + "|" + agency + "|" + email + "|" + phone + "|" + country + "|" + timestamp + "|" + conference);
                }

                pdfReader.Close();

            }
        } */ 
		
		foreach (string s in files.ToArray())
        {
            var fi = new FileInfo(s);

            if (!s.Contains("View_"))
            {
                string pdfTemplate = fi.FullName;
                var pdfReader = new PdfReader(pdfTemplate);                            
                
                string conference = pdfReader.AcroFields.GetField("txtConferenceID");
                bool sideNigths = Boolean.Parse(pdfReader.AcroFields.GetField("txtSidenights"));                

                if(conference.Contains("2017") || conference.Contains("2016"))
                {
                    continue;
                }                

                if (!sideNigths)
                {                   

                    string DepositTransactionID = pdfReader.AcroFields.GetField("txtDepositTransactionID");
                    string FinaleBalanceTransactionID = pdfReader.AcroFields.GetField("txtFinalBalanceTransactionID");

                    if (DepositTransactionID != null && DepositTransactionID.Length > 3)
                    {
                        numberOfSubmittedHA++;
                    }

                    if(FinaleBalanceTransactionID != null && FinaleBalanceTransactionID.Length > 3)
                    {
                        numberOfSubmittedHA++;
                    }                   
                }
                else
                {
                    string FinaleBalanceTransactionID = pdfReader.AcroFields.GetField("txtFinalBalanceTransactionID");

                    if (FinaleBalanceTransactionID != null && FinaleBalanceTransactionID.Length > 3)
                    {
                        numberOfSubmittedSideNights++;
                    }

                }

                pdfReader.Close();                

            }
        }
		
		using (StreamWriter sw = File.AppendText(Server.MapPath("/RepositoryChargedAgreements.txt")))
		{
			sw.WriteLine("numberOfSubmittedHA: " + numberOfSubmittedHA + " " + "numberOfSubmittedSideNights: " + numberOfSubmittedSideNights);
		}
    }
}