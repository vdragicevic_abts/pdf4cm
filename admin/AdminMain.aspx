﻿<%@ Page Language="C#" MasterPageFile="~/admin/Admin.master" AutoEventWireup="true" CodeFile="AdminMain.aspx.cs" Inherits="AdminMain" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<p id="pUsers" runat="server" visible="false"><h3 style="color:Black;">Users registered on <%=System.DateTime.Now.ToShortDateString()%></h3></p>
<asp:GridView ID="gvUsers" runat="server" AutoGenerateColumns="False" 
        CssClass="grid-view" onrowdatabound="gvUsers_RowDataBound">
    <Columns>
        <asp:BoundField DataField="Company" HeaderText="Company" />
        <asp:BoundField DataField="FirstName" HeaderText="FirstName" />
        <asp:BoundField DataField="LastName" HeaderText="LastName" />
        <asp:BoundField DataField="eMail" HeaderText="Email" />
        <asp:BoundField DataField="UserName" HeaderText="UserName" />
    </Columns>
</asp:GridView><br /><br /><br />
<p id="pAgreements" visible="false" runat="server"><h3 style="color:Black;">Users requested Housing Agreement on <%=System.DateTime.Now.ToShortDateString()%></h3></p>
<asp:GridView ID="gvHousingAgreements" runat="server" CssClass="grid-view" 
        onrowdatabound="gvHousingAgreements_RowDataBound">
</asp:GridView>
</asp:Content>

