﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using iTextSharp.text.pdf;

public partial class admin_PDFData : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString().Length > 0)
            {
                ExtractPDF(Request.QueryString["id"].ToString());
            }
        }
    }

    private void ExtractPDF(string id)
    {
        string[] strFiles = Directory.GetFiles(Server.MapPath("../agreements/"), "*.pdf", SearchOption.AllDirectories);

        var p = from s in strFiles
                where s.ToString().Contains(id)
                select s;
        
        foreach (string s in p)
        {
            FileInfo fi = new FileInfo(s);
            DirectoryInfo i  = fi.Directory;
            string conferenceID = i.Name;
            ExtractPDFData(fi.Name, conferenceID);
            break;
        }            
        
        //byte[] fileBytes = File.ReadAllBytes(Server.MapPath("../agreements/" + strFiles[0].ToString()));
    }

    private void ExtractPDFData(string path, string conferenceID)
    {
        string pdfTemplate = Server.MapPath("../agreements/" + conferenceID + "/") + path;
        PdfReader pdfReader = new PdfReader(pdfTemplate);

        string txtSponsoringGroupName = pdfReader.AcroFields.GetField("txtSponsoringGroupName");
        string txtAgency = pdfReader.AcroFields.GetField("txtAgency");
        string txtAddress = pdfReader.AcroFields.GetField("txtAddress");
        string txtCity = pdfReader.AcroFields.GetField("txtCity");
        string txtPostalCode = pdfReader.AcroFields.GetField("txtPostal Code");
        string txtCountry = pdfReader.AcroFields.GetField("ddlCountry");
        string txtPhone = pdfReader.AcroFields.GetField("txtPhone");
        string txtEMail = pdfReader.AcroFields.GetField("txtEMail");

        string select1 = pdfReader.AcroFields.GetListSelection("ddlHotel1")[0];
        string select2 = pdfReader.AcroFields.GetListSelection("ddlHotel2")[0];
        string select3 = pdfReader.AcroFields.GetListSelection("ddlHotel3")[0];
       
        string hotel1 = pdfReader.AcroFields.GetListOptionDisplay("ddlHotel1")[0];
        string hotel2 = pdfReader.AcroFields.GetListOptionDisplay("ddlHotel2")[0];
        string hotel3 = pdfReader.AcroFields.GetListOptionDisplay("ddlHotel3")[0];

        string txtHotel1 = hotel1; //pdfReader.AcroFields.GetField("ddlHotel1");
        string txtHotel2 = hotel2; //pdfReader.AcroFields.GetField("ddlHotel2");
        string txtHotel3 = hotel3; //pdfReader.AcroFields.GetField("ddlHotel3");

        string txtTitle = pdfReader.AcroFields.GetField("txtTitle");
        string txtClientAuthorizedRepresentative = pdfReader.AcroFields.GetField("txtClientAuthorizedRepresentative");
        string txtSignature = pdfReader.AcroFields.GetField("txtSignature");
        string txtDate = pdfReader.AcroFields.GetField("txtDate");
        string txtCardNumber = pdfReader.AcroFields.GetField("txtCardNumber");
        string txtCVV = pdfReader.AcroFields.GetField("txtCVV");
        string txtCardholderName = pdfReader.AcroFields.GetField("txtCardholderName");
        string txtCardholderSignature = pdfReader.AcroFields.GetField("txtSignature");

        lblSponsoringGroupName.Text = txtSponsoringGroupName;
        lblAgency.Text = txtAgency;
        lblAddress.Text = txtAddress;
        lblCity.Text = txtCity;
        lblPostalCode.Text = txtPostalCode;
        lblCountry.Text = txtCountry;
        lblPhone.Text = txtPhone;
        lblEmail.Text = txtEMail;
        lblHotel1.Text = txtHotel1;
        lblHotel2.Text = txtHotel2;
        lblHotel3.Text = txtHotel3;
        lblTitle.Text = txtTitle;
        lblClientAuthorizedRepresentative.Text = txtClientAuthorizedRepresentative;
        lblSignature.Text = txtSignature;
        lblDate.Text = txtDate;
        lblCardNumber.Text = txtCardNumber;
        lblCVV.Text = txtCVV;
        lblCardHolderName.Text = txtCardholderName;
        lblCardHolderName.Text = txtCardholderSignature;                
    }
}