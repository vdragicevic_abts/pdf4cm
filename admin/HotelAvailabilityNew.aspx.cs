﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.DynamicData;
using System.Web.UI;
using System.Web.UI.WebControls;
using HotelRooms;
using System.Net;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using System.Xml.Linq;

public partial class admin_HotelAvailabilityNew : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //Session["CrrentHotelName"] = "";

            //HotelAvailabilityGetAll();

            LoadConferences();
        }
    }

    protected void LoadConferences()
    {
        DataSet dsConferences = new DataSet();

        System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(Server.MapPath("\\") + "\\" + "App_Data\\Conferences.xml");

        dsConferences.ReadXml(reader);

        foreach (DataRow dr in dsConferences.Tables[0].Rows)
        {
            if (Boolean.Parse(dr["active"].ToString()))
            {
                ListItem li = new ListItem(dr["conferenceID"].ToString(), dr["conferenceID"].ToString());
                ddlConferences.Items.Add(li);
            }
            else
            {
                ListItem li = new ListItem(dr["conferenceID"].ToString(), dr["conferenceID"].ToString());
                li.Enabled = false;
                ddlConferences.Items.Add(li);
            }
        }

        ddlConferences.DataBind();

        ListItem select = new ListItem("Select", "-1");

        ddlConferences.Items.Insert(0, select);
    }

    protected void HotelAvailabilityGetAll()
    {

        
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string timeStamp = "";

        var a = new HotelRooms.ABTSoluteWebServices();

        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

        var conferenceName = ddlConferences.SelectedItem.Text.Substring(ddlConferences.SelectedItem.Text.Length - 4) + " " + ddlConferences.SelectedItem.Text.Substring(0, ddlConferences.SelectedItem.Text.Length - 4);

        conferenceName = conferenceName.Contains("AAAAI") ? "2018 AAAAI/WAO" : conferenceName;

        HotelRooms.SInventory si = a.GetHotelAvailabilityByConference(conferenceName, "9DF3B529-54B6-49AD-8B6E-46F7BA0F737D", timeStamp);

        if (si == null) { return; };

        HotelRooms.SRoomBlock[] ha = si.RoomBlockList;
        DateTime lastUpdateTime = si.InventoryDate;

        string xmlData = string.Empty;

        
      
        XmlSerializer xsSubmit = new XmlSerializer(typeof(SInventory));

        using (var sww = new StringWriter())
        {
            using (XmlWriter writer = XmlWriter.Create(sww))
            {
                writer.WriteProcessingInstruction("xml", "version='1.0'");
                xsSubmit.Serialize(writer, si);               
                xmlData = sww.ToString(); // Your XML
            }
        }
        
        if (!CheckIfSourceFileExists(ddlConferences.SelectedItem.Text))
        {             
            Directory.CreateDirectory(Server.MapPath("~//App_Data//" + ddlConferences.SelectedItem.Text)); 
            File.WriteAllText(Server.MapPath("~//App_Data//" + ddlConferences.SelectedItem.Text + "//" + ddlConferences.SelectedItem.Text + ".xml"), xmlData);
        }
        else
        {
            File.WriteAllText(Server.MapPath("~//App_Data//" + ddlConferences.SelectedItem.Text + "//" + ddlConferences.SelectedItem.Text + ".xml"), xmlData);
        }

        HotelAvailabilitySelectByConferenceName(ddlConferences.SelectedItem.Text);

    }



    protected void gvHotelAvailability_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //string currentHotelName = Session["CrrentHotelName"].ToString();

        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    if (currentHotelName != e.Row.Cells[0].Text)
        //    {
        //        Session["CrrentHotelName"] = e.Row.Cells[0].Text;
        //    }
        //    else
        //    {
        //        e.Row.Cells[0].Text = "";
        //    }
        //}
    }

    protected void gvHotelAvailability_DataBinding(object sender, EventArgs e)
    {
        string currentHotelName = "";
    }

    private void helper_GroupHeader(string groupName, object[] values, GridViewRow row)
    {
        if (groupName == "Vendor")
        {
            row.BackColor = Color.Gainsboro;
            row.Font.Size = 10;
            row.Font.Bold = true;
            row.Cells[0].Text = "&nbsp;&nbsp;" + row.Cells[0].Text;
        }
        else if (groupName == "Name")
        {
            row.BackColor = Color.Beige;
            row.Font.Italic = true;
            row.Font.Size = 8;
            row.Font.Bold = true;
        }

    }

    protected void ddlConferences_SelectedIndexChanged(object sender, EventArgs e)
    {
        HotelAvailabilitySelectByConferenceName(ddlConferences.SelectedItem.Text);
    }

    private bool CheckIfSourceFileExists(string conference)
    {
        conference = conference + ".xml";

        if (File.Exists(Server.MapPath("~/App_Data/" + ddlConferences.SelectedItem.Text + "/" + conference)))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private void HotelAvailabilitySelectByConferenceName(string conference)
    {
        if (!CheckIfSourceFileExists(conference))
        {
            gvHotelAvailability.DataSource = null;
            gvHotelAvailability.DataBind();

            return;
        }

        Availability.HotelAvailability ds = new Availability.HotelAvailability();
        ds.ReadXml(Server.MapPath("~/App_Data/" + conference + "/" + conference + ".xml"), XmlReadMode.ReadSchema);

        var hotelAvailability = from srb in ds.SRoomBlock
                                join srbd in ds.RoomBlockDetails on srb.SRoomBlock_Id equals srbd.SRoomBlock_Id
                                join srbds in ds.SRoomBlockDetails on srbd.RoomBlockDetails_Id equals srbds.RoomBlockDetails_Id
                                select new
                                {
                                    Vendor = srb.HotelName,
                                    Code = srb.Code,
                                    Name = srb.RoomBlockName,
                                    Occupancy = srb.Occupancy,
                                    SellingPrice = srb.SellingPrice,
                                    VariableRates = srb.VariableRates,
                                    ForDate = srbds.Date,
                                    Quantity = srbds.Available,
                                    QuantityIndividuals = srbds.AvailableIndividuals,
                                    SideNights = srbds.SideNight,
                                    TimeStamp = System.DateTime.Now.ToShortDateString()
                                };

        gvHotelAvailability.DataSource = hotelAvailability;
        gvHotelAvailability.DataBind();

        var helper = new GridViewHelper(this.gvHotelAvailability);
               
        helper.RegisterGroup("Vendor", true, true);
        //helper.RegisterSummary("Quantity", SummaryOperation.Sum, "Vendor");
        helper.RegisterGroup("Occupancy", false, true);
        helper.RegisterGroup("Name", true, true);

        helper.GroupHeader += new GroupEvent(helper_GroupHeader);
        helper.ApplyGroupSort();

        
    }

    protected void gvHotelAvailability_Sorting(object sender, GridViewSortEventArgs e)
    {
        gvHotelAvailability.DataBind();
    }
}