﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;


public partial class Admin_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["sessionTicket"] != null)
            {
                string sessionTicket = Request.QueryString["sessionTicket"].ToString();
                sessionTicket = Helpers.DecodeFrom64(HttpUtility.UrlDecode(sessionTicket));

                string[] user = sessionTicket.Split('&');

                string username = user[0];
                string password = user[1];               

                XmlDocument doc = new XmlDocument();
                doc.Load(Server.MapPath("..\\") + "\\" + "App_Data\\Users.xml");

                XmlNode node = doc.SelectSingleNode("//user[./password/text()='" + StringHelpers.Encrypt(password) + "' and ./username/text()='" + username + "']");

                if (node != null)
                {
                    Session["Admin"] = "True";
                    Session["SuperAdmin"] = false;

                    string[] superAdmin = ConfigurationManager.AppSettings["SuperAdmin"].ToString().Split(';');

                    foreach (string s in superAdmin)
                    {
                        if (s == username)
                        {
                            Session["SuperAdmin"] = true;
                        }
                    }

                    Response.Redirect("../2pdf4cm/Admin/AdminMain.aspx");
                }
                                              
            }

            if (Session["Admin"] == null || (string)Session["Admin"] == "False")
            {
                Response.Redirect("../2pdf4cm/Admin/Login.aspx");
            }
            else
            {
                Response.Redirect("../2pdf4cm/Admin/RegistrationFormAdmin.aspx");
            }
        }
    }
}
