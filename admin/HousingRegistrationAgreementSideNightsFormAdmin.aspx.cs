﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using iTextSharp.text.pdf;

public partial class admin_HousingRegistrationAgreementSideNightsFormAdmin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //DataTable dt = LoadHousingAgreementPickupNights();
            //DataView dv = dt.AsDataView();
            //dv.Sort = "Date Desc";
            //dgrForm.DataSource = dv;
            //dgrForm.DataBind();

            LoadConferences();
        }
    }

    protected void LoadConferences()
    {
        DataSet dsConferences = new DataSet();

        System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(Server.MapPath("..\\") + "\\" + "App_Data\\Conferences.xml");

        dsConferences.ReadXml(reader);

        foreach (DataRow dr in dsConferences.Tables[0].Rows)
        {
            ListItem li = new ListItem(dr["conferenceID"].ToString(), dr["conferenceID"].ToString());
            ddlConferences.Items.Add(li);
        }

        ddlConferences.DataBind();

        ListItem select = new ListItem("Select", "-1");

        ddlConferences.Items.Insert(0, select);
    }

    protected DataTable LoadHousingAgreementPickupNights(string conferencesID)
    {      
        string[] strFiles = Directory.GetFiles(Server.MapPath("../agreements/" + conferencesID), "*.pdf", SearchOption.TopDirectoryOnly);

        DataTable dtFiles = new DataTable();

        DataColumn dt1 = new DataColumn();
        dt1.ColumnName = "Code";
        dt1.DataType = Type.GetType("System.String");

        DataColumn dt2 = new DataColumn();
        dt2.ColumnName = "Name";
        dt2.DataType = Type.GetType("System.String");

        DataColumn dt3 = new DataColumn();
        dt3.ColumnName = "Date";
        dt3.DataType = Type.GetType("System.DateTime");

        DataColumn dt4 = new DataColumn();
        dt4.ColumnName = "DepositTransactionID";
        dt4.DataType = Type.GetType("System.String");

        DataColumn dt5 = new DataColumn();
        dt5.ColumnName = "DepositAmount";
        dt5.DataType = Type.GetType("System.String");

        DataColumn dt6 = new DataColumn();
        dt6.ColumnName = "FinalBalanceTransactionID";
        dt6.DataType = Type.GetType("System.String");

        DataColumn dt7 = new DataColumn();
        dt7.ColumnName = "FinalBalanceAmount";
        dt7.DataType = Type.GetType("System.String");

        DataColumn dt8 = new DataColumn();
        dt8.ColumnName = "CardHolderName";
        dt8.DataType = Type.GetType("System.String");

        DataColumn dt9 = new DataColumn();
        dt9.ColumnName = "Expiry_Date";
        dt9.DataType = Type.GetType("System.String");

        DataColumn dt10 = new DataColumn();
        dt10.ColumnName = "CardType";
        dt10.DataType = Type.GetType("System.String");

        DataColumn dt11 = new DataColumn();
        dt11.ColumnName = "CreditCardNumber";
        dt11.DataType = Type.GetType("System.String");

        DataColumn dt12 = new DataColumn();
        dt12.ColumnName = "Conference";
        dt12.DataType = Type.GetType("System.String");

        DataColumn dt13 = new DataColumn();
        dt13.ColumnName = "Token";
        dt13.DataType = Type.GetType("System.String");

        DataColumn dt14 = new DataColumn();
        dt14.ColumnName = "Merchant";
        dt14.DataType = Type.GetType("System.String");

        dtFiles.Columns.Add(dt1);
        dtFiles.Columns.Add(dt2);
        dtFiles.Columns.Add(dt3);
        dtFiles.Columns.Add(dt4);
        dtFiles.Columns.Add(dt5);
        dtFiles.Columns.Add(dt6);
        dtFiles.Columns.Add(dt7);
        dtFiles.Columns.Add(dt8);
        dtFiles.Columns.Add(dt9);
        dtFiles.Columns.Add(dt10);
        dtFiles.Columns.Add(dt11);
        dtFiles.Columns.Add(dt12);
        dtFiles.Columns.Add(dt13);
        dtFiles.Columns.Add(dt14);

        foreach (string file in strFiles)
        {
            DataRow row = dtFiles.NewRow();

            FileInfo fi = new FileInfo(file);
            string name = fi.Name;
            DateTime creationDate = fi.CreationTime;  //fi.LastWriteTime; //fi.CreationTime; 
            string code = name.Substring(name.LastIndexOf("_") + 1).Replace(".pdf", "");
          
            row["Code"] = code;
            row["Name"] = name;
            row["Date"] = creationDate;
            row["Conference"] = conferencesID;

            ExtractHousingAgreementContext(code, conferencesID, name, ref row);

            if (!name.Contains("View"))
            {
                dtFiles.Rows.Add(row);
            }

            //if (Boolean.Parse(Session["SuperAdmin"].ToString()) && !name.Contains("View"))
            //{
            //    dtFiles.Rows.Add(row);
            //}
            //else if (!Boolean.Parse(Session["SuperAdmin"].ToString()) && name.Contains("View"))
            //{
            //    dtFiles.Rows.Add(row);
            //}

            

        }

        return dtFiles;
    }

    protected void ExtractHousingAgreementContext(string code, string conferenceID, string fielName, ref DataRow row)
    {
        using (MemoryStream ms = new MemoryStream())
        {
            PdfReader reader = new PdfReader(Server.MapPath("../agreements/" + conferenceID + "/" + fielName));
            PdfStamper stamper = new PdfStamper(reader, ms);

            if (!Server.MapPath("../agreements/" + conferenceID + "/" + fielName).Contains("View_"))
            {
                try
                {
                    AcroFields pdfFormFields = stamper.AcroFields;
                    AcroFields.Item item = pdfFormFields.GetFieldItem("txtDepositTransactionID");
                    AcroFields.Item item1 = pdfFormFields.GetFieldItem("txtFinalBalanceTransactionID");
                    AcroFields.Item item3 = pdfFormFields.GetFieldItem("txtToken");
                    AcroFields.Item item4 = pdfFormFields.GetFieldItem("txtMerchant");

                    bool sidenights = bool.Parse(pdfFormFields.GetField("txtSidenights"));

                    if (item == null)
                    {
                        row["DepositTransactionID"] = "N/A";
                        row["DepositAmount"] = "0.00";
                    }
                    else
                    {
                        if (!sidenights)
                        {
                            row["DepositTransactionID"] = pdfFormFields.GetField("txtDepositTransactionID") == "0"? "N/A" : pdfFormFields.GetField("txtDepositTransactionID");
                            row["DepositAmount"] = pdfFormFields.GetField("txtTotalSD");
                        }
                        else
                        {
                            row["DepositTransactionID"] = "N/A";
                            row["DepositAmount"] = "0.00";
                        }
                    }

                    if (item1 == null)
                    {
                        row["FinalBalanceTransactionID"] = "N/A";
                        row["FinalBalanceAmount"] = "N/A";
                    }
                    else
                    {
                        if (!sidenights)
                        {
                            row["FinalBalanceTransactionID"] = pdfFormFields.GetField("txtFinalBalanceTransactionID") == "0"? "N/A" : pdfFormFields.GetField("txtFinalBalanceTransactionID");
                            row["FinalBalanceAmount"] = pdfFormFields.GetField("txtTotalPrice");
                        }
                        else
                        {
                            row["FinalBalanceTransactionID"] = pdfFormFields.GetField("txtFinalBalanceTransactionID") == "0"? "N/A" : pdfFormFields.GetField("txtFinalBalanceTransactionID"); 
                            row["FinalBalanceAmount"] = pdfFormFields.GetField("txtTotalAmount"); 
                        }
                    }

                    row["CardHolderName"] = pdfFormFields.GetField("txtCardholderName");
                    row["CreditCardNumber"] = pdfFormFields.GetField("txtCardNumber").Length > 16 ? StringHelpers.Decrypt(pdfFormFields.GetField("txtCardNumber")) : pdfFormFields.GetField("txtCardNumber");

                    if (!sidenights)
                    {
                        row["Expiry_Date"] = (Convert.ToInt32(pdfFormFields.GetField("txtExpMoYr")[0])) <= 9 && (!pdfFormFields.GetField("txtExpMoYr").StartsWith("0")) ? "0" + pdfFormFields.GetField("txtExpMoYr").Split('/')[0] + pdfFormFields.GetField("txtExpMoYr").Split('/')[1] : pdfFormFields.GetField("txtExpMoYr").Split('/')[0] + pdfFormFields.GetField("txtExpMoYr").Split('/')[1];
                        //Convert.ToInt32(pdfFormFields.GetField("txtExpMoYr").Split('/')[0]) <= 9 ? "0" + pdfFormFields.GetField("txtExpMoYr").Split('/')[0] + pdfFormFields.GetField("txtExpMoYr").Split('/')[1] : pdfFormFields.GetField("txtExpMoYr").Split('/')[0] + pdfFormFields.GetField("txtExpMoYr").Split('/')[1];

                        if (reader.AcroFields.GetField("rbCreditCard").Contains("rb"))
                        {
                            row["CardType"] = reader.AcroFields.GetField("rbCreditCard").Replace("rb", "");
                        }
                        else
                        {
							int cardType = reader.AcroFields.GetField("rbCreditCard").Length > 0 ? Convert.ToInt32(reader.AcroFields.GetField("rbCreditCard")) : 1;
                            //int cardType = Convert.ToInt32(reader.AcroFields.GetField("rbCreditCard"));

                            switch (cardType)
                            {
                                case 1:
                                    row["CardType"] = "Visa";
                                    break;
                                case 2:
                                   row["CardType"] = "Mastercard";
                                    break;
                                case 3:
                                    row["CardType"] = "American Express";
                                    break;
                                default:
                                    row["CardType"] = "Visa";
                                    break;
                            }
                        }
                    }
                    else
                    {
                        row["Expiry_Date"] = (Convert.ToInt32(pdfFormFields.GetField("txtExpDate")[0])) <= 9 && (!pdfFormFields.GetField("txtExpDate").StartsWith("0")) ? "0" + pdfFormFields.GetField("txtExpDate").Split('/')[0] + pdfFormFields.GetField("txtExpDate").Split('/')[1] : pdfFormFields.GetField("txtExpDate").Split('/')[0] + pdfFormFields.GetField("txtExpDate").Split('/')[1];
                        //row["Expiry_Date"] = Convert.ToInt32(pdfFormFields.GetField("txtExpDate").Split('/')[0]) <= 9 ? "0" + pdfFormFields.GetField("txtExpDate").Split('/')[0] + pdfFormFields.GetField("txtExpDate").Split('/')[1] : pdfFormFields.GetField("txtExpDate").Split('/')[0] + pdfFormFields.GetField("txtExpDate").Split('/')[1];


                        if (reader.AcroFields.GetField("Group12").Contains("rb"))
                        {
                            row["CardType"] = reader.AcroFields.GetField("Group12").Replace("rb", "");
                        }
                        else
                        {
							int cardType = reader.AcroFields.GetField("Group12").Length > 0 ? Convert.ToInt32(reader.AcroFields.GetField("Group12")) : 1;
                            //int cardType = Convert.ToInt32(reader.AcroFields.GetField("Group12"));

                            switch (cardType)
                            {
                                case 1:
                                    row["CardType"] = "Visa";
                                    break;
                                case 2:
                                   row["CardType"] = "Mastercard";
                                    break;
                                case 3:
                                   row["CardType"] = "American Express";
                                    break;
                                default:
                                    row["CardType"] = "Visa";
                                    break;
                            }
                        }
                    }

                    if (item3 != null)
                    {
                        row["Token"] = pdfFormFields.GetField("txtToken");
                    }
                    else
                    {
                        row["Token"] = "N/A";
                    }

                    if (item4 != null)
                    {
                        row["Merchant"] = pdfFormFields.GetField("txtMerchant");
                    }
                    else
                    {
                        row["Merchant"] = "0";
                    }
                }
                catch (Exception ex)
                {

                }
            }

            stamper.Close();
        }
    }

    protected void dgrForm_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "Print")
        {
            //string path = e.Item.Cells[1].Text;

            string path = Boolean.Parse(Session["SuperAdmin"].ToString()) ? e.Item.Cells[1].Text : "View_" + e.Item.Cells[1].Text;

            //byte[] fileBytes = File.ReadAllBytes(Server.MapPath("../agreements/" + this.ddlConferences.SelectedValue + "/" + path));

            using (StreamWriter sw = File.AppendText(Server.MapPath("/Login.txt")))
            //using (StreamWriter sw = File.AppendText("C:\\Windows\\System32\\winevt\\Logs\\PDFView.txt"))
            {
                sw.WriteLine(Session["username"].ToString() + ", " + System.DateTime.Now.ToString() + ", " + "View/Print: File: " + path + ", Conference: " + this.ddlConferences.SelectedValue);
            }

            using (MemoryStream ms = new MemoryStream())
            {
                PdfReader reader = new PdfReader(Server.MapPath("../agreements/" + this.ddlConferences.SelectedValue + "/" + path));
                PdfStamper stamper = new PdfStamper(reader, ms);

                if (!path.Contains("View_"))
                {
                    try
                    {
                        AcroFields pdfFormFields = stamper.AcroFields;
                        string card = pdfFormFields.GetField("txtCardNumber");
                        string ccNumber = StringHelpers.Decrypt(card);

                        AcroFields.Item item = pdfFormFields.GetFieldItem("txtCardNumber");
                        PdfDictionary dic = item.GetMerged(0);
                        dic.Put(PdfName.MAXLEN, new PdfNumber(16));
                        pdfFormFields.SetField("txtCardNumber", ccNumber);
                    }
                    catch (Exception ex)
                    {

                    }
                }

                stamper.Close();

                byte[] bytes = ms.ToArray();

                Context.Response.ContentType = "application/octet-stream"; //"application/pdf";
                Context.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", e.Item.Cells[1].Text));
                Context.Response.BinaryWrite(bytes);
                Context.Response.Flush();
                Context.Response.End();
            }

            //Response.ContentType = "application/pdf";
            //Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", e.Item.Cells[1].Text));
            //Response.BinaryWrite(fileBytes.ToArray());
            //Response.End();
        }
    }

    protected void dgrForm_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
        {
            HyperLink hl = (HyperLink) e.Item.Cells[3].Controls[1];
            hl.Attributes.Add("onclick", "window.open('../agreements/" + e.Item.Cells[1].Text + "', 'lovechild','resizable=1,menubar=0,toolbar=0,location=0,directories=0,scrollbars=0,status=0');");
            hl.Text = "Print";
            string id = e.Item.Cells[1].Text.Substring(e.Item.Cells[1].Text.LastIndexOf("_") + 1).Replace(".pdf", "");

            HyperLink hl1 = (HyperLink) e.Item.Cells[5].Controls[1];
            hl1.Attributes.Add("onclick", "window.open('../admin/PDFData.aspx?id=" + id + "', 'lovechild','left=600, top=200, width=300, height=450, resizable=0,menubar=0,toolbar=0,location=0,directories=0,scrollbars=0,status=0');");
            hl1.Text = "Extract";

            DataRow row = ((DataView) Session["dvHousingAgreements"])[e.Item.ItemIndex].Row;

            if (row["Token"].ToString() == "N/A" || row["Token"].ToString().Contains("Error"))
            {
                ((TextBox) e.Item.Cells[7].Controls[1]).Enabled = false;
                ((TextBox) e.Item.Cells[9].Controls[1]).Enabled = false;
                ((CheckBox) e.Item.Cells[10].Controls[1]).Enabled = false;
                ((DropDownList)e.Item.Cells[11].Controls[1]).Enabled = false;

                ((TextBox) e.Item.Cells[7].Controls[1]).BackColor = Color.Gainsboro;
                ((TextBox) e.Item.Cells[7].Controls[1]).BorderStyle = BorderStyle.Groove;
                ((TextBox) e.Item.Cells[7].Controls[1]).ForeColor = Color.DimGray;

                ((TextBox) e.Item.Cells[9].Controls[1]).BackColor = Color.Gainsboro;
                ((TextBox) e.Item.Cells[9].Controls[1]).BorderStyle = BorderStyle.Groove;
                ((TextBox) e.Item.Cells[9].Controls[1]).ForeColor = Color.DimGray;
            }
            else
            {

                if ((row["DepositTransactionID"].ToString() == "N/A") && (row["FinalBalanceTransactionID"].ToString() == "N/A")  && row["Name"].ToString().Contains("Housingagreement"))
                {
                    ((TextBox) e.Item.Cells[7].Controls[1]).Enabled = true;
                    ((TextBox)e.Item.Cells[9].Controls[1]).Enabled = false;

                    ((TextBox)e.Item.Cells[9].Controls[1]).BackColor = Color.Gainsboro;
                    ((TextBox)e.Item.Cells[9].Controls[1]).BorderStyle = BorderStyle.Groove;
                    ((TextBox)e.Item.Cells[9].Controls[1]).ForeColor = Color.DimGray;

                }
                else if ((row["DepositTransactionID"].ToString() != "N/A") && (row["FinalBalanceTransactionID"].ToString() == "N/A") && row["Name"].ToString().Contains("Housingagreement"))
                {
                    ((TextBox)e.Item.Cells[7].Controls[1]).Enabled = false;
                    ((TextBox)e.Item.Cells[9].Controls[1]).Enabled = true;

                    ((TextBox)e.Item.Cells[7].Controls[1]).BackColor = Color.Gainsboro;
                    ((TextBox)e.Item.Cells[7].Controls[1]).BorderStyle = BorderStyle.Groove;
                    ((TextBox)e.Item.Cells[7].Controls[1]).ForeColor = Color.DimGray;
                }
                else if ((row["DepositTransactionID"].ToString() != "N/A") && (row["FinalBalanceTransactionID"].ToString() != "N/A") && row["Name"].ToString().Contains("Housingagreement"))
                {
                    ((TextBox)e.Item.Cells[7].Controls[1]).Enabled = false;
                    ((TextBox)e.Item.Cells[9].Controls[1]).Enabled = false;
                    ((CheckBox)e.Item.Cells[10].Controls[1]).Enabled = false;
                    ((DropDownList)e.Item.Cells[11].Controls[1]).Enabled = false;

                    ((TextBox)e.Item.Cells[7].Controls[1]).BackColor = Color.Gainsboro;
                    ((TextBox)e.Item.Cells[7].Controls[1]).BorderStyle = BorderStyle.Groove;
                    ((TextBox)e.Item.Cells[7].Controls[1]).ForeColor = Color.DimGray;

                    ((TextBox)e.Item.Cells[9].Controls[1]).BackColor = Color.Gainsboro;
                    ((TextBox)e.Item.Cells[9].Controls[1]).BorderStyle = BorderStyle.Groove;
                    ((TextBox)e.Item.Cells[9].Controls[1]).ForeColor = Color.DimGray;
                }
                else if ((row["DepositTransactionID"].ToString() == "N/A") && (row["FinalBalanceTransactionID"].ToString() == "N/A") && row["Name"].ToString().Contains("Sidenights"))
                {
                    ((TextBox)e.Item.Cells[7].Controls[1]).Enabled = false;
                    ((TextBox)e.Item.Cells[9].Controls[1]).Enabled = true;

                    ((TextBox)e.Item.Cells[7].Controls[1]).BackColor = Color.Gainsboro;
                    ((TextBox)e.Item.Cells[7].Controls[1]).BorderStyle = BorderStyle.Groove;
                    ((TextBox)e.Item.Cells[7].Controls[1]).ForeColor = Color.DimGray;
                }
                else if ((row["DepositTransactionID"].ToString() == "N/A") && (row["FinalBalanceTransactionID"].ToString() != "N/A") && row["Name"].ToString().Contains("Sidenights"))
                {
                    ((TextBox)e.Item.Cells[7].Controls[1]).Enabled = false;
                    ((TextBox)e.Item.Cells[9].Controls[1]).Enabled = false;
                    ((CheckBox)e.Item.Cells[10].Controls[1]).Enabled = false;
                    ((DropDownList)e.Item.Cells[11].Controls[1]).Enabled = false;

                    ((TextBox)e.Item.Cells[7].Controls[1]).BackColor = Color.Gainsboro;
                    ((TextBox)e.Item.Cells[7].Controls[1]).BorderStyle = BorderStyle.Groove;
                    ((TextBox)e.Item.Cells[7].Controls[1]).ForeColor = Color.DimGray;

                    ((TextBox)e.Item.Cells[9].Controls[1]).BackColor = Color.Gainsboro;
                    ((TextBox)e.Item.Cells[9].Controls[1]).BorderStyle = BorderStyle.Groove;
                    ((TextBox)e.Item.Cells[9].Controls[1]).ForeColor = Color.DimGray;
                }
            }
        }
    }

    protected void ddlConferences_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlConferences.SelectedValue != "-1")
        {
            DataTable dt = LoadHousingAgreementPickupNights(this.ddlConferences.SelectedValue);
            DataView dv = dt.AsDataView();
            dv.Sort = "Date Desc";
            
            Session["dvHousingAgreements"] = dv;

            dgrForm.DataSource = dv;
            dgrForm.DataBind();
        }
        else
        {
            Session["dvHousingAgreements"] = null;
            dgrForm.DataSource = null;
            dgrForm.DataBind();
        }
    }

    protected void ReloadDataGrid()
    {
        if (ddlConferences.SelectedValue != "-1")
        {
            DataTable dt = LoadHousingAgreementPickupNights(this.ddlConferences.SelectedValue);
            DataView dv = dt.AsDataView();
            dv.Sort = "Date Desc";

            Session["dvHousingAgreements"] = dv;

            dgrForm.DataSource = dv;
            dgrForm.DataBind();
        }
        else
        {
            Session["dvHousingAgreements"] = null;
            dgrForm.DataSource = null;
            dgrForm.DataBind();
        }
    }

    public enum PaymentType
    {
        DepositPayment = 1,
        FinalBalancePayment = 2
    }

    protected void btnCharge_Click(object sender, EventArgs e)
    {
        string message = "'";

        foreach (DataGridItem item in dgrForm.Items)
        {
            if ((item.ItemType == ListItemType.Item) || (item.ItemType == ListItemType.AlternatingItem))
            {
                if (((CheckBox)item.Cells[10].Controls[1]).Checked)
                {
                    string merchantID = ((DropDownList)item.Cells[11].Controls[1]).SelectedValue;

                    string depositPayment = ((TextBox)item.Cells[7].Controls[1]).Text;
                    string finalBalancePayment = ((TextBox)item.Cells[9].Controls[1]).Text;

                    Merchant merchant = (Merchant)Enum.Parse(typeof(Merchant), merchantID);

                    var payment = !((TextBox)item.Cells[7].Controls[1]).Enabled ? PaymentType.FinalBalancePayment : PaymentType.DepositPayment;                       ;
                    
                    var itemIndex = item.ItemIndex;

                    DataRow row = ((DataView) Session["dvHousingAgreements"])[itemIndex].Row;
                    var pa = new PaymentAuthorizationFirstData();

                    pa.ProductPrice = payment == PaymentType.DepositPayment ? depositPayment.ToString() : finalBalancePayment.ToString();

                    GetPaymentAuthorizationFirstData(ref pa, row, payment, merchant);

                    string transactionID = string.Empty;
                    string messagePP = string.Empty;

                    if (!pa.Merchant.ToString().Contains("PP"))
                    {
                        transactionID = InternetSecureAPI.SendUserInformation2BoaRestServiceTokenVersion(pa.ConferenceID, pa.ProductDescription, pa.ProductPrice, pa.CardHolderName, pa.Token, pa.CardType, pa.ExparationMonthYear, pa.Test, pa.ExactID, pa.Password, pa.HashID, pa.HashKey);
                    }
                    else
                    {
						pa.Merchant = pa.CardType.ToString().ToLower() == "american express" ? (Merchant)Enum.Parse(typeof(Merchant), pa.Merchant.ToString()) : pa.Merchant;
                        transactionID = new Planet.PlanetAPI(merchant).SendUserInformation2PlanetTokenVersion(pa.Token, pa.ProductPrice, pa.ProposalID, pa.ProductDescription, Planet.Currency.USD, pa.Test, pa.Merchant, ref messagePP, Planet.CurrencyIndicator.DomesticTransaction, pa.CardType, "", "", "", pa.ConferenceID, "", pa.ProductDescription);
                    }

                    if (transactionID.Length > 3)
                    {
                        message += "Conference: " + pa.ConferenceID + ", Code: " + pa.ProductDescription + " , TransactionID: " + transactionID + ", Amount: " + "$ " + pa.ProductPrice + ", Status: " + "Sucessfull\\n";

                        CreatHACopyAndSetTransactionID(transactionID, row, payment, merchantID);

                        using (StreamWriter sw = File.AppendText(Server.MapPath("/Log.txt")))
                        //using (StreamWriter sw = File.AppendText("C:\\Windows\\System32\\winevt\\Logs\\PDFView.txt"))
                        {
                            sw.WriteLine(Session["username"].ToString() + ", " + System.DateTime.Now.ToString() + ", " + "Sucessfully Charged: " + "Conference: " + pa.ConferenceID + ", Code: " + pa.ProductDescription + " , TransactionID: " + transactionID + ", Amount: " + "$ " + pa.ProductPrice + ", Merchant: " + merchant.ToString());
                        }

                    }
                    else
                    {
                        string res = String.Empty;

                        try
                        {
                            if (!pa.Merchant.ToString().Contains("PP"))
                            {
                                var firstData = ((InternetSecureAPI.FirstData)Int64.Parse(transactionID));
                                string result = firstData.ToString();
                                res = new string((result.SelectMany((c, i) => i != 0 && char.IsUpper(c) && !char.IsUpper(result[i - 1]) ? new char[] { ' ', c } : new char[] { c })).ToArray());
                            }
                            else
                            {
                                res = messagePP;
                            }
                        }
                        catch (Exception)
                        {

                            res = "Invalid Transaction";
                        }


                        message += "Conference: " + pa.ConferenceID + ", Code: " + pa.ProductDescription + " , TransactionID: " + transactionID + ", Amount: " + "$ " + pa.ProductPrice + ", Status: " + res + "\\n";

                        using (StreamWriter sw = File.AppendText(Server.MapPath("/Log.txt")))
                        //using (StreamWriter sw = File.AppendText("C:\\Windows\\System32\\winevt\\Logs\\PDFView.txt"))
                        {
                            sw.WriteLine(Session["username"].ToString() + ", " + System.DateTime.Now.ToString() + ", " + "Not Sucessfully Charged: " + "Conference: " + pa.ConferenceID + ", Code: " + pa.ProductDescription + " , TransactionID: " + transactionID + ", Amount: " + "$ " + pa.ProductPrice + ", Merchant: " + merchant.ToString());
                        }

                    }
                }
            }
        }

        message = message + "'";

        ClientScript.RegisterStartupScript(this.GetType(), "000", "alert(" + message + ");", true);

        ReloadDataGrid();
        
    }

    private void CreatHACopyAndSetTransactionID(string transactionID, DataRow dr, PaymentType pt, string merchantID)
    {
        string conferenceID = dr["Conference"].ToString();
        string fileName = dr["Name"].ToString();

        PdfReader pdfReader = new PdfReader(Server.MapPath("..\\agreements\\" + conferenceID + "\\" + fileName));
        PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(Server.MapPath("..\\agreements\\" + conferenceID) + "\\" + fileName.Replace(".pdf", "") + "_Copy.pdf", FileMode.Create));

        AcroFields pdfFormFields = pdfStamper.AcroFields;

        pdfFormFields.SetField(pt == PaymentType.FinalBalancePayment ? "txtFinalBalanceTransactionID" : "txtDepositTransactionID", transactionID);
        pdfFormFields.SetField("txtMerchant", merchantID);

        pdfReader.Close();
        pdfStamper.Close();

        File.Delete(Server.MapPath("..\\agreements\\" + conferenceID + "\\" + fileName));
        File.Move(Server.MapPath("..\\agreements\\" + conferenceID + "\\" + fileName.Replace(".pdf", "") + "_Copy.pdf"), Server.MapPath("..\\agreements\\" + conferenceID + "\\" + fileName));
        File.Delete(Server.MapPath("..\\agreements\\" + conferenceID + "\\" + fileName.Replace(".pdf", "") + "_Copy.pdf"));
    }

    protected void GetPaymentAuthorizationFirstData(ref PaymentAuthorizationFirstData pa, DataRow dr, PaymentType pt, Merchant merchant)
    {
        pa.CardType = dr["CardType"].ToString();
        pa.CreditCardNumber = dr["CreditCardNumber"].ToString();
        pa.ExparationMonthYear = dr["Expiry_Date"].ToString();
        pa.CardHolderName = dr["CardHolderName"].ToString();
        pa.Test = false;
        pa.ExactID = DatabaseConnection.GetGatewayID(merchant);
        pa.Password = DatabaseConnection.GetGatewayPassword(merchant);
        pa.ProductDescription = dr["code"].ToString();
        pa.Currency = "USD";
        pa.ConferenceID = dr["Conference"].ToString();
        //pa.ProductPrice = pt == PaymentType.DepositPayment? dr["DepositAmount"].ToString() : dr["FinalBalanceAmount"].ToString();
        pa.Token = StringHelpers.Decrypt(dr["Token"].ToString());
        pa.HashID = DatabaseConnection.GetHashID(merchant);
        pa.HashKey = DatabaseConnection.GetHashPassword(merchant);
        pa.Merchant = merchant;
    }
}