﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Duo;

public partial class admin_Login2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Form["sig_response"] == null)
            {
                if (Request.QueryString["Request"] != null)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "123",
                        @"Duo.init({
                                'host': 'api-65fd4c61.duosecurity.com',
                                'sig_request': '" + Request.QueryString["Request"].ToString() + @"',
                                'post_action': '" + ConfigurationManager.AppSettings["Path"].ToString() + @"admin/Login2.aspx'
                                });", true);
                }
            }
            else
            {
                string sig_response = Request.Form["sig_response"];
                var username = Web.VerifyResponse(ConfigurationManager.AppSettings["IntegrationKey"].ToString(), ConfigurationManager.AppSettings["SecretKey"].ToString(), ConfigurationManager.AppSettings["AKey"].ToString(), sig_response, System.DateTime.UtcNow);

                if (username != null)
                {
                    using (StreamWriter sw = File.AppendText(Server.MapPath("/Login.txt")))
                    {
                        sw.WriteLine(username + ", " + System.DateTime.Now.ToString() + ", " + "User logged in");
                    }

                    Session["Admin"] = "True";
                    Session["SuperAdmin"] = false;
                    Session["username"] = username;

                    string[] superAdmin = ConfigurationManager.AppSettings["SuperAdmin"].ToString().Split(';');

                    foreach (string s in superAdmin)
                    {
                        if (s == username)
                        {
                            Session["SuperAdmin"] = true;
                        }
                    }

                    Response.Redirect("https://www.roomhandler.com/pdf4cm/Admin/AdminMain.aspx");
                }
            }
        }
    }
}