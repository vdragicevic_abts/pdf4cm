﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/Admin.master" AutoEventWireup="true" CodeFile="HousingRegistrationAgreementSideNightsFormAdmin.aspx.cs" Inherits="admin_HousingRegistrationAgreementSideNightsFormAdmin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AdminPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script language="javascript" type="text/javascript">

        $("[id*='text']").live('keypress', function (e) {
            return isNumberKey(e);
        });

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }




        $("[id*=chkHeader]").live("click", function () {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function () {
                if (chkHeader.is(":checked")) {
                    if (!$(this).is(":disabled")) {
                        $(this).attr("checked", "checked");
                        $("td", $(this).closest("tr")).addClass("selected");
                    }
                } else {
                    if (!$(this).is(":disabled")) {
                        $(this).removeAttr("checked");
                        $("td", $(this).closest("tr")).removeClass("selected");
                    }
                }
            });
        });
        $("[id*=chkRow]").live("click", function () {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeader]", grid);
            if (!$(this).is(":checked")) {
                $("td", $(this).closest("tr")).removeClass("selected");
                chkHeader.removeAttr("checked");
            } else {
                $("td", $(this).closest("tr")).addClass("selected");
                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }
        });

        function CheckAmounts() {
            var b = 0;
            var grid = $('#<%=dgrForm.ClientID %>').closest("table");
            $("input[type*='text']", grid).each(function () {
                if (($(this).val() == "0.00")) {
                    if (!$(this).is('[readonly]')) {

                        var a = $(this).parent("td").closest('tr').find($("input[type*='checkbox']")).is(":checked");
                        if (a) {
                            $(this).css('background-color', 'palegoldenrod');
                            b++;
                        }
                    }
                }
            });

            if (b > 0) {
                return false;
            } else {

                if (confirm('Are you sure you want to charge selected transactions?')) {
                    return true;
                } else {
                    return false;
                }

            }
        }

    </script>


    <div style="clear: both;"></div>
    <div style="margin: 7px;">
        <span style="color: Black;">Conferences: </span>
        <asp:DropDownList ID="ddlConferences" runat="server" AutoPostBack="True"
            OnSelectedIndexChanged="ddlConferences_SelectedIndexChanged">
        </asp:DropDownList><br />
        <br />
        <asp:DataGrid ID="dgrForm" runat="server" AutoGenerateColumns="False"
            OnItemCommand="dgrForm_ItemCommand" CellPadding="4" ForeColor="#333333"
            GridLines="None" OnItemDataBound="dgrForm_ItemDataBound">
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <EditItemStyle BackColor="#999999" />
            <SelectedItemStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <AlternatingItemStyle BackColor="White" ForeColor="#284775" />
            <ItemStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <Columns>
                <asp:BoundColumn DataField="Code" HeaderText="Code">
                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False"
                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Right" />
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Name" HeaderText="Name">
                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False"
                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Right" />
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Date" HeaderText="Date" DataFormatString="{0:d}"></asp:BoundColumn>
                <asp:TemplateColumn Visible="False">
                    <ItemTemplate>
                        <asp:HyperLink ID="hlDocument" runat="server" NavigateUrl="#" Style="cursor: pointer; text-decoration: none;">HyperLink</asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:ButtonColumn CommandName="Print" Text="Print"></asp:ButtonColumn>
                <asp:TemplateColumn>
                    <ItemTemplate>
                        <asp:HyperLink ID="hlDocument1" runat="server" NavigateUrl="#" Style="cursor: pointer; text-decoration: none;" Visible="false">Extract</asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="DepositTransactionID" HeaderText="Deposit<br/>TransactionID"></asp:BoundColumn>
                <asp:TemplateColumn HeaderText="Deposit&lt;br/&gt;Amount">
                    <ItemTemplate>
                        <span>$</span>
                        <asp:TextBox Width="70px" ID="tbDepositAmount" runat="server" Text='<%# Eval("DepositAmount") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="FinalBalanceTransactionID" HeaderText="FinalBalance<br/>TransactionID"></asp:BoundColumn>
                <asp:TemplateColumn HeaderText="FinalBalance<br/>Amount">
                    <ItemTemplate>
                        <span>$</span>
                        <asp:TextBox Width="70px" ID="tbFinalBalanceAmount" runat="server" Text='<%# Eval("FinalBalanceAmount") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <%--<asp:BoundColumn DataField="FinalBalanceAmount" HeaderText="FinalBalance<br/>Amount"></asp:BoundColumn>--%>
                <asp:TemplateColumn>
                    <HeaderTemplate>
                        <asp:CheckBox ID="chkHeader" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkRow" runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn>
                    <HeaderTemplate>
                        <span>Merchant</span>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:DropDownList ID="ddlMerchant" runat="server" Width="150" AppendDataBoundItems="true" DataValueField="merchant" SelectedValue='<%# Eval("merchant") %>'>
                            <asp:ListItem Text="Select" Value="0" Selected="True" />
                            <%--<asp:listitem text="TERM RETAIL" value="1" selected="False" />
                             <asp:listitem text="TERM ECOMM" value="2" selected="False" />
                             <asp:listitem text="TERM MOTO" value="3" selected="False" />--%>
                            <asp:ListItem Text="IGD INTERNATIONAL GROUP" Value="1" Selected="False" />
                            <asp:ListItem Text="CMR GLOBAL GROUP SERVICE" Value="2" Selected="False" />
                            <asp:ListItem Text="ABTS CONVENTION SERVICES" Value="3" Selected="False" />
                            <asp:ListItem Text="INTERNATIONAL GROUP HOUSING" Value="4" Selected="False" />
                            <asp:ListItem Text="SWME" Value="5" Selected="False" />
                            <asp:ListItem Text="IGD INTERNATIONAL GROUP PP" Value="6" Selected="False" />
                            <asp:ListItem Text="CMR GLOBAL GROUP SERVICE PP" Value="7" Selected="False" />
                            <asp:ListItem Text="IGH INTERNATIONAL GROUP HOUSING PP" Value="8" Selected="False" />
							<asp:listitem text="CMR GLOBAL GROUP SERVICE AMEX PP" value="9" selected="False" />
							<asp:listitem text="IGD INTERNATIONAL GROUP AMEX PP" value="10" selected="False" />
							<asp:listitem text="IGH INTERNATIONAL GROUP HOUSING AMEX PP" value="11" selected="False" />
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        </asp:DataGrid>
    </div>
    <div align="right" style="margin-top: 10px; margin-bottom: 10px;">
        <asp:Button Width="130" runat="server" ID="btnCharge" Text="Charge Selected" OnClientClick="return CheckAmounts()" OnClick="btnCharge_Click" />
    </div>
</asp:Content>

