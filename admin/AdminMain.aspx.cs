﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;


public partial class AdminMain : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //GetAllUsers();
            //GetAllHousingAgreement();

            if (Session["Admin"] == null || (string)Session["Admin"] == "False")
            {
                Response.Redirect("../pdf4cm/Admin/Login.aspx");
            }
        }
    }

    protected void GetAllUsers()
    {
        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        SqlConnection conn = new SqlConnection(connectionString);

        SqlCommand cmd = new SqlCommand();
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "Select Company, FirstName, LastName, eMail, UserName from dbo.Users where Administrator = 0 and userID is null and TimeStamp >= '" + System.DateTime.Now.ToShortDateString() + "'";
        cmd.Connection = conn;

        SqlDataAdapter adapter = new SqlDataAdapter(cmd);

        DataTable dtUsers = new DataTable();
        adapter.Fill(dtUsers);

        if (dtUsers.Rows.Count > 0)
        {
            gvUsers.DataSource = dtUsers;
            gvUsers.DataBind();
            pUsers.Visible = true;
        }
        else
            pUsers.Visible = false;

        
    }

    protected void GetAllHousingAgreement()
    {
        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        SqlConnection conn = new SqlConnection(connectionString);

        SqlCommand cmd = new SqlCommand();
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "Select GroupName, Agency, Address, City, Phone, eMail from dbo.[Housing&RegistrationAgreement] where Date >= '" + System.DateTime.Now.ToShortDateString() + "'";
        cmd.Connection = conn;

        SqlDataAdapter adapter = new SqlDataAdapter(cmd);

        DataTable dtHousingAgreemnt = new DataTable();
        adapter.Fill(dtHousingAgreemnt);

        if (dtHousingAgreemnt.Rows.Count > 0)
        {
            gvHousingAgreements.DataSource = dtHousingAgreemnt;
            gvHousingAgreements.DataBind();
            pAgreements.Visible = true;
        }
        else
            pAgreements.Visible = false;
    }


    protected void gvUsers_RowDataBound(object sender, GridViewRowEventArgs e)
    {       

        if (e.Row.RowType == DataControlRowType.Header)
            e.Row.CssClass = "header";

        //Add CSS class on normal row.
        if (e.Row.RowType == DataControlRowType.DataRow &&
                  e.Row.RowState == DataControlRowState.Normal)
            e.Row.CssClass = "normal";

        //Add CSS class on alternate row.
        if (e.Row.RowType == DataControlRowType.DataRow &&
                  e.Row.RowState == DataControlRowState.Alternate)
            e.Row.CssClass = "alternate";
    }
    protected void gvHousingAgreements_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
            e.Row.CssClass = "header";

        //Add CSS class on normal row.
        if (e.Row.RowType == DataControlRowType.DataRow &&
                  e.Row.RowState == DataControlRowState.Normal)
            e.Row.CssClass = "normal";

        //Add CSS class on alternate row.
        if (e.Row.RowType == DataControlRowType.DataRow &&
                  e.Row.RowState == DataControlRowState.Alternate)
            e.Row.CssClass = "alternate";
    }
}
