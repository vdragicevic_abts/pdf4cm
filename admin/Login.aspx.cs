﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Xml;
using Duo;

public partial class Admin_Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["Admin"] == "True")
        {
            Response.Redirect("../2pdf4cm/Admin/AdminMain.aspx");
        }

        if (!IsPostBack)
        {
            
        }
    }

    public string RemoveSpecialCharacters(string str)
    {
        return Regex.Replace(str, "[^a-zA-Z0-9_.@]+", "", RegexOptions.Compiled);
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        string username = this.tbUserName.Text.Trim().Replace(" ", "");
        string password = this.tbPassword.Text.Trim().Replace(" ", "");

        username = RemoveSpecialCharacters(username);

        XmlDocument doc = new XmlDocument();
        doc.Load(Server.MapPath("..\\") + "\\" + "App_Data\\Users.xml");

        XmlNode node = doc.SelectSingleNode("//user[./password/text()='" + StringHelpers.Encrypt(password) + "' and ./username/text()='" + username + "']");
     
        try
        {          
            if (node !=null)
            {
                //string request = Web.SignRequest(ConfigurationManager.AppSettings["IntegrationKey"].ToString(), ConfigurationManager.AppSettings["SecretKey"].ToString(), ConfigurationManager.AppSettings["AKey"].ToString(), username, System.DateTime.UtcNow);
                //Response.Redirect("https://www.roomhandler.com/pdf4cm/admin/Login2.aspx?request=" + request);

                using (StreamWriter sw = File.AppendText(Server.MapPath("/Login.txt")))
                {
                    sw.WriteLine(username + ", " + System.DateTime.Now.ToString());
                }

                Session["Admin"] = "True";
                Session["SuperAdmin"] = false;
                Session["username"] = username;

                string[] superAdmin = ConfigurationManager.AppSettings["SuperAdmin"].ToString().Split(';');

                foreach (string s in superAdmin)
                {
                    if (s == username)
                    {
                        Session["SuperAdmin"] = true;
                    }
                }

                Response.Redirect("../2pdf4cm/Admin/AdminMain.aspx");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "001", "alert('Incorrect Login!!!!');", true);    
            }
        }
        catch (Exception ex)
        {          
            Session["ERROR"] = ex.Message;
        }       
    }
}
