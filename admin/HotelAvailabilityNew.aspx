﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/Admin.master" AutoEventWireup="true" CodeFile="HotelAvailabilityNew.aspx.cs" Inherits="admin_HotelAvailabilityNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AdminPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div style="clear:both;"></div>
<div style="margin:7px; margin-top:0px;">
        <div style="padding:10px; border: 1px solid grey; background-color:lightgray; width:400px; height:58px;">
            <div style="float:left; margin-right:10px;">
                <asp:DropDownList Width="150px" runat="server" AutoPostBack="true" ID="ddlConferences" OnSelectedIndexChanged="ddlConferences_SelectedIndexChanged"></asp:DropDownList>
            </div>
            <div style="float:right;">
                <asp:Button Width="200px" runat="server" ID="btnUpdate" Text="Update" Style="float: right; margin-right: 0px; margin-top: 0px; margin-bottom: 10px;" OnClick="btnUpdate_Click" />
            </div>
        </div>
        <br/><br/><br/>
        <asp:GridView runat="server" ID="gvHotelAvailability" Font-Size="10pt" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" OnItemDataBound="gvHotelAvailability_ItemDataBound" OnDataBinding="gvHotelAvailability_DataBinding" OnRowDataBound="gvHotelAvailability_RowDataBound" OnSorting="gvHotelAvailability_Sorting" >
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:BoundField DataField="Vendor" HeaderText="Hotel" />
                <asp:BoundField DataField="Code" HeaderText="Code" />
                <asp:BoundField DataField="Name" HeaderText="Name" />
                 <%--<asp:BoundField DataField="Quantity" HeaderText="Quantity" />--%>
                <asp:BoundField DataField="Quantity" HeaderText="QuantityGrp" />
                <asp:BoundField DataField="QuantityIndividuals" HeaderText="QuantityInd" />
                <asp:BoundField DataField="ForDate" HeaderText="Date" />
                <asp:BoundField DataField="Quantity" HeaderText="Quantity" />
                <asp:BoundField DataField="SideNights" HeaderText="SideNights" />
                <asp:BoundField DataField="SellingPrice" HeaderText="SellingPrice" />
                <asp:BoundField DataField="Occupancy" HeaderText="Occupancy" />
                <asp:BoundField DataField="VariableRates" HeaderText="VariableRates" />
                <asp:BoundField DataField="TimeStamp" HeaderText="Last Update" />
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
       <%-- <asp:SqlDataSource ID="HotelAvailability" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="
          Select v.Name Vendor
          , rb.Code
          , rb.Name
          , rb.Occupancy
          , CASE When rb.VariableRates = 0 then rb.SellingPrice else rbd.SellingPrice end SellingPrice
          , rb.VariableRates
          , Convert(nvarchar, rbd.ForDate, 101) ForDate
          , rbd.Quantity
          , rbd.QuantityIndividuals
          , rbd.SideNights
          , rb.TimeStamp
          From RoomBlock rb
          inner join RoomBlockDetails rbd on rb.IDRoomBlock = rbd.IDRoomBlock
          inner join Vendor v on rb.Code = v.Code">
        </asp:SqlDataSource>--%>
    </div>
</asp:Content>

