﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PDFData.aspx.cs" Inherits="admin_PDFData" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Extracted Data</title>
    <style type="text/css">
        .naslov
        {
            font-weight:bold;
            font-family:Arial;
        }
    
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <span class="naslov">Sponsoring Group Name: </span><asp:Label ID="lblSponsoringGroupName" runat="server"></asp:Label><br />
        <span class="naslov">Agency: </span><asp:Label ID="lblAgency" runat="server"></asp:Label><br />
        <span class="naslov">Address: </span><asp:Label ID="lblAddress" runat="server"></asp:Label><br />
        <span class="naslov">City: </span><asp:Label ID="lblCity" runat="server"></asp:Label><br />
        <span class="naslov">Postal Code: </span><asp:Label ID="lblPostalCode" runat="server"></asp:Label><br />
        <span class="naslov">Country: </span><asp:Label ID="lblCountry" runat="server"></asp:Label><br />
        <span class="naslov">Phone: </span><asp:Label ID="lblPhone" runat="server"></asp:Label><br />
        <span class="naslov">Email: </span><asp:Label ID="lblEmail" runat="server"></asp:Label><br />       
        <span class="naslov">Hotel First Choice: </span><asp:Label ID="lblHotel1" runat="server"></asp:Label><br />
        <span class="naslov">Hotel Second Choice: </span><asp:Label ID="lblHotel2" runat="server"></asp:Label><br />
        <span class="naslov">Hotel Third Choice: </span><asp:Label ID="lblHotel3" runat="server"></asp:Label><br />
        <span class="naslov">Client Authorized Representative: </span><asp:Label ID="lblClientAuthorizedRepresentative" runat="server"></asp:Label><br />
        <span class="naslov">Title: </span><asp:Label ID="lblTitle" runat="server"></asp:Label><br />
        <span class="naslov">Signature: </span><asp:Label ID="lblSignature" runat="server"></asp:Label><br />
        <span class="naslov">Date: </span><asp:Label ID="lblDate" runat="server"></asp:Label><br />
        <span class="naslov">Card Number: </span><asp:Label ID="lblCardNumber" runat="server"></asp:Label><br />
        <span class="naslov">CVV: </span><asp:Label ID="lblCVV" runat="server"></asp:Label><br />
        <span class="naslov">Card Holder Name: </span><asp:Label ID="lblCardHolderName" runat="server"></asp:Label><br />
        <span class="naslov">Card Holder Signature: </span><asp:Label ID="Label2" runat="server"></asp:Label><br />    
    </div>
    </form>
</body>
</html>
