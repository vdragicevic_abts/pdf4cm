﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public partial class Admin_Admin : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Admin"] == null || (String)Session["Admin"] == "False")
        {
            Response.Redirect("../2pdf4cm/Admin/Login.aspx");
        }
    }
    
    protected void logout_Click(object sender, EventArgs e)
    {
        Session["Admin"] = null;

        using (StreamWriter sw = File.AppendText(Server.MapPath("/Login.txt")))
        {
            sw.WriteLine(Session["username"].ToString() + ", " + System.DateTime.Now.ToString() + ", " + "Logged Out");
        }

        Response.Redirect("../2pdf4cm/Admin/Login.aspx");
    }
}
