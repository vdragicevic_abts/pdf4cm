﻿function RenderTableNights(conferenceName) {
    $.ajax({
        type: "POST",
        url: "PDFGenerator.aspx/RenderTableNights",
        data: "{ 'conference':'" + conferenceName + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $('#divNights').html(data.d);
        },
        error: function (xhr, errStatus, errorThrown) {
            alert(xhr.responseText);
            var err = JSON.parse(xhr.responseText);
            var errorMessage = err.Message;
            alert(errorMessage);
        }
    });
}

function RenderTableHiddenFields(conferenceName) {
    $.ajax({
        type: "POST",
        url: "PDFGenerator.aspx/CreateHiddenFields",
        data: "{ 'conference':'" + conferenceName + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $('#divHFNights').html(data.d);
        },
        error: function (xhr, errStatus, errorThrown) {
            alert(xhr.responseText);
            var err = JSON.parse(xhr.responseText);
            var errorMessage = err.Message;
            alert(errorMessage);
        }
    });
}

function RenderTableNightsPrices(conferenceName) {
    $.ajax({
        type: "POST",
        url: "PDFGenerator.aspx/RenderTableNightsPrices",
        data: "{ 'conference':'" + conferenceName + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $('#divNightPrices').html(data.d);
        },
        error: function (xhr, errStatus, errorThrown) {
            alert(xhr.responseText);
            var err = JSON.parse(xhr.responseText);
            var errorMessage = err.Message;
            alert(errorMessage);
        }
    });
}

function LoadConference(conferenceName) {
    $.ajax({
        type: "POST",
        url: "PDFGenerator.aspx/GetHotelsForConference",
        data: "{ 'conference':'" + conferenceName + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $('#ddlHotels').empty();
            $('#ddlHotels1').empty();
            $('#ddlHotels2').empty();

            var ddlHotels = $('#ddlHotels');
            var ddlHotels1 = $('#ddlHotels1');
            var ddlHotels2 = $('#ddlHotels2');

            ddlHotels1.append(new Option('Select', '-1'));
            ddlHotels2.append(new Option('Select', '-1'));
            ddlHotels.append(new Option('Select', '-1'));

            $.each(data.d, function () {
                ddlHotels.append(
                    new Option(this['Text'], this['Value'])
                );
                ddlHotels1.append(
                    new Option(this['Text'], this['Value'])
                );
                ddlHotels2.append(
                    new Option(this['Text'], this['Value'])
                );
            });

            $('#ddlHotels').val(-1);
            $('#ddlHotels1').val(-1);
            $('#ddlHotels2').val(-1);

            LoadConferenceDates(conferenceName);
            RenderTableNights(conferenceName);
            RenderTableNightsPrices(conferenceName);
            RenderTableHiddenFields(conferenceName);
        },
        error: function (xhr, errStatus, errorThrown) {
            alert(xhr.responseText);
            var err = JSON.parse(xhr.responseText);
            var errorMessage = err.Message;
            alert(errorMessage);
        }
    });
}

function LoadConferenceDates(conferenceName) {
    $.ajax({
        type: "POST",
        url: "PDFGenerator.aspx/GetConferenceDates",
        data: "{ 'conference':'" + conferenceName + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $('#ddlDateFrom').empty();
            $('#ddlDateTo').empty();
            var ddlDateFrom = $('#ddlDateFrom');
            var ddlDateTo = $('#ddlDateTo');
            ddlDateFrom.append(new Option('Select', '-1'));
            ddlDateTo.append(new Option('Select', '-1'));

            $.each(JSON.parse(data.d),
                function () {
                    ddlDateFrom.append('<option sidenight="' + this.SideNight + '" value="' + this.Date + '">' + this.Date + '</option>');
                    ddlDateTo.append('<option sidenight="' + this.SideNight + '" value="' + this.Date + '">' + this.Date + '</option>');
                });
            $('#ddlDateFrom').val(-1);
            $('#ddlDateTo').val(-1);
        },
        error: function (xhr, errStatus, errorThrown) {
            alert(xhr.responseText);
            var err = JSON.parse(xhr.responseText);
            var errorMessage = err.Message;
            alert(errorMessage);
        }
    });
}

function LoadSingleRoomType(conferenceName, code) {
    $.ajax({
        type: "POST",
        url: "PDFGenerator.aspx/GetRoomTypeForConference",
        data: "{ 'conference':'" + conferenceName + "','code':'" + code + "','roomType':1}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $('#ddlRoomTypeSingle').empty();
            var ddlRoomTypeSingle = $('#ddlRoomTypeSingle');
            ddlRoomTypeSingle.append(new Option('Select', '-1'));
            $.each(JSON.parse(data.d),
                function () {
                    ddlRoomTypeSingle.append('<option IDServiceItem="' + this.IdServiceItem + '" RoomTypeName="' + this.RoomTypeName + '" sglPrice="' + this.SglPrice + '"  value="' + this.Occupancy + '">' + this.Name + '</option>');
                });
        },
        error: function (xhr, errStatus, errorThrown) {
            alert(xhr.responseText);
            var err = JSON.parse(xhr.responseText);
            var errorMessage = err.Message;
            alert(errorMessage);
        }
    });
}

function LoadDoubleRoomType(conferenceName, code) {
    $.ajax({
        type: "POST",
        url: "PDFGenerator.aspx/GetRoomTypeForConference",
        data: "{ 'conference':'" + conferenceName + "','code':'" + code + "','roomType':2}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $('#ddlRoomTypeDouble').empty();
            var ddlRoomTypeDouble = $('#ddlRoomTypeDouble');
            ddlRoomTypeDouble.append(new Option('Select', '-1'));
            $.each(JSON.parse(data.d),
                function () {
                    ddlRoomTypeDouble.append('<option IDServiceItem="' + this.IdServiceItem + '" RoomTypeName="' + this.RoomTypeName + '" dblPrice="' + this.DblPrice + '"  value="' + this.Occupancy + '">' + this.Name + '</option>');
                });
        },
        error: function (xhr, errStatus, errorThrown) {
            alert(xhr.responseText);
            var err = JSON.parse(xhr.responseText);
            var errorMessage = err.Message;
            alert(errorMessage);
        }
    });
}

function LoadRoomTypes(code) {
    var conference = $('#ddlConferences :selected').text();
    var hotelCode = code;
    LoadSingleRoomType(conference, hotelCode);
    LoadDoubleRoomType(conference, hotelCode);
}

function SetSingleRoomDisplayName() {

    if ($('#ddlRoomTypeSingle')[0].value != '-1') {
        var singleRoomName = $('#ddlRoomTypeSingle option:selected').attr('roomtypename');
        $('#txtSingleDisplayName').val(singleRoomName);
        $('#txtSingleDisplayName').show();
        $('#spanDoubleRoomType').css('margin-top', '-10px');
        $('#spanSingleDisplayName').show();
    }
    else {
        $('#txtSingleDisplayName').hide();
        $('#spanNumberOfRooms').css('margin-top', '7px');
        $('#spanDoubleRoomType').css('margin-top', '0px');
        $('#spanSingleDisplayName').hide();
    }
}

function SetDoubleRoomDisplayName() {
    if ($('#ddlRoomTypeDouble')[0].value != '-1') {
        var doubleRoomName = $('#ddlRoomTypeDouble option:selected').attr('roomtypename');
        $('#txtDoubleDisplayName').val(doubleRoomName);
        $('#txtDoubleDisplayName').show();
        $('#spanNumberOfRooms').css('margin-top', '-10px');
        $('#ddlDateFrom').css('margin-top', '18px');
        $('#spanDoubleDisplayName').show();
    }
    else {
        $('#txtDoubleDisplayName').hide();
        $('#spanNumberOfRooms').css('margin-top', '7px');
        $('#ddlDateFrom').css('margin-top', '5px');
        $('#spanDoubleDisplayName').hide();
    }
}

function SetPrice() {

    if ($('#ddlRoomTypeSingle')[0].value == '-1'
        && $('#ddlRoomTypeDouble')[0].value == '-1') {
        $('#lblSingleDoublePrice').html('$ ' + 0 + ' / ' + '$ ' + 0);
        return;
    }


    if ($('#ddlRoomTypeSingle')[0].selectedIndex != '-1') {
        $('#ddlRoomTypeDouble').attr('display', 'false');
    }
    else {
        $('#ddlRoomTypeDouble').attr('display', 'true');
    }


    if ($('#ddlRoomTypeDouble')[0].selectedIndex != '-1') {
        $('#ddlRoomTypeSingle').attr('display', 'false');
    }
    else {
        $('#ddlRoomTypeSingle').attr('display', 'true');
    }

    $('#divNightPrices').hide();

    var sprice = parseFloat($('#ddlRoomTypeSingle option:selected').attr('sglPrice')).toFixed(2);
    var dprice = parseFloat($('#ddlRoomTypeDouble option:selected').attr('dblPrice')).toFixed(2);

    if (!isNaN(sprice) || !isNaN(dprice)) {

        sprice = isNaN(sprice) ? 0 : sprice;
        dprice = isNaN(dprice) ? 0 : dprice;

        $('#lblSingleDoublePrice').html('$ ' + sprice + ' / ' + '$ ' + dprice);

    } else {

        $('#divNightPrices').show();
        $('#divNightPrices').height(100);

        $('#lblSingleDoublePrice').hide();
        $('#lblNightPrices').show();

        var selectobject = $('#ddlHotels option:selected');

        var datefrom = document.getElementById("ddlDateFrom");

        for (var i = 1; i < datefrom.length; i++) {

            var ch = '/';
            var date = datefrom.options[i].value.replace('/', "-").replace('/', "-");

            var d = new Date(datefrom.options[i].value);

            $('#tbsr' + d.getDate()).val(parseFloat(selectobject.attr('[' + date + ']').split('|')[0].toString()).toFixed(2));
            $('#tbdr' + d.getDate()).val(parseFloat(selectobject.attr('[' + date + ']').split('|')[1].toString()).toFixed(2));

        }
    }
}

function isNumeric(keyCode) {
    return ((keyCode >= 48 && keyCode <= 57) || keyCode == 8 || keyCode == 9 || keyCode == 189 || (keyCode >= 96 && keyCode <= 105)); // keyCode == 32 ||
}

$(document).ready(
    function () {

        // LOOP FOR NO_OF_ROOMS SELECT TAG
        var options = $('#ddlRooms').html();
        for (i = 1; i < 401; i++) {
            var nja = "<option value='" + i + "'>" + i + "</option>";
            options += nja;
        }
        $('#ddlRooms').html(options);


        $(".formFields").focus(
            function () {
                $(this).css('background', '#f1f1f1');
            }
        )

        $(".formFields").blur(
            function () {
                $(this).css('background', '#fff');
            }
        )


        $currentItem = 0;
        $lastItem = 0;
        for ($i = 1; $i < 20; $i++) {
            if ($('#dlRSS_ctl0' + $i + '_TitleLink').length > 0) {
                $('#dlRSS_ctl0' + $i + '_TitleLink').parent().parent().css('display', 'none');
                $lastItem = $i;
            }
        }
        $('#previous').click(
            function () {
                if ($currentItem > 0) {
                    $('#dlRSS_ctl0' + $currentItem + '_TitleLink').parent().parent().css('display', 'none');
                    $('#dlRSS_ctl0' + ($currentItem - 1) + '_TitleLink').parent().parent().fadeIn();
                    $currentItem--;
                    $('#newsNav').children('a:nth-child(2)').css('color', '#0A568C');
                }
                else {
                    $(this).css('color', '#999');
                }
            }
        );
        $('#next').click(
            function () {
                if ($currentItem < $lastItem) {
                    $('#dlRSS_ctl0' + $currentItem + '_TitleLink').parent().parent().css('display', 'none');
                    $('#dlRSS_ctl0' + ($currentItem + 1) + '_TitleLink').parent().parent().fadeIn();
                    $currentItem++;
                    $('#newsNav').children('a:nth-child(1)').css('color', '#0A568C');
                }
                else {
                    $(this).css('color', '#999');
                }
            }
        );

        GetCompanyInformationByEmail();
        GetCompanyInformationByCompanyName();
        GetCompanyInformationByGroupName();
    }
)

var _complete = false;
var _complete_2 = false;
var _complete_3 = false;

function copyAllTableNights2HiddenFields() {

    $("#divNights table tr td").each(function() {
        // $(this) refers to the div
        if ($(this)[0].firstChild.type === 'text') {
            if ($(this)[0].firstChild.id.substring(0, 3) !== 'tbd') {
                $('#hf' + $(this)[0].firstChild.id.substring(2)).val($(this)[0].firstChild.value);
            } else {
                $('#hfd' + $(this)[0].firstChild.id.substring(3)).val($(this)[0].firstChild.value);
            }
        } 
    });
}

function resetAllHiddenFields() {

    $("#divNights table tr td").each(function () {
        // $(this) refers to the div
        if ($(this)[0].firstChild.type == 'text') {
            $($(this)[0].firstChild.id.substring(2)).val($('#ddlRooms').val());
        }
    }
    );

}

function showTableNightPrices() {

    $("#divNightPrices").dialog('open');
}

function showTableNights() {

    if ($('#ddlRooms').val() !== '-1') {

        if ($("#hfEditNights").val() === 'false') {


            if (($('#ddlRoomTypeSingle').val() !== "-1") && (($('#ddlRoomTypeDouble').val() === "-1"))) {
                $('#divNights table tr:eq(1)').show();
                $('#divNights table tr:eq(2)').hide();
            }

            if (($('#ddlRoomTypeSingle').val() === "-1") && (($('#ddlRoomTypeDouble').val() !== "-1"))) {
                $('#divNights table tr:eq(1)').hide();
                $('#divNights table tr:eq(2)').show();
            }

            if (($('#ddlRoomTypeSingle').val() !== "-1") && (($('#ddlRoomTypeDouble').val() !== "-1"))) {
                $('#divNights table tr:eq(1)').show();
                $('#divNights table tr:eq(2)').show();
            }

            //switch ($('#ddlRoomType').val()) {
            //    case '3':
            //        {
            //            $('#divNights table tr:eq(1)').show();
            //            $('#divNights table tr:eq(2)').show();
            //        }
            //        break;
            //    case '2':
            //        {
            //            $('#divNights table tr:eq(1)').hide();
            //            $('#divNights table tr:eq(2)').show();
            //        }
            //        break;
            //    case '1':
            //        {
            //            $('#divNights table tr:eq(1)').show();
            //            $('#divNights table tr:eq(2)').hide();
            //        }
            //        break;
            //}

            $("#divNights table tr td").each(function () {
                // $(this) refers to the div
                if ($(this)[0].firstChild.type === 'text') {

                    if (($('#ddlRoomTypeSingle').val() !== "-1") && (($('#ddlRoomTypeDouble').val() === "-1"))) {
                        if ($(this)[0].firstChild.id.indexOf('tbd') === '-1') {
                            $(this)[0].firstChild.value = $('#ddlRooms').val();
                        }
                        else {
                            $(this)[0].firstChild.value = '0';
                        }
                    }

                    if (($('#ddlRoomTypeSingle').val() === "-1") && (($('#ddlRoomTypeDouble').val() !== "-1"))) {
                        if ($(this)[0].firstChild.id.indexOf('tbd') === '0') {
                            $(this)[0].firstChild.value = $('#ddlRooms').val();
                        }
                        else {
                            $(this)[0].firstChild.value = '0';
                        }
                    }

                    if (($('#ddlRoomTypeSingle').val() !== "-1") && (($('#ddlRoomTypeDouble').val() !== "-1"))) {
                        $(this)[0].firstChild.value = $('#ddlRooms').val();
                    }

                    //switch ($('#ddlRoomType').val()) {
                    //    case '3':
                    //        {
                    //            $(this)[0].firstChild.value = $('#ddlRooms').val();
                    //        }
                    //        break;
                    //    case '2':
                    //        {
                    //            if ($(this)[0].firstChild.id.indexOf('tbd') == '0') {
                    //                $(this)[0].firstChild.value = $('#ddlRooms').val();
                    //            }
                    //            else {
                    //                $(this)[0].firstChild.value = '0';
                    //            }
                    //        }
                    //        break;


                    //    case '1':
                    //        {
                    //            if ($(this)[0].firstChild.id.indexOf('tbd') == '-1') {
                    //                $(this)[0].firstChild.value = $('#ddlRooms').val();
                    //            }
                    //            else {
                    //                $(this)[0].firstChild.value = '0';
                    //            }
                    //        }
                    //        break;
                    //}
                }
            }
            );
        }

        $("#divNights").dialog('open');

    }
    else {

        alert('Please first select number of rooms to continue with editing!');

    }


}

function closeTableNights() {
    $("#divNights").dialog('close');
}

function GetCustomerInformation(bookingID) {

    $.ajax({
        type: "POST",
        url: "Users.asmx/GetProposalCompanyInformationByBookingNumber",
        data: "{ 'bookingNumber' : '" + bookingID + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if ($.parseJSON(data.d).length === 0) {
                alert('No data for selected BookingID.');
                $('#bookingNumber').removeClass('loadinggif');
                return;
            }

            var parsed = $.parseJSON(data.d);
            $.each(parsed, function (i, jsondata) {
                $('#tbFullName').val(jsondata.GroupName);
                $('#tbPhone').val(jsondata.ContactPhone);
                $('#tbEmail').val(jsondata.ContactEmail);
                $('#tbAddress').val(jsondata.Address);
                $("#ddlCountry option:contains(" + jsondata.Country + ")").attr('selected', 'selected');
                $("#tbAgencyName").val(jsondata.CompanyName);
                $("#tbCity").val(jsondata.City);
                $("#tbPhone").val(jsondata.ContactPhone);
                $("#tbEmail").val(jsondata.ContactEmail);
                $("#tbZip").val(jsondata.ZIP);

                if ($("#ddlConferences option:selected").text() === jsondata.ProjectName.substring(5).split('/')[0] + jsondata.ProjectName.substring(0, 4)) {

                    $("#ddlHotels option[value='" + jsondata.HotelCode + "']").prop('selected', true);
                    $('#bookingNumber').removeClass('loadinggif');

                    //if ($("#ddlHotels").find(":contains('" + jsondata.Hotel.capitalize('title') + "')").length > 0) {
                    //    $("#ddlHotels").find(":contains('" + jsondata.Hotel.capitalize('title') + "')").attr('selected', 'selected');
                    //    $("#ddlHotels").change();
                    //    $('#bookingNumber').removeClass('loadinggif'); 
                    //}                              
                }
                else {
                    $("#ddlConferences option:contains(" + jsondata.ProjectName.substring(5).split('/')[0] + jsondata.ProjectName.substring(0, 4) + ")").attr('selected', 'selected');
                    $("#ddlConferences").change();
                }

                //if ($("#ddlHotels").find(":contains('" + jsondata.Hotel.capitalize('title') + "')").length > 0) {
                //    $("#ddlHotels").find(":contains('" + jsondata.Hotel.capitalize('title') + "')").attr('selected', 'selected');                            
                //}
            });

            $('#bookingNumber').removeClass('loadinggif');
        },
        error: function (XHR, errStatus, errorThrown) {
            var err = JSON.parse(XHR.responseText);
            errorMessage = err.Message;
            alert(errorMessage);
        }
    });
}



function GetCompanyInformationByEmail() {

    $("#tbEmail").autocomplete({
        source: function (request, response) {
            if (_complete) {
                LoaderShow(true, $("#loaderEmail"), $("#imgEmail"));
                $.ajax({
                    type: "POST",
                    url: "PDFGenerator.aspx/GetCompanyInfoByEmail",
                    data: "{ 'email' : '" + $("#tbEmail").val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (data.d.length > 0) {
                            //response(data.d);
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[0],
                                    value: item.split('-')[1]
                                }
                            }))
                        }
                        else {
                            //$("#loaderEmail").hideLoading();
                            LoaderShow(false, $("#loaderEmail"), $("#imgEmail"));
                            alert("No Match");
                        }
                        _complete = false;
                        LoaderShow(false, $("#loaderEmail"), $("#imgEmail"));
                    },
                    error: function (result) {
                        _complete = false;
                        LoaderShow(false, $("#loaderEmail"), $("#imgEmail"));
                        //$("#loaderEmail").hideLoading();
                        alert("No Match");
                    }
                });
            }
        },
        select: function (event, ui) {
            _complete = false;
            GetCompanyDetailsByCompanyCode(ui.item.value);
            LoaderShow(false, $("#loaderEmail"), $("#imgEmail"));
            //$("#loaderEmail").hideLoading();
            event.preventDefault();
        },
        minLength: 3
    }).focus(function () {
        _complete = false;
        LoaderShow(false, $("#loaderEmail"), $("#imgEmail"));
        //$("#loaderEmail").hideLoading();
        event.preventDefault();
    });
}

function GetCompanyInformationByCompanyName() {
    $("#tbAgencyName").autocomplete({
        source: function (request, response) {
            if (_complete_2) {
                //$("#loaderAgency").showLoading();
                LoaderShow(true, $("#loaderAgency"), $("#imgAgencyName"));
                $.ajax({
                    type: "POST",
                    url: "PDFGenerator.aspx/GetCompanyInfoByCompanyName",
                    data: "{ 'agency' : '" + $("#tbAgencyName").val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        //response(data.d);
                        if (data.d.length > 0) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[0],
                                    value: item.split('-')[1]
                                }
                            }))
                        }
                        else {
                            LoaderShow(false, $("#loaderAgency"), $("#imgAgencyName"));
                            //$("#loaderAgency").hideLoading();
                            alert("No Match");
                        }
                        _complete_2 = false;
                        LoaderShow(false, $("#loaderAgency"), $("#imgAgencyName"));
                    },
                    error: function (result) {
                        _complete_2 = false;
                        //$("#loaderAgency").hideLoading();
                        LoaderShow(false, $("#loaderAgency"), $("#imgAgencyName"));
                        alert("No Match");
                    }
                });
            }
        },
        select: function (event, ui) {
            _complete_2 = false;
            //$('#tbAgencyName').val(ui.item.value);
            GetCompanyDetailsByCompanyCode(ui.item.value);
            LoaderShow(false, $("#loaderAgency"), $("#imgAgencyName"));
            //$("#loaderAgency").hideLoading();
            event.preventDefault();
        },
        minLength: 3
    }).focus(function () {
        _complete_2 = false;
        LoaderShow(false, $("#loaderAgency"), $("#imgAgencyName"));
        //$("#loaderAgency").hideLoading();
        event.preventDefault();
    });
}

function GetCompanyInformationByGroupName() {
    $("#tbFullName").autocomplete({
        source: function (request, response) {
            if (_complete_3) {
                //$("#loaderAgency").showLoading();
                LoaderShow(true, $("#loaderGroupName"), $("#imgGroupName"));
                $.ajax({
                    type: "POST",
                    url: "PDFGenerator.aspx/GetCompanyInfoByCompanyName",
                    data: "{ 'agency' : '" + $("#tbFullName").val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        //response(data.d);
                        if (data.d.length > 0) {
                            response($.map(data.d,
                                function(item) {
                                    return {
                                        label: item.split('-')[0],
                                        value: item.split('-')[1]
                                    }
                                }));
                        }
                        else {
                            LoaderShow(false, $("#loaderGroupName"), $("#imgGroupName"));
                            //$("#loaderAgency").hideLoading();
                            alert("No Match");
                        }
                        _complete_3 = false;
                        LoaderShow(false, $("#loaderGroupName"), $("#imgGroupName"));
                    },
                    error: function (result) {
                        _complete_3 = false;
                        //$("#loaderAgency").hideLoading();
                        LoaderShow(false, $("#loaderGroupName"), $("#imgGroupName"));
                        alert("No Match");
                    }
                });
            }
        },
        select: function (event, ui) {
            _complete_3 = false;
            //$('#tbAgencyName').val(ui.item.value);
            GetCompanyDetailsByCompanyCode(ui.item.value);
            LoaderShow(false, $("#loaderGroupName"), $("#imgGroupName"));
            //$("#loaderAgency").hideLoading();
            event.preventDefault();
        },
        minLength: 3
    }).focus(function () {
        _complete_3 = false;
        LoaderShow(false, $("#loaderGroupName"), $("#imgGroupName"));
        //$("#loaderAgency").hideLoading();
        event.preventDefault();
    });
}


function GetCompanyDetailsByCompanyCode(code) {

    $.ajax({
        type: "POST",
        url: "PDFGenerator.aspx/GetCompanyDetailsByCompanyCode",
        data: "{ 'companyCode' : '" + code + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var companyDetails = jQuery.parseJSON(data.d);
            ResetCompanyDetails();
            PopulateCompanyDetails(companyDetails[0]);
        },
        error: function (result) {
            alert("No Match");
        }
    });
}

function PopulateCompanyDetails(details) {

    details.Country = details.Country == "USA" ? 'United States' : details.Country;

    if ($('#tbAgencyName').is(":focus") || $('#tbEmail').is(":focus")) {
        $('#tbAgencyName').val(details.Name);
    }
    else if ($('#tbFullName').is(":focus")) {
        $('#tbFullName').val(details.Name);
    }

    $('#tbAddress').val(details.Address);
    $('#tbZip').val(details.ZIP);
    $('#tbCity').val(details.City);

    $("#ddlCountry option").each(function () {
        if ($(this).html() == details.Country) {
            $(this).attr("selected", "selected");
            return;
        }
    });

    $('#ddlState').val(details.State);
    $('#tbPhone').val(details.GeneralPhone);

    $('#hfCompanyCode').val(details.Code);
}

function ResetCompanyDetails() {
    $('#tbAgencyName').val('');
    $('#tbAddress').val('');
    $('#tbZip').val('');
    $('#tbCity').val('');
    $("#ddlCountry option:first-child").attr("selected", "selected");
    $('#ddlState').val('');
    $('#tbPhone').val('');
}

function LoaderShow(show, obj, btn) {
    if (show) {
        $(obj).showLoading();
        $(btn).attr("src", "");
    }
    else {
        $(obj).hideLoading();
        $(btn).attr("src", "visuals/img/if_magnifier_zoom_12100.png");
    }
}

//JQ popup
$(document).ready(
    function () {
        $("#divNights").dialog({
            height: 200,
            width: 800,
            modal: true,
            autoOpen: false,
            show: 'slide',
            resizable: false,

            buttons: {
                "Confirm": function () {

                    //Prikaz podataka iz tabele tableNights  

                    $("#hfEditNights").val('true');
                    copyAllTableNights2HiddenFields();
                    $(this).dialog("close");
                },
                Cancel: function () {
                    $("#hfEditNights").val('false');
                    resetAllHiddenFields();
                    $(this).dialog("close");
                }
            }
        });
    }
);

$(document).ready(
    function () {
        $('#divNightPrices').hide();
    });

function GeneratePdf() {
    var pdfParameters = new Object();
    pdfParameters.Conference = $("#ddlConferences option:selected").text();
    pdfParameters.RoomTypeSingleIndex = $('#ddlRoomTypeSingle option:selected').index();
    pdfParameters.RoomTypeDoubleIndex = $('#ddlRoomTypeDouble option:selected').index();;
    pdfParameters.DateFromIndex = $('#ddlDateFrom option:selected').index();
    pdfParameters.DateToIndex = $('#ddlDateTo option:selected').index();
    pdfParameters.RoomTypeSingleName = $('#txtSingleDisplayName').val();
    pdfParameters.RoomTypeDoubleName = $('#txtDoubleDisplayName').val();
    pdfParameters.Address = $('#tbAddress').val();
    pdfParameters.City = $("#tbCity").val();
    pdfParameters.Zip = $("#tbZip").val();
    pdfParameters.Phone = $('#tbPhone').val();
    pdfParameters.FullName = $('#tbFullName').val();
    pdfParameters.Email = $('#tbEmail').val();
    pdfParameters.AgencyName = $("#tbAgencyName").val();
    pdfParameters.HotelName1 = $('#ddlHotels option:selected').text();
    pdfParameters.HotelCode1 = $('#ddlHotels option:selected').val();
    pdfParameters.HotelName2 = $('#ddlHotels1 option:selected').text();
    pdfParameters.HotelCode2 = $('#ddlHotels1 option:selected').val();
    pdfParameters.HotelName3 = $('#ddlHotels2 option:selected').text();
    pdfParameters.HotelCode3 = $('#ddlHotels2 option:selected').val();
    pdfParameters.HfEditNights = $("#hfEditNights").val();
    pdfParameters.CountryName = $("#ddlCountry option:selected").text();
    pdfParameters.CountryCode = $("#ddlCountry option:selected").val();;
    pdfParameters.HfCompanyCode = $('#hfCompanyCode').val();
    pdfParameters.CreateProposal = $('#chkCreatePropsal').is(':checked');
    pdfParameters.NumberOfRooms = $('#ddlRooms').val();
    pdfParameters.HiddenFieldsSingleNights = [];
    pdfParameters.HiddenFieldsDoubleNights = [];


    if ($("#hfEditNights").val() === 'true') {

        $("#divNights table tr td").each(function () {
            if ($(this)[0].firstChild.type === 'text') {
                if ($(this)[0].firstChild.id.substring(0, 3) !== 'tbd') {
                    //alert($(this)[0].firstChild.value);
                    pdfParameters.HiddenFieldsSingleNights.push($(this)[0].firstChild.value);
                    //$('#hf' + $(this)[0].firstChild.id.substring(2)).val($(this)[0].firstChild.value);
                } else {
                    //alert($(this)[0].firstChild.value);
                    pdfParameters.HiddenFieldsDoubleNights.push($(this)[0].firstChild.value);
                    //$('#hfd' + $(this)[0].firstChild.id.substring(3)).val($(this)[0].firstChild.value);
                }
            }
        });
    }

    $.ajax({
        type: "POST",
        url: "PDFGenerator.aspx/GeneratePdf",
        data: JSON.stringify({ 'pdfParameters': pdfParameters }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            var links = JSON.parse(data.d);

            $('#lnkPickNights').attr('href', links[0]);
            $('#lnkSideNights').attr('href', links[1]);

            if (links[0] === undefined || links[0].length === 0) {
                $('#lnkPickNights').hide();
            }

            if (links[1] === undefined || links[1].length === 0) {
                $('#lnkSideNights').hide();
            }

            $('#generatedLinks').dialog('open');
        },
        error: function (result) {
            alert("No Match");
        }
    });
}

$(document).ready(
    function () {
        $('#generatedLinks').dialog({
            height: 200,
            width: 400,
            modal: true,
            autoOpen: false,
            show: 'slide',
            resizable: false,
            title: 'Housing Agreement Links',
            overlay: { backgroundColor: "#000000", opacity: 0.5 }
        });
    }
);

function ResetForm() {
    $("#ddlConferences").val("-1");
    $('#ddlRoomTypeSingle').val("-1");
    $('#ddlRoomTypeDouble').val("-1");
    $('#ddlDateFrom').val("-1");
    $('#ddlDateTo').val("-1");
    $('#txtSingleDisplayName').val("");
    $('#txtDoubleDisplayName').val("");
    $('#tbAddress').val('');
    $("#tbCity").val('');
    $("#tbZip").val('');
    $('#tbPhone').val('');
    $('#tbFullName').val('');
    $('#tbEmail').val('');
    $("#tbAgencyName").val('');
    $('#ddlHotels').val("-1");
    $('#ddlHotels1').val("-1");

    $('#ddlHotels2').val("-1");
    $("#hfEditNights").val('');
    $("#ddlCountry").val("-1");
    $('#hfCompanyCode').val('');
    $('#chkCreatePropsal').prop('checked', false);
    $('#ddlRooms').val('-1');
    $('#txtSingleDisplayName').hide();
    $('#txtDoubleDisplayName').hide();
    $('#spanSingleDisplayName').hide();
    $('#spanDoubleDisplayName').hide();
    $('#spanNumberOfRooms').css('margin-top', '7px');
    $('#spanDoubleRoomType').css('margin-top', '0px');
    $('#ddlDateFrom').css('margin-top', '5px');
    $('#bookingNumber').val('');
}

//$(document).ready(function() {
//    $('#generatedLinks').dialog('open');
//});