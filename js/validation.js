function Validate(frm) {

    var len = frm.elements.length;
    var message = '';

    for (var i = 0; i < len; i++) {

        if (frm.elements[i].attributes["display"] != undefined && frm.elements[i].attributes["display"].nodeValue == "true") {

            if ((frm.elements[i].attributes["message"] != undefined) && (frm.elements[i].tagName == "INPUT")) {

                if (frm.elements[i].value == "") {

                    frm.elements[i].style.backgroundColor = '#FFF9D1';
                    message += frm.elements[i].attributes["message"].nodeValue + '\n'
                }
                else {

                    if (frm.elements[i].attributes["regularexpression"] != undefined) {

                        if (frm.elements[i].value.search(frm.elements[i].attributes["regularexpression"].nodeValue) == -1) {

                            frm.elements[i].style.backgroundColor = '#FFF9D1';
                            message += frm.elements[i].attributes["regmessage"].nodeValue + '\n';

                        }
                        else {

                            frm.elements[i].style.backgroundColor = 'white';
                        }

                    }
                    else {

                        frm.elements[i].style.backgroundColor = 'white';
                    }
                }
            }

            if ((frm.elements[i].attributes["message"] != undefined) && (frm.elements[i].tagName == "SELECT")) {

                frm.elements[i].style.backgroundColor = 'white';

                if (frm.elements[i].id == 'ctl00_ContentPlaceHolder1_ddlHotelChoice1' && frm.elements[i].options[frm.elements[i].options.selectedIndex].value == '-1') {

                    frm.elements[i].style.backgroundColor = '#FFF9D1';
                    message += frm.elements[i].attributes["message"].nodeValue + '\n';
                }

                if (frm.elements[i].id == 'ctl00_ContentPlaceHolder1_ddlHotelChoice1' && frm.elements[i].options[frm.elements[i].options.selectedIndex].text.search("Sold") > 0) {

                    frm.elements[i].style.backgroundColor = '#FFF9D1';
                    message += "*Hotel sold out! \n";
                }

                if (frm.elements[i].id != 'ctl00_ContentPlaceHolder1_ddlHotelChoice1') {

                    if (frm.elements[i].options[frm.elements[i].options.selectedIndex].value == '-1') {
                        frm.elements[i].style.backgroundColor = '#FFF9D1';
                        message += frm.elements[i].attributes["message"].nodeValue + '\n';
                    }

                    if (frm.elements[i].options[frm.elements[i].options.selectedIndex].text.search("Sold") > 0) {

                        frm.elements[i].style.backgroundColor = '#FFF9D1';
                        message += "*Hotel sold out! \n";
                    }
                }
            }
        }
    }    

    if (message.length != '') {
        //showSalesKitPage();
        alert(message);
        return false;
    }
    else {
        //showSalesKitPage();
        return true;
        
    }
}

function validateCountry() {

    if (document.getElementById('ctl00_ContentPlaceHolder1_ddlCountries').options[document.getElementById('ctl00_ContentPlaceHolder1_ddlCountries').options.selectedIndex].value == '226') {

        document.getElementById('ctl00_ContentPlaceHolder1_tbState').setAttribute("display", 'true');
    }
    else {

        document.getElementById('ctl00_ContentPlaceHolder1_tbState').removeAttribute("display", 0);
        document.getElementById('ctl00_ContentPlaceHolder1_tbState').style.backgroundColor = 'white';
    }
}

function  showSalesKitPage() {
    var x = (document.body.clientHeight/2) + 150;
    var y = (document.body.clientWidth/2) - 200;
    window.open("SalesKit.htm","mywindow","location=0,status=0,scrollbars=0, resizable=0,top="+ x +",height=50,left=" + y + ",width=400");
}